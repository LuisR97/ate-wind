﻿using UnityEngine;
using System.Collections;

//You have to use this to notify Unity that a class should show up in the inspector, if you're using C#:
[System.Serializable]

//Also notice MonoBehavior was removed from the following line:
public class TurbineClass{
	public string turbineName;
	public float windSpeed;
	public Texture2D icon;
	public int id;

	public TurbineClass(string turbineName, float windSpeed, int ID){}

	public TurbineClass(string turbineName, float windSpeed, Texture2D icon, int ID){}

}
