﻿using UnityEngine;
using System.Collections;

public class SafetyTextAdventure : MonoBehaviour {
	
	public bool tomVersion;
	public string debugMessage;

	//This is the default GUI color.
	public Color defaultGUIColor;
	
	
	// ------------------------ LEVEL FLOW CONTROL START ----------------------- //
	
	//Level flow control. (Do not alter these numbers, even if you're changing the order of scenes.)
	int mainMenu = 0;
	
	int chooseScenario = 1;
	
	int faultCode = 2;
	int scadaIndicates = 3;
	int creatingCausalMap = 16;
	int investigatingCause = 10;
	int selectCause = 4;

	int checkPLC = 17;
	int checkLog = 18;
	int checkMaintenance = 19;

	int causeFound = 11;
	int causeNotFound = 12;
	
	int incorrectPLCSettings = 6;
	int powerSupplyFailure = 7;
	
	int selectCircuitFail = 5;
	int onSiteInvestigation = 13;
	int circuitRepairSuccess = 8;
	int circuitRepairFailure = 9;
	
	int wrongCause = 14;
	int issueFixed = 15;
	
	// ------------------------ LEVEL FLOW CONTROL END ----------------------- //
	
	public int attempt;
	public int level;
	public int scenario;
	
	//Scene background.
	public Texture2D office;
	public Texture2D garage;
	public Texture2D outside;
	public Texture2D tower;
	public Texture2D nacelle;
	
	//For wiring section.
	public Texture2D opencircuit;
	public Texture2D shorttoground;
	public Texture2D shorttopower;
	public Texture2D excessiveresistance;
	
	//For showing a scene in the background.
	public Texture2D background;
	public Texture2D defaultBackground;
	public bool backgroundIsDefault;
	public bool backgroundDebug;

	//Textures.
	public Texture2D issue;
	public Texture2D turbineIcon;
	
	//For determining wind speed.
	public float mainSpeed;
	public float actualSpeed;
	public string mainSpeedText;
	public string actualSpeedText;
	public TurbineClass MainTurbine;
	public TurbineClass CompareTurbine1;
	public TurbineClass CompareTurbine2;

	void Start () {

		//In this version of the safety simulator, we're starting without a prompt.
		level = faultCode;
		scenario = Random.Range (1,8);
		timer = 0;
		displayTimer = true;

		backgroundIsDefault = true;
		defaultGUIColor = Color.white;

		//These turbines make up our "Virtual Wind Park". Main Turbine is the one with the error.
		MainTurbine = new TurbineClass("Main Turbine", mainSpeed, 0);
		CompareTurbine1 = new TurbineClass("Compare Turbine 1", actualSpeed, 1);
		CompareTurbine2 = new TurbineClass("Compare Turbine 2", actualSpeed, 2);

		Cursor.visible = true;
		Screen.lockCursor = false;
	}
	

	void Awake(){
		Application.targetFrameRate = 30;
	}
	
	//---------------------TIMER START -----------------------//
	public bool displayTimer;
	public bool complete;
	bool savedScore;
	
	int addedHours;
	int addedMinutes;
	
	int actualHours;
	int actualMinutes;
	int actualSeconds;
	
	int hours;
	int minutes;
	int seconds;
	
	public float timer;
	public string timeToBeat;
	public string workTime;
	
	void Update(){
		if(displayTimer && !complete)timer += Time.deltaTime;
		if(complete){
			if(savedScore){
				timeToBeat = workTime;
				timer = 0;
				addedHours = 0;
				addedMinutes = 0;
				savedScore = false;
				complete = false;
			}
		}

	//---------------------- TIMER END ---------------------------//


	//-------------------- PROBLEM SCENARIOS AND ASSOCIATED WIND SPEEDS END --------------------//
		
		//If the scenario is "actual high wind speed"... (High wind speed.)
		if(gameObject.GetComponent<SafetyTextAdventure>().scenario == 1){
			
			actualSpeed = 29.0f;
			mainSpeed = actualSpeed;
		}
		
		//If the scenario is a corrupt power supply, open circuit, or short to ground... (Zero wind speed.)
		if(gameObject.GetComponent<SafetyTextAdventure>().scenario == 3 || gameObject.GetComponent<SafetyTextAdventure>().scenario == 4 || gameObject.GetComponent<SafetyTextAdventure>().scenario == 6){
			
			actualSpeed = 16.2f;
			mainSpeed = 0.0f;
		}
		
		//If the scenario is a short to power... (Glitchy wind speed.)
		if(gameObject.GetComponent<SafetyTextAdventure>().scenario == 5){
			
			actualSpeed = 16.2f;

			mainSpeed = Mathf.Abs (mainSpeed-200);
			
		}
		
		//If the scenario is PLC Error... (Off by a scale factor.)
		if(gameObject.GetComponent<SafetyTextAdventure>().scenario == 2){
			
			actualSpeed = 16.2f;
			mainSpeed = 162.0f;
		}
		
		//If the scenario is excessive resistance... (Unknown at this point what the error would look like.)
		if(gameObject.GetComponent<SafetyTextAdventure>().scenario == 7){
			
			actualSpeed = 16.2f;
			mainSpeed = 0.01f;
		}
		
		actualSpeedText = actualSpeed.ToString();
		mainSpeedText = mainSpeed.ToString();
		
		//-------------------- PROBLEM SCENARIOS AND WIND SPEEDS END --------------------//


		// Exit the PC and return to the game.
		if(Input.GetKeyDown (KeyCode.Escape)){
			Cursor.visible = false;
			Screen.lockCursor = true;
			Application.LoadLevel ("Safety_Simulation_2");
		}
	}
	
	void OnGUI () {
		
		//This is the background image.
		GUILayout.BeginArea (new Rect((0 - (Screen.width/2)),(0 - (Screen.height/2)),(Screen.width * 2),(Screen.height * 2)));
		GUILayout.FlexibleSpace();
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label(background);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace ();
		GUILayout.EndArea ();
		
		//This is the background selection tool.
		if(backgroundIsDefault){
			background = defaultBackground;
		}
		if(backgroundDebug){
			GUILayout.BeginArea(new Rect(Screen.width-190,10,180,46));
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("Background");
			if(GUILayout.Button ("Default")){
				backgroundIsDefault = true;
			}
			if(GUILayout.Button ("Hide")){
				background = null;
				backgroundIsDefault = false;
				
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("1")){
				background = office;
				backgroundIsDefault = false;
			}
			if(GUILayout.Button ("2")){
				background = garage;
				backgroundIsDefault = false;
			}
			if(GUILayout.Button ("3")){
				background = outside;
				backgroundIsDefault = false;
			}
			if(GUILayout.Button ("4")){
				background = tower;
				backgroundIsDefault = false;
			}
			if(GUILayout.Button ("5")){
				background = nacelle;
				backgroundIsDefault = false;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndArea ();
		}
		
		//This is our debug message (to Tom).
		GUILayout.BeginArea (new Rect(Screen.width/2-450,Screen.height-36,900,24));
		GUILayout.FlexibleSpace();
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label(debugMessage);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.FlexibleSpace ();
		GUILayout.EndArea ();
		
		//---------------------TIMER-----------------------//
		actualHours = Mathf.FloorToInt (timer/3600F);
		actualMinutes = Mathf.FloorToInt ((timer/60F) - (actualHours * 60));
		actualSeconds = Mathf.FloorToInt (timer - (actualMinutes * 60));
		
		if(addedMinutes > 59){
			addedMinutes = 0;
			addedHours++;
		}
		
		hours = actualHours + addedHours;
		minutes = actualMinutes + addedMinutes;
		seconds = actualSeconds;
		
		workTime = string.Format ("{0:00}:{1:00}:{2:00}",hours,minutes,seconds);
		GUILayout.BeginArea (new Rect(Screen.width/2-50,10,100,24));
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace ();
		if(displayTimer){
			GUILayout.Label (workTime);
		}
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal();
		GUILayout.EndArea ();
		
		GUILayout.BeginArea (new Rect(10,10,400,24));
		GUILayout.FlexibleSpace ();
		if(attempt > 0){
			GUILayout.Label ("Time to Beat: " + timeToBeat);
		}
		GUILayout.FlexibleSpace ();
		GUILayout.EndArea ();
		
		//-------------------------------------------------//
		
		
		
		
		GUILayout.BeginArea (new Rect(Screen.width/2-450,Screen.height/2-450,900,900));
		GUILayout.FlexibleSpace ();
		
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		
		
		if(level == mainMenu){
			//Opportunity to pick a scenario.
			defaultBackground = null;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("Welcome! This is a text-driven walkthrough of a turbine repair simulation. Read along as if you are the technician.");
			if(tomVersion){
				debugMessage = "Hi, Tom! I will be asking for your guidance with questions marked 'TOM' at the bottom.";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Random Scenario", GUILayout.Height (40),GUILayout.Width (200))){
				level = faultCode;
				scenario = Random.Range (1,8);
				timer = 0;
				displayTimer = true;
			}
			if(GUILayout.Button ("Specify Scenario", GUILayout.Height (40),GUILayout.Width (200))){
				level = chooseScenario;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				if(GUILayout.Button ("I'm not Tom!", GUILayout.Height (40),GUILayout.Width (200))){
					tomVersion = !tomVersion;
					backgroundDebug = false;
					debugMessage = null;
				}
			}
			else if(!tomVersion){
				if(GUILayout.Button ("I'm Tom!", GUILayout.Height (40),GUILayout.Width (200))){
					tomVersion = !tomVersion;
				}
			}
			
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		
		
		if(level == chooseScenario){
			//Specify scenario.
			defaultBackground = null;
			if(tomVersion){
				backgroundDebug = true;
			}
			
			GUILayout.BeginVertical();
			GUILayout.FlexibleSpace ();
			
			if(tomVersion){
				debugMessage = "The tools on the upper-right alter the background image. These may come in handy when critiquing the simulation.";
			}
			
			
			if(GUILayout.Button ("Actual High Wind Speed", GUILayout.Height (40),GUILayout.Width (200))){
				scenario = 1;
				level = faultCode;
				timer = 0;
				displayTimer = true;
			}
			if(GUILayout.Button ("PLC/SCADA Error", GUILayout.Height (40),GUILayout.Width (200))){
				scenario = 2;
				level = faultCode;
				timer = 0;
				displayTimer = true;
			}
			if(GUILayout.Button ("Power Supply Corrupt", GUILayout.Height (40),GUILayout.Width (200))){
				scenario = 3;
				level = faultCode;
				timer = 0;
				displayTimer = true;
			}
			if(GUILayout.Button ("Open Circuit", GUILayout.Height (40),GUILayout.Width (200))){
				scenario = 4;
				level = faultCode;
				timer = 0;
				displayTimer = true;
			}
			if(GUILayout.Button ("Short to Power", GUILayout.Height (40),GUILayout.Width (200))){
				scenario = 5;
				level = faultCode;
				timer = 0;
				displayTimer = true;
			}
			if(GUILayout.Button ("Short to Ground", GUILayout.Height (40),GUILayout.Width (200))){
				scenario = 6;
				level = faultCode;
				timer = 0;
				displayTimer = true;
			}
			if(GUILayout.Button ("Excessive Resistance", GUILayout.Height (40),GUILayout.Width (200))){
				scenario = 7;
				level = faultCode;
				timer = 0;
				displayTimer = true;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		
		
		if(level == faultCode){
			//Start scenario.
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("Working from your PC, you notice the following fault code arise:");
			if(scenario == 1 || scenario == 2 || scenario == 5){
				GUILayout.Label ("The turbine has shut down due to high wind speed.");
			}
			if(scenario == 3 || scenario == 4 || scenario == 6 || scenario == 7){
				GUILayout.Label ("There is no wind reading at the turbine.");
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Check SCADA & Weather Information for Your Wind Park", GUILayout.Height (40),GUILayout.Width (400))){
				level = scadaIndicates;
				addedMinutes = addedMinutes + 20;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				debugMessage = "TOM: Should we list other options to check, in addition to SCADA and Weather Information?";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		
		
		if(level == scadaIndicates){
			//Checking SCADA.
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("Actual wind speed is: ");
			GUILayout.Label (actualSpeedText);
			GUILayout.Label ("m/s.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();

			//Turbine 1:
			GUILayout.BeginVertical ();
			if(mainSpeed == actualSpeed && mainSpeed > 0 && mainSpeed < 25){
				GUI.color = Color.green;
			}
			else if(mainSpeed == 0 || mainSpeed != actualSpeed || mainSpeed > 25){
				GUI.color = Color.red;
			}
			GUILayout.Box(turbineIcon);
			GUI.color = defaultGUIColor;
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace ();
			GUILayout.Label (mainSpeedText);
			GUILayout.Label ("m/s.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.EndVertical ();

			//Turbine 2:
			GUILayout.BeginVertical ();
			if(actualSpeed > 0 && actualSpeed < 25){
				GUI.color = Color.green;
			}
			else GUI.color = Color.red;
			GUILayout.Box(turbineIcon);
			GUI.color = defaultGUIColor;
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace ();
			GUILayout.Label (actualSpeedText);
			GUILayout.Label ("m/s.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.EndVertical ();

			//Turbine 3:
			GUILayout.BeginVertical ();
			if(actualSpeed > 0 && actualSpeed < 25){
				GUI.color = Color.green;
			}
			else GUI.color = Color.red;
			GUILayout.Box(turbineIcon);
			GUI.color = defaultGUIColor;
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace ();
			GUILayout.Label (actualSpeedText);
			GUILayout.Label ("m/s.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.EndVertical ();

			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(scenario == 1){
				if(GUILayout.Button ("Wait for conditions to return to normal and restart turbine.", GUILayout.Height (40),GUILayout.Width (350))){
					level = issueFixed;
					addedMinutes = addedMinutes + 30;
					complete = true;
				}
			}
			if(scenario > 1){
				if(GUILayout.Button ("Investigate Further", GUILayout.Height (40),GUILayout.Width (200))){
					level = investigatingCause;
				}
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace ();
			if(scenario != 1){
				GUILayout.Label ("You may want to take a note of this, and any other information you uncover.");
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				debugMessage = "TOM: How does SCADA indicate to the technician if there is high wind speed? Numerically? Graphically?";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		
		
		if(level == selectCause){
			//After creating a causal map.
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("After conducting an investigation, what do you believe is the cause?");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Circuitry Problem", GUILayout.Height (40),GUILayout.Width (200))){
				if(scenario > 3) {
					level = onSiteInvestigation;
					addedHours = addedHours + 1;
				}
				else {
					level = wrongCause;
					addedHours = addedHours + 6;
				}
			}
			if(GUILayout.Button ("Incorrect PLC Settings", GUILayout.Height (40),GUILayout.Width (200))){
				if(scenario == 2) {
					level = incorrectPLCSettings;
					addedMinutes = addedMinutes + 20;
				}
				else {
					level = wrongCause;
					addedMinutes = addedMinutes + 40;
				}
				
			}
			if(GUILayout.Button ("Power Supply Corrupt", GUILayout.Height (40),GUILayout.Width (200))){
				if(scenario == 3) {
					level = powerSupplyFailure;
					addedHours = addedHours + 1;
				}
				else {
					level = wrongCause;
					addedHours = addedHours + 6;
				}
			}
			if(tomVersion){
				debugMessage = "";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("Checking the circuitry or power supply will require a trip to the site.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
			
		}
		
		
		if(level == selectCircuitFail){
			//Checking circuitry on site for a problem.
			defaultBackground = nacelle;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(scenario == 4){
				issue = opencircuit;
			}
			if(scenario == 5){
				issue = shorttopower;
			}
			if(scenario == 6){
				issue = shorttoground;
			}
			if(scenario == 7){
				issue = excessiveresistance;
			}
			GUILayout.Label (issue);
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("The wiring doesn't look right. What is the problem?");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Open Circuit", GUILayout.Height (40),GUILayout.Width (200))){
				if(scenario == 4){
					level = circuitRepairSuccess;
				}
				else level = circuitRepairFailure;
			}
			if(GUILayout.Button ("Short to Power", GUILayout.Height (40),GUILayout.Width (200))){
				if(scenario == 5){
					level = circuitRepairSuccess;
				}
				else level = circuitRepairFailure;
			}
			if(GUILayout.Button ("Short to Ground", GUILayout.Height (40),GUILayout.Width (200))){
				if(scenario == 6){
					level = circuitRepairSuccess;
				}
				else level = circuitRepairFailure;
			}
			if(GUILayout.Button ("Excessive Resistance", GUILayout.Height (40),GUILayout.Width (200))){
				if(scenario == 7){
					level = circuitRepairSuccess;
				}
				else level = circuitRepairFailure;
			}
			if(tomVersion){
				debugMessage = "";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		
		
		if(level == incorrectPLCSettings){
			//PLC/SCADA Error
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("You find that the PLC settings were set incorrectly inside SCADA.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Correct the Settings", GUILayout.Height (40),GUILayout.Width (200))){
				level = issueFixed;
				addedMinutes = addedMinutes + 2;
				complete = true;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				debugMessage = "TOM: How would SCADA indicate that the PLC settings are incorrect? A diagnostic tool?";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		
		
		if(level == powerSupplyFailure){
			//Power Supply Corrupt
			defaultBackground = outside;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("You arrive on site and notice that the power supply is corrupted.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Replace the Power Supply", GUILayout.Height (40),GUILayout.Width (200))){
				level = issueFixed;
				addedMinutes = addedMinutes + 10;
				addedHours = addedHours + 1;
				complete = true;
			}
			if(tomVersion){
				debugMessage = "";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		if(level == circuitRepairSuccess){
			//Discovered Circuit Problem.
			defaultBackground = nacelle;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("You have correctly identified the circuitry problem.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Repair Circuitry", GUILayout.Height (40),GUILayout.Width (200))){
				level = issueFixed;
				addedMinutes = addedMinutes + 40;
				complete = true;
			}
			if(tomVersion){
				debugMessage = "";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		if(level == circuitRepairFailure){
			//Incorrect Circuit Diagnosis.
			defaultBackground = nacelle;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("Sorry, you have not correctly identified the problem.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Check Image Again", GUILayout.Height (40),GUILayout.Width (200))){
				level = selectCircuitFail;
				addedMinutes = addedMinutes + 40;
			}
			if(tomVersion){
				debugMessage = "";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		if(level == investigatingCause){
			//Additional causal map scene.
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("What would you like to investigate? What do you think the cause is based on the information you have?");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("PLC Diagnostics", GUILayout.Height (40),GUILayout.Width (200))){
				level = checkPLC;
				addedMinutes = addedMinutes + 20;
			}
			if(GUILayout.Button ("Turbine Log", GUILayout.Height (40),GUILayout.Width (200))){
				level = checkLog;
				addedMinutes = addedMinutes + 20;
			}
			if(GUILayout.Button ("Maintenance History", GUILayout.Height (40),GUILayout.Width (200))){
				level = checkMaintenance;
				addedMinutes = addedMinutes + 20;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("I've checked enough. I have enough data to create a causal map.", GUILayout.Height (40),GUILayout.Width (400))){
				level = creatingCausalMap;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				debugMessage = "TOM: Aside from these three options, what other things could the technician investigate?";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		
		
		if(level == checkPLC){
			//Investigating PLC Settings.
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if(scenario > 1){
				GUILayout.Label ("You notice ");
				if(scenario == 2){
					GUILayout.Label ("that the PLC data is incorrect.");
				}
				if(scenario == 3 || scenario == 4 || scenario == 5 || scenario == 6 || scenario == 7){
					GUILayout.Label ("that everything seems fine with the PLC settings.");
				}
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Done Checking", GUILayout.Height (40),GUILayout.Width (200))){
				level = investigatingCause;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				debugMessage = "TOM: What would this particular evidence look like, if this was the issue?";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		if(level == checkLog){
			//Investigating the turbine log.
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if(scenario > 1){
				GUILayout.Label ("The turbine log ");
				if(scenario == 2){
					GUILayout.Label ("shows this same wind speed consistently since the last manual shutdown.");
				}
				if(scenario == 3 || scenario == 4 || scenario == 6){
					GUILayout.Label ("shows that the wind speed dropped to 0 at some point recently.");
				}
				if(scenario == 5){
					GUILayout.Label ("is erratic, showing high and low values since some point recently.");
				}
				if(scenario == 7){
					GUILayout.Label ("shows a decrease in wind speed recently.");
				}
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Done Checking", GUILayout.Height (40),GUILayout.Width (200))){
				level = investigatingCause;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				debugMessage = "TOM: What would this particular evidence look like, if this was the issue?";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		if(level == checkMaintenance){
			//Investigating maintenance history.
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if(scenario > 1){
				GUILayout.Label ("The maintenance history shows ");
				if(scenario == 2){
					GUILayout.Label ("a recent anemometer change.");
				}
				if(scenario == 3 || scenario == 7){
					GUILayout.Label ("no activity for a long time.");
				}
				if(scenario == 4 || scenario == 5 || scenario == 6){
					GUILayout.Label ("a technician recently conducted work involving wiring.");
				}
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Done Checking", GUILayout.Height (40),GUILayout.Width (200))){
				level = investigatingCause;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				debugMessage = "TOM: What would this particular evidence look like, if this was the issue?";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}

		/*if(level == causeFound){
			//Finding the cause of an error.
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if(scenario > 1){
				GUILayout.Label ("You noticed evidence of ");
				if(scenario == 2){
					GUILayout.Label ("a PLC error.");
				}
				if(scenario == 3){
					GUILayout.Label ("a power supply failure.");
				}
				if(scenario > 3){
					GUILayout.Label ("a circuitry problem.");
				}
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Done Checking", GUILayout.Height (40),GUILayout.Width (200))){
				level = investigatingCause;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				debugMessage = "TOM: What would this particular evidence look like, if this was the issue?";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}*/
		
		
		/*if(level == causeNotFound){
			//Investigating and not finding anything.
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label ("Nothing looks unusual.");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Done Checking", GUILayout.Height (40),GUILayout.Width (200))){
				level = investigatingCause;
			}
			if(tomVersion){
				debugMessage = "";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}*/
		
		
		if(level == onSiteInvestigation){
			//Arrive on site. What to investigate?
			defaultBackground = tower;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("There is a circuit failure. Try to find the cause.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Check Wiring", GUILayout.Height (40),GUILayout.Width (200))){
				level = selectCircuitFail;
				addedMinutes = addedMinutes + Random.Range (15,46);
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				debugMessage = "TOM: What are other things we could be checking/investigating after arriving on-site?";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		
		
		if(level == wrongCause){
			//You selected the wrong plan.
			defaultBackground = outside;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("You have searched for a long time and can't find any problems.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Return to Causal Map", GUILayout.Height (40),GUILayout.Width (200))){
				level = selectCause;
			}
			if(tomVersion){
				debugMessage = "";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		if(level == issueFixed){
			//You won!
			defaultBackground = null;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("Congratulations! You have solved the issue. The turbine has been restarted and is functioning!");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Restart Simulation", GUILayout.Height (40),GUILayout.Width (200))){
				level = mainMenu;
				scenario = 0;
				attempt++;
				savedScore = true;
				displayTimer = false;
			}
			if(GUILayout.Button ("Quit", GUILayout.Height (40),GUILayout.Width (200))){
				Application.Quit ();
			}
			if(tomVersion){
				debugMessage = "TOM: If you are using a web browser to play this, 'Quit' will not close anything.";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
			
		}
		
		if(level == creatingCausalMap){
			//Pause to create causal map.
			defaultBackground = office;
			
			//Heading.
			GUILayout.BeginVertical ();
			GUILayout.FlexibleSpace ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("Please take this time to create a causal map. When you are finished, have your causal map ready and click 'Causal Map Finished'.");
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			
			//Button Options.
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(GUILayout.Button ("Causal Map Finished", GUILayout.Height (40),GUILayout.Width (200))){
				level = selectCause;
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			if(tomVersion){
				debugMessage = "";
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.EndVertical ();
		}
		
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		
		GUILayout.FlexibleSpace ();
		GUILayout.EndArea ();
	}
	
}
