﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class ProjectChecklist : MonoBehaviour {

	/*
	//For demo.
	public int readInstructions;
	public int readLoadOut;
	public bool shutDown;
	public bool shutDownLog;
	*/

	public List<ItemClass> playerInventoryTransition = new List<ItemClass>();
	public GameObject ThisGUIManager;

	void Start () {
		ThisGUIManager = GameObject.FindGameObjectWithTag ("GUIManager");
	}

	void Update () {

		if(ThisGUIManager == null){
			ThisGUIManager = GameObject.FindGameObjectWithTag ("GUIManager");
		}

		//if(SceneManager.isLoadingLevel){
		if(SceneManager.GetActiveScene().isLoaded)
		{

			playerInventoryTransition = ThisGUIManager.GetComponent<Inventory>().playerInventory;
			Debug.Log (playerInventoryTransition.Count);

		}
	}

	void Awake () {
		DontDestroyOnLoad(gameObject);
		if(FindObjectsOfType (GetType ()).Length > 1){
			Destroy (gameObject);
		}
	}

	void OnLevelWasLoaded () {
		ThisGUIManager = GameObject.FindGameObjectWithTag ("GUIManager");
	}

	void OnGUI () {

		/*
		//For demo.
		
		//Demo Office button.
		if(ThisGUIManager.GetComponent<GUIManager>().showMenu && readInstructions == 0){
			if(GUI.Button (new Rect(Screen.width/2 - 250, Screen.height/2-45, 500, 30), "Your scenario deems it necessary to grab a screwdriver.")){
				readInstructions++;
			}
		}
		else if(ThisGUIManager.GetComponent<GUIManager>().showMenu && readInstructions == 1){
			if(GUI.Button (new Rect(Screen.width/2 - 250, Screen.height/2-45, 500, 30), "Click tools to add them to your inventory.")){
				readInstructions++;
			}
		}
		else if(ThisGUIManager.GetComponent<GUIManager>().showMenu && readInstructions == 2){
			if(GUI.Button (new Rect(Screen.width/2 - 250, Screen.height/2-45, 500, 30), "Select tools in your inventory to use or drop them.")){
				readInstructions++;
			}
		}
		else if(ThisGUIManager.GetComponent<GUIManager>().showMenu && readInstructions == 3){
			if(GUI.Button (new Rect(Screen.width/2 - 250, Screen.height/2-45, 500, 30), "You can press 'e' to open and close your inventory.")){
				readInstructions++;
			}
		}

		//Demo Nacelle button.
		if(Application.loadedLevelName == "Nacelle" && ThisGUIManager.GetComponent<GUIManager>().showMenu && readLoadOut ==  0){
			if(GUI.Button (new Rect(Screen.width/2 - 250, Screen.height/2-45, 500, 30), "Great, you have the screwdriver! Time to get to work.")){
				readLoadOut++;
			}
		}
		else if(Application.loadedLevelName == "Nacelle" && ThisGUIManager.GetComponent<GUIManager>().showMenu && readLoadOut ==  1){
			if(GUI.Button (new Rect(Screen.width/2 - 250, Screen.height/2-45, 500, 30), "Select tools in your inventory to use or drop them.")){
				readLoadOut++;
			}
		}
		else if(Application.loadedLevelName == "Nacelle" && ThisGUIManager.GetComponent<GUIManager>().showMenu && readLoadOut ==  2){
			if(GUI.Button (new Rect(Screen.width/2 - 250, Screen.height/2-45, 500, 30), "You can press 'e' to open and close your inventory.")){
				readLoadOut++;
			}
		}

		//Demo shutdown button.
		if(Application.loadedLevelName == "Nacelle" && ThisGUIManager.GetComponent<GUIManager>().showMenu && shutDown && !shutDownLog){
			if(GUI.Button (new Rect(Screen.width/2 - 250, Screen.height/2-45, 500, 30), "Congratulations, you shut off the machine safely!")){
				shutDownLog = true;
				Application.LoadLevel ("Office");
			}
		}
		*/

	}
}
