﻿using UnityEngine;
using System.Collections;

public class LoadingGear : MonoBehaviour {


	//Only show gear when toolActive is on. (See GUIManager.cs).
	public bool gearActive = false;
	public GameObject GUIManager;

	void Start () {
		gameObject.SetActive (gearActive);
	}

	void Update () {
		gearActive = GUIManager.GetComponent<GUIManager>().toolActive;
		gameObject.SetActive (gearActive);
	}

}