﻿using UnityEngine;
using System.Collections;

public class TimerLadder : MonoBehaviour
{

	public float timer;
	public static bool timeStarted = false;
	public GUIStyle timerStyle;

	public float popup_show_time = 5.0f;
	private float popup_time = 0;
	private float popup_value;


	// Use this for initialization
	void Start()
	{
		timeStarted = true;
		timer = 0;
	}

	// Update is called once per frame
	void Update()
	{
		if (timeStarted)
		{
			timer += Time.deltaTime;
		}
	}

	void OnGUI()
	{
		GUI.skin.label.alignment = TextAnchor.MiddleCenter;
		float hours = Mathf.Floor(timer / 3600);
		float minutes = (Mathf.Floor(timer / 60)) % 60;
		float seconds = Mathf.FloorToInt(timer % 60);
		string hou;
		string min;
		string sec;

		min = string.Format("{0,2:N0} min ", minutes);
		sec = string.Format("{0,2:N0} sec ", seconds);

		if (hours < 1)
			hou = "";
		else
			hou = string.Format("{0,2:N0} hr ", hours);

		GUI.Label(new Rect(Screen.width / 2 - 80, 0, 160, 18), "Job Time");
		GUI.Label(new Rect(Screen.width / 2 - 80, 0, 170, 40), hou + min + sec, timerStyle);

		if (popup_time > 0)
		{
			GUI.skin.label.alignment = TextAnchor.UpperLeft;
			GUI.skin.label.richText = true;
			GUI.Label(new Rect(Screen.width / 2 + 30, 0, 80, 30),
					  "<color=#ffffffff> + " + string.Format("{0:N0}", popup_value / 60.0f) + " Min.</color>");

			popup_time -= Time.deltaTime;
			if (popup_time < 0)
			{
				popup_time = 0;
			}
		}

	}

	public void AddElapsed(float e_time_sec)
	{
		if (popup_time > 0)
			return;

		popup_value = e_time_sec;
		popup_time = popup_show_time;
		timer += popup_value;
	}
}
