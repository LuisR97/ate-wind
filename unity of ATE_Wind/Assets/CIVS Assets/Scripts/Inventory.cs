﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using QuestionItem;

public class Inventory : MonoBehaviour {

	//This variable stores which GUI element is being hovered.
	public string hover;
	public bool hoverB;
	public Texture icon;
	public GameObject toolPreview;
	public string activeDescription;

	//Define inventory menu appearance.
	public GUIStyle InventoryStyle;

	public List<ItemClass> playerInventory = new List<ItemClass>();
	Vector2 scrollView = new Vector2();

	//This is the active item slot.
	public ItemClass equippedItem = new ItemClass();
	public bool equipped = false;
	private bool qm_show_last = false;

	//Checking for a project checklist.
	public GameObject FoundChecklist;

	void Start () {
		if(GameObject.FindGameObjectWithTag("Checklist") != null){
			FoundChecklist = GameObject.FindGameObjectWithTag("Checklist");
			playerInventory = FoundChecklist.GetComponent<ProjectChecklist>().playerInventoryTransition;
		}
	}

	void Update () {


		//This determines whether an item is currently equipped.
		if(equippedItem.itemType == ItemClass.ItemKind.ToolItem){
			equipped = true;
		}
		else{
			equipped = false;
		}


		//This shows the highlighted object in the inventory preview.
		if(gameObject.GetComponent<GUIManager>().showMenu){
			if(hover != ""){
				hoverB=true;
				icon = gameObject.GetComponent<AddItem>().loot[int.Parse(hover)].icon;
				//toolPreview.GetComponent<MeshRenderer>().sharedMaterial = gameObject.GetComponent<AddItem>().loot[int.Parse (hover)].model.GetComponent<MeshRenderer>().sharedMaterial;
				Debug.Log("get dis");
			}
			else if(hover == ""){
				hoverB=false;
				icon = null;
				toolPreview.GetComponent<MeshFilter>().sharedMesh = null;
				toolPreview.GetComponent<MeshRenderer>().sharedMaterial = null;
			}
		}
		else if(!gameObject.GetComponent<GUIManager>().showMenu){
			toolPreview.GetComponent<MeshFilter>().sharedMesh = null;
			toolPreview.GetComponent<MeshRenderer>().sharedMaterial = null;
		}

		//This allows the item's description to be shown only when the item is highlighted.
		if(hover != ""){
			activeDescription = gameObject.GetComponent<AddItem>().loot[int.Parse (hover)].description;
		}
		else activeDescription = null;

	}

	void OnGUI () {

		if(gameObject.GetComponent<GUIManager>().showMenu && playerInventory.Count > 0){
			GUILayout.BeginArea (new Rect(3 *(Screen.width/4), Screen.height - 410, (Screen.width/4) - 10,200));
			GUILayout.FlexibleSpace ();
			if(activeDescription!=null){
				GUILayout.Box(activeDescription,InventoryStyle);
			}
			GUILayout.EndArea ();
			GUILayout.BeginArea(new Rect(3 * (Screen.width/4), Screen.height - 210, Screen.width/4,200));
			//The following line shows how to create a scroll bar.
			GUILayout.Label(" Inventory (' I ') ",InventoryStyle);

			scrollView = GUILayout.BeginScrollView(
				scrollView, 
				GUILayout.Width ((Screen.width/4) - 10),
				GUILayout.Height(160));

			if (GUILayout.Button("Work Order",InventoryStyle)) {  
				//TaskManager.qm.show_gui();
				gameObject.GetComponent<GUIManager>().showMenu = false;
				gameObject.GetComponent<GUIManager>().showWorkOrder = true;
			}   
			for(int x = 0; x < playerInventory.Count; x++){
				if(GUILayout.Button ( new GUIContent(
						playerInventory[x].itemName, 
				        playerInventory[x].id.ToString()), 
						InventoryStyle) 
				   && playerInventory[x].itemType == ItemClass.ItemKind.ToolItem && !equipped){

					equippedItem = playerInventory[x];
					playerInventory.RemoveAt (x);
					//Do not continue to render if the item has been removed.
					return;
				}
			}
			//Don't forget to include this if you're using a scroll bar.
			GUILayout.EndScrollView ();
			GUILayout.EndArea();
		}
		else if(gameObject.GetComponent<GUIManager>().showMenu){
			GUILayout.BeginArea(new Rect(3 * (Screen.width/4), Screen.height - 210, (Screen.width/4) - 10,200));
			GUILayout.Label(" Inventory 'I' ",InventoryStyle);
			if (GUILayout.Button("Work Order",InventoryStyle, GUILayout.Width ((Screen.width/4) - 10))) {  
				//TaskManager.qm.show_gui(); 
				gameObject.GetComponent<GUIManager>().showMenu = false;
				gameObject.GetComponent<GUIManager>().showWorkOrder = true;
			}   
			GUILayout.Box ("You are not carrying any tools.", InventoryStyle, GUILayout.Width ((Screen.width/4) - 10));
			
			GUILayout.EndArea();
		}
		else if(!gameObject.GetComponent<GUIManager>().showMenu){
			GUI.Label(
				new Rect(3 *(Screen.width/4), Screen.height - 30, (Screen.width/4) - 10, 28),
				" Inventory 'I' ",
				InventoryStyle);
			GUI.Label(
				new Rect(3 *(Screen.width/4), Screen.height - 60, (Screen.width/4) - 10, 28),
				" Work Order 'O' ",
				InventoryStyle);
		}

		if(equipped){
			//Here we will display an active item slot.


			if(GUI.Button(new Rect(10, Screen.height - 158, 128, 128), equippedItem.icon, InventoryStyle) && equipped){
				playerInventory.Add (equippedItem);
				equippedItem = new ItemClass();
			}
			GUI.Button (new Rect(10, Screen.height - 30, 128, 20), equippedItem.itemName, InventoryStyle);
		}
		
		if(hoverB){
			GUI.Button (new Rect(Screen.width/2-100, Screen.height/2-75, 150, 150), icon,InventoryStyle);
		}

		hover = GUI.tooltip;
		//Debug.Log (hover);

		if (gameObject.GetComponent<GUIManager>().showWorkOrder && 
		    gameObject.GetComponent<GUIManager>().showWorkOrder != qm_show_last)
			TaskManager.qm.show_gui(); 

		bool test = TaskManager.qm.questions_gui();

		if (qm_show_last != test) {
			gameObject.GetComponent<GUIManager>().showWorkOrder = test;
		}

		qm_show_last = test;
	}

}