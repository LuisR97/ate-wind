﻿using UnityEngine;
using System.Collections;

public class PlayerStatusTrack : MonoBehaviour {
	
	//int determine how many time the tool is used
	public int sliderUsed;
	public int ladderClimbed;
	
	public int radioUsed; //could be multi times?
	public int safetyMeeting;	//could be multi times?
	public bool schematicOn;
	public bool turbineSafeState; //if turn to safe mode
	public bool endTask; //if player press end task button yet
	
	
	//teleport time useage 
	//in Minute!
	//make change in Unity Inspector Panel (Not here);
	public float truckTransTime = 90;
	public float radioCallTime = 20;
	public float safetyMeetingTime = 30;
	public float turbineSafeStateTime = 25;
	public float ladder_lvl1_Time = 50;
	public float ladder_lvl2_Time = 40;
	public float ladder_lvl3_Time = 35;
	
	//get player object and info
	public GameObject player;
	
	// Use this for initialization
	void Start () {
		sliderUsed = 0;
		ladderClimbed = 0;
		radioUsed = 0;
		safetyMeeting = 0;
		schematicOn = false;
		turbineSafeState = false;
		endTask = false;
		
		player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
		//player position
		//Debug.Log(player.transform.position);
	}

	void OnGUI()
	{
		if (player != null)
		GUI.Label(new Rect(5,5,100,50),player.transform.position.ToString ());
	}
}
