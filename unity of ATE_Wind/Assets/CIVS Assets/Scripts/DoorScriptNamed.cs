using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DoorScriptNamed : MonoBehaviour {
	
	public string myName;

	// Use this for initialization
	void Start () {
		
		myName = gameObject.name;
	
	}
	
	void OnTriggerEnter (Collider col){
		
		if(col.gameObject.tag == "Player"){
			if(myName == "ExitDoor"){
				SceneManager.LoadScene("Nacelle");
			}
			if(myName == "OfficeDoor"){
				SceneManager.LoadScene("Office");
			}
			if(myName == "GarageDoor"){
				SceneManager.LoadScene("Garage");
			}
		}
	}
}
