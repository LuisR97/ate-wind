﻿using UnityEngine;
using UnityEngine.SceneManagement;


 //Note: to change the height of the player, just change the y position of the camera

public class Ladder_2 : MonoBehaviour
{

    public bool topB;
    public bool midB;
    public bool botB;
    public bool jumpUp;
    public bool jumpDown;

    public bool climbB;
    public bool downB;
    public bool timeB;
    public bool atTop;
    public bool doorB;
    public bool onGround = true;
    public bool endB;
    public bool inNacelle;
    public bool atTopNacelle;

    public GameObject ladder1_side;
    public GameObject ladder2_side;
    public GameObject ladder3_side;
    public GameObject slider;
    /*testes*/
    public GameObject CarabinerRightItem;
    public GameObject rope;
    public LineRenderer ropeLineRender;
    public GameObject ropePlayerRight;
    public GameObject ropePlayerLeft;
    GameObject entrance;
    public LadderEntrance ladderEntrance;
    public PickEquipments equipments;

    public float lvl1_time;
    public float lvl2_time;
    public float lvl3_time;
    float jumpTime;

    float yieldTime;
    public int top = 0;
    public Vector3 atTopPos;
    public Quaternion atTopRot;
    float speed;
    public bool show;

    bool begin = true; //for harness do not delete

    void Start()
    {
        top = 0;
        slider = gameObject.GetComponentInChildren<ItemScript>().slider;
        CarabinerRightItem = gameObject.GetComponentInChildren<ItemScript>().CarabinerRightItem;
        speed = gameObject.GetComponent<FirstPersonController>().movementSpeed;

        lvl1_time = gameObject.GetComponentInChildren<PlayerStatusTrack>().ladder_lvl1_Time;
        lvl2_time = gameObject.GetComponentInChildren<PlayerStatusTrack>().ladder_lvl2_Time;
        lvl3_time = gameObject.GetComponentInChildren<PlayerStatusTrack>().ladder_lvl3_Time;
    }

    void Update()
    {
        ladder1_side.SetActive(gameObject.GetComponentInChildren<ItemScript>().connectB);
        ladder2_side.SetActive(gameObject.GetComponentInChildren<ItemScript>().connectB);
        ladder3_side.SetActive(gameObject.GetComponentInChildren<ItemScript>().connectB);

        if (top == 1)
        {
            jumpTime = lvl1_time;
        }

        if (top == 2)
        {
            jumpTime = lvl2_time;
        }

        if (top == 3)
        {
            jumpTime = lvl3_time;
        }

        if (downB)
        {
            if (Input.GetKey(KeyCode.B))
            {
                gameObject.transform.position -= new Vector3(0, 0.016f, 0);
            }

            if (jumpDown)
            {
                Fade();
                climbB = false;
                downB = false;
            }

            if (doorB)
            {
                downB = false;
            }
        }

        if (climbB)
        {
            if (Input.GetKey(KeyCode.C))
            {
                gameObject.transform.position += new Vector3(0, 0.016f, 0);
            }

            if (jumpUp)
            {
                Fade();
                climbB = false;
                downB = false;
            }

            if (doorB)
            {
                climbB = false;
            }
        }
        //FPSInputController
        if (onGround)
        {
            gameObject.GetComponent<FirstPersonController>().verticalVelocity = -1;  
        }
        else
        {
            gameObject.GetComponent<FirstPersonController>().verticalVelocity = 0;
        }

        slider.transform.position = new Vector3(slider.transform.position.x, gameObject.transform.position.y + 1f, slider.transform.position.z);
        CarabinerRightItem.transform.position = new Vector3(CarabinerRightItem.transform.position.x, gameObject.transform.position.y + 1f, CarabinerRightItem.transform.position.z);
        rope.transform.position = new Vector3(461.15f, gameObject.transform.position.y + 0.75f, rope.transform.position.z);
        //rope.transform.position = new Vector3(461.15f, gameObject.transform.position.y + 0.75f, 3230.5f);

       
        if(equipments.isHarnessInInventory && begin)
        {
            ropePlayerRight.SetActive(true);
            ropePlayerLeft.SetActive(true);
            begin = false;
        }

        if(ladderEntrance.show_FallDown_FromNacelle)
        {
            gameObject.transform.position -= new Vector3(0, 0.5f, 0);
        }


        if (timeB)
        {
            gameObject.GetComponentInChildren<TimerLadder>().AddElapsed(jumpTime * 60);
            timeB = false;
        }

        if(ladderEntrance.TopNacelle)
        {
            climbB = false;
            downB =  false;
        }
        
    }//end Update


    void OnTriggerEnter(Collider ladder)
    {

        if (ladder.name == "entrance_floor_collider")
        {
            onGround = false;
        }

        if (ladder.name == "Door_Collider")
        {
            doorB = true;
        }

        if (ladder.name == "ladder1_bottom_On")
        {
            climbB = true;
            top = 1;
        }


        if (ladder.name == "Ladder1_bottom_stop")
        {
            downB = false;
        }

        if (ladder.name == "ladder1_top_On")      //Down top=1
        {
            downB = true;
            top = 1;
        }

        if (ladder.name == "ladder2_bottom_On")
        {
            climbB = true;
            top = 2;
            slider.transform.rotation = GameObject.Find("Ladder2_Bottom").transform.rotation;
            slider.transform.position = new Vector3(461.635f, 624f, 3230.75f);
            //rope.transform.rotation = GameObject.Find("Ladder2_Bottom").transform.rotation;
            rope.transform.eulerAngles = new Vector3(187.961f, -270f, -90f);
            rope.transform.position = new Vector3(461.252f, 623.907f, 3230.672f);
        }

        if (ladder.name == "ladder2_top_On")      //Down top=2
        {
            downB = true;
            top = 2;
        }

        if (ladder.name == "ladder3_bottom_On")
        {
            climbB = true;
            top = 3;
            slider.transform.position = new Vector3(461.635f, 651f, 3230.767f);
            slider.transform.rotation = GameObject.Find("Ladder3_Bottom").transform.rotation;
            rope.transform.position = new Vector3(461.273f, 650.919f, 3230.686f);
            rope.transform.eulerAngles = new Vector3(187.961f, -270f, -90f);
        }

        if (ladder.name == "JumpUp")
        {
            jumpUp = true;
        }

        if (ladder.name == "JumpDown")
        {
            jumpDown = true;
        }

        if (ladder.name == "ladder3_top_Stop" && !inNacelle)
        {
            JumpUp();
            climbB = false;
        }
    }

    void OnTriggerExit(Collider ladder)
    {
        if (ladder.name == "Ladder1_bottom_stop")
        {
            downB = true;
        }
        if (ladder.name == "Ladder1_top_Stop")
        {
            climbB = true;
        }

        if (ladder.name == "Ladder2_Top_Stop")
        {
            climbB = true;
        }

        if (ladder.name == "Ladder3_Bottom_Stop")
        {
            downB = true;
        }
        if (ladder.name == "ladder3_top_Stop" && !inNacelle)
        {
            downB = true; 
            climbB = true;
            //atTop = false;
            inNacelle = false;
            top = 3;
            slider.transform.position = new Vector3(461.633f, 670.868f, 3230.775f);
            slider.transform.eulerAngles = new Vector3(-90f, 90f, -90f);
            rope.transform.position = new Vector3(461.45f, 670.3f, 3230.64f);
            rope.transform.eulerAngles = new Vector3(30f, 90f, 0f);
        }

        if (ladder.name == "entrance_floor_collider" && !inNacelle)
        {
            onGround = true;
        }
        if (ladder.name == "Door_Collider")
        {
            doorB = false;
        }
    }

    void Fade()
    {
        if (jumpUp)
        {
            Invoke("JumpUp", 1);
        }

        if (jumpDown)
        {
            Invoke("JumpDown", 1);
        }

        GameObject.Find("SceneFade").GetComponent<SceneFadeInOut>().EndScene();
        timeB = true;
        yieldTime = 0;
        gameObject.GetComponentInChildren<PlayerStatusTrack>().ladderClimbed += 1;
    }

    void JumpUp()
    {
        Debug.Log("calledJumpUp");
        if (top == 1)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 623f, gameObject.transform.position.z);
        }
        if (top == 2)
        {
            gameObject.transform.position = new Vector3(461.75f, 650f, gameObject.transform.position.z);
        }
        if (top == 3)
        {
            //gameObject.transform.position = GameObject.Find("TopTowerEntrance").transform.position;
            //gameObject.transform.rotation = GameObject.Find("TopTowerEntrance").transform.rotation;
            gameObject.transform.position = GameObject.Find("TopTowerEntranceNewNoAngle").transform.position;
            gameObject.transform.rotation = GameObject.Find("TopTowerEntranceNewNoAngle").transform.rotation;
            atTop = true;
        }
        climbB = true;
        downB = true;
        jumpUp = false;
    }

    void JumpDown()
    {
        Debug.Log("calledJumpDown");
        if (top == 1)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 595.3313f, gameObject.transform.position.z);
        }
        if (top == 2)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 623f, gameObject.transform.position.z);
        }
        if (top == 3)
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 650f, gameObject.transform.position.z);
        }
        climbB = true;
        downB = true;   // NEW INCLUIDED
        jumpDown = false;
    }

    void OnGUI()
    {
        if (climbB)
        {
            GUI.Label(new Rect(Screen.width / 2 - 90, Screen.height - 90, 190, 40), "Hold \"C\" button to Climb up", gameObject.GetComponentInChildren<Inventory>().InventoryStyle);
        }
        if (downB)
        {
            GUI.Label(new Rect(Screen.width / 2 - 90, Screen.height - 45, 190, 40), "Hold \"B\" button to Back down", gameObject.GetComponentInChildren<Inventory>().InventoryStyle);
        }
        
        if (atTop)
        {
            GUI.Box(new Rect(Screen.width / 2 - 90, Screen.height / 2 - 30, 200, 150), "You have reached the top. At this point, you can untie off your safety rope before entering the nacelle. \nLeft click to enter nacelle.", gameObject.GetComponentInChildren<Inventory>().InventoryStyle);
            if (Input.GetMouseButton(0))
            {

                atTopPos = gameObject.transform.position;
                atTopRot = gameObject.transform.rotation;
                gameObject.transform.position = GameObject.Find("NacellePlayerPosInside").transform.position;
                gameObject.transform.rotation = GameObject.Find("NacellePlayerPosInside").transform.rotation;

                //gameObject.transform.position = GameObject.Find("TopNacelleEntrance").transform.position;
                //gameObject.transform.rotation = GameObject.Find("TopNacelleEntrance").transform.rotation;
                ladderEntrance.sliderCollider.SetActive(true);
                ropeLineRender.useWorldSpace = true;
                ropePlayerLeft.SetActive(true);
                inNacelle = true;
                atTopNacelle = true;
                ladderEntrance.show_InsideNacelle = true;
                downB = false;
                climbB = false;
                atTop = false;
            }

        }

  
        if (inNacelle)
        {
            gameObject.GetComponent<FirstPersonController>().movementSpeed = 0.35f;
            GUI.Box(new Rect(10, Screen.height - 140, 240, 150), "Press \"L\" to Leave nacelle. \n\nPress \"E\" to End checking and quit." +
                "\n\nPress \"P\" to show again the panel inside the nacelle.", gameObject.GetComponentInChildren<Inventory>().InventoryStyle);

            if (Input.GetKeyUp(KeyCode.L))
            {
                slider.SetActive(true);
                ropePlayerLeft.SetActive(true);
                gameObject.transform.position = atTopPos;
                gameObject.transform.rotation = atTopRot;
                downB = true;
                inNacelle = false;
                gameObject.GetComponent<FirstPersonController>().movementSpeed = speed;
            }
            if (Input.GetKeyUp(KeyCode.E))
            {
                endB = true;
            }
            if (Input.GetKeyUp(KeyCode.P))
            {
                ladderEntrance.show_InsideNacelle = true;
            }
        }

        /*if (ladderEntrance.TopNacelle)
        {
            //gameObject.GetComponent<FirstPersonController>().movementSpeed = 0.35f;
            GUI.Box(new Rect(10, Screen.height - 140, 240, 150), "Press \"T\" to Leave top of nacelle. ", gameObject.GetComponentInChildren<Inventory>().InventoryStyle);
            ladderEntrance.show_FallDown_FromNacelle = false;
            if (Input.GetKeyUp(KeyCode.T))
            {
                ladderEntrance.show_FallDown_FromNacelle = false;
                ladderEntrance.TopNacelle = false;
                //gameObject.transform.position = atTopPos;
                //gameObject.transform.rotation = atTopRot;
                gameObject.transform.position = GameObject.Find("NacellePlayerPosInside").transform.position;
                gameObject.transform.rotation = GameObject.Find("NacellePlayerPosInside").transform.rotation;
                //downB = true;
                //atTopNacelle = false;
                //gameObject.GetComponent<FirstPersonController>().movementSpeed = speed;

                ropePlayerLeft.SetActive(true);
                inNacelle = true;
                atTopNacelle = true;
                ladderEntrance.show_InsideNacelle = true;
                downB = false;
                climbB = false;
                atTop = false;
                ladderEntrance.TopNacelle = false;
                ladderEntrance.show_FallDown_FromNacelle = false;
            }
        }*/

        if (endB)
        {
            gameObject.GetComponentInChildren<GUIManager>().showOther = true;
            GUI.Box(new Rect(Screen.width / 2 - 90, Screen.height / 2 - 90, 180, 60), "Are you sure to end?", gameObject.GetComponentInChildren<Inventory>().InventoryStyle);
            if (GUI.Button(new Rect(Screen.width / 2 - 160, Screen.height / 2 - 20, 150, 50), "End", gameObject.GetComponentInChildren<Inventory>().InventoryStyle))
            {
                SceneManager.LoadScene(0, LoadSceneMode.Additive);   //need to know where i want to go
                gameObject.GetComponentInChildren<PlayerStatusTrack>().endTask = true;
                gameObject.GetComponentInChildren<GUIManager>().showOther = false;
            }
            if (GUI.Button(new Rect(Screen.width / 2 + 10, Screen.height / 2 - 20, 150, 50), "Cancel", gameObject.GetComponentInChildren<Inventory>().InventoryStyle))
            {
                endB = false;
                gameObject.GetComponentInChildren<GUIManager>().showOther = false;
            }
        }
    }
}
