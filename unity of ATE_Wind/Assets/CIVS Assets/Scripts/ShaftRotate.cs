using UnityEngine;
using System.Collections;

public class ShaftRotate : MonoBehaviour {

	public bool shaftRotating = true;

	/*
	//For demo.
	public bool sentAlert;
	*/

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(shaftRotating){
			transform.Rotate(0, 0, 540 * Time.deltaTime);
		}

		/*
		//For demo.
		else if(!shaftRotating && !sentAlert){
			GameObject.FindGameObjectWithTag("Checklist").GetComponent<ProjectChecklist>().shutDown = true;
			GameObject.FindGameObjectWithTag ("GUIManager").GetComponent<GUIManager>().showMenu = true;
			sentAlert = true;
		}
		*/

	}
}
