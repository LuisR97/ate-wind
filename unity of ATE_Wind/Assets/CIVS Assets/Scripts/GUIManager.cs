using UnityEngine;
using System.Collections;
using System.Collections.Generic;

enum MessageType {
	Timed,
	Alert,
	FrameByFrame,
	Info,
	Question
};

enum MessageLocation {
	UpperLeft,
	UpperRight,
	Center,
	LowerLeft,
	LowerRight,
	All
};

class Messages {
	public string body;
	public MessageType type;
	public MessageLocation loc;
	public float time;
	public float total_time;
	public string yes_str;
	public string no_str;
}



public class GUIManager : MonoBehaviour {

	//Whether to show or hide menus. (See inventory.cs).
	public bool showMenu;
	public bool showOther;
	public bool showWorkOrder;

	//Texture for reticle.
	public Texture2D reticle;
	public GameObject GearReticle;

	//Establish GUIStyles.
	public GUIStyle defaultGUI;

	//Tools settings.
	public bool toolActive;
	public bool toolAlreadyActive;

	public float gui_message_show = 1.0f;
	private string gui_message = "";
	private string gui_message_ul = "";
	private string gui_message_ur = "";
	private string gui_message_ll = "";
	private string gui_message_lr = "";
	private List<Messages> gui_message_list;


    //private float msg_time = 0;
    //private bool one_frame = false;

    [System.Obsolete]
    void Start () {

		/*
		//For demo.
		if(Application.loadedLevelName == "Office" || Application.loadedLevelName == "Nacelle" ){
			showMenu = true;
		}
		*/

		Cursor.visible = true;
		Screen.lockCursor = false;

		gui_message_list = new List<Messages>();
	}
	
	/*void Update () {

		if(showMenu || showOther || showWorkOrder){
			Cursor.visible = true;
			Screen.lockCursor = false;
		}else{
			Cursor.visible = false;
			Screen.lockCursor = true;
		}
		
		//Quick escape.
		if(Input.GetKeyDown (KeyCode.Escape)){
			Application.Quit ();
		}

		//Press 'e' to show or hide the inventory menu.
		if(Input.GetButtonDown ("Menu")){
			showMenu = !showMenu;
		}

		if(Input.GetButtonDown ("WorkOrder")){
			showWorkOrder = true;
		}

		//Show spinning gear on reticle if tool is active (See LoadingGear.cs).
		if(gameObject.GetComponent<Inventory>().equipped){
			toolActive = true;
		}
		else if(!gameObject.GetComponent<Inventory>().equipped){
			toolActive = false;
		}

		//Just making sure we only turn off the menu, turn on the gear, and lock/hide the cursor ONCE instead of continually.
		if(toolActive && toolActive != toolAlreadyActive){
			GearReticle.SetActive (true);
			showMenu = false;
			//Screen.lockCursor = true;
			//Screen.showCursor = false;
			toolAlreadyActive = true;
		}
		else if(!toolActive){
			toolAlreadyActive = false;
		}

		int n = 0;
		gui_message = "";
		gui_message_ll = "";
		gui_message_lr = "";
		gui_message_ul = "";
		gui_message_ur = "";

		while (n < gui_message_list.Count) {

			string local_msg = "";

			Messages m = gui_message_list[n];

			if (m.type == MessageType.Timed) {	
				//Color c = new Color(1.0f,1.0f,0,(float)(m.time/m.total_time));
				local_msg +=  "<color=yellow" + 
				//string.Format("#{0:X}{1:X}{2:X}{3:X}",255,255,0,124).ToLower () +  
					"><b>" +
					m.body + "</b></color>";

				m.time -= Time.deltaTime;
				if (m.time < 0) 
					gui_message_list.Remove (m);
			}
			if (m.type == MessageType.FrameByFrame) {
				local_msg += "<color=yellow><b>" + m.body + "</b></color>";
				gui_message_list.Remove(m);
			}
			 if (m.type == MessageType.Info) {

			}
			if (m.type == MessageType.Alert) {

			} 

			local_msg += "\n";
			bool empty;

			if (m.loc == MessageLocation.UpperLeft || m.loc == MessageLocation.All) {
				empty = (gui_message_ul == "");
				gui_message_ul += ((empty) ? "" : "\n") + "<color=white><b>" +  m.body + "</b></color>"; 
			}
			if (m.loc == MessageLocation.UpperRight || m.loc == MessageLocation.All) {
				empty = (gui_message_ur == "");
				gui_message_ur += ((empty) ? "" : "\n") + "<color=white><b>" +  m.body + "</b></color>";
			}
			if (m.loc == MessageLocation.Center|| m.loc == MessageLocation.All) {	
				empty = (gui_message == "");
				gui_message += ((empty) ? "" : "\n") + "<color=yellow><b>" +  m.body + "</b></color>";
			}
			if (m.loc == MessageLocation.LowerLeft || m.loc == MessageLocation.All) {
				empty = (gui_message_ll == "");
				gui_message_ll += ((empty) ? "" : "\n") + "<color=white><b>" +  m.body + "</b></color>";
			}
			if (m.loc == MessageLocation.LowerRight || m.loc == MessageLocation.All) {
				empty = (gui_message_lr == "");
				gui_message_lr += ((empty) ? "" : "\n") + "<color=white><b>" +  m.body + "</b></color>";
			}
				
			n++;
		}
	}*/

	void OnGUI () {

		//Altering the GUIStyle.
		GUI.backgroundColor = Color.yellow;
		GUI.skin.label.fontSize = 12;
		GUI.skin.label.alignment = TextAnchor.UpperCenter;
		GUI.skin.label.richText = true;
		GUI.skin.label.wordWrap = true;
		//GUI.skin.label.font.material.SetColor ("1",Color.clear);

		//Show reticle in center of screen.
		if (!showMenu && !showOther && !showWorkOrder)
			GUI.Box (new Rect(Screen.width/2,Screen.height/2-4,1,1),"+",defaultGUI);
	
		int save_size = GUI.skin.label.fontSize;


		if (gui_message != "") {
			GUI.skin.label.fontSize = 24;
			GUI.Label (new Rect(Screen.width/4,Screen.height/4,Screen.width/2,Screen.height/2), gui_message );
		}

		GUI.skin.label.fontSize = 14;

		if (gui_message_ul != "") {
			GUI.skin.label.alignment = TextAnchor.UpperLeft;
			GUI.Label (new Rect(0,0,Screen.width/4,Screen.height/4), gui_message_ul );		
		}

		if (gui_message_ur != "") {
			GUI.skin.label.alignment = TextAnchor.UpperLeft;
			GUI.Label (new Rect( 3 * Screen.width/4,0,Screen.width/4,Screen.height/4), gui_message_ur );
		}

		if (gui_message_ll != "") {
			GUI.skin.label.alignment = TextAnchor.LowerLeft;
			GUI.Label (new Rect(0,3 * Screen.height/4,Screen.width/4,Screen.height/4), gui_message_ll );
		}

		if (gui_message_lr != "") {
			GUI.skin.label.alignment = TextAnchor.LowerLeft;
			GUI.Label (new Rect(3 * Screen.width/4,3 * Screen.height/4,Screen.width/4,Screen.height/4), gui_message_lr );
		}

		GUI.skin.label.fontSize = save_size;
	}


	public void GUIMessageTimed(string msg)
	{
		GUIMessageTimed (msg,gui_message_show);
	}

	public void GUIMessageTimed(string msg,float time)
	{
		Messages m = new Messages();
		m.body = msg;
		m.type = MessageType.Timed;
		m.loc = MessageLocation.Center;
		m.time = time;
		m.total_time = time;
		gui_message_list.Add(m);
	}

	public void GUIMessage(string msg)
	{
		Messages m = new Messages();
		m.body = msg;
		m.type = MessageType.FrameByFrame;
		gui_message_list.Add(m);
	}
}
