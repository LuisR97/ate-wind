using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]
public class FirstPersonController : MonoBehaviour {

	//GUIManager should be a CHILD OF PLAYER, THE OBJECT TO WHICH THIS SCRIPT IS ATTACHED.
	public GameObject GUIManager;
	
	//Define move and look speed. DWL
	public float movementSpeed = 5.0f;
	public float mouseSensitivity = 1.0f;

	//Store move and look speed. DWL
	public float storedSpeed;
	public float storedSensitivity;
	
	//Clamp pitch rotation. DWL
	public float verticalRotation = 0;
	public float verticalAngleLimit = 80.0f;
	
	//Calculating for gravity and jumping. DWL
	public float verticalVelocity = -1;
	public float jumpSpeed = 8;
	
	//Determine throwable object acquisition. DWL
	public static bool throwObject = true;
	
	CharacterController characterController;

	float yawLook;
	// Use this for initialization
	[System.Obsolete]
    void Start () {
		//Screen.lockCursor = true;
		characterController = GetComponent<CharacterController>();
		storedSpeed = movementSpeed;
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetMouseButton(1))
        {
            //Left-Right Rotation (Declaration). DWL
            yawLook = Input.GetAxis ("Mouse X") * mouseSensitivity;

            //Up-Down Rotation (Declaration). DWL
            verticalRotation -= Input.GetAxis ("Mouse Y") * mouseSensitivity;
            verticalRotation = Mathf.Clamp(verticalRotation, -verticalAngleLimit, verticalAngleLimit);

        }
        else
        {
            yawLook = 0;
        }

        //If menu is open, don't allow movement.
        if (!(GUIManager.GetComponent<GUIManager>().showMenu ||
             GUIManager.GetComponent<GUIManager>().showOther ||
             GUIManager.GetComponent<GUIManager>().showWorkOrder))
        {

            //Movement (Declaration). DWL
            float forwardSpeed = Input.GetAxis("Vertical") * (movementSpeed + movementSpeed);
            float sideSpeed = Input.GetAxis("Horizontal") * (movementSpeed + movementSpeed);
            Vector3 speed = new Vector3(sideSpeed, verticalVelocity, forwardSpeed);
            speed = transform.rotation * speed;

            //Left-Right Rotation. DWL
            transform.Rotate(0, yawLook, 0);

            //Up-Down Rotation. DWL
            Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

            //Movement. DWL
            //verticalVelocity += Physics.gravity.y * Time.deltaTime;

            //Call movement. DWL
            characterController.Move(speed * Time.deltaTime);
        }

        if (characterController.isGrounded && Input.GetButtonDown("Jump"))
        {
            verticalVelocity = jumpSpeed;
        }
    }	
}