﻿using UnityEngine;
using System.Collections;

//You have to use this to notify Unity that a class should show up in the inspector, if you're using C#:
[System.Serializable]

//Also notice MonoBehavior was removed from the following line:
public class ItemClass{
		public string itemName;
		public string description;
		public Texture2D icon;
		public int id;
		public GameObject model;

	public ItemKind itemType = new ItemKind{};
	public enum ItemKind{ None, ToolItem, Other}
}
