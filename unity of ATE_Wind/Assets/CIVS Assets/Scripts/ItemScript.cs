using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ItemScript : MonoBehaviour
{

    //What is this object's number?
    public int objectID;

    public GameObject equippedObject;
    public Quaternion defaultRotation = new Quaternion(0, 0, 0, 0);
    public bool showIcon;
    public bool pickS;

    public bool callB;
    public bool radioB;
    public bool sliderB;
    public bool connectB;
    public bool schemB;

    public float callTime;

    float yieldTime;
    public GameObject slider;
    public GameObject CarabinerRightItem;
    public GameObject CarabinerLeftItem;


    // Use this for initialization
    void Start()
    {
        callTime = gameObject.GetComponent<PlayerStatusTrack>().radioCallTime;
    }

    void OnGUI()
    {
        if (showIcon)
        {

            GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 75, 150, 150), gameObject.GetComponent<AddItem>().loot[objectID].icon, gameObject.GetComponent<Inventory>().InventoryStyle);
            GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 75 - 60, 150, 50), gameObject.GetComponent<AddItem>().loot[objectID].itemName, gameObject.GetComponent<Inventory>().InventoryStyle);
            GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 75 + 160, 200, 80), gameObject.GetComponent<AddItem>().loot[objectID].description, gameObject.GetComponent<Inventory>().InventoryStyle);


            gameObject.GetComponent<GUIManager>().showMenu = true;

            if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 75 + 160 + 90, 100, 40), "Pick Up", gameObject.GetComponent<Inventory>().InventoryStyle))
            {
                gameObject.GetComponent<GUIManager>().showMenu = false;
                gameObject.GetComponent<AddItem>().giveLoot(objectID);
                GameObject.Destroy(GameObject.Find(objectID.ToString()));
                showIcon = false;
            }
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90, 100, 40), "Cancel", gameObject.GetComponent<Inventory>().InventoryStyle))
            {
                gameObject.GetComponent<GUIManager>().showMenu = false;
                showIcon = false;
            }
        }



        if (callB)
        {
            yieldTime += Time.deltaTime;

            GUI.Label(new Rect(Screen.width / 2 + 40, 20, 80, 30), "+ " + callTime.ToString() + " Min.");
            GUI.Label(
                new Rect(Screen.width / 2 - 100, Screen.height / 2, 200, 100),
                "Log in with monitoring center",
                gameObject.GetComponent<Inventory>().InventoryStyle);

            if (yieldTime > 5)
            {
                gameObject.GetComponent<PlayerStatusTrack>().radioUsed += 1;
                gameObject.GetComponent<TimerLadder>().timer += Mathf.RoundToInt(callTime * 60);
                callB = false;
            }
        }


        if (sliderB)
        {
            if (gameObject.GetComponentInParent<Ladder_2>().onGround)
            {
                yieldTime += Time.deltaTime;
                GUI.Label(
                    new Rect(Screen.width / 2 - 80, Screen.height - 150, 160, 50),
                    "Nothing to connect with",
                    gameObject.GetComponent<Inventory>().InventoryStyle);
                if (yieldTime > 4)
                {
                    sliderB = false;
                    yieldTime = 0;
                }
                Debug.Log("slider gone");
            }
            else
            {
                yieldTime += Time.deltaTime;
                GUI.Label(
                    new Rect(Screen.width / 2 - 80, Screen.height - 150, 160, 50),
                    "connected to slider \n(left click to disconnect)",
                    gameObject.GetComponent<Inventory>().InventoryStyle);
                connectB = true;
                if (Input.GetMouseButton(0) && yieldTime > 2)
                {
                    gameObject.GetComponent<PlayerStatusTrack>().sliderUsed += 1;
                    connectB = false;
                    sliderB = false;
                }
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        //show slider when use
        slider.SetActive(connectB);
        CarabinerRightItem.SetActive(connectB);
        CarabinerLeftItem.SetActive(connectB);

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        //If we don't currently have an item equipped, and the menu is not currently being shown...
        if (!gameObject.GetComponent<Inventory>().equipped && !gameObject.GetComponent<GUIManager>().showMenu)
        {
            //This determines whether an object is an item, and picks it up if it is.
            if (Physics.Raycast(ray, out hit, 80))
            {
                if (hit.collider.transform.tag == "Item")
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        objectID = int.Parse(hit.collider.name);
                        showIcon = true;

                        //hit.collider.gameObject.transform.Translate(0,-1000,0);
                    }
                }
                else if (hit.collider.transform.tag == "Secret")
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        foreach (ItemClass item in gameObject.GetComponent<AddItem>().loot)
                        {
                            gameObject.GetComponent<AddItem>().giveLoot(item.id);
                        }
                    }
                }

                //THIS IS HOW TO ACCESS THE SAFETY TEXT ADVENTURE WHEN CLICKING THE PC.
                else if (hit.collider.transform.tag == "PC")
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        Cursor.visible = true;
                        //Screen.lockCursor = false;
                        Cursor.lockState = CursorLockMode.None;
                        //Application.LoadLevel ("TextAdventure");
                        SceneManager.LoadScene("TextAdventure", LoadSceneMode.Additive);
                    }
                }
            }
        }

        else if (gameObject.GetComponent<Inventory>().equipped)
        {
            equippedObject = gameObject.GetComponent<Inventory>().equippedItem.model;

            //left click to use when equipped
            if (Input.GetMouseButtonDown(0) && !gameObject.GetComponent<GUIManager>().showMenu)
            {
                if (gameObject.GetComponent<Inventory>().equippedItem.itemName == "Two-Way Radio")
                {
                    gameObject.GetComponent<Inventory>().equippedItem = new ItemClass();
                    gameObject.GetComponent<AddItem>().giveLoot(3);
                    callB = true;
                    yieldTime = 0;

                }
                if (gameObject.GetComponent<Inventory>().equippedItem.itemName == "Slider")
                {
                    gameObject.GetComponent<Inventory>().equippedItem = new ItemClass();
                    gameObject.GetComponent<AddItem>().giveLoot(23);

                    sliderB = true;
                    yieldTime = 0;

                }
                if (gameObject.GetComponent<Inventory>().equippedItem.itemName == "Digital Schematic")
                {
                    gameObject.GetComponent<Inventory>().equippedItem = new ItemClass();
                    gameObject.GetComponent<AddItem>().giveLoot(26);
                    gameObject.GetComponent<PlayerStatusTrack>().schematicOn = true;
                }
            }

            //---right click to drop off tools
            if (Input.GetMouseButtonDown(1) && !gameObject.GetComponent<GUIManager>().showMenu)
            {
                Physics.Raycast(ray, out hit, 80);
                GameObject instantiatedObject = (GameObject)Instantiate(equippedObject, (hit.point), defaultRotation);
                instantiatedObject.name = gameObject.GetComponent<Inventory>().equippedItem.model.name;
                gameObject.GetComponent<Inventory>().equippedItem = new ItemClass();
            }



            //---i button to back to cancel equipped
            if (Input.GetButtonDown("Menu"))
            {
                gameObject.GetComponent<GUIManager>().showOther = false;
                gameObject.GetComponent<Inventory>().equippedItem = new ItemClass();
                gameObject.GetComponent<AddItem>().giveLoot(int.Parse(equippedObject.name));
            }
        }

    }
}