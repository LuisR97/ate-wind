﻿/*
 * Created by Myriam Changoluisa
 * Date: January 12th, 2021
 * Email:mchangol@pnw.edu
 * Script not attached to object
 * 
 * 
 * To chek for key
 * If hte object (key) equipment is not active in hirarchy
 * then the key is true and the red lock is unlocked
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryKey : MonoBehaviour
{
    static int numberOfDoors = 10;
    public static bool[] keys = new bool[numberOfDoors];
    public static bool[] interactable = new bool[numberOfDoors];
    public static bool[] firstEntrance = new bool[numberOfDoors];
    public static bool[] SecondEntrance = new bool[numberOfDoors];

    private void Awake()
    {
        for(int i = 0; i < numberOfDoors; i++)
        {
            keys[i] = false;
            interactable[i] = false;
            firstEntrance[i] = true;
            SecondEntrance[i] = false;
        }
    }

}
