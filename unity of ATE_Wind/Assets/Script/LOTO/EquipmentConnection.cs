﻿/*
 * Created by Myriam Changoluisa
 * Date: February 22ndo, 2021
 * Email:mchangol@pnw.edu
 * Script not attached to object
 * I need to have the equipments 
 * Access to the inventory slots 
 * Also Access to the sprite of the inventory 
 * Access to the names of the sprites 
 * Compare those names and deactive 
 * if the names are present in the inventory 
 * 
 * THE INDEX IN THE SLOTHOLDER DOES NOT HAVE TO MATTER WITH 
 * THE DOOR INDEX 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentConnection : MonoBehaviour
{
    //input 
    //LIST OF EQUIPMENTS NAME FROM INVENTORY OF 1ST SCENE
    public List<Image> slotsImageHolder = new List<Image>();   //1
    public List<string> equipmentNamesFromInventoryPanel = new List<string>();
    //private List<string> equipmentNamesFromInventoryPanel = new List<string>();

    //LIST OF EQUIPMENTS FROM INVENTORY
    public List<GameObject> slotsGameObjectHolder = new List<GameObject>();
    private List<string> equipmentNamesOfGameObjects = new List<string>();
    
    public int currentSize = 0;

    public string nameOfEquipLevel4;
    public string nameOfEquipLevel2;
   
    //// output
    public bool isLevel4InInventory;
    public bool isLevel2InInventory;
    //public int indexL2;
    //public int indexL4;

    public PickEquipments pE;
    public bool onlyOnce = true;

    private void Update()
    {

        if(pE.B_WarehouseScene == false && onlyOnce == true)
        {
            if (pE.isLevel4InInventory == true)
            {
                pE.inInventoryL4 = true;
                pE.readyToSwitchL2 = true;
                onlyOnce = false;
            }  
            
            if (pE.isLevel2InInventory == true)
            {
                pE.inInventoryL2 = true;
                pE.readyToSwitchL4 = true;
                onlyOnce = false;
            }
        }

        for (int i = 0; i < slotsImageHolder.Count; ++i)
        {
            if (slotsImageHolder[i].GetComponent<Image>().sprite != null)
            {
                //Debug.Log("the names are STRING: " + slotsImageHolder[i].GetComponent<Image>().sprite.name.ToString());

                equipmentNamesFromInventoryPanel.Add(slotsImageHolder[i].GetComponent<Image>().sprite.name.ToString());
                currentSize = i;
            }
        }
        //Debug.Log("++++++++++" + currentSize);

        for (int i = 0; i < slotsGameObjectHolder.Count; ++i)
        {
            if (slotsGameObjectHolder[i] != null)
            {
                //Debug.Log("the names of Objects are : " + slotsGameObjectHolder[i].name);

                equipmentNamesOfGameObjects.Add(slotsGameObjectHolder[i].name);
            }
        }

        for (int i = 0; i < slotsGameObjectHolder.Count; ++i)
        {
            //Debug.Log("Does the names contains ? : " + equipmentNamesFromInventoryPanel.Contains(equipmentNamesOfGameObjects[i]));

            if (equipmentNamesFromInventoryPanel.Contains(equipmentNamesOfGameObjects[i]))
            {
                slotsGameObjectHolder[i].SetActive(false);
                if(pE.isLevel4InInventory == true)
                {
                    pE._Level2Equip.SetActive(true);
                }
                if (pE.isLevel2InInventory == true)
                {
                    pE._Level4Equip.SetActive(true);
                }

            }
        }
    }
}
