﻿/*
 * Created by Myriam Changoluisa
 * Date: February 16th, 2021
 * Email:mchangol@pnw.edu
 * Script attached to object with Tag "Redlock"
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewLockScript : MonoBehaviour
{

    //index
    public int index = -1;

    //NEW VARIABLE
    public bool redLockLock = true;
    public bool redLockUnlock = false;
    public bool redLockPresent = true;
    public bool redLockNotPresent = false;
    public bool redLockReadyToRemove = false;
    public bool redLockReadyToPlaceBack = false;
    public bool isPlacedBack = false;
    //

    public bool isLevel4EquipNeeded;
    public bool isLevel2EquipNeeded;

    // CANVAS
    bool isCanvasReadyToActive = false;
    public bool canvasActive = false;

    //DISTANCE
    public GameObject padLock;
    public string padlockName;
    public GameObject door;
    public GameObject doorKey;
    public GameObject cam;

    float dist;
    float distCam;
    float distDoor;

    //BUTTON
    public GameObject Canvas;
    public Button yourButton;

    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }


    void TaskOnClick()    //this is related to the canvas
    {
        Debug.Log("##########################" + "You have clicked the button inside the canvas!");
        Debug.Log("#### THE INDEX OF THIS PADLOCK IS : " + index);
        Debug.Log("#### THE INDEX OF THIS door IS : " + door.GetComponentInParent<NewDoorScript>().index);

        RemoveDoorPadlock();
    }

    void CalculateDistance(GameObject redlockRef, GameObject doorRef, GameObject camRef)
    {
        dist = Vector3.Distance(redlockRef.transform.position, doorRef.transform.position);
        distCam = Vector3.Distance(redlockRef.transform.position, camRef.transform.position);
        distDoor = Vector3.Distance(doorRef.transform.position, camRef.transform.position);
        print("Distance beetween door and padlock ********* : " + dist + " index : " + index);
        print("Distance beetween camera and padlock: " + distCam + " index : " + index);
        print("Distance beetween camera and door: " + distDoor + " index : " + index);
    }

    public void RemovePadlockFromDoor()
    {
        redLockReadyToRemove = CanRemovePadlockFromDoor();

        if (redLockReadyToRemove == true)
        {
            canvasActive = false;
            padLock.SetActive(false);
            Canvas.SetActive(false);
            redLockPresent = false;
            redLockNotPresent = true;
            door.GetComponent<BoxCollider>().enabled = true;
            door.GetComponentInParent<NewDoorScript>().doorLock = false;   //here the door gets unlocked 
            door.GetComponentInParent<NewDoorScript>().doorUnlock = true;

        }
        else
        {
            redLockPresent = true;
            redLockNotPresent = false;
        }
    }

    public void RemoveDoorPadlock()
    {
        if (padLock.name == padlockName && InventoryKey.interactable[index] == true)   //since padlock is interactable / has the key, you can remove the padlock
        {
            CalculateDistance(padLock, door, cam);
            if (dist < dist + 3.0f && distCam < distCam + 3.0f && distDoor < distDoor + 3.0f)
            {
                Debug.Log("Removed the " + padLock + " from the " + door);
                RemovePadlockFromDoor();
                Debug.Log("It was removed ??");
                //redLockReadyToRemove = false;    ////????????????ew
            }

        }
    }

    public bool CanCanvasActivate()
    {
        if (redLockUnlock == true && redLockPresent == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CanRemovePadlockFromDoor()
    {
        if (canvasActive == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void PlaceBackPadlock()
    {
        Debug.Log("We are ready to place the padlock back ");
        padLock.SetActive(true);
        redLockNotPresent = false;
        redLockPresent = true;
        redLockLock = true;
        redLockUnlock = false;
        door.GetComponentInParent<NewDoorScript>().doorClose = true;
        door.GetComponentInParent<NewDoorScript>().doorOpen = false;
        door.GetComponentInParent<NewDoorScript>().doorUnlock = false;
        door.GetComponentInParent<NewDoorScript>().doorLock = true;
        door.GetComponentInParent<NewDoorScript>().doorReadyToClose = false;
        door.GetComponentInParent<NewDoorScript>().doorReadyToOpen = false;
        door.GetComponentInParent<NewDoorScript>().doorLock = true;
        door.GetComponent<BoxCollider>().enabled = false;
        Debug.Log("el door colider se desactivo ");
        isPlacedBack = true;
        redLockReadyToRemove = false;
        redLockReadyToPlaceBack = false;
    }

    void Update()
    {
        isCanvasReadyToActive = CanCanvasActivate();

        if (isCanvasReadyToActive == true)
        {
            if (canvasActive == false)
            {
                Canvas.SetActive(true);
                canvasActive = true;
            }
        }
        else if (redLockReadyToPlaceBack == true)
        {
            PlaceBackPadlock();
            redLockReadyToPlaceBack = false;
        }
    }
}
