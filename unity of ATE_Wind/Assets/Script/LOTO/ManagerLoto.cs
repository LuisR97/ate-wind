﻿/*
 * Created by Myriam Changoluisa
 * Date: February 16th, 2021
 * Email:mchangol@pnw.edu
 * Script attached to the camera
 */

using System;
using System.Collections;
using System.Collections.Generic; 
using UnityEngine;
using UnityEngine.UI;

public class ManagerLoto : MonoBehaviour
{

    public float interactDistance = 1000f;
    //public EquipmentConnection EquipConectionCheck;
    public PickEquipments pEinML;

    private void Start()
    {
        //EquipConectionCheck = GameObject.FindGameObjectWithTag("ScriptHolder").GetComponent<EquipmentConnection>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //Ray ray = new Ray(transform.position, transform.forward);
            //RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, interactDistance))
            {
                if (hit.collider.CompareTag("Door"))
                {
                    NewDoorScript newDoorScript = hit.collider.transform.GetComponent<NewDoorScript>();

                    Debug.Log("Highlighted door is: " + hit.collider.transform.parent.name);
                    Debug.Log("The index that is being called is: " + hit.collider.transform.GetComponent<NewDoorScript>().index);
                    Debug.Log("InventoryKey Reference key is " + InventoryKey.keys[newDoorScript.index] + " and the first entrance ref is :  " + InventoryKey.firstEntrance[newDoorScript.index]);
                    Debug.Log("redLockScriptReference is: " + newDoorScript.padlockRef.GetComponentInParent<NewLockScript>().gameObject.name);

                    if (newDoorScript == null) return;

                    if (newDoorScript.doorClose == true && newDoorScript.doorUnlock == true) // Check the door is closed and Unlocked 
                    {
                        Debug.Log("Enters if: door is closed and it is the first click to send the signal to OPEN THE DOOR ");
                        Debug.Log(" what is the value printed of : " + newDoorScript.IsPadlockPresentInDoor());
                        Debug.Log(" what is the value printed of : " + newDoorScript.doorHasRedlock);
                        Debug.Log(" what is the value printed of keys : " + InventoryKey.keys[newDoorScript.index]);
                        Debug.Log(" what is the value printed of first entrance: " + InventoryKey.firstEntrance[newDoorScript.index]);
                        Debug.Log(" what is the value printed of second  entrance : " + InventoryKey.SecondEntrance[newDoorScript.index]);

                        if (newDoorScript.padlockRef.GetComponentInParent<NewLockScript>().redLockNotPresent == true
                        && InventoryKey.keys[newDoorScript.index] && InventoryKey.firstEntrance[newDoorScript.index] == false)
                        {
                            Debug.Log("Enters if: door is closed and the padlock is not present it is the first click to send the signal to OPEN THE DOOR ");
                            Debug.Log("THE NAME IS " + newDoorScript.name + " AND THE INDEX IS : " + newDoorScript.index);

                            newDoorScript.doorReadyToOpen = true;
                            //InventoryKey.firstEntrance[newDoorScript.index] = false;
                            //InventoryKey.SecondEntrance[newDoorScript.index] = false;
                            InventoryKey.firstEntrance[newDoorScript.index] = true;
                            InventoryKey.SecondEntrance[newDoorScript.index] = true;
                            Debug.Log(" what is the value printed of second  entrance insside if: " + InventoryKey.SecondEntrance[newDoorScript.index]);
                        }
                        else if (newDoorScript.padlockRef.GetComponentInParent<NewLockScript>().redLockNotPresent == true
                            && InventoryKey.keys[newDoorScript.index] && InventoryKey.SecondEntrance[newDoorScript.index] == true)  ///aqui
                        {
                            Debug.Log("!!!!!!!!!!!!Entro aqui?? !!!!!!!!!!!");
                            newDoorScript.padlockRef.GetComponentInParent<NewLockScript>().redLockReadyToPlaceBack = true;
                            //InventoryKey.SecondEntrance[newDoorScript.index] = false;
                            //InventoryKey.firstEntrance[newDoorScript.index] = true;
                            InventoryKey.SecondEntrance[newDoorScript.index] = true;
                            InventoryKey.firstEntrance[newDoorScript.index] = false;
                            newDoorScript.padlockRef.GetComponentInParent<NewLockScript>().doorKey.SetActive(true);
                        }
                        else
                        {
                            Debug.Log("!!!!!!!!!!!!No entro !!!!!!!!!!!");
                        }

                    }

                    else if (newDoorScript.padlockRef.GetComponentInParent<NewLockScript>().redLockNotPresent = true
                        && newDoorScript.doorUnlock == true)   //if padlock is not present 
                    {
                        Debug.Log("Enters if: redlock is not present and key are false and it is the second click for the door");


                        Debug.Log("Enters if: door is open and reset is true to send signal to CLOSE THE DOOR");

                        newDoorScript.doorReadyToOpen = false;
                        newDoorScript.doorReadyToClose = true;
                        InventoryKey.SecondEntrance[newDoorScript.index] = false; // new included
                        Debug.Log(" what is the value printed of AFTER CLSOING THE DOOR: " + InventoryKey.SecondEntrance[newDoorScript.index]);

                    }
                    else
                    {
                        Debug.Log("!!!!!!!!!!!!No entro en ninguno!!!!!!!!!!!");
                    }
                }

                else if (hit.collider.CompareTag("Key"))
                {
                    InventoryKey.keys[hit.collider.GetComponent<Key>().index] = true;  //Key is a class 
                    //Destroy(hit.collider.gameObject);    //this disapears the object but in our case we have our own system to collect the objects 
                    hit.collider.gameObject.SetActive(false);    //this disapears the object but in our case we have our own system to collect the objects 
                    Debug.Log("The key is click and it desapears");
                }

                else if (hit.collider.CompareTag("RedLock"))
                {
                    Debug.Log("Raycaster hits the Redlock for the firts time");
                    NewLockScript newLockScript = hit.collider.transform.parent.GetComponent<NewLockScript>();

                    if (newLockScript == null) return;

                    if (newLockScript.redLockLock == true && newLockScript.redLockPresent == true && newLockScript.redLockUnlock == false && newLockScript.redLockNotPresent == false)
                    {
                        Debug.Log("Enter: initial state the REDLOCK is locked and PRESENT");
                        Debug.Log("THE NAME IS " + newLockScript.name + " AND THE INDEX IS : " + newLockScript.index);
                        
                        if (newLockScript.isLevel4EquipNeeded == true)
                        {                          
                            if (InventoryKey.keys[newLockScript.index] == true && pEinML.isLevel4InInventory == true)
                            {
                                Debug.Log("LEVEL 4 *****the key is true and it is the firts time it checks");
                                newLockScript.redLockLock = false;
                                newLockScript.redLockUnlock = true;

                                //the padlock can be interactable and can be used 
                                InventoryKey.interactable[newLockScript.index] = true;

                                Debug.Log("Congratulations!, you have the equipment needed to open the door, the lock will be removed");
                            }
                            else
                            {
                                Debug.Log("Danger LEVEL 4! You dont have the equipment needed to open the door, please check your inventory to identify the equipment needed");
                                //redLockScript.redLockLock = true;
                                //redLockScript.redLockUnlock = false;
                            }

                        }
                        else if(newLockScript.isLevel2EquipNeeded == true)
                        {
                            if (InventoryKey.keys[newLockScript.index] == true && pEinML.isLevel2InInventory == true)
                            {
                                Debug.Log("LEVEL 2 *****the key is true and it is the firts time it checks");
                                newLockScript.redLockLock = false;
                                newLockScript.redLockUnlock = true;

                                //the padlock can be interactable and can be used 
                                InventoryKey.interactable[newLockScript.index] = true;

                                Debug.Log("Congratulations!, you have the equipment needed to open the door, the lock will be removed");
                            }
                            else
                            {
                                Debug.Log("Danger LEVEL 2! You dont have the equipment needed to open the door, please check your inventory to identify the equipment needed");
                                //redLockScript.redLockLock = true;
                                //redLockScript.redLockUnlock = false;
                            }
                        }


                    }

                }

            }
        }
    }
}
