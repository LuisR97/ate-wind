﻿/*
 * Created by Myriam Changoluisa
 * Date: February 16th, 2021
 * Email:mchangol@pnw.edu
 * Script attached to object with Tag "Door"
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewDoorScript : MonoBehaviour
{
    //key
    public int index = -1;

    //door characteristics  //No need to initialize 
    public float doorOpenAngle;
    public float doorCloseAngle;
    public float doorOpenAngleNearDoor;
    public float doorCloseAngleNearDoor;
    public float smooth = 2f;

    //Door
    public bool doorLock = true;
    public bool doorUnlock = false;
    public bool doorOpen = false;
    public bool doorClose = true;
    public bool doorReadyToOpen = false;
    public bool doorReadyToClose = false;

    public GameObject nearDoor;



    // Padlock
    public GameObject padlockRef;
    public bool doorHasRedlock;

    public bool IsPadlockPresentInDoor()
    {
        doorHasRedlock = padlockRef.GetComponentInParent<NewLockScript>().redLockNotPresent;
        if (doorHasRedlock == true)
        {
            return doorHasRedlock;
        }
        else
            return false;
    }

    public void OpenDoor()
    {
        // RIGHT DOOR 
        Quaternion targetRotationClose = Quaternion.Euler(90, 0, doorOpenAngle);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotationClose, smooth * Time.deltaTime);   //check this rotation if local or global

        // LEFT DOOR 
        Quaternion nearTargetRotationClose = Quaternion.Euler(270, 0, doorOpenAngleNearDoor);
        nearDoor.transform.localRotation = Quaternion.Slerp(nearDoor.transform.localRotation, nearTargetRotationClose, smooth * Time.deltaTime);   //check this rotation if local or global

        doorClose = false;
    }


    public void CloseDoor()  //new code to close the door 
    {
        // RIGHT DOOR 
        Quaternion targetRotationClose = Quaternion.Euler(90, 0, doorCloseAngle);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotationClose, smooth * Time.deltaTime);   //check this rotation if local or global

        // LEFT DOOR 
        Quaternion nearTargetRotationClose = Quaternion.Euler(270, 0, doorCloseAngleNearDoor);
        nearDoor.transform.localRotation = Quaternion.Slerp(nearDoor.transform.localRotation, nearTargetRotationClose, smooth * Time.deltaTime);   //check this rotation if local or global

        doorClose = true;
        InventoryKey.SecondEntrance[index] = true;
    }


    void Update()
    {
        if (doorReadyToOpen == true)
        {
            doorReadyToClose = false;
            OpenDoor();
        }
        else if (doorReadyToClose == true)
        {
            doorReadyToOpen = false;
            CloseDoor();
        }



    }
}

