﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RunTime : MonoBehaviour {

	public GameObject speedText;
	public GameObject hubSafetySheet;
	public Slider sliderBar;
	GameObject[] obj;

	void Start(){
	}

	void Update(){
	
		speedText.GetComponent<Text> ().text = (Mathf.Round ((GameObject.Find ("Main Camera").GetComponent<CameraFly> ().mainSpeed * 0.844f) * 10) / 10).ToString ();

		if (!hubSafetySheet.activeSelf) {
			if (Input.GetAxis ("Mouse ScrollWheel") > 0) {	
				sliderBar.value += 0.12f;
			} else if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
				sliderBar.value -= 0.12f;
			}
	
		}
	
	}


	public void ResetColor(){
		obj = GameObject.FindGameObjectsWithTag ("Button");
		foreach(GameObject button in obj){
			button.GetComponent<Image> ().color = Color.white;
		}

	}




}
