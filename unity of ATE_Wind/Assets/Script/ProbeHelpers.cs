﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProbeHelpers : MonoBehaviour {

    //float width = 490;
    //float height = 265;

    //the content in the viewport
    //public RectTransform content;

    //the high and low probe ui objects
    public RectTransform HighProbe, LowProbe;

    //the gameobjects in the scene for the helper bubbles
    public GameObject HighBubble, LowBubble;

    //the speed that the helper bubbles move at
    public float helperMoveSpeed = 5.0f;

    //the positions of the minimum and maximum position values of the viewport window as defined as percentages of the screen size
    float minX = Screen.width * 0.4f;
    float maxX = Screen.width - (Screen.width * .065f);
    float minY = Screen.height * 0.15f;
    float maxY = Screen.height - (Screen.height * .18f);

    public void Update() {

        //sets the bubbles to be visible if the probes are not visible in the viewport
        if(ItemIsVisibleInViewport(HighProbe)) {
            HighBubble.SetActive(false);
        }else {
            HighBubble.SetActive(true);
        }
 
        if(ItemIsVisibleInViewport(LowProbe)) {
            LowBubble.SetActive(false);
        }else {
            LowBubble.SetActive(true);
        }


        //if either bubble is active, handle the movement
        if(LowBubble.activeSelf) {
            MoveHelper(LowBubble.GetComponent<RectTransform>(),LowProbe);
        }

        if(HighBubble.activeSelf) {
            MoveHelper(HighBubble.GetComponent<RectTransform>(),HighProbe);
        }

    }

    //checks if item is within the viewing bounds
	public bool ItemIsVisibleInViewport(RectTransform probe) {
       
        if(probe.position.x>minX*0.75f && probe.position.x<maxX*1.25f && probe.position.y>minY*0.75f && probe.position.y<maxY*1.25f) {
            return true;
        }
        return false;
    }


    //handles helper movement to move towards the corresponding probe
    public void MoveHelper(RectTransform helperBubble, RectTransform probe) {

       

        //Debug.Log(minX + "," +maxX+"----" + minY +","+maxY);

        if(helperBubble.position.x < minX) {
            helperBubble.position = new Vector3(minX, helperBubble.position.y, helperBubble.position.z);
        }
        if (helperBubble.position.x > maxX) {
            helperBubble.position = new Vector3(maxX, helperBubble.position.y, helperBubble.position.z);
        }
        if (helperBubble.position.y < minY) {
            helperBubble.position = new Vector3(helperBubble.position.x, minY, helperBubble.position.z);
        }
        if (helperBubble.position.y > maxY) {
            helperBubble.position = new Vector3(helperBubble.position.x, maxY, helperBubble.position.z);
        }

        float finalMoveSpeed = helperMoveSpeed * (Vector2.Distance(new Vector2(helperBubble.position.x, helperBubble.position.y), new Vector2(probe.position.x, probe.position.y))/200f);

        if(helperBubble.position.x<probe.position.x) {
            helperBubble.position += new Vector3(finalMoveSpeed, 0, 0);
        }
        if(helperBubble.position.x > probe.position.x) {
            helperBubble.position -= new Vector3(finalMoveSpeed, 0, 0);
        }
        if (helperBubble.position.y < probe.position.y) {
            helperBubble.position += new Vector3(0, finalMoveSpeed, 0);
        }
        if (helperBubble.position.y > probe.position.y) {
            helperBubble.position -= new Vector3(0, finalMoveSpeed, 0);
        }

    }


    public void FitWithinBounds(RectTransform rect) {
        if (rect.position.x < minX) {
            rect.position = new Vector3(minX, rect.position.y, rect.position.z);
        }
        if (rect.position.x > maxX) {
            rect.position = new Vector3(maxX, rect.position.y, rect.position.z);
        }
        if (rect.position.y < minY) {
            rect.position = new Vector3(rect.position.x, minY, rect.position.z);
        }
        if (rect.position.y > maxY) {
            rect.position = new Vector3(rect.position.x, maxY, rect.position.z);
        }
    }

}
