﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//this script is designed for use in the custom node editor
public class EditNodeValues : MonoBehaviour {

    //the new measurement script currently attached to the same game object as this script
    private NewMeasurement values;

    //the panel with the editable fields
    public GameObject panel;    //this one is most likely obsolete
    public GameObject EditPanel;

    private InputField parentInputField;

    NewMeasurement nm;


    public void Start() {

        if (values==null) {
            values = GetComponent<NewMeasurement>();
        }

        if(nm==null) {
            nm = GetComponent<NewMeasurement>();
        }

        //finds the edit panel if null from a link in the scriptholder
        if(panel==null) {
            panel = GameObject.Find("Scriptholder").GetComponent<PropertiesFromCSV>().editPanel;
        }

        //sets the non-interactable node text input to this game objects name so the user can see which node they are editing
        panel.transform.Find("Node").GetComponent<InputField>().text = gameObject.name;

        parentInputField = panel.transform.Find("Parent").GetComponent<InputField>();

        //upon clicking this node, show the panel
        GetComponent<Button>().onClick.AddListener(ShowPanel);

    }

    public void TogglePanel() {
        Debug.Log("Toggling Panel");
        panel.SetActive(!panel.activeSelf);
    }

    public void ShowPanel() {
        Debug.Log("Showing Panel");

        panel.SetActive(true);

        //sets the current node on the panel to this objects
        panel.GetComponent<CurrentNode>().currentlyEditingNode = gameObject;

        //fills in all the inputs with the current values on this node
        panel.transform.Find("Node").GetComponent<InputField>().text = gameObject.name;
        panel.transform.Find("Voltage").GetComponent<InputField>().text = nm.voltageValue.ToString();
        panel.transform.Find("AC").GetComponent<Toggle>().isOn = nm.isAC;
        panel.transform.Find("Phase").GetComponent<InputField>().text = nm.phaseNumber.ToString();
        panel.transform.Find("Neutral").GetComponent<Toggle>().isOn = nm.isNeutral;
        panel.transform.Find("SinglePhase").GetComponent<Toggle>().isOn = nm.isSinglePhase;
        panel.transform.Find("Disconnected").GetComponent<Toggle>().isOn = nm.isDisconnected;


        //handles the formatting of the branch parent node name 
        if(nm.branchParent==null) {
            if(nm.branchParentName!=null) {
                panel.transform.Find("Parent").GetComponent<InputField>().text = nm.branchParentName;
            }else {
                panel.transform.Find("Parent").GetComponent<InputField>().text = "null";
            }
        }else {
            string tempString = "";
            GameObject mo = nm.branchParent.transform.parent.parent.gameObject;
            if (mo.name.Contains("Center")) {
                tempString += "CenterBox";
            } else if(mo.name.Contains("Axis") && mo.name.Contains("1")){
                tempString += "AxisBox1";
            } else if (mo.name.Contains("Axis") && mo.name.Contains("2")) {
                tempString += "AxisBox2";
            } else if (mo.name.Contains("Axis") && mo.name.Contains("3")) {
                tempString += "AxisBox3";
            } else if (mo.name.Contains("Battery") && mo.name.Contains("1")) {
                tempString += "BatteryBox1";
            } else if (mo.name.Contains("Battery") && mo.name.Contains("2")) {
                tempString += "BatteryBox2";
            } else if (mo.name.Contains("Battery") && mo.name.Contains("3")) {
                tempString += "BatteryBox3"; 
            }

            tempString +="-"+ nm.branchParent.transform.name;
            panel.transform.Find("Parent").GetComponent<InputField>().text = tempString;
        }
        




    }

    //changes the values in the new measurement script based on the user input values
    public void UpdateValues() {
        Debug.Log("updating values");
        values.voltageValue = float.Parse(panel.transform.Find("Voltage").GetComponent<InputField>().text);
        values.isAC = panel.transform.Find("AC").GetComponent<Toggle>().isOn;
        values.phaseNumber = int.Parse(panel.transform.Find("Phase").GetComponent<InputField>().text);
        values.isNeutral = panel.transform.Find("Neutral").GetComponent<Toggle>().isOn;
        values.isSinglePhase = panel.transform.Find("SinglePhase").GetComponent<Toggle>().isOn;
        values.isDisconnected = panel.transform.Find("Disconnected").GetComponent<Toggle>().isOn;
        values.branchParentName = panel.transform.Find("Parent").GetComponent<InputField>().text;
        panel.SetActive(false);
    }


    //handles the parent picker interface for intuitively choosing a parent node (see the parentPicker and pickNode scripts for mode details)
    public void Update() {
        if(parentInputField==null) {
            return;
        }

        if(parentInputField.isFocused) {
            Debug.Log(transform.name + " parent input field is focused");
            if(Input.GetKeyDown(KeyCode.Tab)) {
                GameObject.Find("ParentPicker").GetComponent<ParentPicker>().pickParentPanel.SetActive(true);
                GameObject.Find("ParentPicker").GetComponent<ParentPicker>().currentField = parentInputField;
            }
        }
    }

    public void PickParent() {
        GameObject.Find("ParentPicker").GetComponent<ParentPicker>().pickParentPanel.SetActive(true);
        GameObject.Find("ParentPicker").GetComponent<ParentPicker>().currentField = parentInputField;
    }

}
