﻿using UnityEngine;
using System.Collections;

public class Movetoward : MonoBehaviour {
    public Transform NacelleEndPosition;
    public Transform HubEndPosition;
    public Transform BladesEndPosition;
    public Transform TowerEndPosition;
    public Transform InteriorEndPosition;
    public Transform TobBoxEndPosition;

    public float Speed;
    public GameObject MainCamera;
    private bool MoveNacelle;
    private bool MoveHub;
    private bool MoveBlades;
    private bool MoveTower;
    private bool MoveInterior;
    private bool MoveTBox;

    private Vector3 currentPosition;
    private Quaternion currentAngle;

    void Start()
    {
        //startTime = Time.deltaTime;
        MoveNacelle = false;
        MoveHub = false;
        MoveBlades = false;
        MoveTower = false;
    }
	void Update () {

        if(Input.GetKeyDown(KeyCode.I)) {
            MoveToInterior();
        }

        if(Input.GetKeyDown(KeyCode.O)) {
            MoveToTopBox();
        }


        /*if (MoveNacelle)
        {
            DisableCollider();
            float distance = Vector3.Distance(currentPosition, NacelleEndPosition.position);
            float angle = Quaternion.Angle(currentAngle, NacelleEndPosition.rotation);
            float rotateSpeed = (angle / distance) * Speed;

            float journeyLength = Vector3.Distance(MainCamera.transform.position, NacelleEndPosition.position);
            float angleCovered = Quaternion.Angle(MainCamera.transform.rotation, NacelleEndPosition.rotation);

            if (journeyLength < 0.05f && angleCovered < 0.05f)
            {
                MoveNacelle = false;
                ReenableCollider();
            }
            else
            {
                if (journeyLength > 25f)
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, NacelleEndPosition.position, 5 * Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, NacelleEndPosition.rotation, 5 * rotateSpeed * Time.deltaTime);
                }
                else
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, NacelleEndPosition.position, Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, NacelleEndPosition.rotation, rotateSpeed * Time.deltaTime);
                }
            }

            if (Input.anyKeyDown)
            {
                MoveNacelle = false;
            }


        }
        if (MoveHub)
        {
            DisableCollider();
            float distance = Vector3.Distance(currentPosition, HubEndPosition.position);
            float angle = Quaternion.Angle(currentAngle, HubEndPosition.rotation);
            float rotateSpeed = (angle / distance) * Speed;

            float journeyLength = Vector3.Distance(MainCamera.transform.position, HubEndPosition.position);
            float angleCovered = Quaternion.Angle(MainCamera.transform.rotation, HubEndPosition.rotation);

            if (journeyLength < 0.05f && angleCovered < 0.05f)
            {
                MoveHub = false;
                ReenableCollider();
            }
            else
            {
                if (journeyLength > 25f)
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, HubEndPosition.position, 5 * Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, HubEndPosition.rotation, 5 * rotateSpeed * Time.deltaTime);
                }
                else
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, HubEndPosition.position, Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, HubEndPosition.rotation, rotateSpeed * Time.deltaTime);
                }
            }

            if (Input.anyKeyDown)
            {
                MoveHub = false;
            }
        }
        if (MoveBlades)
        {
            DisableCollider();
            float distance = Vector3.Distance(currentPosition, BladesEndPosition.position);
            float angle = Quaternion.Angle(currentAngle, BladesEndPosition.rotation);
            float rotateSpeed = (angle / distance) * Speed;

            float journeyLength = Vector3.Distance(MainCamera.transform.position, BladesEndPosition.position);
            float angleCovered = Quaternion.Angle(MainCamera.transform.rotation, BladesEndPosition.rotation);

            if (journeyLength < 0.05f && angleCovered < 0.05f)
            {
                MoveBlades = false;
                ReenableCollider();
            }
            else
            {
                if (journeyLength > 25f)
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, BladesEndPosition.position, 5 * Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, BladesEndPosition.rotation, 5 * rotateSpeed * Time.deltaTime);
                }
                else
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, BladesEndPosition.position, Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, BladesEndPosition.rotation, rotateSpeed * Time.deltaTime);
                }
            }

            if (Input.anyKeyDown)
            {
                MoveBlades = false;
            }
        }
        if (MoveTower)
        {
            DisableCollider();
            float distance = Vector3.Distance(currentPosition, TowerEndPosition.position);
            float angle = Quaternion.Angle(currentAngle, TowerEndPosition.rotation);
            float rotateSpeed = (angle / distance) * Speed;

            float journeyLength = Vector3.Distance(MainCamera.transform.position, TowerEndPosition.position);
            float angleCovered = Quaternion.Angle(MainCamera.transform.rotation, TowerEndPosition.rotation);

            if (journeyLength < 0.05f && angleCovered < 0.05f)
            {
                MoveTower = false;
                ReenableCollider();
            }
            else
            {
                if (journeyLength > 25f)
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, TowerEndPosition.position, 5 * Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, TowerEndPosition.rotation, 5 * rotateSpeed * Time.deltaTime);
                }
                else
                {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, TowerEndPosition.position, Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, TowerEndPosition.rotation, rotateSpeed * Time.deltaTime);
                }
            }

            if (Input.anyKeyDown)
            {
                MoveTower = false;
            }
        }*/

        if(MoveNacelle) {
            if(MoveTower || MoveHub || MoveBlades || MoveTBox) {
                return;
            }
            MoveNacelle = MoveToPosition(NacelleEndPosition);
        }

        if(MoveTower) {
            if (MoveNacelle || MoveHub || MoveBlades || MoveTBox) {
                return;
            }
            MoveTower = MoveToPosition(TowerEndPosition);
        }

        if(MoveBlades) {
            if (MoveTower || MoveHub || MoveNacelle || MoveTBox) {
                return;
            }
            MoveBlades = MoveToPosition(BladesEndPosition);
        }

        if(MoveHub) {
            if (MoveTower || MoveBlades || MoveNacelle || MoveTBox) {
                return;
            }
            MoveHub = MoveToPosition(HubEndPosition);
        }

        if(MoveTBox) {
            if (MoveTower || MoveHub || MoveBlades || MoveNacelle) {
                return;
            }
            MoveTBox = MoveToPosition(TobBoxEndPosition);
        }


        /*if (MoveInterior) {
            DisableCollider();
            float distance = Vector3.Distance(currentPosition, InteriorEndPosition.position);
            float angle = Quaternion.Angle(currentAngle, InteriorEndPosition.rotation);
            float rotateSpeed = (angle / distance) * Speed;

            float journeyLength = Vector3.Distance(MainCamera.transform.position, InteriorEndPosition.position);
            float angleCovered = Quaternion.Angle(MainCamera.transform.rotation, InteriorEndPosition.rotation);

            if (journeyLength < 0.05f && angleCovered < 0.05f) {
                MoveInterior = false;
                ReenableCollider();
            } else {
                if (journeyLength > 25f) {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, InteriorEndPosition.position, 5 * Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, InteriorEndPosition.rotation, 5 * rotateSpeed * Time.deltaTime);
                } else {
                    MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, InteriorEndPosition.position, Speed * Time.deltaTime);
                    MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, InteriorEndPosition.rotation, rotateSpeed * Time.deltaTime);
                }
            }

        

           // if (Input.anyKeyDown) {
            //    MoveInterior = false;
            //}
        }*/

        if(MoveInterior) {
            MoveInterior = MoveToPosition(InteriorEndPosition);
        }

    }


    public bool MoveToPosition(Transform target) {

        bool b = true;

        DisableCollider();
        float distance = Vector3.Distance(currentPosition, target.position);
        float angle = Quaternion.Angle(currentAngle, target.rotation);
        float rotateSpeed = (angle / distance) * Speed;

        float journeyLength = Vector3.Distance(MainCamera.transform.position, target.position);
        float angleCovered = Quaternion.Angle(MainCamera.transform.rotation, target.rotation);

        if (journeyLength < 0.05f && angleCovered < 0.05f) {
            b = false;
            ReenableCollider();
        } else {
            if (journeyLength > 25f) {
                MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, target.position, 5 * Speed * Time.deltaTime);
                MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, target.rotation, 5 * rotateSpeed * Time.deltaTime);
            } else {
                MainCamera.transform.position = Vector3.MoveTowards(MainCamera.transform.position, target.position, Speed * Time.deltaTime);
                MainCamera.transform.rotation = Quaternion.RotateTowards(MainCamera.transform.rotation, target.rotation, rotateSpeed * Time.deltaTime);
            }
        }

        return b;
    }




    public void MovetoNacelle()
    {
        MoveNacelle = true;

        MoveTower = false;
        MoveTBox = false;
        MoveHub = false;
        MoveBlades = false;
        MoveInterior = false;

        currentPosition = MainCamera.transform.position;
        currentAngle = MainCamera.transform.rotation;
    }
    public void MovetoHub()
    {
        MoveHub = true;

        MoveTower = false;
        MoveTBox = false;
        MoveNacelle = false;
        MoveBlades = false;
        MoveInterior = false;

        currentPosition = MainCamera.transform.position;
        currentAngle = MainCamera.transform.rotation;
    }
    public void MovetoBlades()
    {

        MoveTower = false;
        MoveTBox = false;
        MoveHub = false;
        MoveNacelle = false;
        MoveInterior = false;

        MoveBlades = true;
        currentPosition = MainCamera.transform.position;
        currentAngle = MainCamera.transform.rotation;
    }
    public void MovetoTower()
    {
        MoveTower = true;

        MoveNacelle = false;
        MoveTBox = false;
        MoveHub = false;
        MoveBlades = false;
        MoveInterior = false;

        currentPosition = MainCamera.transform.position;
        currentAngle = MainCamera.transform.rotation;
    }

    public void MoveToInterior() {
        MoveInterior = true;

        MoveTower = false;
        MoveTBox = false;
        MoveHub = false;
        MoveBlades = false;
        MoveNacelle = false;

        currentPosition = MainCamera.transform.position;
        currentAngle = MainCamera.transform.rotation;
    }

    public void MoveToTopBox() {
        MoveTBox = true;

        MoveTower = false;
        MoveNacelle = false;
        MoveHub = false;
        MoveBlades = false;
        MoveInterior = false;

        currentPosition = MainCamera.transform.position;
        currentAngle = MainCamera.transform.rotation;
    }

    public void DisableCollider() {

    }

    public void ReenableCollider() {

    }
}
