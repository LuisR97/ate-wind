﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;

public class GraphicsOverrideController : MonoBehaviour {

    public Dropdown presetDrowdown;
    public GraphicsPreset[] presets;

    public Slider distanceSlider;
    public Slider densitySlider;
    public Slider cameraRangeSlider;

    public Toggle ppeToggle;
    public Toggle treeToggle;

    


    public static int grassDrawDistance;
    public static float grassDensity;
    public static bool usePostProcessingEffects;
    public static bool renderTrees;
    public static float cameraRange;

    void Start() {

        //Debug.Log("hello");

        ApplyPreset(1);

        DontDestroyOnLoad(gameObject);
        Submit();
    }

    void Update() {
        if(Input.GetKeyDown(KeyCode.Keypad5)) {
           // Debug.Log(grassDrawDistance + "," + grassDensity + "," + usePostProcessingEffects);
        }
    }

    public void TestVaribaleValues() {
       // Debug.Log("Grass Distance: " + grassDrawDistance);
       // Debug.Log("Grass Density: " + grassDensity);
    }

    public int getGrassDistance() {
        return grassDrawDistance;
    }

    public float getGrasssDensity() {
        return grassDensity;
    }

    public bool getUsePPE() {
        return usePostProcessingEffects;
    }

    public bool getRenderTrees() {
        return renderTrees;
    }

    public float getCameraRange() {
        return cameraRange;
    }

    public void Submit() {
        //if (SceneManager.GetActiveScene().buildIndex == 0) {
        //    return;
        //}
        grassDrawDistance = (int)distanceSlider.value;
        grassDensity = densitySlider.value;
        usePostProcessingEffects = ppeToggle.isOn;
        renderTrees = treeToggle.isOn;
        cameraRange = cameraRangeSlider.value;

        TestVaribaleValues();

    }


    public void ApplyPreset(int index) {
        //Debug.Log(SceneManager.GetActiveScene().buildIndex);
        //if (SceneManager.GetActiveScene().buildIndex==0) {
        //    return;
        //}else {

       // Debug.Log("Applying preset: " + presets[1].name + ", with index: " + index);

            GraphicsPreset p = presets[index];
            distanceSlider.value = p.grassDistance;
            densitySlider.value = p.grassDensity;
            ppeToggle.isOn = p.usePPE;
            treeToggle.isOn = p.RenderTrees;
            cameraRangeSlider.value = p.CameraRange;
        //}
        Submit();
    }

    public static void ApplyCustomSettings(int newGrassDistance, float newGrassDensity, bool newPPE, bool newTrees, float newCamRange) {
        //Debug.Log(SceneManager.GetActiveScene().buildIndex);
        if (SceneManager.GetActiveScene().buildIndex == 0) {
            return;
        } else {
            grassDrawDistance =newGrassDistance;
            grassDensity=newGrassDensity;
            usePostProcessingEffects=newPPE;
            renderTrees=newTrees;
            cameraRange=newCamRange;
        }
}

}
