﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForMeasurement : MonoBehaviour {

    public enum WaitMode { enableObject, nextPanel }

    public WaitMode mode = WaitMode.enableObject;

    public NewMeasurement highNode, lowNode;
    public GameObject[] objectsToEnable;
    public GameObject[] objectsToDisable;
    public TutorialController tutController;
	
	// Update is called once per frame
	void Update () {
		if(highNode.isHighProbeOverlapping() && lowNode.isLowProbeOverlapping()) {
            if(mode==WaitMode.nextPanel) {
                tutController.NextPanel();
            }else if(mode==WaitMode.enableObject) {



                //objectToEnable.SetActive(true);
                foreach(GameObject obj in objectsToEnable) {
                    obj.SetActive(true);
                }
                foreach(GameObject obj in objectsToDisable) {
                    obj.SetActive(false);
                }



            }
        }
	}
}
