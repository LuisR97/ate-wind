﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySceneTransfer : MonoBehaviour {

    public Sprite[] slots = new Sprite[12];
    public PickEquipments scripholder;


    public void Start() {

        DontDestroyOnLoad(gameObject);

        if(scripholder==null && GameObject.FindGameObjectWithTag("ScriptHolder")!=null) {
            scripholder = GameObject.FindGameObjectWithTag("ScriptHolder").GetComponent<PickEquipments>();
        }

        if(scripholder!=null) {
            OverrideCurrentInventory();
        }
    }

    void Update() {
        if (scripholder == null && GameObject.FindGameObjectWithTag("ScriptHolder") != null) {
            scripholder = GameObject.FindGameObjectWithTag("ScriptHolder").GetComponent<PickEquipments>();
            OverrideCurrentInventory();
        }

        if (Input.GetKeyDown(KeyCode.O)) {
            OverrideCurrentInventory();
        }
    }

    //this function changes the inventory in the pickequipment script based on the contents of this script
    public void OverrideCurrentInventory() {
        if (scripholder != null)
        {
            for (int i = 0; i < scripholder.slots.Length; i++)
            {
                if (scripholder != null)
                {
                    scripholder.slots[i].GetComponent<Image>().sprite = slots[i];
                }
            }
        }
    }

    //this function is called externally to update the slots in this script
    public void UpdateThisInventory(GameObject[] newSlots) {
        //slots = newSlots;
        for(int i=0; i<slots.Length; i++) {
            slots[i] = newSlots[i].GetComponent<Image>().sprite;
        }
    }

}
