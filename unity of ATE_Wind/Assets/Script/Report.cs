﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class Report : MonoBehaviour {

    public InputField fNameInput, lNameInput, problemInput, solutionInput, notepadInput;

    public login loginSys;

	public void Start() {
        string path = Application.dataPath + "/../Report";
        if(!Directory.Exists(path)) {
            Directory.CreateDirectory(path);
        }

        if(loginSys==null) {
            if(GameObject.Find("LoginSystem")!=null){
                Debug.Log("found login system");
                loginSys=GameObject.Find("LoginSystem").GetComponent<login>();
                if(GameObject.Find("LoginSystem").GetComponent<login>()==null) {
                    Debug.Log("hello");
                }else {
                    Debug.Log(GameObject.Find("LoginSystem").GetComponent<login>());
                }
            }
        }
    }



    public void ReportProgress() {
        Debug.Log("filing report");
        string sceneName = "";
        string actualProblem = "", actualSolution = "";
        switch(SceneManager.GetActiveScene().buildIndex) {
            case 5:
                sceneName = "Tutorial";
                actualProblem = "There is a disconnection between terminal block 1 and the tower light switch";
                actualSolution = "Check and replace the wire or reconnect as necessary";
                break;
            case 1:
                sceneName = "Warehouse";
                break;
            case 2:
                sceneName = "WindTurbine";
				actualProblem = "One of the Pitch Motor Brakes faild";
				actualSolution = "Replace Brake Assembly";
                break;
            case 3:
                sceneName = "TroubleShootingBox";
                actualProblem = "There is a bad connection to the light at either the neutral terminal or the neutral of the light itself.";
                actualSolution = "Replace the entire wire from the light neutral to the terminal block.";
                break;
	
	
        }

		string fileName = fNameInput.text+ "_"+lNameInput.text + "-" + sceneName + "(" + System.DateTime.Now.ToString("MM-dd-yyyy") + ")";
        ExportLocalFile(fileName, actualProblem, actualSolution, sceneName);      
    }

    public void ExportLocalFile(string fileName, string actualProblem, string actualSolution, string scene) {
        string path = "Report/" + fileName + ".txt";
        Debug.Log("exporting file: " + path);
        string outputString = "";
        outputString += "Name: "+fNameInput.text+" "+lNameInput.text+ "\r\n";
        outputString += "Time: 00:00 \r\n";
        outputString += "User Problem: " + problemInput.text + "\r\n";
        outputString += "User Solution: " + solutionInput.text + "\r\n";
        outputString += "Actual Problem: " + actualProblem + "\r\n";
        outputString += "Actual Solution: " + actualSolution+ "\r\n";
		outputString += "\n\n\n\n";
		outputString += "Notes: " + notepadInput.text + "\r\n";

        if (loginSys != null) {
            Debug.Log("saving report to database");
            loginSys.SaveRecordToDatabase(fNameInput.text, lNameInput.text, loginSys.GetLoggedInUser(),scene, problemInput.text, solutionInput.text, "00:00");
        }else {
            Debug.Log("login system not found");
        }

        StreamWriter writer = new StreamWriter(path);
        writer.Write(outputString);
        writer.Close();
    }


    public void Login() {

    }

    public void Logout() {

    }

}
