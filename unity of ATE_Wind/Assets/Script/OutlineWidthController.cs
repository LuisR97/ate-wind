﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineWidthController : MonoBehaviour {

    public GameObject cam;
    public float disappearDistance = 30f;
    public float maxDistance = 60f;



    public void Update()
    {
        float distance = Vector3.Distance(cam.transform.position, transform.position);
        float width = Mathf.InverseLerp(30, 60, distance);
        //Debug.Log(width);

        foreach (MeshRenderer mat in GetComponentsInChildren<MeshRenderer>())
        {
            mat.material.SetFloat("_Outline", 60f * width);
            Debug.Log(60f * width);
        }
    }
}
