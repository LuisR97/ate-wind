﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

    public class Timer : MonoBehaviour
    {

        public enum TimerMode { Incrementing, Decrementing }

        public TimerMode mode = TimerMode.Incrementing;

        public Text TimerText;

        public GameObject hintPanel;
        public Text hintText;
        public Toggle hintToggle;

        public bool disableHints;

        public HintTimestamp[] hints;
        public CompletionStep[] completionSteps;

        public TimerTime startTime;
        public TimerTime stopTime;
        private TimerTime currentTime;

        private bool TimerIsFinished = false;

        public void Start()
        {
            StartTimer();
        }

        public bool IsTimerFinished()
        {
            return TimerIsFinished;
        }

        public void StartTimer()
        {
            if (mode == TimerMode.Incrementing)
            {
                currentTime = startTime;
                InvokeRepeating("IncrementTimer", 0.0f, 1.0f);
            }
            else if (mode == TimerMode.Decrementing)
            {
                currentTime = startTime;
                InvokeRepeating("DecrementTimer", 0.0f, 1.0f);
            }
        }

        public void StopTimer()
        {
            CancelInvoke();
        }

        public void IncrementTimer()
        {
            currentTime.AddSeconds(1);
            UpdateString();
        }

        public void DecrementTimer()
        {
            currentTime.AddSeconds(-1);
            CheckCompletion();
            UpdateString();
        }

        public void UpdateString()
        {
            TimerText.text = currentTime.minutes.ToString("00") + ":" + currentTime.seconds.ToString("00");
            ProcessHints();
        }

        public void ProcessHints()
        {
            if (hintPanel == null)
            {
                return;
            }

            if (disableHints)
            {
                return;
            }

            if (hintToggle.isOn)
            {
                disableHints = true;
            }

            foreach (HintTimestamp hint in hints)
            {
                if (hint.timeStamp.GetSeconds() == currentTime.GetSeconds())
                {
                    if (hint.stepIndex < 0)
                    {      //only shows the hint if the completion step is not completed
                        hintPanel.SetActive(true);
                        hintText.text = hint.HintString;
                    }
                    else if (!completionSteps[hint.stepIndex].isCompleted)
                    {
                        hintPanel.SetActive(true);
                        hintText.text = hint.HintString;
                    }
                }
            }
        }

        public void CheckCompletion()
        {
            if (mode == TimerMode.Incrementing)
            {
                if (currentTime.GetSeconds() >= stopTime.GetSeconds())
                {
                    StopTimer();
                    TimerFinished();
                }
            }
            else
            {
                if (currentTime.seconds <= 0 && currentTime.minutes <= 0)
                {
                    StopTimer();
                    TimerFinished();
                }
            }
        }

        public void TimerFinished()
        {
            TimerIsFinished = true;
        }


        //see completion step struct below for more info
        public void SetCompletionStep(int index, bool value)
        {
            completionSteps[index].isCompleted = value;
        }

        public void SetCompletionStep(CheckStep step, bool value)
        {
            for (int i = 0; i < completionSteps.Length; i++)
            {
                if (completionSteps[i].step == step)
                {
                    completionSteps[i].isCompleted = value;
                }
            }
        }



    }

    [System.Serializable]
    public struct TimerTime
    {
        public int minutes;
        public int seconds;

        public void AddSeconds(int secondsToAdd)
        {

            seconds += secondsToAdd;
            if (seconds > 60)
            {
                minutes += 1;
                seconds = seconds - 60;
            }
            if (seconds < 0)
            {
                minutes -= 1;
                seconds = 59;
            }
        }

        public int GetSeconds()
        {
            return (60 * minutes) + seconds;
        }
    }

    [System.Serializable]
    public struct HintTimestamp
    {
        public TimerTime timeStamp;
        public string HintString;
        public int stepIndex; //the index of the completion step as defined by the inspector editable array (-1 = no step index, shows regardless)
    }



    //the completion step struct is used to define certain steps the player must complete in the troubleshooting scene
    //if they are not completed, a corresponding hint will be shown at the time zone as defined by the hint
    //if the step is completed, the hint will be ignored
    [System.Serializable]
    public struct CompletionStep
    {
        public CheckStep step;
        public bool isCompleted;
    }

    public enum CheckStep { checkedSwitch, openedMeasurement, checkedSchematics }