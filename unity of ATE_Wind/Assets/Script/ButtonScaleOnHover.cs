﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonScaleOnHover : MonoBehaviour {

	/*
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
	*/

	public void OnHover (GameObject buttonToScale) {

		buttonToScale.GetComponent<RectTransform> ().sizeDelta = new Vector2 (30f,30f);


	}

	public void OnHoverTextScale (Text textToScale) {

		textToScale.fontSize = 20;

	}

	public void OnExit (GameObject buttonToScale) {

		buttonToScale.GetComponent<RectTransform> ().sizeDelta = new Vector2 (14f,14f);;

	}

	public void OnExitTextScale (Text textToScale) {

		textToScale.fontSize = 12;

	}

	public void OnExitLargeButton (GameObject buttonToScale) {

		buttonToScale.GetComponent<RectTransform> ().sizeDelta = new Vector2 (20f,20f);;

	}

	public void OnExitTextScaleLargeButton (Text textToScale) {

		textToScale.fontSize = 14;

	}

	public void OnExitTerminalBlock (GameObject buttonToScale) {

		buttonToScale.GetComponent<RectTransform> ().sizeDelta = new Vector2 (12f,12f);;

	}

	public void OnExitTextScaleTerminal (Text textToScale) {

		textToScale.fontSize = 10;

	}
}
