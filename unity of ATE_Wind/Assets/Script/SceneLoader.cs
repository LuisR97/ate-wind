﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadSchematicsModule()
	{
		SceneManager.LoadScene("SchematicsModule", LoadSceneMode.Single); 
	}

	public void LoadBasicElectricModule()
	{
		SceneManager.LoadScene("BasicElectric", LoadSceneMode.Single); 
	}

	public void LoadBasicElectricSafetyModule()
	{
		SceneManager.LoadScene("BasicElectricSafety", LoadSceneMode.Single); 
	}
	public void LoadBasicElectricalMeasurementsModule()
	{
		SceneManager.LoadScene("ElectricMeasurements", LoadSceneMode.Single); 
	}

	public void LoadErrorCodeModule()
	{
		SceneManager.LoadScene("ElectricalTroubleshooting", LoadSceneMode.Single);
	}

	public void LoadHydraulicSafety()
	{
		SceneManager.LoadScene("HydraulicSafety",LoadSceneMode.Single);
	}

	public void LoadMechanicalSafety()
	{
		SceneManager.LoadScene("MechanicalSafety",LoadSceneMode.Single);
	}
}
