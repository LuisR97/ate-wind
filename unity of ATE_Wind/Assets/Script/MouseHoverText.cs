﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseHoverText : MonoBehaviour {

    public Text text;
    public string message;
    public float distance;
    public GameObject cam;

    public GameObject[] overlappingMenus;

    public void Update() {
        if(text.gameObject.activeSelf) {
            if (overlappingMenus != null) {
                foreach (GameObject menu in overlappingMenus) {
                    if(menu.activeSelf) {
                        text.gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    void OnMouseOver() {
        if(overlappingMenus!=null) {
            foreach(GameObject menu in overlappingMenus) {
                if(menu.activeSelf) {
                    return;
                }
            }
        }

        if(Vector3.Distance(transform.position, cam.transform.position) < distance){
        text.gameObject.SetActive(true);
        text.text = message;
        }
    }

    void OnMouseExit() {
        text.gameObject.SetActive(false);
    }

}
