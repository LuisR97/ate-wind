﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This declares the possible subtopics for changing values with on clicks
/// </summary>
public enum ThisCodeSubtopic
{
    //Note that this will need to change based on the values that will populate in the Subtopic Names Category

    DrivingSafety,
    HazardAnalysis,
    PPE,
    ArcFlash

}

public class ChecklistValuesUpdate : MonoBehaviour {

    [Header("The Player Preferences Values Object to interact with")]
    [Tooltip("The PlayerPreferences_XXX Object in the hierarchy")]
    public PlayerPreferenceExample playerPrefsObject;

    [Header("The Title of the Overall Subject")]
    [Tooltip("This is the name it uses to look for subtopic values")]
    public string topicName = "SafetyTopics";

    [Header("Values associated with this code")]
    [Tooltip("The toggles associated with this script")]
    public List<Toggle> checklistToggles;

    [Tooltip("Auto Updating values of the toggles in checklistToggles[]")]
    public List<bool> checklistToggleValues;

    [Tooltip("Subtopics associated with each toggle")]
    public List<string> subtopicNames;

    [Tooltip("Values pulled from the player preferences object")]
    public List<bool> valuesFromPlayerPrefs;

 


	// Use this for initialization
	void Start () {

        InitializeChecklistToggles();
        InitializeToggleValues();
        InitializePlayerPreferenceValues();

    }
	
	// Update is called once per frame
	void Update () {

        UpdatePlayerPreferenceValues();
        UpdateToggleValues();

	}

    /// <summary>
    /// Initializes the toggles that are children of the gameobject this code is attached to into
    /// the checklistToggles[] list
    /// </summary>
    void InitializeChecklistToggles ()
    {

        checklistToggles.Clear();

        for (int i=0; i < this.gameObject.transform.childCount; i++)
        {

            checklistToggles.Add(this.gameObject.transform.GetChild(i).GetComponent<Toggle>());

        }

    }

    /// <summary>
    /// Initializes the initial toggle boolean values into a list,
    /// but does not update them until the Update() function runs
    /// </summary>
    void InitializeToggleValues()
    {

        foreach (Toggle booleanValue in checklistToggles)
        {

            checklistToggleValues.Add(booleanValue.isOn);

        }

    }

    /// <summary>
    /// Grabs subtopic names and values from the PlayerPreferences Script attached to playerPrefsObject at startup
    /// </summary>
    void InitializePlayerPreferenceValues()
    {

        subtopicNames.Clear();
        valuesFromPlayerPrefs.Clear();

        foreach (string subtopic in playerPrefsObject.subtopics)
        {

            subtopicNames.Add(subtopic);

        }

        foreach (bool value in playerPrefsObject.boolValues)
        {

            valuesFromPlayerPrefs.Add(value);

        }

    }

    /// <summary>
    /// Grabs subtopic names and values from the PlayerPreferences Script attached to playerPrefsObject every frame
    /// </summary>
    void UpdatePlayerPreferenceValues()
    {

        subtopicNames.Clear();
        valuesFromPlayerPrefs.Clear();

        foreach (string subtopic in playerPrefsObject.subtopics)
        {

            subtopicNames.Add(subtopic);

        }

        foreach (bool value in playerPrefsObject.boolValues)
        {

            valuesFromPlayerPrefs.Add(value);

        }

    }
    
    /// <summary>
    /// Update the values of the toggles based on the playerPrefsObject every frame
    /// </summary>
    void UpdateToggleValues ()
    {

        for (int i=0; i<playerPrefsObject.subtopics.Count; i++)
        {

            checklistToggles[i].isOn = valuesFromPlayerPrefs[i];
            checklistToggleValues[i] = checklistToggles[i].isOn;

        }

        //foreach (bool value in valuesFromPlayerPrefs)
        //{

        //    int i = valuesFromPlayerPrefs.IndexOf(value);

        //    checklistToggles[i].isOn = value;

        //}

    }

    /// <summary>
    /// Changes the boolean value of the topic associated with the Enum subtopic to "ON", and
    /// is meant to be accessed from a button, but is usable otherwise
    /// </summary>
    /// <param name="subtopic">This is the selection that is particular to this code that
    /// denotes the topic to access</param>
    public void ChangeCompletionValueTo_ON_OnClick(ThisCodeSubtopic subtopic)
    {

        switch (subtopic)
        {

            case ThisCodeSubtopic.DrivingSafety:
                playerPrefsObject.SubtopicValueUpdate("Driving Safety", "1");
                break;
            case ThisCodeSubtopic.HazardAnalysis:
                playerPrefsObject.SubtopicValueUpdate("Hazard Analysis", "1");
                break;
            case ThisCodeSubtopic.PPE:
                playerPrefsObject.SubtopicValueUpdate("Personal Protection Equipment", "1");
                break;
            case ThisCodeSubtopic.ArcFlash:
                playerPrefsObject.SubtopicValueUpdate("ArcFlash", "1");
                break;
            default:
                break;

        }

    }

    /// <summary>
    /// Changes the boolean value of the topic associated with the Enum subtopic to "ON", and
    /// is meant to be accessed from a button, but is usable otherwise
    /// </summary>
    /// <param name="subtopic">This is the selection that is particular to this code that
    /// denotes the topic to access</param>
    public void ChangeCompletionValueTo_OFF_OnClick(ThisCodeSubtopic subtopic)
    {

        switch (subtopic)
        {

            case ThisCodeSubtopic.DrivingSafety:
                playerPrefsObject.SubtopicValueUpdate("Driving Safety", "0");
                break;
            case ThisCodeSubtopic.HazardAnalysis:
                playerPrefsObject.SubtopicValueUpdate("Hazard Analysis", "0");
                break;
            case ThisCodeSubtopic.PPE:
                playerPrefsObject.SubtopicValueUpdate("Personal Protection Equipment", "0");
                break;
            case ThisCodeSubtopic.ArcFlash:
                playerPrefsObject.SubtopicValueUpdate("ArcFlash", "0");
                break;
            default:
                break;

        }

    }

}
