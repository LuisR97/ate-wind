﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainSceneTutorial : MonoBehaviour {

	public int NOrder;
	public bool TutorialMode;

	public bool B_step16;
	public bool B_step17;
	public bool B_step18;
	public bool B_step19;
	public bool B_step20;
	public bool B_step21;
	public bool B_step22;
	public bool B_step23;
	public bool B_step24;
	public bool B_step25;
	public bool B_step26;
	public bool B_step27;
	public bool B_step27a;
	public bool B_step27b;
	public bool B_step27c;
	public bool B_step28;
	public bool B_step29;
	public bool B_step30;
	public bool B_step31;
	public bool B_step32;
	public bool B_step33;
	public bool B_step34;
	public bool B_step35;


	public bool w;
	public bool a;
	public bool s;
	public bool d;
	public bool e;
	public bool c;

	public GameObject TutorialPanel16;
	public GameObject TutorialPanel17;
	public GameObject TutorialPanel18;
	public GameObject TutorialPanel19;
	public GameObject TutorialPanel20;
	public GameObject TutorialPanel21;
	public GameObject TutorialPanel22;
	public GameObject TutorialPanel23;
	public GameObject TutorialPanel24;
	public GameObject TutorialPanel25;
	public GameObject TutorialPanel26;
	public GameObject TutorialPanel27;
	public GameObject TutorialPanel27a;
	public GameObject TutorialPanel27b;
	public GameObject TutorialPanel27c;
	public GameObject TutorialPanel28;
	public GameObject TutorialPanel29;
	public GameObject TutorialPanel30;
	public GameObject TutorialPanel31;
	public GameObject TutorialPanel32;
	public GameObject TutorialPanel33;
	public GameObject TutorialPanel34;
	public GameObject TutorialPanel35;

	public GameObject TopBoxPanel;
	public GameObject Multimeter;


	public Slider mainSlider;

	private Ray ray;
	private RaycastHit hit;

	// Use this for initialization
	void Start () {
		TutorialPanel16.SetActive (true);
		TutorialMode = true;
	}
	
	// Update is called once per frame
	void Update () {
		ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Tutorial();
	}

	public void Tutorial()
	{

		if (Input.GetMouseButtonDown (0)) {

			Debug.Log ("click");
			if (Physics.Raycast (ray, out hit,8)) {	
				
				Debug.Log ("clicked on thing");

				if (hit.collider.name == "Nacelle shelled") {		
					B_step22 = true;
				}

				//if ( hit.collider.tag == "TopBox" ||hit.collider.gameObject.name == "RightDoorTB" || hit.collider.gameObject.name == "LeftDoorTB" ) {		
				if (GameObject.Find("LeftDoorTB").GetComponent<ChangeDoorPosition>().Doorisopen) {
					B_step24 = true;
					Debug.Log ("clicked on topbox");
				}


			}
		}



		B_step16 = true;

		if (B_step16) {
			NOrder = 16;
			TutorialPanel16.SetActive (true);

			if (Input.GetMouseButtonUp (1)) {
				B_step17 = true;			
			}
		}

		if (B_step17) {
			NOrder = 17;
			TutorialPanel17.SetActive (true);
			TutorialPanel16.SetActive (false);

			if (Input.GetKeyUp (KeyCode.W)) {
				B_step18 = true;
			}
		}

		if (B_step18) {
			NOrder = 18;
			TutorialPanel18.SetActive (true);
			TutorialPanel17.SetActive (false);

			if (Input.GetKey (KeyCode.W)&& Input.GetMouseButton(1)) {
				B_step19 = true;

			}
		}

		if (B_step19) {
			NOrder = 19;
			TutorialPanel19.SetActive (true);
			TutorialPanel18.SetActive (false);

			if (Input.GetKeyUp(KeyCode.W)){
				w = true;
			}
			if (Input.GetKeyUp(KeyCode.A)){
				a = true;
			}
			if (Input.GetKeyUp(KeyCode.S)){
				s = true;
			}
			if (Input.GetKeyUp(KeyCode.D)){
				d = true;
			}
			if (Input.GetKeyUp(KeyCode.E)){
				e = true;
			}
			if (Input.GetKeyUp(KeyCode.C)){
				c = true;
			}
			if (w && a && s && d && e && c) {
				B_step20 = true;
			}
		}

		if (B_step20) {
			NOrder = 20;
			TutorialPanel20.SetActive (true);
			TutorialPanel19.SetActive (false);
			mainSlider.onValueChanged.AddListener(delegate {B_step21 = true;});


		}

		if (B_step21) {
			NOrder = 21;
			TutorialPanel21.SetActive (true);
			TutorialPanel20.SetActive (false);
			if (Input.GetKeyUp(KeyCode.W)){
				B_step22 = true;
			}
		}

		if (B_step22) {
			NOrder = 22;
			TutorialPanel22.SetActive (true);
			TutorialPanel21.SetActive (false);

			if (Input.GetKeyUp(KeyCode.W)){
				B_step23 = true;
			}
		}

		if (B_step23) {
			NOrder = 23;
			TutorialPanel23.SetActive (true);
			TutorialPanel22.SetActive (false);

		}

		if (B_step24) {
			NOrder = 24;
			TutorialPanel24.SetActive (true);
			TutorialPanel23.SetActive (false);

			if (TopBoxPanel.activeSelf == true) {
				B_step25 = true;
			}
		}

		if (B_step25) {
			NOrder = 25;
			TutorialPanel25.SetActive (true);
			TutorialPanel24.SetActive (false);
		}

		if (B_step26) {
			NOrder = 26;
			TutorialPanel26.SetActive (true);
			TutorialPanel25.SetActive (false);
		}

		if (B_step27) {
			NOrder = 27;
			TutorialPanel27.SetActive (true);
			TutorialPanel26.SetActive (false);
		}

		if (B_step28) {
			NOrder = 28;
			TutorialPanel28.SetActive (true);
			TutorialPanel27.SetActive (false);
		}

		if (B_step29) {
			NOrder = 29;
			TutorialPanel29.SetActive (true);
			TutorialPanel28.SetActive (false);
		}

		if (B_step30) {
			NOrder = 30;
			TutorialPanel30.SetActive (true);
			TutorialPanel29.SetActive (false);
		}

		if (B_step31) {
			NOrder = 31;
			TutorialPanel31.SetActive (true);
			TutorialPanel30.SetActive (false);

			if (Multimeter.activeSelf == true) {
				B_step32 = true;
			}
		}

		if (B_step32) {
			NOrder = 32;
			TutorialPanel32.SetActive (true);
			TutorialPanel31.SetActive (false);
		}

		if (B_step33) {
			NOrder = 33;
			TutorialPanel33.SetActive (true);
			TutorialPanel32.SetActive (false);
		}

		if (B_step34) {
			NOrder = 34;
			TutorialPanel34.SetActive (true);
			TutorialPanel33.SetActive (false);
		}

		if (B_step35) {
			NOrder = 35;
			TutorialPanel35.SetActive (true);
			TutorialPanel34.SetActive (false);
		}








	}


	public void step26()
	{
		B_step26 = true;
	}

	public void step27()
	{
		B_step27 = true;
	}

	public void step28()
	{
		B_step28 = true;
	}

	public void step29()
	{
		B_step29 = true;
	}

	public void step30()
	{
		B_step30 = true;
	}

	public void step31()
	{
		B_step31 = true;
	}

	public void step32()
	{
		B_step32 = true;
	}

	public void step33()
	{
		B_step33 = true;
	}

	public void step34()
	{
		B_step34 = true;
	}

	public void step35()
	{
		B_step35 = true;
	}
}
