﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Navigate : MonoBehaviour {

    ReferenceMenuScript referenceMenuScript;


    // Use this for initialization
    void Start () {
        referenceMenuScript = this.gameObject.GetComponent<ReferenceMenuScript>();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButtonDown("SlideAdvanceForward") && !referenceMenuScript.IsCurrentPageLast())
        {
            //Debug.Log("Right");
            referenceMenuScript.NextButtonOnClick();
        }
        else if (Input.GetButtonDown("SlideAdvanceBackward") && (referenceMenuScript.GetCurrentPageNumber() != 1))
        {
            //Debug.Log("Left");
            referenceMenuScript.BackButtonOnClick();
        }



	}
}
