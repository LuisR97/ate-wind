﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class UICursor : MonoBehaviour {

    //the speed for moving the mouse with the joystick
    public int speed;

    //the rect transform of the parent cursor object (not the child image)
    public RectTransform rect;

    //the image in the child object to hide and show
    public Image img;

    //variables for whether or not the cursor is hidden and whether or not to use the joystick for mouse movement
    public bool cursorHidden = false;
    public bool useVirtualCursor = true;

    //the first person flying character controller which will need to be disabled to fix the camera in place when using the mouse
    public FirstPersonControllerFly fps;

    //hides the cursor and links the rect transform if not done already in the inspector
    public void Start() {
        rect = GetComponent<RectTransform>();
        Cursor.visible = false;

        ProcessCursorChange();
    }
    
    //function used for handling input detection
    void Update () {

        //offset used to compensate for differing coordinate systems (0,0 in bottom corner vs 0,0 in center of screen)
        Vector3 offset = new Vector3(Screen.width / 2, Screen.height / 2, 0);

        //if not using joystick for cursor, position the cursor at the mouse position
        if (!useVirtualCursor) {
            rect.localPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y,0)-offset;
        }

        //can use keyboard arrow keys for mouse movement
	    if(Input.GetKey(KeyCode.UpArrow)) {
            rect.localPosition += Vector3.up * speed;
        }
        if (Input.GetKey(KeyCode.DownArrow)) {
            rect.localPosition += Vector3.down * speed;
        }
        if (Input.GetKey(KeyCode.LeftArrow)) {
            rect.localPosition += Vector3.left * speed;
        }
        if (Input.GetKey(KeyCode.RightArrow)) {
            rect.localPosition += Vector3.right*speed;
        }

        

        //applies cursor movement based on the right joystick (either axis works)
        if (Input.GetAxis("Mouse X Stick") >0.01 || Input.GetAxis("Mouse X Stick") <-0.01) {
            rect.localPosition += new Vector3(Input.GetAxis("Mouse X Stick"),0,0) * speed;
        }
        if(Input.GetAxis("Mouse Y Stick") > 0.01 || Input.GetAxis("Mouse Y Stick") < -0.01) {
            rect.localPosition -= new Vector3(0, Input.GetAxis("Mouse Y Stick"), 0) * speed;
        }

        if (Input.GetAxis("Horizontal") > 0.01 || Input.GetAxis("Horizontal") < -0.01) {
            rect.localPosition += new Vector3(Input.GetAxis("Horizontal"), 0, 0) * speed;
        }
        if (Input.GetAxis("Vertical") > 0.01 || Input.GetAxis("Vertical") < -0.01) {
            rect.localPosition += new Vector3(0, Input.GetAxis("Vertical"), 0) * speed;
        }


        //handles button inputs for toggling mouse visibility and joystick control
        if(Input.GetButtonDown("ToggleMouseVisibility")){
            Debug.Log("toggle mouse visibility");
            ToggleCursorVisibility();
        }
        if(Input.GetButtonDown("ToggleJoystickMouse")){
            Debug.Log("toggle joystick");
            ToggleUseVirtualCursor();
        }

        //hides the OS cursor every frame
        Cursor.visible = false;
    }

    //function used by external script (VirtualPointerInputModule and VirtualUIInputModule) to get the current position of the cursor
    public Vector3 GetLocalPosition(){
        Vector3 pos = new Vector3();
        Vector3 offset = new Vector3(Screen.width / 2, Screen.height / 2, 0);
        pos = rect.localPosition + offset;
        return pos;
    }

    //function for toggling the cursor's visibility, then updating
    public void ToggleCursorVisibility() {
        cursorHidden = !cursorHidden;
        ProcessCursorChange();
    }

    //function for toggling the joystick control, then updating
    public void ToggleUseVirtualCursor() {
        useVirtualCursor = !useVirtualCursor;
        ProcessCursorChange();
    }

    //function used for updating the current settings
    public void ProcessCursorChange() {
        if(cursorHidden) {

            Debug.Log("cursor locked");

            Cursor.lockState = CursorLockMode.Locked;
            img.enabled = false;

            if(fps!=null) {
                fps.enabled = true;
            }
        }else {

            Debug.Log("cursor unlocked");

            Cursor.lockState = CursorLockMode.None;
            if(fps!=null) {
                fps.enabled = false;
            }
            img.enabled = true;
        }
    }


}//end of class

