﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LadderEntrance : MonoBehaviour {

	//public Transform destination;
	Collider player;
	float yieldTime;
	bool timeB;
	public float transTime;
	public bool show_dialog = false;
	public bool show_dialog_NoRequ = false;
	public bool show_dialogAgain = false;
	public bool show_dialog_disclamer = false;
	public bool show_dialog_start = false;
	public bool show_dialog_moreInfo = false; 
	public bool show_attach_rope = false;
    public bool show_begin_scene = true;
    public bool show_InsideNacelle = false;
    public bool TopNacelle = false;
    private GUIManager gm;
	private GUIStyle style;

	public Texture2D iconDiscalmer;
	public Texture2D iconEquipment;
	public Texture2D iconStart;
	public Texture2D iconRequirement;


	public GameObject startHere;
	public bool numberOfTimes = false;

	public BoxCollider ladderEntrance;
    public GameObject sliderCollider;


    public PickEquipments equipments;
	public bool hasEquipment;

    public Ladder_2 ladder;

    // Top Nacelle variables
    public LineRenderer safetyRopeRightLineRender;
    public LineRenderer safetyRopeRightLineRender2;
    public LineRenderer safetyRopeRightLineRender3;
    public GameObject safetyRopeRight;
    public LineRenderer safetyRopeLeftLineRender;
    public LineRenderer safetyRopeLeftLineRender2;
    public LineRenderer safetyRopeLeftLineRender3;
    public GameObject safetyRopeLeft;
    public GameObject safetyRopeRight1;
    public GameObject safetyRopeLeft1;
    public GameObject safetyRopeRight2;
    public GameObject safetyRopeLeft2;
    public GameObject carabinerLeft;
    public GameObject carabinerLeft2;
    public GameObject carabinerLeft3;
    public GameObject carabinerRight;
    public GameObject carabinerRight2;
    public GameObject carabinerRight3;

    public GameObject Handrail_Left_1;
    public GameObject Handrail_Left_2;
    public GameObject Handrail_Left_3;
    public GameObject Handrail_Right_1;
    public GameObject Handrail_Right_2;
    public GameObject Handrail_Right_3;

    public bool isLeftRopeAttached;
    public bool leftRopeDone;
    public bool rightRopeDone;
    public bool isRightRopeAttached;
    public bool show_FallDown_FromNacelle;
    public bool nextPanelTest;
    public bool show_FallingDownFromNacelle;
    public bool show_GettingToFloor;
    //public int countTies = 0;
    public bool firtsAttemp;
    public bool activatePanel;

    bool nexxtPanle = false;

    // Use this for initialization
    void Start()
	{
		transTime = GameObject.Find("GUIManager").GetComponent<PlayerStatusTrack>().truckTransTime;
		gm = GameObject.Find("GUIManager").GetComponent<GUIManager>();
		style = GameObject.Find("GUIManager").GetComponent<Inventory>().InventoryStyle;
		//hasEquipment = equipments.inInventoryL2;
	}

	void OnTriggerEnter(Collider other)
	{
		hasEquipment = equipments.isHarnessInInventory;

        if (other.name == "Ladder_Entrance")
        {
			player = other;

			if(hasEquipment)
            {
				show_dialog = true;
			}
			else
            {
				show_dialog_NoRequ = true;
			}
		}

        if(other.name == "Slider_Collider")
        {
            show_attach_rope = true;
        }

        if (other.name == "CeilingNacelle_ColliderNew2" || other.name == "CeilingNacelle_ColliderNew"
            || other.name == "CeilingNacelle_ColliderNew3" || other.name == "CeilingNacelle_ColliderNew4")
        {
            //gm.showOther = false;
            show_FallingDownFromNacelle = true;
            Debug.Log("ENTROOOOOOP++++");
        }

        if (other.name == "CeilingNacelle_ColliderNew2Floor" || other.name == "CeilingNacelle_ColliderNewFloor"
            || other.name == "CeilingNacelle_ColliderNew3Floor" || other.name == "CeilingNacelle_ColliderNew4Floor")
        {
            //gm.showOther = false;
            show_GettingToFloor = true;
            Debug.Log("ENTROOOOOOP++++");
        }


    }

    void OnTriggerExit(Collider other)
    {
        //if (other.name == "CeilingNacelle_Collider" || other.name == "CeilingNacelle_Right_Collider" || other.name == "CeilingNacelle_Left_Collider")
        if (other.name == "CeilingNacelle_Collider")
        {
            show_FallDown_FromNacelle = true;
        }
    }

    void OnGUI()
	{
		if (show_dialog)
		{
			gm.showOther = true;

            //			//(Rect(x,y,width,height))
            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 100, 275, 150), "\t     AWESOME! \n\nYou have the required personal protection equipment (PPE) to climb." +
                "\n\n- Harness", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 55, 275, 40), "\t       Continue", style))
			{
				show_dialog_disclamer = true;
				//Debug.Log("in IF statement ");
				gm.showOther = false;
				show_dialog = false;
			}
		}

		if(show_dialog_NoRequ)
        {
			GUI.Button(new Rect(Screen.width / 2 - 75 + 25, Screen.height / 2 - 75, 100, 150), iconRequirement, style);
			GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160, 300, 150), "ATTENTION!			You do not have the required personal protection equipment to climb", style);
			GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160, 300, 105), "Go back to the truck to get the required personal protection equipment", style);



			if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25, 150, 40), "             Yes", style))
			{
				Debug.Log("close the panels ");
				gm.showOther = false;
				show_dialog_NoRequ = false;
			}

			if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25, 150, 40), "             No", style))
			{
				show_dialog_disclamer = true;
				show_dialog = false;
				gm.showOther = false;
				show_dialog_NoRequ = false;
			}
		}

		if (show_dialog_disclamer)
		{
			gm.showOther = true;

			GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 245, 100, 110), iconDiscalmer, style);
			GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 130, 400, 300), "\t\t     DISCLAMER! \n\nThe purpose of the Personal Protective Equipment Policies " +
				"is to protect the employees from exposure to work place hazards and the risk " +
				"of injury through the use of personal protective equipment (PPE). " +
                "\n\nFor more information regarding PPE procedures refer to the PPE educational module: \n" +
                "\n\nGo to MAIN MENU \n--> START \n--> SAFETY TRAINING \n--> SAFETY REFERENCES", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 175 , 400, 40), "\t\t     Continue", style))
			{
				show_dialog_start = true;
				gm.showOther = false;
				show_dialog = false;
				show_dialog_NoRequ = false;
				show_dialog_disclamer = false;
				//show_dialog_moreInfo = false;
				Debug.Log("Show more inform ");
				
			}
		}
		
		if (show_dialog_start)
		{
			gm.showOther = true;

			GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 100, 300, 150), "\t        START! " +
                "\n\nFirst activity: \nClimb up using a slide on the ladder with tie off. " +
                "\n\nMove to the back of the ladder to start your climbing experience.", style);
			GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 55, 300, 50),"Click \"Start\" to begin climbing", style);

			if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 110, 300, 40), "\t\tStart", style))
			{
				Debug.Log("in IF statement ");
				gm.showOther = false;
				show_dialog_start = false;
				ladderEntrance.enabled = false;
				startHere.SetActive(true);

			}
		}

        
        if (show_attach_rope)
		{
			gm.showOther = true;

			//GUI.Button(new Rect(Screen.width / 2 - 75 + 25, Screen.height / 2 - 75, 100, 150), iconStart, style);
			GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 100, 300, 150), "For safety reason, tieing off your harness to the carbiner with a rope is important ", style);
			GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 55, 300, 50), "Click \"ATTACH\" to tie off your harness with the rope", style);


            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 110, 300, 40), "                           Attach", style))
            {
                show_attach_rope = false;
                gm.showOther = false;
                sliderCollider.SetActive(false);
                //ladder.rope.SetActive(true);   // Delete prototype rope -------------------------------------
                ladder.ropeLineRender.useWorldSpace = true;//*********************************************************************
                ladder.ropePlayerLeft.SetActive(false);
                //show_dialog_moreInfo = false;
                Debug.Log("ATTACH TO CARABINER ");

            }
        }

        if (show_begin_scene)
        {
            gm.showOther = true;

            //GUI.Button(new Rect(Screen.width / 2 - 75 + 25, Screen.height / 2 - 75, 100, 150), iconStart, style);
            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 50, 350, 250), "Welcome to the Competent Climber Check \n\nYou have 3 activities to complete in this scene. \n " +
                "\n1. Climb up using a slide on the ladder with tie off. \n" +
                "\n2. Tie off at the top of Nacelle. \n" +
                "\n3. While on top, maintain a 100% tie off. \n", style);
            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160, 350, 105), "Click \"Begin\" to start this simulator", style);


            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25, 350, 40), "                                     Begin", style))
            {
                show_begin_scene = false;
                gm.showOther = false;
            }
        }

        if(ladder.atTopNacelle)
        {
            gameObject.GetComponent<FirstPersonController>().movementSpeed = 0.35f;

            ladder.onGround = false;
        
            if (show_InsideNacelle)
            {
                gm.showOther = true;
                //gameObject.GetComponent<FirstPersonController>().movementSpeed = 0.35f;
                GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 50, 350, 250), "Welcome to the Nacelle. You have completed the climbing up activity." +
                    "\n\nNext, there are 2 activities you can perform here." +
                    "\n\n1. You can walk around and interact with the parts. " +
                    "\n\n2. Look at the hatch to move up to the top of the Nacelle and perform the tie off activity. " +
                    "\n\nPress \"P\" to show this panel again.", style);

                if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 130, 350, 40), "     \t\tContinue", style))
                {
                    gm.showOther = false;
                    Debug.Log("in IF statement ");
                    gameObject.transform.position = GameObject.Find("NacellePlayerPosInside").transform.position;
                    gameObject.transform.rotation = GameObject.Find("NacellePlayerPosInside").transform.rotation;                
                    show_InsideNacelle = false;
                    ladder.inNacelle = true;
                    ladder.climbB = false;
                    ladder.downB = false;
                    ladder.slider.SetActive(false);
                    //ladder.rope.SetActive(false); // Delete prototype rope -------------------------------------
                    ladder.ropeLineRender.useWorldSpace = false;
                }
            }
        }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Correct 
        if(equipments.isCarabinerRightSelected)  // isSafetyRopeRightSelected is been replaced by isCarabinerRightSelected
        {
            gm.showOther = true;

            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to untie off you right safety rope?", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
            {
                equipments.isCarabinerRightSelected = false;
                carabinerRight.SetActive(false);
                gm.showOther = false;
                //safetyRopeRight.SetActive(false);
                safetyRopeRightLineRender.useWorldSpace = false;
                safetyRopeRight1.SetActive(false);
                safetyRopeRight2.SetActive(false);
                ladder.ropePlayerRight.SetActive(true);
                isRightRopeAttached = false;
                rightRopeDone = false;
                activatePanel = true;
                Handrail_Right_1.SetActive(true);


            /////Add this code to 
            //if ((leftRopeDone || rightRopeDone) && equipments.countTies == 3 && activatePanel == true)
                if ((leftRopeDone || rightRopeDone) && equipments.isPanelReadyToOpen == true && activatePanel == true)
                {
                    gm.showOther = false;

                    GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200 + 100 - 35, 300, 75 + 35), "Excellent! You have completed a 100% tie off, Activity 3. \n\nMake sure you mantain a 100% tie off while walking and performing work at the top of the nacelle", style);

                    if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155 + 100, 300, 40), "                              OK", style))
                    {
                        leftRopeDone = false;
                        rightRopeDone = false;
                        nextPanelTest = true;
                        gm.showOther = false;
                        equipments.countTies = 0;
                        activatePanel = false;
                        equipments.isPanelReadyToOpen = false;
                    }
                }
            }

            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
            {
                equipments.isCarabinerRightSelected = false;
                isRightRopeAttached = true;
                rightRopeDone = true;
                gm.showOther = false;
            }
        }

        if (equipments.isHandrailRight_1Selected)  //CORRECT
        {
            gm.showOther = true;

            if (ladder.ropePlayerRight.activeInHierarchy == false)
            {
                Debug.Log("////////////////si entrooooo en la necesidad/////////");
                GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 - 160 + 200, 300, 75), "First, you need to untie off from the previous anchor", style);

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 300, 40), "             \t\tOK", style))
                {
                    Debug.Log("////////////////no entrooooo/////////");
                    equipments.isHandrailRight_1Selected = false;
                    isRightRopeAttached = false;
                    rightRopeDone = false;
                    gm.showOther = false;
                }

            }
            else
            {
                GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to tie off you right safety rope?", style);

                if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
                {
                    equipments.isHandrailRight_1Selected = false;
                    gm.showOther = false;
                    carabinerRight.SetActive(true);
                    safetyRopeRightLineRender.useWorldSpace = true;
                    isRightRopeAttached = true;
                    rightRopeDone = true;
                    ladder.ropePlayerRight.SetActive(false);
                    Debug.Log("////////////////si entrooooo/////////");
                    Handrail_Right_1.SetActive(false);

                }

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
                {
                    Debug.Log("////////////////no entrooooo/////////");
                    equipments.isHandrailRight_1Selected = false;
                    isRightRopeAttached = false;
                    rightRopeDone = false;
                    gm.showOther = false;
                }
            }
        }

        //--------------------------------------------------------------------------------------------------------------------------------------------------
        //Correct 
        if (equipments.isCarabinerRight2Selected)  // isSafetyRopeRightSelected is been replaced by isCarabinerRightSelected
        {
            gm.showOther = true;

            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to untie off you right safety rope?", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
            {
                equipments.isCarabinerRight2Selected = false;
                carabinerRight2.SetActive(false);
                gm.showOther = false;
                safetyRopeRightLineRender2.useWorldSpace = false;
                ladder.ropePlayerRight.SetActive(true);
                isRightRopeAttached = false;
                rightRopeDone = false;
                activatePanel = true;
                Handrail_Right_2.SetActive(true);

            }
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
            {
                equipments.isCarabinerRight2Selected = false;
                isRightRopeAttached = true;
                rightRopeDone = true;
                gm.showOther = false;
            }
        }


        if (equipments.isHandrailRight_2Selected)  //CORRECT
        {
            gm.showOther = true;

            if (ladder.ropePlayerRight.activeInHierarchy == false)
            {
                Debug.Log("////////////////si entrooooo en la necesidad/////////");
                GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 - 160 + 200, 300, 75), "First, you need to untie off from the previous anchor", style);

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 300, 40), "             \t\tOK", style))
                {
                    Debug.Log("////////////////no entrooooo/////////");
                    equipments.isHandrailRight_2Selected = false;
                    isRightRopeAttached = false;
                    rightRopeDone = false;
                    gm.showOther = false;
                }

            }
            else
            {
                GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to tie off you right safety rope?", style);

                if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
                {
                    equipments.isHandrailRight_2Selected = false;
                    gm.showOther = false;
                    carabinerRight2.SetActive(true);
                    safetyRopeRightLineRender2.useWorldSpace = true;
                    isRightRopeAttached = true;
                    rightRopeDone = true;
                    ladder.ropePlayerRight.SetActive(false);
                    Debug.Log("////////////////si entrooooo/////////");
                    Handrail_Right_2.SetActive(false);

                }

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
                {
                    Debug.Log("////////////////no entrooooo/////////");
                    equipments.isHandrailRight_2Selected = false;
                    isRightRopeAttached = false;
                    rightRopeDone = false;
                    gm.showOther = false;
                }
            }
        }





        //--------------------------------------------------------------------------------------------------------------------------------------------------
        //Correct 
        if (equipments.isCarabinerRight3Selected)  // isSafetyRopeRightSelected is been replaced by isCarabinerRightSelected
        {
            gm.showOther = true;

            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to untie off you right safety rope?", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
            {
                equipments.isCarabinerRight3Selected = false;
                carabinerRight3.SetActive(false);
                gm.showOther = false;
                safetyRopeRightLineRender3.useWorldSpace = false;
                ladder.ropePlayerRight.SetActive(true);
                isRightRopeAttached = false;
                rightRopeDone = false;
                activatePanel = true;
                Handrail_Right_3.SetActive(true);

            }
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
            {
                equipments.isCarabinerRight3Selected = false;
                isRightRopeAttached = true;
                rightRopeDone = true;
                gm.showOther = false;
            }
        }


        if (equipments.isHandrailRight_3Selected)  //CORRECT
        {
            gm.showOther = true;

            if (ladder.ropePlayerRight.activeInHierarchy == false)
            {
                Debug.Log("////////////////si entrooooo en la necesidad/////////");
                GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 - 160 + 200, 300, 75), "First, you need to untie off from the previous anchor", style);

                if (GUI.Button(new Rect(Screen.width / 2 , Screen.height / 2 - 75 + 160 + 90 + 25 - 155 , 300, 40),  "             \t\tOK", style))
                {
                    Debug.Log("////////////////no entrooooo/////////");
                    equipments.isHandrailRight_3Selected = false;
                    isRightRopeAttached = false;
                    rightRopeDone = false;
                    gm.showOther = false;
                }

            }
            else
            {
                GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to tie off you right safety rope?", style);

                if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
                {
                    equipments.isHandrailRight_3Selected = false;
                    gm.showOther = false;
                    carabinerRight3.SetActive(true);
                    safetyRopeRightLineRender3.useWorldSpace = true;
                    isRightRopeAttached = true;
                    rightRopeDone = true;
                    ladder.ropePlayerRight.SetActive(false);
                    Debug.Log("////////////////si entrooooo/////////");
                    Handrail_Right_3.SetActive(false);

                }

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
                {
                    Debug.Log("////////////////no entrooooo/////////");
                    equipments.isHandrailRight_3Selected = false;
                    isRightRopeAttached = false;
                    rightRopeDone = false;
                    gm.showOther = false;
                }
            }
        }


        //-*---------*****************************************************------------------------------------------------------------------------




        //-*---------*****************************************************------------------------------------------------------------------------
        //Correct 
        if (equipments.isHandrailLeft_1Selected)
        {
            gm.showOther = true;

            if (ladder.ropePlayerLeft.activeInHierarchy == false)
            {
                Debug.Log("////////////////si entrooooo en la necesidad/////////");
                GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 - 160 + 200, 300, 75), "First, you need to untie off from the previous anchor", style);

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
                {
                    equipments.isHandrailLeft_1Selected = false;
                    isLeftRopeAttached = false;
                    leftRopeDone = false;
                    gm.showOther = false;
                }

            }
            else
            {
                GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to tie off you left safety rope?", style);

                if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
                {
                    equipments.isHandrailLeft_1Selected = false;
                    carabinerLeft.SetActive(true);
                    gm.showOther = false;
                    safetyRopeLeftLineRender.useWorldSpace = true;
                    isLeftRopeAttached = true;
                    leftRopeDone = true;
                    ladder.ropePlayerLeft.SetActive(false);
                    Handrail_Left_1.SetActive(false);

                }

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
                {
                    equipments.isHandrailLeft_1Selected = false;
                    isLeftRopeAttached = false;
                    leftRopeDone = false;
                    gm.showOther = false;
                }
            }
        }
    

        //Correct 
        if (equipments.isCarabinerLeftSelected)   // isSafetyRopeLeftSelected is beeen replaced by isCarabinerLeftSelected
        {
            gm.showOther = true;

            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to untie off you left safety rope?", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
            {
                equipments.isCarabinerLeftSelected = false;
                carabinerLeft.SetActive(false);
                gm.showOther = false;
                //safetyRopeLeft.SetActive(false);
                safetyRopeLeftLineRender.useWorldSpace = false;
                safetyRopeLeft1.SetActive(false);
                safetyRopeLeft2.SetActive(false);
                isLeftRopeAttached = false;
                leftRopeDone = false;
                ladder.ropePlayerLeft.SetActive(true);
                Handrail_Left_1.SetActive(true);
                
                /////Add this code to 
                if ((leftRopeDone || rightRopeDone) && equipments.isPanelReadyToOpen == true && activatePanel == true)
                {
                    gm.showOther = false;

                    GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200 + 100 - 35, 300, 75 + 35), "Excellent! You have completed a 100% tie off, Activity 3. \n\nMake sure you mantain a 100% tie off while walking and performing work at the top of the nacelle", style);

                    if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155 + 100, 300, 40), "                              OK", style))
                    {
                        leftRopeDone = false;
                        rightRopeDone = false;
                        nextPanelTest = true;
                        gm.showOther = false;
                        equipments.countTies = 0;
                        activatePanel = false;
                        equipments.isPanelReadyToOpen = false;
                    }
                }


            }
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
            {
                equipments.isCarabinerLeftSelected = false;
                isLeftRopeAttached = true;
                leftRopeDone = true;
                gm.showOther = false;
            }
        }
        // /////////////////////////////===========================================================================================================================

        //-*---------*****************************************************------------------------------------------------------------------------
        //Correct 
        if (equipments.isHandrailLeft_2Selected)
        {
            gm.showOther = true;

            if (ladder.ropePlayerLeft.activeInHierarchy == false)
            {
                Debug.Log("////////////////si entrooooo en la necesidad/////////");
                GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 - 160 + 200, 300, 75), "First, you need to untie off from the previous anchor", style);

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 300, 40), "             \t\tOK", style))
                {
                    equipments.isHandrailLeft_2Selected = false;
                    isLeftRopeAttached = false;
                    leftRopeDone = false;
                    gm.showOther = false;
                }

            }
            else
            {
                GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to tie off you left safety rope?", style);

                if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
                {
                    equipments.isHandrailLeft_2Selected = false;
                    carabinerLeft2.SetActive(true);
                    gm.showOther = false;
                    safetyRopeLeftLineRender2.useWorldSpace = true;
                    isLeftRopeAttached = true;
                    leftRopeDone = true;
                    ladder.ropePlayerLeft.SetActive(false);
                    Handrail_Left_2.SetActive(false);

                }

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
                {
                    equipments.isHandrailLeft_2Selected = false;
                    isLeftRopeAttached = false;
                    leftRopeDone = false;
                    gm.showOther = false;
                }
            }
        }

        //Correct 
        if (equipments.isCarabinerLeft2Selected)   // isSafetyRopeLeftSelected is beeen replaced by isCarabinerLeftSelected
        {
            gm.showOther = true;

            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to untie off you left safety rope?", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
            {
                equipments.isCarabinerLeft2Selected = false;
                carabinerLeft2.SetActive(false);
                gm.showOther = false;
                safetyRopeLeftLineRender2.useWorldSpace = false;
                isLeftRopeAttached = false;
                leftRopeDone = false;
                ladder.ropePlayerLeft.SetActive(true);
                Handrail_Left_2.SetActive(true);

            }
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
            {
                equipments.isCarabinerLeft2Selected = false;
                isLeftRopeAttached = true;
                leftRopeDone = true;
                gm.showOther = false;
            }
        }


        //-*---------*****************************************************------------------------------------------------------------------------
        //Correct 
        if (equipments.isHandrailLeft_3Selected)
        {
            gm.showOther = true;

            if (ladder.ropePlayerLeft.activeInHierarchy == false)
            {
                Debug.Log("////////////////si entrooooo en la necesidad/////////");
                GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 - 160 + 200, 300, 75), "First, you need to untie off from the previous anchor", style);

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 300, 40), "             \t\tOK", style))
                {
                    equipments.isHandrailLeft_3Selected = false;
                    isLeftRopeAttached = false;
                    leftRopeDone = false;
                    gm.showOther = false;
                }

            }
            else
            {
                GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to tie off you left safety rope?", style);

                if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
                {
                    equipments.isHandrailLeft_3Selected = false;
                    carabinerLeft3.SetActive(true);
                    gm.showOther = false;
                    safetyRopeLeftLineRender3.useWorldSpace = true;
                    isLeftRopeAttached = true;
                    leftRopeDone = true;
                    ladder.ropePlayerLeft.SetActive(false);
                    Handrail_Left_3.SetActive(false);

                }

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
                {
                    equipments.isHandrailLeft_3Selected = false;
                    isLeftRopeAttached = false;
                    leftRopeDone = false;
                    gm.showOther = false;
                }
            }
        }

        //Correct 
        if (equipments.isCarabinerLeft3Selected)   // isSafetyRopeLeftSelected is beeen replaced by isCarabinerLeftSelected
        {
            gm.showOther = true;

            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Would you like to untie off you left safety rope?", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
            {
                equipments.isCarabinerLeft3Selected = false;
                carabinerLeft3.SetActive(false);
                gm.showOther = false;
                safetyRopeLeftLineRender3.useWorldSpace = false;
                isLeftRopeAttached = false;
                leftRopeDone = false;
                ladder.ropePlayerLeft.SetActive(true);
                Handrail_Left_3.SetActive(true);

            }
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
            {
                equipments.isCarabinerLeft3Selected = false;
                isLeftRopeAttached = true;
                leftRopeDone = true;
                gm.showOther = false;
            }
        }




        // /////////////////////////////===========================================================================================================================


        // /////////////////////////////===========================================================================================================================


        //Correct 
        if (equipments.isHatchSelected)
        {
            gm.showOther = true;

            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 75), "Are you ready to go to the top of the nacelle?", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
            {
                gm.showOther = false;
                //gameObject.GetComponent<FirstPersonController>().movementSpeed = 0.35f;
                gameObject.transform.position = GameObject.Find("NacellePlayerPosOutside").transform.position;
                gameObject.transform.rotation = GameObject.Find("NacellePlayerPosOutside").transform.rotation;
                show_InsideNacelle = false;
                ladder.inNacelle = false;
                ladder.climbB = false;
                ladder.downB = false;
                ladder.slider.SetActive(false);
                TopNacelle = true;
                //ladder.rope.SetActive(false); // Delete prototype rope -------------------------------------
                ladder.ropeLineRender.useWorldSpace = false;//*********************************************************************
                equipments.isHatchSelected = false;
                equipments.cellingNacelle.enabled = true;


                /*carabinerRight.SetActive(false);
                carabinerRight2.SetActive(false);
                carabinerRight3.SetActive(false);
                carabinerLeft.SetActive(false);
                carabinerLeft2.SetActive(false);
                carabinerLeft3.SetActive(false);*/

            }
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
            {
                equipments.isHatchSelected = false;
                isLeftRopeAttached = false;
                leftRopeDone = false;
                gm.showOther = false;
            }
        }

        //Correct 
        if (equipments.isHatchBackSelected)
        {
            gm.showOther = true;

            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200 - 5, 300, 75), "Are you ready to go back to the nacelle? \n\nMake sure you maintain 100% while climbing down the wind turbine", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             Yes", style))
            {
                gm.showOther = false;

                //gameObject.GetComponent<FirstPersonController>().movementSpeed = 0.35f;
                gameObject.transform.position = GameObject.Find("NacellePlayerPosInside").transform.position;
                gameObject.transform.rotation = GameObject.Find("NacellePlayerPosInside").transform.rotation;
                show_InsideNacelle = true;
                ladder.inNacelle = true;
                ladder.climbB = false;
                ladder.downB = false;
                ladder.slider.SetActive(false);
                //ladder.rope.SetActive(false); // Delete prototype rope -------------------------------------
                ladder.ropeLineRender.useWorldSpace = false;//*********************************************************************
                equipments.isHatchBackSelected = false;
                equipments.cellingNacelle.enabled = false;
                TopNacelle = false;

                safetyRopeLeftLineRender.gameObject.SetActive(false);
                safetyRopeLeftLineRender.gameObject.SetActive(false);
                safetyRopeLeftLineRender2.gameObject.SetActive(false);
                safetyRopeLeftLineRender3.gameObject.SetActive(false);
                safetyRopeRightLineRender.gameObject.SetActive(false);
                safetyRopeRightLineRender2.gameObject.SetActive(false);
                safetyRopeRightLineRender3.gameObject.SetActive(false);


            }
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 - 75 + 160 + 90 + 25 - 155, 150, 40), "             No", style))
            {
                equipments.isHatchBackSelected = false;
                isLeftRopeAttached = false;
                leftRopeDone = false;
                gm.showOther = false;
            }
        }


        //Correct 
        if ((leftRopeDone || rightRopeDone) && (equipments.countTies == 1 || equipments.countTies == 2) && firtsAttemp == true)
        {
            gm.showOther = false;

            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200 + 100, 300, 75), "Excellent Activity 2 is completed. \n\nYou have completed tie off at the top of the nacelle", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155 + 100, 300, 40), "                              OK", style))
            {
                leftRopeDone = false;
                rightRopeDone = false;
                nextPanelTest = true;
                gm.showOther = false;
                firtsAttemp = false;
            }
        }

        //Correct 
       /* if ((leftRopeDone || rightRopeDone) && equipments.countTies == 3 && activatePanel == true)
        {
            gm.showOther = false;

            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200 + 100 - 35, 300, 75 + 35), "Excellent! You have completed a 100% tie off, Activity 3. \n\nMake sure you mantain a 100% tie off while walking and performing work at the top of the nacelle", style);

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155 + 100, 300, 40), "                              OK", style))
            {
                leftRopeDone = false;
                rightRopeDone = false;
                nextPanelTest = true;
                gm.showOther = false;
            }
        }
        */
        //Correct 
        if ( show_FallingDownFromNacelle && isLeftRopeAttached  && isRightRopeAttached)
        {
            gm.showOther = false;
            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 100), "Hopefully, you mantain a 100% tie which protects you from falling down. \n\nLook around to see how far you have fall down.", style);
            show_FallDown_FromNacelle = false;

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155 + 25 , 300, 40), "Click here to go back to the top of nacelle", style))
            {
                gameObject.transform.position = GameObject.Find("NacellePlayerPosOutsideEdge").transform.position;
                gameObject.transform.rotation = GameObject.Find("NacellePlayerPosOutsideEdge").transform.rotation;
                show_FallingDownFromNacelle = false;
            }

        }

        //Correct 
        if (show_FallingDownFromNacelle && ((isLeftRopeAttached == true && isRightRopeAttached == false) || (isLeftRopeAttached == false && isRightRopeAttached == true)))
        {
            gm.showOther = false;
            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 100), "You are tieing off using only one lanyard. You did not mantain a 100% tie which protect you from falling down. \n\nLook around to see how far you have fall down.", style);
            show_FallDown_FromNacelle = false;

            if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155 + 25, 300, 40), "Click here to start again", style))
            {
                Debug.Log("I am in the third if ++++++++++++++++++++++");
                gameObject.transform.position = GameObject.Find("NacellePlayerPosOutsideEdge").transform.position;
                gameObject.transform.rotation = GameObject.Find("NacellePlayerPosOutsideEdge").transform.rotation;
                show_FallingDownFromNacelle = false;
            }
        }

        //Correct 
        if (show_FallingDownFromNacelle && isLeftRopeAttached == false && isRightRopeAttached == false)
        {
            GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 - 160 + 200, 300, 100), "Sadly, you did not mantain a 100% tie which does not protect you from falling down. \n\nStart again.", style);
            show_FallDown_FromNacelle = true;

            if (show_GettingToFloor)
            {
                gm.showOther = false;
                show_FallDown_FromNacelle = false;

                if (GUI.Button(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 75 + 160 + 90 + 25 - 155 + 25, 300, 40), "Click here to start again", style))
                {
                    Debug.Log("I am in the second if ++++++++++++++++++++++");
                    gameObject.transform.position = GameObject.Find("NacellePlayerPosOutsideBeginAgain").transform.position;
                    gameObject.transform.rotation = GameObject.Find("NacellePlayerPosOutsideBeginAgain").transform.rotation;
                    show_FallingDownFromNacelle = false;
                    // I need to include the slider in this section
                    ladder.slider.SetActive(true);
                }
            }
        }


        if (timeB)
		{

			yieldTime += Time.deltaTime;

			if (yieldTime > 5)
			{
				timeB = false;
			}
		}
	}

	public void doTransport()
	{


	}


    /*
     * The main safety measure is known as 100% tie-off, which ensures climbers are connected to the tower at all times.
     * */

}
