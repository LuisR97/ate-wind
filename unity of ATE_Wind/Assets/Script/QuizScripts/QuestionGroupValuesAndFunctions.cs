﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/*
 * This code is attached to the following objects in the hierarchy [all caps]
 * 
 *  ->GROUP OF QUESTIONS
 *  
 *      ->QuestionController
 *          
 *          ->Question
 *          
 *              ->Background
 *               
 *              ->Answer 1
 *              
 *              ->Answer 2
 * 
 *  ->GroupOverview
 *      
 *      ->OverallRatio
 *      
 *      ->CategoryList
 *      
 *      ->ScoreList
 * 
 */


public class QuestionGroupValuesAndFunctions : MonoBehaviour {

    //values are for editor read/debug only, do not manually edit
    [System.Serializable]
    public class QuestionGroupValues
    {

        //This keeps track of values used for the grading section
        [Tooltip("Total number of questions.")]
        public int numberOfQuestions = 0;

        [Tooltip("Total number of questions answered correctly.")]
        public int numberOfCorrectAnswers = 0;

        [Tooltip("Percentage of correct answers")]
        public float percentileOfCorrectAnswers = 0f;

        [Tooltip("List of categories based on QuestionController fields")]
        public List<string> categories = new List<string>();

        [Tooltip("Number of questions answered correctly by category")]
        public int[] correctAnswersByCategory;

        [Tooltip("Total number of questions by category.")]
        public int[] totalAnswersByCategory;

        [Tooltip("Percentage of correct answers by category")]
        public float[] percentileCorrectByCategory;

        [Tooltip("Whether the current total percentile of correct answers is greater or equal to the passingGradeIntegerValue input")]
        public bool passingGradeAchieved = false;

    }

    [Tooltip("Set the passing integer grade of this quiz.")]
    public int passingGradeIntegerValue;

    [Tooltip("Keeps track of various information like number of correct and total questions, how the questions are grouped, and whether a passing grade has been achieved.")]
    public QuestionGroupValues questionGroupValues;

    [Tooltip("Attach the GroupOverview related to this group in the hierarchy.")]
    public GameObject overviewPanel;


    //Objects in this list will be sensitive to naming
    private List<GameObject> overviewPanelChildren = new List<GameObject>();

	public GameObject passpanel;

	public GameObject failedpanel;

	public Button nextbutton;

	public GameObject GroupOverview;

	public Button passbutton;

    public PlayerPreferenceExample playerPreferenceExample;

    public string currentSubtopic = "";

    //public Button SafetyTopic;

    public PrefScript prefScript;

    // Use this for initialization
    void Start () {

        InitializeNumberOfQuestions();
        InitializeCategoryList();
        InitializeAnswerArrays();
        InitializeOverviewObjectArray();
		nextbutton.onClick.AddListener (NextbuttonOnClick);
        //		passbutton.onClick.AddListener (PassbuttonOnClick);
        prefScript = GameObject.FindGameObjectWithTag("CurrentTopicHolder").GetComponent<PrefScript>();
        currentSubtopic = GameObject.FindGameObjectWithTag("CurrentTopicHolder").GetComponent<PrefScript>().currentTopic;

	}

    private void FixedUpdate()
    {

        UpdatePercentileCorrect();
        

    }

    // Update is called once per frame
    void Update () {
		
	}

    //Start function to find the number of questions that exist
    void InitializeNumberOfQuestions ()
    {

        questionGroupValues.numberOfQuestions = this.gameObject.transform.childCount;

    }

    void InitializeCategoryList ()
    {


        List<QuestionValuesAndFunctions> childValues = new List<QuestionValuesAndFunctions>();
        foreach (Transform a in this.gameObject.GetComponentInChildren<Transform>())
        {

            childValues.Add(a.gameObject.GetComponent<QuestionValuesAndFunctions>());

        }

        bool alreadyExistsInList = false;
        foreach (QuestionValuesAndFunctions singleSet in childValues)
        {

            string tempString = singleSet.categoryName;
            foreach(string a in questionGroupValues.categories)
            {

                if(a == tempString)
                {

                    alreadyExistsInList = true;

                }

            }
            if (alreadyExistsInList == false)
            {

                questionGroupValues.categories.Add(tempString);

            }

            alreadyExistsInList = false;

        }

    }

    public void InitializeAnswerArrays()
    {

        questionGroupValues.correctAnswersByCategory = new int[questionGroupValues.categories.Count];
        questionGroupValues.totalAnswersByCategory = new int[questionGroupValues.categories.Count];
        questionGroupValues.percentileCorrectByCategory = new float[questionGroupValues.categories.Count];

        int i = 0;
        foreach (int a in questionGroupValues.correctAnswersByCategory)
        {

            questionGroupValues.correctAnswersByCategory[i] = 0;
            questionGroupValues.totalAnswersByCategory[i] = 0;
            questionGroupValues.percentileCorrectByCategory[i] = 0;
            i++;

        }

    }

    public void InitializeOverviewObjectArray()
    {

        for (int i = 0; i < overviewPanel.transform.childCount; i++)
        {

            overviewPanelChildren.Add(overviewPanel.transform.GetChild(i).gameObject);

        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void UpdatePercentileCorrect ()
    {

        questionGroupValues.percentileOfCorrectAnswers = ((float)questionGroupValues.numberOfCorrectAnswers / (float)questionGroupValues.numberOfQuestions) * 100f;

        if (questionGroupValues.percentileOfCorrectAnswers >= (float)passingGradeIntegerValue)
        {

            questionGroupValues.passingGradeAchieved = true;
            // fangzhou zhang 
            //playerPreferenceExample.values [0] ="1";



            foreach (string topicName in playerPreferenceExample.subtopics)
            {


                if (topicName.Trim().Equals(currentSubtopic))
                {

                    //Debug.Log("Topic Name was found");

                    int tempIndex = playerPreferenceExample.subtopics.IndexOf(topicName);

                    if (playerPreferenceExample.values[tempIndex] == "0") 
                    {

                        playerPreferenceExample.values[tempIndex] = "1";

                    }

                }


            }
            

        }
        else
        {

            questionGroupValues.passingGradeAchieved = false;

        }

        int i = 0;
        foreach (string category in questionGroupValues.categories)
        {

            if (questionGroupValues.totalAnswersByCategory[i] != 0)
            {

                questionGroupValues.percentileCorrectByCategory[i] = ((float)questionGroupValues.correctAnswersByCategory[i] / (float)questionGroupValues.totalAnswersByCategory[i]) * 100f;

            } else
            {

                questionGroupValues.percentileCorrectByCategory[i] = 0;

            }
            
            i++;
        }
    }

    void UpdateOverallRatioText (Text inputTextComponent)
    {

        string tempstring = questionGroupValues.numberOfCorrectAnswers.ToString() + " Out of " + questionGroupValues.numberOfQuestions.ToString();
        inputTextComponent.text = tempstring;

    }

    void UpdateSubsectionList (Text inputTextComponent)
    {

        string tempstring = "";
        foreach (string category in questionGroupValues.categories)
        {

            tempstring = tempstring.ToString() + category + ":" + "\n";

        }

        inputTextComponent.text = tempstring;

    }

    void UpdateScoreList (Text inputTextComponent)
    {

        string tempstring = "";
        int i = 0;
        foreach (string category in questionGroupValues.categories)
        {

            Debug.Log(i);
			Debug.Log (questionGroupValues.correctAnswersByCategory [i].ToString ());
			Debug.Log (questionGroupValues.totalAnswersByCategory [i].ToString ());
            tempstring = tempstring + questionGroupValues.correctAnswersByCategory[i].ToString() + "/" + questionGroupValues.totalAnswersByCategory[i].ToString() + "\n";
            i++;

        }
        inputTextComponent.text = tempstring;
    }

    //MAKE SURE ALL NAMING IS CORRECT IN THE HIERARCHY!!!
    public void UpdateOverviewTextObjects ()
    {

        foreach (GameObject child in overviewPanelChildren)
        {

            if (child.name == "OverallRatio")
            {

                UpdateOverallRatioText(child.GetComponent<Text>());

            } else if (child.name == "CategoryList")
            {

                UpdateSubsectionList(child.GetComponent<Text>());

            } else if (child.name == "ScoreList")
            {


                UpdateScoreList(child.GetComponent<Text>());

            }
        }
    }


    //Adds to the Array that keeps track of correct answers
    public void AddToCorrectValues(QuestionValuesAndFunctions fromQuestion)
    {

        int i = 0;
        foreach (string currentString in questionGroupValues.categories)
        {

            if (currentString == fromQuestion.categoryName)
            {

                questionGroupValues.totalAnswersByCategory[i]++;

                Debug.Log("The " + currentString + " category total ++");

                if(fromQuestion.questionValues.correctAnswer == true)
                {

                    questionGroupValues.correctAnswersByCategory[i]++;
                    questionGroupValues.numberOfCorrectAnswers++;

                }
            }
            i++;
        }
    }

	//Open differernt panel according to if passed
	public void NextbuttonOnClick()
	{			
		if (questionGroupValues.passingGradeAchieved == true) {
			passpanel.SetActive (true);
			GroupOverview.SetActive (false);

		} else {

			failedpanel.SetActive (true);
			GroupOverview.SetActive (false);
		}
	}

    //Passed and go back to MenuPanel
    //	public void PassbuttonOnClick()
    //	{
    //		if (passpanel == true) {
    //			passpanel.SetActive (false);
    //		}
    //	}

    //public void GetSafetyTopic(Button topic)
    //{
    //    currentTopic = topic.GetComponentInChildren<Text>().text.ToString();
    //    Debug.Log(currentTopic);
    //}

}
