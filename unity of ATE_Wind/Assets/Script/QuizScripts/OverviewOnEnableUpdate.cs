﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This code is attached to the following objects in the hierarchy [all caps]
 * 
 *  ->GroupOfQuestions
 *  
 *      ->QuestionController
 *          
 *          ->Question
 *          
 *              ->Background
 *               
 *              ->Answer 1
 *              
 *              ->Answer 2
 * 
 *  ->GROUP OVERVIEW
 *      
 *      ->OverallRatio
 *      
 *      ->CategoryList
 *      
 *      ->ScoreList
 * 
 */

public class OverviewOnEnableUpdate : MonoBehaviour {

    public QuestionGroupValuesAndFunctions groupOfQuestions;
    public GameObject passedPanel, failedPanel;

    private bool passed;

    //Activate when object enables
    //Make sure the object names are what are declared in the QuestionGroupValuesAndFunctions
    private void OnEnable()
    {

        StartCoroutine(DelayedUpdateRoutine());

    }

    public void OverviewNextButton ()
    {

        if (passed == true)
        {

            passedPanel.SetActive(true);

        } else
        {

            failedPanel.SetActive(true);

        }

        this.gameObject.SetActive(false);

    }

    IEnumerator DelayedUpdateRoutine()
    {

        yield return new WaitForEndOfFrame();

        Debug.Log("Wait Until EoF");
        groupOfQuestions.UpdateOverviewTextObjects();
        passed = groupOfQuestions.questionGroupValues.passingGradeAchieved;

        yield return null;

    }

}
