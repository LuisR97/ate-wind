﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/*IMPORTANT NOTES
 * 
 * - All panelsForDisplay should be by default be under panelListParent
 * 
 *  ex. 
 *      ->panelListParent
 *              
 *              ->panel1
 *              
 *              ->panel2
 *              
 *              ->panel3
 *              
 *              ->etc.
 * 
 * 
 */
public class ReferenceMenuScript : MonoBehaviour
{

    //Public Variable List

    [Tooltip("Individual Panels that would be swapped out into the viewport (automatically populated based on panelListParent)")]
    public List<GameObject> panelsForDisplay;

    [Tooltip("The Object that is the parent for all the panels we aren't using (all panels should start here, none in the content of the viewport)")]
    public GameObject panelListParent;

    [Tooltip("The viewport 'content' gamebject")]
    public GameObject scrollViewportGameObject;

    [Tooltip("Whether or not the user can go from last page to first page and vice-versa using back and next button")]
    public bool wrapAroundPanels = false;

    [Tooltip("The 'next' button on the menu")]
    public Button nextButton;

    [Tooltip("The 'back' button on the menu")]
    public Button backButton;

    [Tooltip("The 'page number' text on the menu")]
    public Text pageNumber;

    //Private Variable List

    //Current page count
    private int currentPageNumber;
    private int currentPageNumberInArray;

    //Total page count
    private int totalPageNumber;

    private GameObject currentPanel;
    private GameObject lastPanel = null;

    //Start with variable initialization

    private void InitializePanelsForDisplay()
    {

        for(int i=0; i<panelListParent.transform.childCount; i++)
        {

            panelsForDisplay.Add(panelListParent.transform.GetChild(i).gameObject);

        }

    }

    //Unity library functions
    private void Awake()
    {

        InitializePanelsForDisplay();

        currentPageNumber = 1;
        totalPageNumber = panelsForDisplay.Count;
        currentPageNumberInArray = 0;

        ActivatedUpdateActivePage();
    }

    private void Update()
    {

        UpdateInteractivityOfButtons();
        UpdatePageNumberText();
    }

    //Written Functions
    private void ActivatedUpdateActivePage()
    {

        if (lastPanel != null)
        {

            lastPanel.SetActive(false);
            lastPanel.transform.parent = panelListParent.transform;
            
            foreach (GameObject child in panelsForDisplay)
            {

                if (lastPanel == child)
                {

                    lastPanel.transform.SetSiblingIndex(panelsForDisplay.IndexOf(child));

                }

            }

        }

        currentPanel = panelsForDisplay[currentPageNumberInArray];

        currentPanel.transform.parent = scrollViewportGameObject.transform;

        currentPanel.SetActive(true);

        lastPanel = currentPanel;

    }

    //Function that happens when you click the next button
    public void NextButtonOnClick ()
    {

        if (currentPageNumber == panelsForDisplay.Count)
        {

            currentPageNumber = 1;


            currentPageNumberInArray = 0;

        } else
        {

            currentPageNumber++;
            currentPageNumberInArray++;

        }

        ActivatedUpdateActivePage();

    }

    //Function that happens when you hit the back button
    public void BackButtonOnClick()
    {

        if (currentPageNumber == 1)
        {

            currentPageNumber = panelsForDisplay.Count;
            currentPageNumberInArray = panelsForDisplay.Count-1;

        }
        else
        {

            currentPageNumber--;
            currentPageNumberInArray--;

        }

        ActivatedUpdateActivePage();

    }


    //Keeps you from pressing any buttons you shouldn't
    private void UpdateInteractivityOfButtons ()
    {

        if (wrapAroundPanels == false)
        {

            if (currentPageNumber == panelsForDisplay.Count)
            {

                nextButton.interactable = false;

            } else
            {

                nextButton.interactable = true;

            }
            
            
            if (currentPageNumber == 1)
            {

                backButton.interactable = false;

            } else
            {

                backButton.interactable = true;

            }

        }

    }
    
    private void  UpdatePageNumberText ()
    {

        pageNumber.text = currentPageNumber.ToString() + "/" + panelsForDisplay.Count;

    }

    //-----------------------------------
    /// <summary>
    /// This function is meant to make a button that goes to a specific page in the list
    /// </summary>
    /// <param name="pageToJumpTo">Integer value of the page to jump to</param>
    //-----------------------------------
    public void BookmarkOnClickFunction (int pageToJumpTo)
    {

        if (pageToJumpTo < 1)
        {

            pageToJumpTo = 1;

        } else if (pageToJumpTo > panelsForDisplay.Count)
        {

            pageToJumpTo = panelsForDisplay.Count;

        }

        currentPageNumber = pageToJumpTo;
        currentPageNumberInArray = pageToJumpTo - 1;

        ActivatedUpdateActivePage();
    }

    public int GetCurrentPageNumber()
    {

        return currentPageNumber;

    }

    public bool IsCurrentPageLast()
    {

        return (currentPageNumber == panelsForDisplay.Count);

    }

}