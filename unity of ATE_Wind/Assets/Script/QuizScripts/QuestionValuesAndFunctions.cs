﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * This code is attached to the following objects in the hierarchy [all caps]
 * 
 *  ->GroupOfQuestions
 *  
 *      ->QUESTION CONTROLLER
 *          
 *          ->Question
 *          
 *              ->Background
 *               
 *              ->Answer 1
 *              
 *              ->Answer 2
 * 
 *  ->GroupOverview
 *      
 *      ->OverallRatio
 *      
 *      ->CategoryList
 *      
 *      ->ScoreList
 * 
 */


public class QuestionValuesAndFunctions : MonoBehaviour {

    //variables are public only to be read in inspector, do not manually edit
    [System.Serializable]
    public class QuestionValues
    {

        [Tooltip("This question was answered correctly.")]
        public bool correctAnswer = false;

        [Tooltip("This question was answered incorrectly.")]
        public bool incorrectAnswer = false;

    }

    [Tooltip("Attach the GroupOfQuestions above this in the hierarchy.")]
    public GameObject parentQuestionGroup;

    [Tooltip("Keeps track of whether this question was answered right or wrong.")]
    public QuestionValues questionValues;

    [Tooltip("Name of the category (is case and space sensitive)")]
    public string categoryName = "";
    


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickCorrectAnswer ()
    {

        questionValues.correctAnswer = true;
        questionValues.incorrectAnswer = false;
        PassValuesToGroupTracker();

    }

    public void OnClickIncorrectAnswer()
    {

        questionValues.incorrectAnswer = true;
        questionValues.correctAnswer = false;
        PassValuesToGroupTracker();

    }

    public void ResetAnswerBools()
    {

        questionValues.correctAnswer = false;
        questionValues.incorrectAnswer = false;

    }

    public void PassValuesToGroupTracker()
    {

        parentQuestionGroup.GetComponent<QuestionGroupValuesAndFunctions>().AddToCorrectValues(this);

    }

    //Turns this question off, turns next question on,
    //as long as the hierarchy matches
    private void NextQuestionActivation ()
    {

        GameObject thisQuestion;
        GameObject parentController;
        GameObject nextQuestion;
        GameObject overviewPanel;
        int parentControllerChildIndex = 0;
        int nextQuestionControllerIndex = 0;

        //Question answer is a part of
        thisQuestion = this.gameObject.transform.GetChild(0).gameObject;

        //Controller for the question
        parentController = thisQuestion.transform.parent.gameObject;

        //Find indexes for getting the next question
        parentControllerChildIndex = parentController.transform.GetSiblingIndex();
        nextQuestionControllerIndex = parentControllerChildIndex + 1;

        //Debug.Log(parentQuestionGroup.transform.GetChild(nextQuestionControllerIndex).GetChild(0).gameObject.name);

        //Overview panel
        overviewPanel = parentQuestionGroup.GetComponent<QuestionGroupValuesAndFunctions>().overviewPanel;

        if (nextQuestionControllerIndex == parentQuestionGroup.transform.childCount)
        {

            thisQuestion.SetActive(false);
            overviewPanel.SetActive(true);

        } else
        {

            nextQuestion = parentQuestionGroup.transform.GetChild(nextQuestionControllerIndex).GetChild(0).gameObject;

            //Debug.Log(thisQuestion.name);
            //Debug.Log(nextQuestion.name);
            thisQuestion.SetActive(false);
            nextQuestion.SetActive(true);

        }

    }

    //Argument is whether or not the button is correct
    public void AllInOneQuestionAnswerButton(bool isCorrect)
    {
		Debug.Log (isCorrect);
        if (isCorrect == true)
        {

            OnClickCorrectAnswer();

        } else
        {

            OnClickIncorrectAnswer();

        }

        NextQuestionActivation();
        Debug.Log("The All-in-one activated");

    }

}
