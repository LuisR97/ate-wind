﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class GetListOfcustomScenes : MonoBehaviour {

    public GameObject fileButtonPrefab;     //the prefab for the button which will load the scene
    public GameObject buttonCanvasParent;   //the viewport content object to parent buttons to (needs viewport for scrolling)
    public string path;     //the root directory to search for content at (by default "Custom/")

    public void Start() {
        GetListOfScenesFromDirectory();
    }

    public void GetListOfScenesFromDirectory() {
        if(!Directory.Exists(path)) {
            Directory.CreateDirectory(path);
        }

        DirectoryInfo info = new DirectoryInfo(path);
        FileInfo[] listOfFiles = info.GetFiles();
        
        foreach(FileInfo file in listOfFiles) {
            if(file.Name.Contains(".csv")) {
                GameObject button = Instantiate(fileButtonPrefab, buttonCanvasParent.transform);
                button.GetComponentInChildren<Text>().text = file.Name.Replace(".csv"," ");

                button.GetComponent<LoadSceneButton>().path = "Custom/" + file.Name;
            }
        }
    }

}
