﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour {

    public Light lamp;

    void Start() {
        if(lamp==null) {
            lamp = GetComponent<Light>();
        }
    }

	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("ToggleFlashlight")) {
            ToggleFlashLight();
        }
	}

    public void ToggleFlashLight() {
        lamp.enabled = !lamp.enabled;
    }
}
