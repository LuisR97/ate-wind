﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;

//code referenced from: https://stackoverflow.com/questions/2416748/how-to-simulate-mouse-click-in-c

public class VirtualMouse : MonoBehaviour {

    //the speed of the mouse cursor
    public int speed = 10;

    //identifiers for the code for each type of button press
    [Flags]
    public enum MouseEventFlags {
        LeftDown = 0x00000002,
        LeftUp = 0x00000004,
        MiddleDown = 0x00000020,
        MiddleUp = 0x00000040,
        Move = 0x00000001,
        Absolute = 0x00008000,
        RightDown = 0x00000008,
        RightUp = 0x00000010
    }


    void Update() {
       
        //moves mouse cursor based on 
        if(Input.GetKey(KeyCode.UpArrow)) {
            SetCursorPosition(GetCursorPosition().X, GetCursorPosition().Y-speed);
        }
        if (Input.GetKey(KeyCode.DownArrow)) {
            SetCursorPosition(GetCursorPosition().X, GetCursorPosition().Y + speed);
        }
        if (Input.GetKey(KeyCode.LeftArrow)) {
            SetCursorPosition(GetCursorPosition().X - speed, GetCursorPosition().Y);
        }
        if (Input.GetKey(KeyCode.RightArrow)) {
            SetCursorPosition(GetCursorPosition().X + speed, GetCursorPosition().Y);
        }


        //applies cursor movement based on the right joystick
        if (Input.GetAxis("Mouse X Stick") >0.01 || Input.GetAxis("Mouse X Stick") <-0.01) {
            SetCursorPosition(GetCursorPosition().X + (int)(speed * Input.GetAxis("Mouse X Stick")), GetCursorPosition().Y);
        }
        if(Input.GetAxis("Mouse Y Stick") > 0.01 || Input.GetAxis("Mouse Y Stick") < -0.01) {
            SetCursorPosition(GetCursorPosition().X , GetCursorPosition().Y - (int)(speed * Input.GetAxis("Mouse Y Stick")));
        }

        //handles the simulation of mouse button down and mouse button up on button press
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("MoveUp")) {
            MousePoint pos = GetCursorPosition();
            MouseEvent(MouseEventFlags.LeftDown);
            SetCursorPosition(pos.X, pos.Y);
        }
        if (Input.GetKeyUp(KeyCode.Space) || Input.GetButtonUp("MoveUp")) {
            MousePoint pos = GetCursorPosition();
            MouseEvent(MouseEventFlags.LeftUp);
            SetCursorPosition(pos.X, pos.Y);
        }

    }



    [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool SetCursorPos(int X, int Y);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GetCursorPos(out MousePoint lpMousePoint);

    [DllImport("user32.dll")]
    private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

    public static void SetCursorPosition(int X, int Y) {
        SetCursorPos(X, Y);
    }

    public static void SetCursorPosition(MousePoint point) {
        SetCursorPos(point.X, point.Y);
    }

    public static MousePoint GetCursorPosition() {
        MousePoint currentMousePoint;
        var gotPoint = GetCursorPos(out currentMousePoint);
        if (!gotPoint) { currentMousePoint = new MousePoint(0, 0); }
        return currentMousePoint;
    }

    public static void MouseEvent(MouseEventFlags value) {
        MousePoint position = GetCursorPosition();

        mouse_event
            ((int)value,
             position.X,
             position.Y,
             0,
             0)
            ;
    }

    //a struct used to define two int values, x and y, for mouse position
    [StructLayout(LayoutKind.Sequential)]
    public struct MousePoint {
        public int X;
        public int Y;

        public MousePoint(int x, int y) {
            X = x;
            Y = y;
        }

    }

}
