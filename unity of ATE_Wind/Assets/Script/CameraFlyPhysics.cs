﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public class CameraFlyPhysics : MonoBehaviour {


//    private float currentSpeed;

//    public float mainSpeed = 20;
//    public float customSpeed;
//    public float shiftAdd = 8;
//    public float altSlow = 0.5f;
//    public float maxShift = 1000;
//    public float camSens = 0.25f;


//    private CharacterController cc;

//    void Start() {
//        cc = GetComponent<CharacterController>();
//    }

//    void Update () {
//        if(Input.GetKey(KeyCode.LeftShift)){
//            currentSpeed = mainSpeed * shiftAdd;
//        }else if(Input.GetKey(KeyCode.LeftAlt)) {
//            currentSpeed = mainSpeed * altSlow;
//        }else {
//            currentSpeed = mainSpeed;
//        }


//	    if(Input.GetKey(KeyCode.W)) {
//            cc.Move(Vector3.forward * currentSpeed);
//        }
//        if(Input.GetKey(KeyCode.A)) {
//            cc.Move(Vector3.left * currentSpeed);
//        }
//        if(Input.GetKey(KeyCode.S)) {
//            cc.Move(Vector3.back * currentSpeed);
//        }
//        if(Input.GetKey(KeyCode.D)) {
//            cc.Move(Vector3.right * currentSpeed);
//        }
//        if(Input.GetKey(KeyCode.E)) {
//            cc.Move(Vector3.up * currentSpeed);
//        }
//        if(Input.GetKey(KeyCode.C)) {
//            cc.Move(Vector3.down * currentSpeed);
//        }



//	}
//}

public class CameraFlyPhysics : MonoBehaviour
{


    private float currentSpeed;

    public float mainSpeed = 20;
    public float customSpeed;
    public float shiftAdd = 8;
    public float altSlow = 0.5f;
    public float maxShift = 1000;
    public float camSens = 0.25f;


    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    private bool isGrounded;
    private Vector3 velocity;
    public float gravity = -9.81f;

    public CharacterController cc;

    void Start()
    {
        //cc = GetComponent<CharacterController>();
    }

    void Update()
    {

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }


        if (Input.GetKey(KeyCode.LeftShift))
        {
            currentSpeed = mainSpeed * shiftAdd;
        }
        else if (Input.GetKey(KeyCode.LeftAlt))
        {
            currentSpeed = mainSpeed * altSlow;
        }
        else
        {
            currentSpeed = mainSpeed;
        }


        if (Input.GetKey(KeyCode.W))
        {
            cc.Move(Vector3.forward * currentSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            cc.Move(Vector3.left * currentSpeed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            cc.Move(Vector3.back * currentSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            cc.Move(Vector3.right * currentSpeed);
        }
        if (Input.GetKey(KeyCode.E))
        {
            cc.Move(Vector3.up * currentSpeed);
        }
        if (Input.GetKey(KeyCode.C))
        {
            cc.Move(Vector3.down * currentSpeed);
        }

        velocity.y += gravity * Time.deltaTime;
        cc.Move(velocity * Time.deltaTime);


    }
}
