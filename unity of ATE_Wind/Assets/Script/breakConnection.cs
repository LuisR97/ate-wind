﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class breakConnection : MonoBehaviour {

    public Changelight lightScript;
    public bool breakDoor, breakSwitch, breakConnections;
    public NewMeasurement[] nodesToDisconnect;

    public void Break() {
        if(breakDoor) {
            lightScript.TBErrorList.doorWorking = false;
        }
        if(breakSwitch) {
            Debug.Log("breaking switch");
            lightScript.TBErrorList.switchWorking = false;
        }
        if(breakConnections) {
            Debug.Log("breaking connections");
            lightScript.TBErrorList.connectionsWorking = false;
        }

        foreach(NewMeasurement node in nodesToDisconnect) {
            node.isDisconnected = true;
        }
    }
}
