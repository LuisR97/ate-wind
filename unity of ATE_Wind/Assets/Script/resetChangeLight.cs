﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class resetChangeLight : MonoBehaviour {

    public bool powerIsOn = true;
    public bool doorIsOpen = false;
    public bool switchIsOn = false;
    public bool check = false;
    public bool bulbLight = false;
    public bool runLight = true;

    public Changelight lightScript;

    public void Start() {
        if(lightScript==null) {
            lightScript = GameObject.Find("Scriptholder").GetComponent<Changelight>();
        }
        ResetStats();
    }

    public void ResetStats() {
        lightScript.powerison = powerIsOn;
        lightScript.doorisopen = doorIsOpen;
        lightScript.switchison = switchIsOn;
        lightScript.check = check;
        lightScript.bulblight = bulbLight;
        lightScript.runlight = runLight;

        lightScript.UpdateParts();
    }

}
