﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideShowObject : MonoBehaviour {

    public void ShowObject() {
        if (GetComponent<MeshRenderer>() != null) {
            GetComponent<MeshRenderer>().enabled = true;
        }

        foreach (MeshRenderer m in GetComponentsInChildren<MeshRenderer>()) {
            m.enabled = true;
        }
    }

    public void HideObject() {
        if (GetComponent<MeshRenderer>() != null) {
            GetComponent<MeshRenderer>().enabled = false;
        }

        foreach (MeshRenderer m in GetComponentsInChildren<MeshRenderer>()) {
            m.enabled = false;
        }
    }

    public void DisableTargetObject(GameObject target) {
        target.SetActive(false);
    }

    public void EnableTargetObject(GameObject target) {
        target.SetActive(true);
    }

    public void ToggleTargetObject(GameObject target) {
        target.SetActive(!target.activeSelf);
    }

}
