﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImportObjectives : MonoBehaviour {

	// Use this for initialization
	void OnEnable () {

        Debug.Log("ObjectivesController Detected: " + GameObject.Find("ObjectivesController") != null);
        if (GameObject.Find("ObjectivesController")!=null)
        gameObject.GetComponent<Text>().text = GameObject.Find("ObjectivesController").GetComponent<ObjectivePanel>().currentObjectiveTextString;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
