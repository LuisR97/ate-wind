﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerInventoryTransfer : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if(GameObject.FindGameObjectWithTag("InventoryTransfer")!=null) {
            GameObject.Find("InventoryTransfer").GetComponent<InventorySceneTransfer>().OverrideCurrentInventory();
        }
	}
	

}
