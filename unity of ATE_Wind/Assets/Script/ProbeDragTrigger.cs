﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProbeDragTrigger : MonoBehaviour {

	public enum ProbeTypes {high,low };

    public ProbeTypes ProbeType=ProbeTypes.high;
    public measurementAssignment scriptholder;

    private bool isDragging = false;



    //public void BeginDrag() {
    //    if(ProbeType==ProbeTypes.high) {
    //        scriptholder.HighMeasurementButtonToggle();
    //    }else {
    //        scriptholder.LowMeasurementButtonToggle();
    //    }
    //}

    public bool GetIsDraggingBool()
    {

        return isDragging;

    }

    public void SetIsDraggingBool(bool activeState)
    {

        isDragging = activeState;

    }

    //===========================================================================

    public void ProbeOnMouseDrag()
    {

        if (Cursor.visible == true)
        {

            Cursor.visible = false;

        }

        if (isDragging == false)
        {

            isDragging = true;
            scriptholder.SetIsDragging(true);

        }

        //if (scriptholder.GetDragProbeActive() == false)
        //{

        //    scriptholder.ToggleDragProbeActive();

        //}

       // Debug.Log("I am running the drag code!");

        if (ProbeType == ProbeTypes.high)
        {
            if (!scriptholder.GetHighMeasurementBool())
            {

                scriptholder.HighMeasurementButtonToggle();

            }
            
        }
        else
        {
            if (!scriptholder.GetLowMeasurementBool())
            {

                scriptholder.LowMeasurementButtonToggle();

            }
               
        }
    }

    public void EndDrag()
    {

        Cursor.visible = true;

        isDragging = false;
        scriptholder.SetIsDragging(false);

        if (ProbeType == ProbeTypes.high)
        {

            if (scriptholder.GetHighMeasurementBool())
            {

                scriptholder.HighMeasurementButtonToggle();

            }
        
        }
        else
        {

            if (scriptholder.GetLowMeasurementBool())
            {

                scriptholder.LowMeasurementButtonToggle();

            }
        
        }
    }

}
