﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//this long script performs the simple function of culling objects inside the turbine when outside
//and culling objects outside when inside to save performance
public class cullOnInside : MonoBehaviour {

    //this is a list of objects to cull when entering the turbine
    public GameObject[] cullOnEnter;

    //this is a list of objects to cull when exiting the turbine
    public GameObject[] cullOnExit;

    private bool cullOnEnterCheck = false;
    private bool cullOnExitCheck = false;
    private bool cullOnStayCheck = false;

    public GameObject[] overrideOnHiddenObjects;

    public void Start()
    {
        foreach (GameObject obj in cullOnExit)
        {
            //Debug.Log("culling interior objects");
            if (GetComponent<MeshRenderer>() != null)
            {
                GetComponent<MeshRenderer>().enabled = false;
            }


            foreach (MeshRenderer mr in obj.GetComponentsInChildren<MeshRenderer>())
            {
                mr.enabled = false;
                //Debug.Log(mr.gameObject.name + " was culled");
            }
        }
    }

    private void Update()
    {
        
        if (cullOnEnterCheck == true)
        {

            TriggerEnterFunction();
            cullOnEnterCheck = false;

        }

        if (cullOnExitCheck == true)
        {

            TriggerExitFunction();
            cullOnExitCheck = false;

        }

        if (cullOnStayCheck == true)
        {

            TriggerStayFunction();
            cullOnStayCheck = false;

        }



        foreach(GameObject overrideObj in overrideOnHiddenObjects) {
            if(overrideObj.activeSelf==false) {
                Debug.Log("object hidden");
                foreach (GameObject obj in cullOnExit) {
                    if (obj.GetComponent<MeshRenderer>() != null) {
                        obj.GetComponent<MeshRenderer>().enabled = true;
                    }
                    foreach (MeshRenderer mr in obj.GetComponentsInChildren<MeshRenderer>()) {
                        mr.enabled = true;
                    }
                }
            }
        }


    }


    void OnTriggerEnter(Collider other)
    {

        cullOnEnterCheck = true;

    }

    void OnTriggerExit(Collider other)
    {

        cullOnExitCheck = true;

    }




    void OnTriggerStay(Collider other)
    {

        cullOnStayCheck = true;

    }


    void TriggerEnterFunction()
    {

        //Debug.Log("trigger enter culling");
        foreach (GameObject obj in cullOnEnter)
        {
            if (obj.GetComponent<Terrain>() != null)
            {
                obj.SetActive(false);
            }
            if (obj.GetComponent<MeshRenderer>() != null)
            {
                obj.GetComponent<MeshRenderer>().enabled = false;
            }
            foreach (MeshRenderer mr in obj.GetComponentsInChildren<MeshRenderer>())
            {
                mr.enabled = false;
            }
        }

        foreach (GameObject obj in cullOnExit)
        {
            if (obj.GetComponent<MeshRenderer>() != null)
            {
                obj.GetComponent<MeshRenderer>().enabled = true;
            }
            foreach (MeshRenderer mr in obj.GetComponentsInChildren<MeshRenderer>())
            {
                mr.enabled = true;
            }
        }

    }

    void TriggerExitFunction()
    {

        //Debug.Log("trigger exit culling");
        foreach (GameObject obj in cullOnEnter)
        {
            if (obj.GetComponent<Terrain>() != null)
            {
                obj.SetActive(true);
            }
            if (obj.GetComponent<MeshRenderer>() != null)
            {
                obj.GetComponent<MeshRenderer>().enabled = true;
            }
            foreach (MeshRenderer mr in obj.GetComponentsInChildren<MeshRenderer>())
            {
                mr.enabled = true;
            }
        }


        foreach (GameObject obj in cullOnExit)
        {
            if (obj.GetComponent<MeshRenderer>() != null)
            {
                obj.GetComponent<MeshRenderer>().enabled = false;
            }
            foreach (MeshRenderer mr in obj.GetComponentsInChildren<MeshRenderer>())
            {
                mr.enabled = false;
            }
        }

    }
    
    void TriggerStayFunction()
    {

        //Debug.Log("trigger enter culling");
        foreach (GameObject obj in cullOnEnter)
        {
            if (obj.GetComponent<Terrain>() != null)
            {
                obj.SetActive(false);
            }
            if (obj.GetComponent<MeshRenderer>() != null)
            {
                obj.GetComponent<MeshRenderer>().enabled = false;
            }
            foreach (MeshRenderer mr in obj.GetComponentsInChildren<MeshRenderer>())
            {
                mr.enabled = false;
            }
        }

        foreach (GameObject obj in cullOnExit)
        {
            if (obj.GetComponent<MeshRenderer>() != null)
            {
                obj.GetComponent<MeshRenderer>().enabled = true;
            }
            foreach (MeshRenderer mr in obj.GetComponentsInChildren<MeshRenderer>())
            {
                mr.enabled = true;
            }
        }

    } 


}
