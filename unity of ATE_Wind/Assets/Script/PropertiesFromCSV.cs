﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
//using UnityEditor;

public class PropertiesFromCSV : MonoBehaviour {

    //strings to hold content from a file
    private string fileContents;
    private string[] lines;

    //list of main objects and nodes (see classes below)
    public MainObjects[] objects;
    public NodeProperties[] nodes;

    //input field for file name
    public InputField savingFileNameInput;

    public GameObject editPanel;

    public string UploadURL = "localhost/customScenes/saveCsv.php";

    public InputField URLInput;

    public void Start() {

        //used when loading a custom scene
        if(GameObject.Find("CustomSceneHolder") !=null) {
            if(GameObject.Find("CustomSceneHolder").GetComponent<CustomSceneDontDestroyOnLoad>().CustomScenePath!=null && GameObject.Find("CustomSceneHolder").GetComponent<CustomSceneDontDestroyOnLoad>().CustomScenePath!="") {
                if(GameObject.Find("CustomSceneHolder").GetComponent<CustomSceneDontDestroyOnLoad>().isOnServer) {
                    ReadCSVFromServer(GameObject.Find("CustomSceneHolder").GetComponent<CustomSceneDontDestroyOnLoad>().CustomScenePath);
                    Debug.Log(GameObject.Find("CustomSceneHolder").GetComponent<CustomSceneDontDestroyOnLoad>().CustomScenePath);
                } else {
                    ReadString(GameObject.Find("CustomSceneHolder").GetComponent<CustomSceneDontDestroyOnLoad>().CustomScenePath);
                    Debug.Log(GameObject.Find("CustomSceneHolder").GetComponent<CustomSceneDontDestroyOnLoad>().CustomScenePath);
                }
            }
        }


       
        //checks if path exists, if not, it creates it
        string path = Application.dataPath+"/../Custom";
        if (!Directory.Exists(path)){
            Debug.Log("Directory does not exist, creating now: "+path);
            Directory.CreateDirectory(path);
        }
    }


    //reads data from csv file
    public void ReadString(string path) {

        //creates a stream reader and gets all data from file to a string
        StreamReader reader = new StreamReader(path);
        fileContents = reader.ReadToEnd();
        reader.Close();

        //splits string by newlines (each node is on its own line
        lines = fileContents.Split(new char[] {'\n' });

        //gets length of array minues 2 for top (headers) and bottom (empty space)
        nodes = new NodeProperties[lines.Length - 2];

        //loop which creates node for each newline string (defined above by split) by splitting again by commas
        for(int i=1; i<lines.Length-1; i++) {
            string line = lines[i];
            nodes[i - 1] = new NodeProperties();
            NodeProperties node = nodes[i - 1];
            string[] cells = line.Split(new char[] { ',' });

            node.obj = cells[0];
            node.node = cells[1];
            node.voltageValue = float.Parse(cells[2]);

            node.isAC = (cells[3].ToLower() == "yes" || cells[3].ToLower() == "true");
            node.phaseNumber = int.Parse(cells[4]);
            node.isNeutral= (cells[5].ToLower() == "yes" || cells[5].ToLower() == "true");
            node.isSinglePhase = (cells[6].ToLower() == "yes" || cells[6].ToLower() == "true");
            node.isDisconnected = (cells[7].ToLower() == "yes" || cells[7].ToLower() == "true");


            node.BranchParent = cells[8];

        }

        //sends properties to the scene
        TransferProperties();
        
    }


    public void ReadCSVFromServer() {
        string url=URLInput.text;
        StartCoroutine(ProcessCSVFromServer(url));
    }


    public void ReadCSVFromServer(string url) {
        StartCoroutine(ProcessCSVFromServer(url));
    }

    string ServerCSVContents;


    public IEnumerator ProcessCSVFromServer(string url) {

        

        Debug.Log("Searching url: " + url);

        WWW www = new WWW(url);

        if(www.error==null) {
            Debug.Log("no error!");
        }

        while(!www.isDone && www.error==null) {

            Debug.Log(www.progress);

            Debug.Log("Running coroutine");

            yield return www;
            if(www.error==null) {
                Debug.Log("Got Data OK!");
            }

            ServerCSVContents = www.text;

        }

        if (www.error != null) {        //checks if there is an error
            Debug.Log(www.error);
            if (www.error.Contains("404 Not Found")) {  //if there is a 404 error, the username is not found
                Debug.Log("file not found");
            }
        }

        if(www.isDone) {
            Debug.Log("WWW is Done!");
            ReadCSVFromServerCallback(ServerCSVContents);
        }



        Debug.Log("Finished coroutine");
        yield return null;
    }

    public void ReadCSVFromServerCallback(string text) {
        lines = text.Split(new char[] { '\n' });
        Debug.Log(text);

        //gets length of array minues 2 for top (headers) and bottom (empty space)
        nodes = new NodeProperties[lines.Length - 2];

        //loop which creates node for each newline string (defined above by split) by splitting again by commas
        for (int i = 1; i < lines.Length - 1; i++) {
            string line = lines[i];
            nodes[i - 1] = new NodeProperties();
            NodeProperties node = nodes[i - 1];
            string[] cells = line.Split(new char[] { ',' });

            node.obj = cells[0];
            node.node = cells[1];
            node.voltageValue = float.Parse(cells[2]);

            node.isAC = (cells[3].ToLower() == "yes" || cells[3].ToLower() == "true");
            node.phaseNumber = int.Parse(cells[4]);
            node.isNeutral = (cells[5].ToLower() == "yes" || cells[5].ToLower() == "true");
            node.isSinglePhase = (cells[6].ToLower() == "yes" || cells[6].ToLower() == "true");
            node.isDisconnected = (cells[7].ToLower() == "yes" || cells[7].ToLower() == "true");


            node.BranchParent = cells[8];

        }

        //sends properties to the scene
        TransferProperties();
    }





    //creates a new node based on a new measurement object in the scene
    public NodeProperties NodeFromScene(NewMeasurement sceneNode) {
        NodeProperties tempNode = new NodeProperties();
        tempNode.node = sceneNode.gameObject.name;
        tempNode.voltageValue = sceneNode.voltageValue;
        tempNode.isAC = sceneNode.isAC;
        tempNode.phaseNumber = sceneNode.phaseNumber;
        tempNode.isNeutral = sceneNode.isNeutral;
        tempNode.isSinglePhase = sceneNode.isSinglePhase;
        tempNode.isDisconnected = sceneNode.isDisconnected;

        //code for handling an empty node parent case
        //if(sceneNode.branchParent!=null) {
        //    tempNode.BranchParent = sceneNode.branchParent.name;
        //}else {
        //   tempNode.BranchParent = "null";
        //}
        tempNode.BranchParent = sceneNode.branchParentName;
        //tempNode.locationDescription = sceneNode.gameObject.transform.GetChild(2).GetChild(0).gameObject.GetComponent<Text>().text;
        tempNode.locationDescription = sceneNode.gameObject.transform.GetChild(2).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text;

        tempNode.locationDescription = tempNode.locationDescription.Replace("\r", string.Empty).Replace("\n", " ");

        //code for properly formatting the transform parent field with proper names
        if (sceneNode.transform.parent.parent.name.Contains("Center")){
            tempNode.obj = "CenterBox";
        }else if(sceneNode.transform.parent.parent.name.Contains("Axis")){
            if(sceneNode.transform.parent.parent.name.Contains("1")){
                tempNode.obj = "AxisBox1";
            } else if (sceneNode.transform.parent.parent.name.Contains("2")){
                tempNode.obj = "AxisBox2";
            } else if (sceneNode.transform.parent.parent.name.Contains("3")){
                tempNode.obj = "AxisBox3";
            }
        } else if(sceneNode.transform.parent.parent.name.Contains("Battery")){
            if (sceneNode.transform.parent.parent.name.Contains("1")){
                tempNode.obj = "BatteryBox1";
            } else if (sceneNode.transform.parent.parent.name.Contains("2")){
                tempNode.obj = "BatteryBox2";
            } else if (sceneNode.transform.parent.parent.name.Contains("3")){
                tempNode.obj = "BatteryBox3";
            }
        } else if (sceneNode.transform.parent.parent.parent.parent.name.Contains("Top"))
        {
            tempNode.obj = "TopBox";
        }

        return tempNode;
    }


    //transfers properties to scene
    public void TransferProperties() {

        foreach(NodeProperties node in nodes) {
            MainObjects mainObject = null;
            GameObject targetNode = null;

            foreach(MainObjects obj in objects) {
                if (node.obj == obj.obj){
                    //Debug.Log("object found: "+node.obj);
                    mainObject = obj;
                }
            }   //end search for object

            if(mainObject!=null) {
                GameObject voltageParent = mainObject.voltageButtonsParent;

                Debug.Log("---------------" + mainObject.obj);
                foreach(Transform child in voltageParent.GetComponentsInChildren<Transform>()) {
                    if(child.name==node.node) {
                        //Debug.Log("node found: "+node.node);
                        targetNode = child.gameObject;
                    }
                }
            }   //end search for node

            if(targetNode!=null) {
                NewMeasurement nm = targetNode.GetComponent<NewMeasurement>();

                nm.voltageValue = node.voltageValue;
                nm.isAC = node.isAC;
                nm.phaseNumber = node.phaseNumber;
                nm.isNeutral = node.isNeutral;
                nm.isSinglePhase = node.isSinglePhase;
                nm.isDisconnected = node.isDisconnected;


                //nm.branchParent = node.BranchParent;

                if(node.BranchParent!=null && node.BranchParent.Contains("-")) {
                    Debug.Log(node.BranchParent);
                    string[] splitString = node.BranchParent.Split(new char[] {'-' });
                    Debug.Log(splitString[0]);
                    Debug.Log(splitString[1]);

                    string parentMainObjectString = splitString[0];
                    string parentNodeString = splitString[1];

                    Debug.Log("Main object is: " + parentMainObjectString + ", and node is: " + parentNodeString);

                    MainObjects parentMainObject = GetMainObjectByName(parentMainObjectString);
                    GameObject parentNode = parentMainObject.voltageButtonsParent.transform.Find(parentNodeString).gameObject;

                    nm.branchParent = parentNode.GetComponent<NewMeasurement>();
                }else {
                    nm.branchParent = null;
                }

            }


        }//end main node loop

    }



    public MainObjects GetMainObjectByName(string name) {
        
        foreach(MainObjects obj in objects) {
            if(name==obj.obj) {
                return obj;
            }
        }
        return null;
    }

    //outputs custom scene from node editor to a csv file
    public void SaveScene() {
        //gets file name from input field
        string fileName = savingFileNameInput.text;



        //creates a list of nodes for adding later
        List<NodeProperties> nodes = new List<NodeProperties>();

        //for each node in the scene, add a node from scene object to the list
        foreach(MainObjects obj in objects) {
            foreach(NewMeasurement childNode in obj.voltageButtonsParent.GetComponentsInChildren<NewMeasurement>()) {
                nodes.Add(NodeFromScene(childNode));
            }

        }

        //sends nodes to be written to file
        WriteTOCSV(nodes, fileName);
       
    }


    public void UploadScene() {
        //gets file name from input field

        Debug.Log("uploading to server at url: " + UploadURL);
        string fileName = savingFileNameInput.text;



        //creates a list of nodes for adding later
        List<NodeProperties> nodes = new List<NodeProperties>();

        //for each node in the scene, add a node from scene object to the list
        foreach (MainObjects obj in objects) {
            foreach (NewMeasurement childNode in obj.voltageButtonsParent.GetComponentsInChildren<NewMeasurement>()) {
                nodes.Add(NodeFromScene(childNode));
            }

        }

        StartCoroutine(UploadProcess(NodesToString(nodes), fileName));

        //sends nodes to be written to file
        //WriteTOCSV(nodes, fileName);


    }

    IEnumerator UploadProcess(string fileContents, string fileName) {

        string url = "";

        WWWForm form = new WWWForm();

        form.AddField("fileName", fileName);
        form.AddField("csvContents", fileContents);

        WWW w = new WWW(UploadURL, form);
        yield return w;
        if(w.error!=null) {
            Debug.Log(w.error);
            if(w.error.Contains("URL")){
                Debug.Log(url);
            }
        }else {
            Debug.Log("successfully uploaded file");
        }

        yield return null;
    }


    //outputs nodes to desired filename
    public void WriteTOCSV(List<NodeProperties> nodes,string name) {
        //defines file path and extension
        string path = "Custom/"+name.Trim()+".csv";

        //defines header string
        string tempString = "Object,Node,Voltage,IsAC,Phase#,Neutral,Single Phase,Disconnected,Branch Parent Name,Location Description\n";

        //for each node, creates a string for each property (plus a newline at the very end
        foreach(NodeProperties node in nodes) {
            string nodeString = node.obj + "," + node.node + "," + node.voltageValue + "," + node.isAC + "," + node.phaseNumber + "," + node.isNeutral + "," + node.isSinglePhase + "," + node.isDisconnected + "," + node.BranchParent + "," + node.locationDescription;
            tempString += nodeString + "\n";
        }

        //creates a writer at the path
        StreamWriter writer = new StreamWriter(path);

        //outputs a trimmed string
        writer.Write(tempString.Trim(new char[] {'\n' }));

        //ShowInExplorer(path);

        //closes writer
        writer.Close();


    }

    public string NodesToString(List<NodeProperties> nodes) {
        //defines file path and extension

        //defines header string
        string tempString = "Object,Node,Voltage,IsAC,Phase#,Neutral,Single Phase,Disconnected,Branch Parent Name,Is Disabled,Location Description\n";

        //for each node, creates a string for each property (plus a newline at the very end
        foreach (NodeProperties node in nodes) {
            string nodeString = node.obj + "," + node.node + "," + node.voltageValue + "," + node.isAC + "," + node.phaseNumber + "," + node.isNeutral + "," + node.isSinglePhase + "," + node.isDisconnected + "," + node.BranchParent + "," + node.locationDescription;
            tempString += nodeString + "\n";
        }

        return tempString;


    }





    public void ShowInExplorer() {
        string path = Application.dataPath.Replace("/Assets","").Replace("/wind_Data","").Replace("/Wind.app/Contents","")+"/Custom/" + savingFileNameInput.text.Trim() + ".csv";
        //Debug.Log(path);
        //path = path.Replace(@"/", @"\");
        //EditorUtility.RevealInFinder(path);
        //System.Diagnostics.Process.Start("explorer.exe", "/select" + path);
        openInExplorer(path);
    }



    static void openInExplorer(string path) {
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
            path = path.Replace("/", @"\");
            Debug.Log(path);
            Debug.LogError(path);
            string arg = "/select,\"" + path + "\"";
            System.Diagnostics.Process.Start("explorer.exe", arg);
        } else if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
            Debug.Log(path);
            Debug.LogError(path);
            System.Diagnostics.Process.Start("open", "-R " + path);
        }
    }





}


//class used to organize all properties of the nodes
[System.Serializable]
public class NodeProperties {
    public string obj="null";
    public string node="null";
    public float voltageValue=0;
    public bool isAC;
    public int phaseNumber=1;
    public bool isNeutral;
    public bool isSinglePhase;
    public bool isDisconnected;
    public string BranchParent="null";
    public string locationDescription;
}


//class used to link a string name and gameobject for voltage button parents
[System.Serializable]
public class MainObjects {
    public string obj;
    public GameObject voltageButtonsParent;
    //public GameObject locationDescriptionsParent;
}