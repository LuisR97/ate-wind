﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class VolumeController : MonoBehaviour {

	public AudioSource[] m_audioSources;
	public Sprite m_volumeIcon;
	public Sprite m_muteVolumeIcon;
	public Button m_volumeButton;
	private bool m_check = false;
	// Use this for initialization
	void Start () {
		m_volumeButton.image.overrideSprite = m_volumeIcon;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void CheckVolume() {
		if (!m_check) {
			MuteVolume ();
			m_check = true;
		} else {
			UnMuteVolume ();
			m_check = false;
		}
	}

	public void MuteVolume() {
		foreach(AudioSource audioSource in m_audioSources) {
			audioSource.volume = 0;
		}
		m_volumeButton.image.overrideSprite = m_muteVolumeIcon;
	}

	public void UnMuteVolume() {
		foreach(AudioSource audioSource in m_audioSources) {
			audioSource.volume = 1;
		}
		m_volumeButton.image.overrideSprite = m_volumeIcon;
	}
}
