﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckForSpinning : MonoBehaviour
{
    public GameObject scenarioDescriptionLoader;
    public ScenarioDescriptionLoader sceneLoader;
    public string scenario;
    public bool rotatorScriptIsOn = true;

    // Start is called before the first frame update
    void Start()
    {
        scenarioDescriptionLoader = GameObject.Find("ScenarioDescriptionLoader");

        if (scenarioDescriptionLoader != null)
        {

            //Debug.Log("I have confirmed that the ScenarioDescriptionLoader has been found!");

            sceneLoader = scenarioDescriptionLoader.GetComponent<ScenarioDescriptionLoader>();
            scenario = sceneLoader.scenarioName;

            //scenario = scenarioDescriptionLoader.GetComponent<ScenarioDescriptionLoader>().scenarioName;

        }

        if (scenarioDescriptionLoader != null && scenario == "TopBox")
        {

            //Debug.Log("I have confirmed that the scenario name is TopBox!");

            GameObject.Find("anemometer02").GetComponent<Rotator>().enabled = false;
        }

    }
}