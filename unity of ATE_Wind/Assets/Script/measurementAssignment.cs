using UnityEngine;
using System.Collections;
using UnityEngine.UI;

#pragma warning disable 0414

public class measurementAssignment : MonoBehaviour {
	
	public GameObject measurementReadoutMenu;
	public Text measurementReadout;
	public Text[] probeSelected;
	public Text unitMeasurementText;
    public Text acOrDc;
     
	private float highMeasurementValue = 0f;
	private float highMeasurementFixedValue = 0f;
	private bool highMeasurementIsTaken = false;
	private bool highMeasurementButtonPressed = false;
	private bool highMeasurementLocationPressed = false;
	private float lowMeasurementValue = 0f;
	private float lowMeasurementFixedValue = 0f;
	private bool lowMeasurementIsTaken = false;
	private bool lowMeasurementButtonPressed = false;
	private bool lowMeasurementLocationPressed = false;
	private bool newLowMeasurementIsTaken = false;
	private bool newHighMeasurementIsTaken = false;
	private bool isAC = false;
	private bool lowIsAC = false;
	private bool highIsAC = false;
    private int acPhase = 1;
    private int highPhase = 1;
    private int lowPhase = 1;
    private bool newMeasurementTaken = false;

    [SerializeField]
	private bool isDisconnected = false;

    [SerializeField]
    private bool isNeutral = false;

    [SerializeField]
    private bool isSinglePhase =false;
    private bool lowSinglePhase = false;
    private bool highSinglePhase = false;

    private float lineToLineFactor = Mathf.Sqrt(3f);
    private float singlePhaseFactor = 1;

	private float ampageValue = 0;
	private float ampageFixedValue = 0;

	private float resistanceValue = 0;
	private float resistanceFixedValue = 0;

	private int highMeasurementId = 0;
	private int lowMeasurementId = 0;

	private int objectNumber = 0;
	private int componentNumber = 0;
	private float overallMeasurement;
	private bool voltageOn = true;
	private bool ampageOn = false;
	private bool resistanceOn = false;
	private bool replaceFuseOn = false;

    //variable used for disconnecting AC nodes on manual mode
    public bool isOnManual = false;

    //variables used for adjusting the randomness of the measurements
    public float percentageOfRandomness = 2.5f;
    private float RandomnessValue = 10f; //this value will be calculated using the current value of meter and PoR
    private float randomnessIntervalValue = .1f;
    private float randomSteadyState;
    private bool maxedInterval = false;

    //variables used to differentiate between drag probes and button probes
    private bool dragProbeIsActive = false;
    private bool isDragging = false;
    private bool highIsHovering = false;
    private bool lowIsHovering = false;
    private bool isHovering = false;
    private bool keepHighMeasurement = false;
    private bool keepLowMeasurement = false;

    private IEnumerator randomnessGeneration;
    private IEnumerator timedDebugLoop;

	// Use this for initialization
	void Start () {

        randomnessGeneration = RandomnessGeneration();
        timedDebugLoop = TimedDebugLoop();

        measurementReadout.text = "0.00";
        StartCoroutine(randomnessGeneration);
        StartCoroutine(timedDebugLoop);


	}
	
	// Update is called once per frame
	void Update () {

        //newLowMeasurementIsTaken = false;
        //newHighMeasurementIsTaken = false;
        DragIsActiveRoutine();
		UpdateReadingParameters();

        if (dragProbeIsActive == false)
        {

            Reading();

        } else
        {
            UpdateKeepMeasurementRoutine();
            DragProbeReading ();

        }

        //AfterMeasurementDragProbeCheck();
		
        AssignHighLowText(probeSelected);

	}

    public void SetLowIsHovering(bool activeState)
    {

        lowIsHovering = activeState;

    }

    public bool GetLowIsHovering()
    {

        return lowIsHovering;

    }

    public void ToggleLowIsHovering()
    {

        lowIsHovering = !lowIsHovering;

    }

    public void SetHighIsHovering(bool activeState)
    {

        highIsHovering = activeState;

    }

    public bool GetHighIsHovering()
    {

        return highIsHovering;

    }

    public void ToggleHighIsHovering()
    {

        highIsHovering = !highIsHovering;

    }

    public void SetIsDragging(bool activeState)
    {

        isDragging = activeState;

    }

    public bool GetIsDragging()
    {

        return isDragging;

    }

    public void ToggleIsDragging()
    {

        isDragging = !isDragging;

    }

    public void SetDragProbeActive(bool activeState)
    {

        dragProbeIsActive = activeState;

    }

    public bool GetDragProbeActive()
    {

        return dragProbeIsActive;

    }

    public void ToggleDragProbeActive()
    {

        dragProbeIsActive = !dragProbeIsActive;

    }

    private void DragIsActiveRoutine()
    {

        if(highIsHovering == true || lowIsHovering == true)
        {

            isHovering = true;

        } else
        {

            isHovering = false;

        }

        if(isHovering == true || isDragging == true)
        {

            dragProbeIsActive = true;

        } else
        {

            dragProbeIsActive = false;

        }

    }

    private void UpdateKeepMeasurementRoutine ()
    {

        if(keepLowMeasurement == true)
        {

            if (lowIsHovering == false)
            {

                keepLowMeasurement = false;
                lowMeasurementIsTaken = false;

            }

        }

        if (keepHighMeasurement == true)
        {

            if (highIsHovering == false)
            {

                keepHighMeasurement = false;
                highMeasurementIsTaken = false;

            }

        }

    } 

	public void LocationIsDisconnected () {

		isDisconnected = true;

	}

    public void LocationPhase(int a) {

        acPhase = a;

    }

    public void IsSinglePhase()
    {

        isSinglePhase = true;

    }

    public void IsNeutral()
    {

        isNeutral = true;

    }

    private void SetHighLocationPhase() {

        if (!keepHighMeasurement)
        {

            if (isNeutral)
            {

                highPhase = 0;

            }
            else
            {

                highPhase = acPhase;

            }

            highSinglePhase = isSinglePhase;

            Debug.Log("setting high phase to: " + isSinglePhase);

            isNeutral = false;
            isSinglePhase = false;

            

        }  

    }

    private void SetLowLocationPhase()
    {

        if (!keepLowMeasurement)
        {

            if (isNeutral)
            {

                lowPhase = 0;

            }
            else
            {

                lowPhase = acPhase;

            }

            lowSinglePhase = isSinglePhase;

            Debug.Log("setting low phase to: " + isSinglePhase);

            isNeutral = false;
            isSinglePhase = false;

            

        }     

    }

    public void LocationIsAC () {

		isAC = true;

        //isDisconnected = isOnManual;

	}

	private void HighLocationACCheck () {

		if (isAC == true) {
			
			highIsAC = true;
			isAC = false;

		}


	}

	private void LowLocationACCheck () {

			if (isAC == true) {

				lowIsAC = true;
				isAC = false;

			}

	}

	public float ReturnMeasurementValue () {

		return highMeasurementValue;

	}

	public void HighMeasurementButtonToggle (){
	
		highMeasurementButtonPressed = !highMeasurementButtonPressed;
		lowMeasurementButtonPressed = false;

	}

	public void HighMeasurementLocationButtonToggle (){
	
		if (highMeasurementButtonPressed == true) {
			
			highMeasurementLocationPressed = !highMeasurementLocationPressed;
		
		}
	}

	public void LowMeasurementButtonToggle (){

		lowMeasurementButtonPressed = !lowMeasurementButtonPressed;
		highMeasurementButtonPressed = false;

	}

	public void LowMeasurementLocationButtonToggle (){

		if (lowMeasurementButtonPressed == true) {
		
			lowMeasurementLocationPressed = !lowMeasurementLocationPressed;
		
		}
	}

	public bool GetNewHighMeasurementTakenBool () {

		return newHighMeasurementIsTaken;

	}

	public bool GetNewLowMeasurementTakenBool () {

		return newLowMeasurementIsTaken;

	}

	public bool GetHighMeasurementTakenBool () {

		return highMeasurementIsTaken;

	}

	public bool GetLowMeasurementTakenBool () {

		return lowMeasurementIsTaken;

	}

	public bool GetHighMeasurementBool () {

		return highMeasurementButtonPressed;

	}

	public bool GetLowMeasurementBool () {

		return lowMeasurementButtonPressed;

	}

	public bool GetHighLocationPressed () {

		return highMeasurementLocationPressed;

	}

	public bool GetLowLocationPressed () {

		return lowMeasurementLocationPressed;

	}

	public void ResetNewMeasurements () {

		newHighMeasurementIsTaken = false;
		newLowMeasurementIsTaken = false;
		isAC = false;

	}

	public void SetObjectNumber (int a) {

		objectNumber = a;

	}

	public void SetComponentNumber (int b) {
	
		componentNumber = b;
	
	}

	public int GetObjectNumber () {
	
		return objectNumber;
	
	}

	public int GetComponentNumber () {
	
		return componentNumber;
	
	}

	public void SetHighMeasurementValue (float a){
	
		highMeasurementValue = a;
	
	}

	public void SetLowMeasurementValue (float b){
	
		lowMeasurementValue = b;
	
	}

	public void SetHighMeasurementID (int b){

		highMeasurementId = b;

	}

	public void SetLowMeasurementID (int b){

		lowMeasurementId = b;

	}

	public void SetAmpage (float b) {

		ampageValue = b;

	}

	public void SetResistance (float b) {

		resistanceValue = b;

	}

	public void ResetReadings () {

		highMeasurementIsTaken = false;
		lowMeasurementIsTaken = false;

	}

	public float VoltageReading () {

		overallMeasurement = highMeasurementFixedValue - lowMeasurementFixedValue;
		return overallMeasurement;

	}


	public void AssignHighLowText (Text[] a) {
	
		if (highMeasurementButtonPressed == true) {

            foreach (Text b in a)
            {

                b.text = "High";

            }

		} else if (lowMeasurementButtonPressed == true) {

            foreach (Text b in a)
            {

                b.text = "Low";

            }

		} else {

            foreach (Text b in a)
            {

                b.text = "None";

            }

		}
	
	}

    private void AfterMeasurementDragProbeCheck ()
    {

        if (highIsHovering == true && highMeasurementIsTaken == true)
        {

            keepHighMeasurement = true;

        }
        else
        {

            highIsAC = false;
            keepHighMeasurement = false;

        }
        if (lowIsHovering == true && lowMeasurementIsTaken == true)
        {

            keepLowMeasurement = true;

        }
        else
        {

            lowIsAC = false;
            keepLowMeasurement = false;

        }

    }

	private void Reading () {

        

        if (voltageOn == true) {
			
			if (highMeasurementButtonPressed == true & highMeasurementLocationPressed == true) {

                

				if (highMeasurementIsTaken == true) {

					newHighMeasurementIsTaken = true;

				}

			
					
				HighLocationACCheck ();
                SetHighLocationPhase();
				highMeasurementIsTaken = true;
				highMeasurementFixedValue = highMeasurementValue;
				highMeasurementButtonPressed = false;
				highMeasurementLocationPressed = false;
				Debug.Log ("highMeasurement value: " + highMeasurementFixedValue);

			}

			if (lowMeasurementButtonPressed == true & lowMeasurementLocationPressed == true) {

				if (lowMeasurementIsTaken == true) {

					newLowMeasurementIsTaken = true;

				}


					
				LowLocationACCheck ();
                SetLowLocationPhase();
				lowMeasurementIsTaken = true;
				lowMeasurementFixedValue = lowMeasurementValue;
				lowMeasurementButtonPressed = false;
				lowMeasurementLocationPressed = false;
				Debug.Log ("lowMeasurement value: " + lowMeasurementFixedValue);

			}

			if (highMeasurementIsTaken == true & lowMeasurementIsTaken == true) {


				if (isDisconnected == true) {

					overallMeasurement = 0f;
                    string a = overallMeasurement.ToString("F");
                    measurementReadout.text = a;
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = "Volts";
					Debug.Log (isDisconnected);
                    if (lowIsAC == true || highIsAC == true)
                    {

                        acOrDc.text = "AC";

                    } else
                    {

                        acOrDc.text = "DC";

                    }



				} else if (highIsAC == true & lowIsAC == true) {

                    acOrDc.text = "AC";

                    if ((highPhase == 0) || (lowPhase == 0))
                    {

                        if (highPhase == 0)
                        {
                            if (lowPhase == 0)
                            {

                                overallMeasurement = 0;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }

                            else if (lowSinglePhase)

                            {

                                overallMeasurement = lowMeasurementFixedValue / singlePhaseFactor;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }
                            else
                            {

                                overallMeasurement = lowMeasurementFixedValue / lineToLineFactor;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }

                        }

                        if (lowPhase == 0)
                        {
                            if (highPhase == 0)
                            {

                                overallMeasurement = 0;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }
                            else if (highSinglePhase)
                            {

                                overallMeasurement = highMeasurementFixedValue / singlePhaseFactor;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }
                            else
                            {

                                overallMeasurement = highMeasurementFixedValue / lineToLineFactor;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }
                        }

                    }
                    else if (highPhase != lowPhase)
                    {
                        {

                            if (highMeasurementFixedValue == lowMeasurementFixedValue)
                            {

                                overallMeasurement = highMeasurementFixedValue;
                                string a = overallMeasurement.ToString("F");
                                measurementReadout.text = a;
                                measurementReadoutMenu.SetActive(true);
                                highMeasurementIsTaken = false;
                                lowMeasurementIsTaken = false;
                                unitMeasurementText.text = "Volts";
                                highIsAC = false;
                                lowIsAC = false;
                                Debug.Log(overallMeasurement);

                            }

                        }
                    } else if (highPhase == lowPhase)
                    {

                        measurementReadout.text = "Invalid";
                        measurementReadoutMenu.SetActive(true);
                        highMeasurementIsTaken = false;
                        lowMeasurementIsTaken = false;
                        unitMeasurementText.text = " ";

                    }


				} else if ((highIsAC == true & lowIsAC == false) || (highIsAC == false & lowIsAC == true)) {

							measurementReadout.text = "Invalid";
							measurementReadoutMenu.SetActive (true);
							highMeasurementIsTaken = false;
							lowMeasurementIsTaken = false;
							unitMeasurementText.text = " ";
					        Debug.Log (overallMeasurement);

				} else {

                    acOrDc.text = "DC";
					overallMeasurement = VoltageReading ();
                    string a = overallMeasurement.ToString("F");
                    measurementReadout.text = a;
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = "Volts";
					Debug.Log (overallMeasurement);

				}

				highIsAC = false;
				lowIsAC = false;
				isDisconnected = false;

                if (overallMeasurement >= 10f)
                {

                    RandomnessValue = overallMeasurement;

                }
                else
                {

                    RandomnessValue = 10f;

                }

                if (measurementReadout.text != "Invalid")
                {
                    randomnessIntervalValue = .1f;
                    maxedInterval = false;
                }

                //Debug.Log("Randomness Value = " + RandomnessValue);

            }
				
		}



		

		if (ampageOn == true) {

			if (highMeasurementButtonPressed == true & highMeasurementLocationPressed == true) {

				if (highMeasurementIsTaken == true) {

					newHighMeasurementIsTaken = true;

				}
					
				highMeasurementIsTaken = true;
				ampageFixedValue = ampageValue;
				highMeasurementButtonPressed = false;
				highMeasurementLocationPressed = false;
								 
			}

			if (lowMeasurementButtonPressed == true & lowMeasurementLocationPressed == true) {

				if (lowMeasurementIsTaken == true) {

					newLowMeasurementIsTaken = true;

				}

				lowMeasurementIsTaken = true;
				lowMeasurementButtonPressed = false;
				lowMeasurementLocationPressed = false;

			}

			if (highMeasurementIsTaken == true & lowMeasurementIsTaken == true) {

				if (highMeasurementId == lowMeasurementId) {
					
					overallMeasurement = ampageFixedValue;
                    string a = overallMeasurement.ToString("F");
                    measurementReadout.text = a;
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = "Amps";

				} else {

					measurementReadout.text = "Invalid";
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = " ";

				}
					
			}

		}

		if (resistanceOn == true) {

			if (highMeasurementButtonPressed == true & highMeasurementLocationPressed == true) {

				if (highMeasurementIsTaken == true) {

					newHighMeasurementIsTaken = true;

				}

				highMeasurementIsTaken = true;
				resistanceFixedValue = resistanceValue;
				highMeasurementButtonPressed = false;
				highMeasurementLocationPressed = false;

			}

			if (lowMeasurementButtonPressed == true & lowMeasurementLocationPressed == true) {

				if (lowMeasurementIsTaken == true) {

					newLowMeasurementIsTaken = true;

				}

				lowMeasurementIsTaken = true;
				lowMeasurementButtonPressed = false;
				lowMeasurementLocationPressed = false;

			}

			if (highMeasurementIsTaken == true & lowMeasurementIsTaken == true) {

				if (highMeasurementId == lowMeasurementId) {

					overallMeasurement = resistanceFixedValue;
					measurementReadout.text = System.Convert.ToString (overallMeasurement);
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = "Ohms";

				} else {

					measurementReadout.text = "Invalid";
					measurementReadoutMenu.SetActive (true);
					highMeasurementIsTaken = false;
					lowMeasurementIsTaken = false;
					unitMeasurementText.text = " ";

				}


                if (overallMeasurement >= 10f)
                {

                    RandomnessValue = overallMeasurement;

                }
                else
                {

                    RandomnessValue = 10f;

                }

                if (measurementReadout.text != "Invalid")
                {
                    randomnessIntervalValue = .1f;
                    maxedInterval = false;
                }
               

            }


        }
	}

    private void DragProbeReading()
    {


        //Debug.Log("Drag Probe Active");

        if (voltageOn == true)
        {


            if (highMeasurementButtonPressed == true & highMeasurementLocationPressed == true)
            {
                

                if(keepHighMeasurement == false)
                {

                    if (highMeasurementIsTaken == true)
                    {

                        newHighMeasurementIsTaken = true;

                    }



                    HighLocationACCheck();
                    SetHighLocationPhase();
                    highMeasurementIsTaken = true;
                    highMeasurementFixedValue = highMeasurementValue;
                    highMeasurementLocationPressed = false;
                    Debug.Log("highMeasurement value: " + highMeasurementFixedValue);

                }

            }
               

            if (lowMeasurementButtonPressed == true & lowMeasurementLocationPressed == true)
            {

                if(keepLowMeasurement == false)
                {

                    if (lowMeasurementIsTaken == true)
                    {

                        newLowMeasurementIsTaken = true;

                    }


                    LowLocationACCheck();
                    SetLowLocationPhase();
                    lowMeasurementIsTaken = true;
                    lowMeasurementFixedValue = lowMeasurementValue;
                    lowMeasurementLocationPressed = false;
                    Debug.Log("lowMeasurement value: " + lowMeasurementFixedValue);

                }

            }
                
            if (!(keepLowMeasurement && keepHighMeasurement))
            {

                if (highMeasurementIsTaken == true & lowMeasurementIsTaken == true)
                {


                    if (isDisconnected == true)
                    {

                        overallMeasurement = 0f;
                        string a = overallMeasurement.ToString("F");
                        measurementReadout.text = a;
                        measurementReadoutMenu.SetActive(true);
                        //highMeasurementIsTaken = false;
                        //lowMeasurementIsTaken = false;
                        unitMeasurementText.text = "Volts";
                        Debug.Log(isDisconnected);
                        if (lowIsAC == true || highIsAC == true)
                        {

                            acOrDc.text = "AC";

                        }
                        else
                        {

                            acOrDc.text = "DC";

                        }



                    }
                    else if (highIsAC == true & lowIsAC == true)
                    {

                        acOrDc.text = "AC";

                        if ((highPhase == 0) || (lowPhase == 0))
                        {

                            Debug.Log("high phase: " + highSinglePhase);
                            Debug.Log("low phase: " + lowSinglePhase);

                            if (highPhase == 0)
                            {
                                if (lowPhase == 0)
                                {

                                    overallMeasurement = 0;
                                    string a = overallMeasurement.ToString("F");
                                    measurementReadout.text = a;
                                    measurementReadoutMenu.SetActive(true);
                                    //highMeasurementIsTaken = false;
                                    //lowMeasurementIsTaken = false;
                                    unitMeasurementText.text = "Volts";
                                    //highIsAC = false;
                                    // lowIsAC = false;
                                    Debug.Log(overallMeasurement);

                                }

                                else if (lowSinglePhase)

                                {

                                    overallMeasurement = lowMeasurementFixedValue / singlePhaseFactor;
                                    string a = overallMeasurement.ToString("F");
                                    measurementReadout.text = a;
                                    measurementReadoutMenu.SetActive(true);
                                    //highMeasurementIsTaken = false;
                                    //lowMeasurementIsTaken = false;
                                    unitMeasurementText.text = "Volts";
                                    //highIsAC = false;
                                    //lowIsAC = false;
                                    Debug.Log(overallMeasurement);

                                }
                                else
                                {

                                    overallMeasurement = lowMeasurementFixedValue / lineToLineFactor;
                                    string a = overallMeasurement.ToString("F");
                                    measurementReadout.text = a;
                                    measurementReadoutMenu.SetActive(true);
                                    //highMeasurementIsTaken = false;
                                    //lowMeasurementIsTaken = false;
                                    unitMeasurementText.text = "Volts";
                                    //highIsAC = false;
                                    //lowIsAC = false;
                                    Debug.Log(overallMeasurement);

                                }

                            }

                            if (lowPhase == 0)
                            {
                                if (highPhase == 0)
                                {

                                    overallMeasurement = 0;
                                    string a = overallMeasurement.ToString("F");
                                    measurementReadout.text = a;
                                    measurementReadoutMenu.SetActive(true);
                                    //highMeasurementIsTaken = false;
                                    //lowMeasurementIsTaken = false;
                                    unitMeasurementText.text = "Volts";
                                    //highIsAC = false;
                                    //lowIsAC = false;
                                    Debug.Log(overallMeasurement);

                                }
                                else if (highSinglePhase)
                                {

                                    overallMeasurement = highMeasurementFixedValue / singlePhaseFactor;
                                    string a = overallMeasurement.ToString("F");
                                    measurementReadout.text = a;
                                    measurementReadoutMenu.SetActive(true);
                                    //highMeasurementIsTaken = false;
                                    //lowMeasurementIsTaken = false;
                                    unitMeasurementText.text = "Volts";
                                    //highIsAC = false;
                                    //lowIsAC = false;
                                    Debug.Log(overallMeasurement);

                                }
                                else
                                {

                                    overallMeasurement = highMeasurementFixedValue / lineToLineFactor;
                                    string a = overallMeasurement.ToString("F");
                                    measurementReadout.text = a;
                                    measurementReadoutMenu.SetActive(true);
                                    //highMeasurementIsTaken = false;
                                    //lowMeasurementIsTaken = false;
                                    unitMeasurementText.text = "Volts";
                                    //highIsAC = false;
                                    //lowIsAC = false;
                                    Debug.Log(overallMeasurement);

                                }
                            }

                        }
                        else if (highPhase != lowPhase)
                        {
                            {

                                if (highMeasurementFixedValue == lowMeasurementFixedValue)
                                {

                                    overallMeasurement = highMeasurementFixedValue;
                                    string a = overallMeasurement.ToString("F");
                                    measurementReadout.text = a;
                                    measurementReadoutMenu.SetActive(true);
                                    //highMeasurementIsTaken = false;
                                    //lowMeasurementIsTaken = false;
                                    unitMeasurementText.text = "Volts";
                                    //highIsAC = false;
                                    //lowIsAC = false;
                                    Debug.Log(overallMeasurement);

                                }

                            }
                        }
                        else if (highPhase == lowPhase)
                        {

                            measurementReadout.text = "Invalid";
                            measurementReadoutMenu.SetActive(true);
                            //highMeasurementIsTaken = false;
                            //lowMeasurementIsTaken = false;
                            unitMeasurementText.text = " ";

                        }


                    }
                    else if ((highIsAC == true & lowIsAC == false) || (highIsAC == false & lowIsAC == true))
                    {

                        measurementReadout.text = "Invalid";
                        measurementReadoutMenu.SetActive(true);
                        //highMeasurementIsTaken = false;
                        //lowMeasurementIsTaken = false;
                        unitMeasurementText.text = " ";
                        Debug.Log(overallMeasurement);

                    }
                    else
                    {

                        acOrDc.text = "DC";
                        overallMeasurement = VoltageReading();
                        string a = overallMeasurement.ToString("F");
                        measurementReadout.text = a;
                        measurementReadoutMenu.SetActive(true);
                        //highMeasurementIsTaken = false;
                        //lowMeasurementIsTaken = false;
                        unitMeasurementText.text = "Volts";
                        Debug.Log(overallMeasurement);

                    }

                    if (measurementReadout.text != "Invalid")
                    {
                        randomnessIntervalValue = .1f;
                        maxedInterval = false;
                    }

                }
            

                AfterMeasurementDragProbeCheck();

                //Debug.Log("High State:" + highIsHovering);
                //Debug.Log("Low State:" + lowIsHovering);
                
                if (highMeasurementIsTaken == true && lowMeasurementIsTaken == true)
                {
                    if (keepHighMeasurement == false)
                    {

                        highMeasurementIsTaken = false;

                    }

                    if (keepLowMeasurement == false)
                    {

                        lowMeasurementIsTaken = false;

                    }   

                }
                






                isDisconnected = false;

                if (overallMeasurement >= 10f)
                {

                    RandomnessValue = overallMeasurement;

                }
                else
                {

                    RandomnessValue = 10f;

                }

                

                //Debug.Log("Randomness Value = " + RandomnessValue);

               // Debug.Log(highMeasurementButtonPressed + " " + lowMeasurementButtonPressed + " " + highMeasurementLocationPressed + " " + lowMeasurementLocationPressed);

            }

        }





        if (ampageOn == true)
        {

            if (highMeasurementButtonPressed == true & highMeasurementLocationPressed == true)
            {

                if (highMeasurementIsTaken == true)
                {

                    newHighMeasurementIsTaken = true;

                }

                highMeasurementIsTaken = true;
                ampageFixedValue = ampageValue;
                highMeasurementButtonPressed = false;
                highMeasurementLocationPressed = false;

            }

            if (lowMeasurementButtonPressed == true & lowMeasurementLocationPressed == true)
            {

                if (lowMeasurementIsTaken == true)
                {

                    newLowMeasurementIsTaken = true;

                }

                lowMeasurementIsTaken = true;
                lowMeasurementButtonPressed = false;
                lowMeasurementLocationPressed = false;

            }

            if (highMeasurementIsTaken == true & lowMeasurementIsTaken == true)
            {

                if (highMeasurementId == lowMeasurementId)
                {

                    overallMeasurement = ampageFixedValue;
                    string a = overallMeasurement.ToString("F");
                    measurementReadout.text = a;
                    measurementReadoutMenu.SetActive(true);
                    highMeasurementIsTaken = false;
                    lowMeasurementIsTaken = false;
                    unitMeasurementText.text = "Amps";

                }
                else
                {

                    measurementReadout.text = "Invalid";
                    measurementReadoutMenu.SetActive(true);
                    highMeasurementIsTaken = false;
                    lowMeasurementIsTaken = false;
                    unitMeasurementText.text = " ";

                }

            }

        }

        if (resistanceOn == true)
        {

            if (highMeasurementButtonPressed == true & highMeasurementLocationPressed == true)
            {

                if (highMeasurementIsTaken == true)
                {

                    newHighMeasurementIsTaken = true;

                }

                highMeasurementIsTaken = true;
                resistanceFixedValue = resistanceValue;
                highMeasurementButtonPressed = false;
                highMeasurementLocationPressed = false;

            }

            if (lowMeasurementButtonPressed == true & lowMeasurementLocationPressed == true)
            {

                if (lowMeasurementIsTaken == true)
                {

                    newLowMeasurementIsTaken = true;

                }

                lowMeasurementIsTaken = true;
                lowMeasurementButtonPressed = false;
                lowMeasurementLocationPressed = false;

            }

            if (highMeasurementIsTaken == true & lowMeasurementIsTaken == true)
            {

                if (highMeasurementId == lowMeasurementId)
                {

                    overallMeasurement = resistanceFixedValue;
                    measurementReadout.text = System.Convert.ToString(overallMeasurement);
                    measurementReadoutMenu.SetActive(true);
                    highMeasurementIsTaken = false;
                    lowMeasurementIsTaken = false;
                    unitMeasurementText.text = "Ohms";

                }
                else
                {

                    measurementReadout.text = "Invalid";
                    measurementReadoutMenu.SetActive(true);
                    highMeasurementIsTaken = false;
                    lowMeasurementIsTaken = false;
                    unitMeasurementText.text = " ";

                }


                if (overallMeasurement >= 10f)
                {

                    RandomnessValue = overallMeasurement;

                }
                else
                {

                    RandomnessValue = 10f;

                }

                if (measurementReadout.text != "Invalid")
                {
                    randomnessIntervalValue = .1f;
                    maxedInterval = false;
                }


            }


        }
    }


    private void UpdateReadingParameters () {

		GameObject scriptHolder = GameObject.FindGameObjectWithTag ("ScriptHolder");

		MeasurementModeSelection particularScript = scriptHolder.GetComponent<MeasurementModeSelection> ();

		voltageOn = particularScript.GetVoltageBool ();

		ampageOn = particularScript.GetAmpageBool ();

		resistanceOn = particularScript.GetResistanceBool ();

	}

    private void UpdateDraggableProbeData()
    {
        //This function will use variables fed from other objects to determine whether or not to operate as the drag meter or click meter

        if (lowIsHovering || highIsHovering)
        {

            isHovering = true;

        } else
        {

            isHovering = false;

        }

        if (isHovering || isDragging)
        {

            dragProbeIsActive = true;

        } else
        {

            dragProbeIsActive = false;

        }

    }

	public void SettingIdOnClick (int a) {

		if (highMeasurementButtonPressed == true) {

			highMeasurementId = a;

		}

		if (lowMeasurementButtonPressed == true) {

			lowMeasurementId = a;

		}
	}

	public void ReplaceFuse(){
		replaceFuseOn = true;
	}

	public void DCMeasurementAssignment(float a) {


		SetHighMeasurementValue (a);
		SetLowMeasurementValue (a);
		HighMeasurementLocationButtonToggle ();
		LowMeasurementLocationButtonToggle ();


	}



    public void SetManualMode(bool value) {
       // Debug.Log("setting manual mode to: " + value);
        isOnManual = value;
    }

    public void ToggleManualMode()
    {

        isOnManual = !isOnManual;

    }

    IEnumerator RandomnessGeneration ()
    {

        while (true)
        {

            yield return new WaitForSeconds(randomnessIntervalValue);

            if (maxedInterval == false)
            {
                float coroutineRandomnessValue = RandomnessValue;
                float calculatedRandomness = ((.01f) * (Random.value * percentageOfRandomness) * (coroutineRandomnessValue)) - ((.01f) * (.5f) * (percentageOfRandomness * coroutineRandomnessValue)) + overallMeasurement;
                randomSteadyState = calculatedRandomness;
                string temporaryMeasurement = calculatedRandomness.ToString("F");

                if (measurementReadout.text != "Invalid")
                {

                    measurementReadout.text = temporaryMeasurement;

                }
                

                
                if (randomnessIntervalValue <= 1.5f)
                {

                    randomnessIntervalValue = randomnessIntervalValue * 2;

                }
                else
                {

                    maxedInterval = true;
                    randomnessIntervalValue = .1f;

                }

            }
            else

                if (measurementReadout.text != "Invalid")
                {

                    measurementReadout.text = randomSteadyState.ToString("F");

                }


            


        }
        


    }

    IEnumerator TimedDebugLoop ()
    {

        while (true)
        {

            yield return new WaitForSecondsRealtime(2f);

            if (dragProbeIsActive == true)
            {

                //Debug.Log("The current state of High Measurement being kept: " + keepHighMeasurement);
                //Debug.Log("The current state of Low Measurement being kept: " + keepLowMeasurement);
                //Debug.Log("Was the actual High Measurement kept: " + highMeasurementIsTaken);
                //Debug.Log("Was the actual Low Measurement kept: " + lowMeasurementIsTaken);
                //Debug.Log("");

            }


        }


    }

    public void PitchMotorVoltagePopup (ManualModeCheck pitchMotorController)
    {

        if(pitchMotorController.motorMenuController.isFailing == false)
        {

            highMeasurementFixedValue = 20f;
            lowMeasurementFixedValue = 0f;
            highMeasurementIsTaken = true;
            lowMeasurementIsTaken = true;

        } else
        {

            highMeasurementFixedValue = 5f;
            lowMeasurementFixedValue = 0f;
            highMeasurementIsTaken = true;
            lowMeasurementIsTaken = true;

        }


    }

    public void PitchMotorResistancePopup (ManualModeCheck pitchMotorController)
    {



        resistanceOn = true;
        voltageOn = false;

        Debug.Log("res mode: " + resistanceOn);
        Debug.Log("vol mode: " + voltageOn);

        if (pitchMotorController.motorMenuController.isFailing == false)
        {
            Debug.Log("isFailing: " + "false");
            SetResistance(13f);
            resistanceFixedValue = 13f;
            resistanceValue = 13f;
            SetHighMeasurementID(1);
            lowMeasurementId = highMeasurementId;
            highMeasurementIsTaken = true;
            lowMeasurementIsTaken = true;

        } else
        {
            Debug.Log("isFailing: " + "true");
            SetResistance(9999f);
            resistanceFixedValue = 9999f;
            resistanceValue = 9999f;
            SetHighMeasurementID(1);
            lowMeasurementId = highMeasurementId;
            highMeasurementIsTaken = true;
            lowMeasurementIsTaken = true;

        }
    

    }


}
