﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomSceneDontDestroyOnLoad : MonoBehaviour {


    //this script handles storing the directory of a custom scene and whether or not it is on a server
    //it must persists from the title screen to the main scene, hence the don't destroy on load function
    public bool isOnServer = false;
    public string CustomScenePath;
    //public string CustomScene

	void Start () {
        DontDestroyOnLoad(gameObject);
	}
	
}
