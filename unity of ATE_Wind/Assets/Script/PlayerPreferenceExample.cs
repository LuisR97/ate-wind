﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public enum state { Use_Int_Bool, Use_Int, Use_Bool};

/// <summary>
/// Example Template for how a player preferences object would interact with the files,
/// Make sure that default values, subtopics, and size of int and bool tables are
/// input.
/// </summary>
public class PlayerPreferenceExample : MonoBehaviour {

   

    private GameObject scriptHolder;

    [Header("Make Sure PlayerPrefs Object is Tagged as PlayerPrefs")]
    public ReadWriteOutputFile playerPreferences;

    public string topic;

    [Header("User Inputs Subtopic Names")]
    public List<string> subtopics;

    [Header("Default Values")]
    public List<string> defaultValues;

    [Header("Updating Values")]
    public List<string> values;

    //private bool preferencesExist = false;


    //DIFFERENT POSSIBLE VALUES FOR TRACKING

    //Use enum to keep track of different possible states
    [Header("Use mode to control which lists to use")]

    public state mode = state.Use_Int_Bool;

    public bool useIntList = false;
    public bool useBoolList = false;

    public List<int> intValues;
    public List<bool> boolValues;

	//public GameObject passedPanel;





	// Use this for initialization
	void Start () {

        if ((scriptHolder != null) || (playerPreferences == null))
        {

            Debug.Log("Finding Scriptholder...");
            playerPreferences = GameObject.Find("TestIOFileObject").GetComponent<ReadWriteOutputFile>();

        }

        //scriptHolder = GameObject.Find("ScriptHolder");
        //Debug.Log(scriptHolder.ToString());

        //if ((scriptHolder != null) && (playerPreferences == null))
        //{

        //    playerPreferences = GameObject.FindWithTag("PlayerPrefs").GetComponent<ReadWriteOutputFile>();

        //}

        //Debug.Log(playerPreferences.ToString());

        InitializeActiveModes();
        InitializeValues();

        UpdateSubtopicValuesFromList();
	



	}
	
	// Update is called once per frame
	void Update () {

        if(CheckForValueChange())
        {

            playerPreferences.UpdateTopicValues(topic, subtopics, values);

        }

        SyncValuesList();

    }
    /// <summary>
    /// This script deals with setting how it reads in the mode it will work in
    /// </summary>
    public void InitializeActiveModes()
    {

        switch (mode)
        {

            case state.Use_Int_Bool:
                useIntList = true;
                useBoolList = true;
                break;
            case state.Use_Int:
                useIntList = true;
                useBoolList = false;
                break;
            case state.Use_Bool:
                useIntList = false;
                useBoolList = true;
                break;

        }
    } 

    /// <summary>
    /// This script reads in values from the ReadWriteOutputFile script object based on the
    /// number of values initialized in the instance of the script
    /// </summary>
    public void UpdateSubtopicValuesFromList ()
    {

        int intCount = intValues.Count;
        int boolCount = boolValues.Count;
        int totalCount = intCount + boolCount;

        //subtopics.Clear();
        //values.Clear();






        if (playerPreferences.groupNames.Contains(topic))
        {

            values = playerPreferences.subjectValues[topic+"Values"];

        } else
        {

            copyDefaultToUpdatingValues();

        }
        

        if (mode == state.Use_Int_Bool)
        {


            for (int i=0; i<intCount; i++)
            {

                intValues[i] = int.Parse(values[i]);

            }

            for (int j=0; j<boolCount; j++)
            {

                boolValues[j] = Convert.ToBoolean(int.Parse(values[j+intCount]));

            }


        } else if (mode == state.Use_Int)
        {

            for (int i = 0; i < intCount; i++)
            {

                intValues[i] = int.Parse(values[i]);

            }

        } else if (mode == state.Use_Bool)
        {

            for (int j = 0; j < boolCount; j++)
            {

                boolValues[j] = Convert.ToBoolean(int.Parse(values[j]));

            }

        }

    }

    /// <summary>
    /// Checks to see if the values already exist in the player preferences file, if not,
    /// create them using the default values put together in this instance of the script
    /// </summary>
    public void InitializeValues()
    {

        if (playerPreferences.groupNames.Contains(topic))
        {

            values = playerPreferences.subjectValues[topic+"Values"];

            Debug.Log("existed in user preferences");

        }
        else
        {

            copyDefaultToUpdatingValues();

            //if it's missing, add to lists

            playerPreferences.AddToUserPreferences(topic, subtopics, defaultValues);

            Debug.Log("added to user preferences");

        }

    }

    public void copyDefaultToUpdatingValues()
    {

        values.Clear();

        for (int i=0; i<defaultValues.Count; i++)
        {

            values.Add(defaultValues[i]);

        }

    }

    /// <summary>
    /// This Function updates the value of the subtopic given. This should be called from another object
    /// </summary>
    /// <param name="subtopicName">Name of the topic to be updated</param>
    /// <param name="value">New value for Subtopic given</param>
    public void SubtopicValueUpdate (string subtopicName, string value)
    {

        if (subtopics.Contains(subtopicName))
        {

            int topicListIndex = subtopics.IndexOf(subtopicName);

            values[topicListIndex] = value;

            
        }

        playerPreferences.UpdateTopicValues(topic, subtopics, values);
        

    }




    /// <summary>
    /// converts true statements into a string "1" or "0"
    /// </summary>
    /// <param name="booleanValue"></param>
    /// <returns></returns>
    public string ConvertBoolToString (bool booleanValue)
    {

        if (booleanValue)
        {

            return "1";

        } else
        {

            return "0";

        }

    }


    public bool CheckForValueChange ()
    {

        bool returnBool = false;
        List<string> oldList = new List<string>();
        oldList = playerPreferences.GetSubtopicValuesFromName(topic);

        for(int i=0; i<oldList.Count; i++)
        {

            if (oldList[i] != values[i])
            {

                returnBool = true;

            }

        }

        return returnBool;

    }

    public void SyncValuesList ()
    {

        int intCount = intValues.Count;
        int boolCount = boolValues.Count;
        int totalCount = intCount + boolCount;

        if (mode == state.Use_Int_Bool)
        {


            for (int i = 0; i < intCount; i++)
            {

                intValues[i] = int.Parse(values[i]);

            }

            for (int j = 0; j < boolCount; j++)
            {

                boolValues[j] = Convert.ToBoolean(int.Parse(values[j + intCount]));

            }


        }
        else if (mode == state.Use_Int)
        {

            for (int i = 0; i < intCount; i++)
            {

                intValues[i] = int.Parse(values[i]);

            }

        }
        else if (mode == state.Use_Bool)
        {

            for (int j = 0; j < boolCount; j++)
            {

                boolValues[j] = Convert.ToBoolean(int.Parse(values[j]));

            }

        }

    }


}
