﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class login : MonoBehaviour {

    //gameobjects to enable and disable for login/logout and error messages
    public GameObject loginButton, logoutButton;
    public GameObject loginPanel, loginSuccessPanel, loginFailurePanel;

    //the current username and password of the logged in user
    private string username, password;

    //bool showing whether or not the user is currently logged in
    private bool isLoggedIn = false;

    //the input fields for the login inputs
    public InputField usernameInput, passwordInput;

    //the urls for the php code for logging in and sending a report, respectively
    private string loginUrl="localhost/windLogin/login.php";
    private string reportUrl = "localhost/windLogin/submitReport.php";
    private string registerUrl = "http://localhost/windLogin/#/register";

    private string tempUsername;

    //login needs to persist across scenes
    void Start() {
        Debug.Log("dont destroy on load");
        DontDestroyOnLoad(transform.parent.gameObject);
    }

    public void Login() {
        string username = usernameInput.text;
        string password = passwordInput.text;
        tempUsername = username;

        string url = loginUrl;

        //creates a www POST form to send to php
        WWWForm form = new WWWForm();

        form.AddField("username", username);
        form.AddField("password", password);

        WWW www = new WWW(url, form);

        //starts a coroutine to process the data on the server
        StartCoroutine(WaitForLogin(www));

    }

    //clears input values, sets loggedin to false, changes buttons to default
    public void Logout() {

        usernameInput.text = "";
        passwordInput.text = "";

        username = "";
        password = "";
        isLoggedIn = false;

        logoutButton.SetActive(false);
        loginButton.SetActive(true);
    }

    //function to save data as a report
    public void SaveRecordToDatabase(string firstName, string lastName, string usernameField, string scene, string problem, string solution, string time) {

        if(!isLoggedIn) {
            Debug.Log("You are not logged in");
            return;
        }else {
            Debug.Log("sending report");
        }

        string url = reportUrl;

        WWWForm form = new WWWForm();
        form.AddField("firstName", firstName);
        form.AddField("lastName", lastName);
        form.AddField("username", username);
        form.AddField("problem", problem);
        form.AddField("solution", solution);
        form.AddField("time", time);

        WWW www = new WWW(url, form);
        StartCoroutine(WaitForReport(www));

        //yield return null;
    }

    public IEnumerator WaitForReport(WWW www) {

        Debug.Log("sending report to server");

        yield return www;

        string returnText;

        if (www.error == null) {
            Debug.Log("www ok!: " + www.text);

        } else {
            Debug.Log("www error: " + www.error);
        }
    }


    public IEnumerator WaitForLogin(WWW www) {
        yield return www;

        string returnText;

        if (www.error == null) {
            //Debug.Log("www ok!: " + www.text);
            returnText = www.text;
            LoginCallback(returnText);
        } else {
           // Debug.Log("www error: " + www.error);
        }
    }


    public void LoginCallback(string loginText) {
        if(loginText.ToLower().Contains("success")) {
            Debug.Log("logged in successfully");

            loginPanel.SetActive(false);
            loginSuccessPanel.SetActive(true);

            loginButton.SetActive(false);
            logoutButton.SetActive(true);

            isLoggedIn = true;
            username = tempUsername;
            Debug.Log("logged in as: " + username);

            //SaveRecordToDatabase("foo", "bar", "test23", "lorem ipsum", "lorem ipsum", "06:66");

        } else {
            Debug.Log("failed to login");

            loginPanel.SetActive(false);
            loginFailurePanel.SetActive(true);

            isLoggedIn = false;
            tempUsername = "";
            username = "";
        }
        
    }



    public bool IsUserLoggedIn() {
        return isLoggedIn;
    }

    public string GetLoggedInUser() {
        return username;
    }


    public void RegisterInBrowser() {
        Debug.Log("opening in browser");
        Application.OpenURL(registerUrl);
    }

}
