﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public Animator[] animators;
    public GameObject[] m_menuPanels;
    public Button[] m_menubuttons;
    public Color m_clickedColor = Color.green;
    public Color m_unclickedColor = Color.white;
    public float newScale = 2.0f;
    public Transform m_subMenu;
    public Animator testAnimator;
    public GameObject m_mainMenuButtons;
    public void PanelStatus()
    {
        panelEnabled(0);
        panelEnabled(1);
        panelEnabled(2);
        panelEnabled(3);
    }

    public void panelEnabled(int index)
    {
        if (m_menuPanels[index].activeInHierarchy)
        {
            /*m_menubuttons[index].GetComponent<RectTransform>().localScale = new Vector3(newScale, 1.0f, 1.0f);
            ChangeButtonColor(m_menubuttons[index], m_clickedColor);*/
            ButtonClicked(m_menubuttons[index], m_clickedColor, newScale);
            //animators[index].SetBool("PanelEnabled", true);
        }
        else
        {
            ButtonClicked(m_menubuttons[index], m_unclickedColor, 1.0f);
            /*m_menubuttons[index].GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
            ChangeButtonColor(m_menubuttons[index], m_unclickedColor);*/
            //animators[index].SetBool("PanelEnabled", false);
        }
    }

    private void ChangeButtonColor(Button t_button, Color t_color)
    {
        Button b = t_button.GetComponent<Button>();
        ColorBlock cb = b.colors;
        cb.normalColor = t_color;
        b.colors = cb;
    }

    private void ButtonClicked(Button t_button, Color t_color, float Scale)
    {
        t_button.GetComponent<RectTransform>().localScale = new Vector3(Scale, 1.0f, 1.0f);
        ChangeButtonColor(t_button, t_color);
    }

    public void BackButtonController()
    {
        for(int i = 0; i < m_subMenu.childCount; i++)
        {
            m_subMenu.GetChild(i).gameObject.SetActive(false);
        }
        PanelStatus();
    }

    public void ResetAnimatorController(Button t_button)
    {
        //Animator t_animator = t_button.GetComponent<Animator>();
        /*int gunSwitchID = Animator.StringToHash("Entry");
        t_animator.Play(gunSwitchID, 0, 0f);*/

        testAnimator.Play("Disabled", -1, 0f);
    }

    public void AboutButton_Click()
    {
        testAnimator.Play("Normal", -1, 0f);
        //Invoke("AboutButtonFunction", 2);
        AboutButtonFunction();
    }

    private void AboutButtonFunction()
    {
        m_mainMenuButtons.SetActive(false);
    }
}