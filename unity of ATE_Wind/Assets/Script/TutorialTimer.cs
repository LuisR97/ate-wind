﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialTimer : MonoBehaviour {

    public Button nextButton;
    public Changelight lightScript;
    public Timer timer;

    public void Update() {
        if(timer.IsTimerFinished()) {
            nextButton.interactable = true;
            lightScript.CloseDoor();
        }
    }
}
