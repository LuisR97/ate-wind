﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnEnable : MonoBehaviour {

    public GameObject objectToDisable;

	void Start () {
        objectToDisable.SetActive(false);
	}
}
