using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ExampleRepair : MonoBehaviour {

	public GameObject repairPanels;
	public GameObject faultyFuse;
	public GameObject correctedFuse;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ReplaceFuse () {

		repairPanels.SetActive (true);
		faultyFuse.SetActive (false);
		correctedFuse.SetActive (true);
	}
}
