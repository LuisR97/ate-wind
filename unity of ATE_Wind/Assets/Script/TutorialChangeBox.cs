﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialChangeBox : MonoBehaviour {

    public Changelight lightScript;

    public bool flipSwitch;
    public bool flipPower;
    public bool flipDoor;


    public void Start() {
        Debug.Log("handling box changes");

        if(flipDoor) {
            lightScript.OpenDoor();
        }
        if(flipSwitch) {
            lightScript.FlipLightSwitch();
        }
        if(flipPower) {
            lightScript.FlipPowerSwitch();
        }
    }
}
