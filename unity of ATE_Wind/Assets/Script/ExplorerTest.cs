﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ExplorerTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
        // ExplorerTestFunction();
        openInExplorer("test");
	}




    static void openInExplorer(string path) {
        if(Application.platform==RuntimePlatform.WindowsPlayer || Application.platform==RuntimePlatform.WindowsEditor) {
            path = path.Replace("/", @"\");
            string arg = "/select,\"" + path + "\"";
            System.Diagnostics.Process.Start("explorer.exe", arg);
        }else if(Application.platform==RuntimePlatform.OSXPlayer || Application.platform==RuntimePlatform.WindowsEditor) {
            System.Diagnostics.Process.Start("open", "-R "+path);
        }

        
    }


}
