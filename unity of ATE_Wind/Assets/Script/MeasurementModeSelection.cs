﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MeasurementModeSelection : MonoBehaviour {

	public Button voltageSelect;
	public Button ampageSelect;
	public Button resistanceSelect;

	private GameObject activeModeObject;
	private bool voltageOn = true;
	private bool ampageOn = false;
	private bool resistanceOn = false;
	//private GameObject[] voltageLocationButtons;
	//private GameObject[] ampageLocationButtons;
	//private GameObject[] resistanceLocationButtons;
	private GameObject activeButtonsObject;
	private GameObject inactive1ButtonsObject;
	private GameObject inactive2ButtonsObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		//Code to call up button arrays here

	}

	//Sets Voltage to true, rest to false
	public void SetVoltageBool (){

		if (voltageOn != true) {

			ampageOn = false;
			resistanceOn = false;
			voltageOn = true;

		}

	}

	//Sets Ampage to true, rest to false
	public void SetAmpageBool (){

		if (ampageOn != true) {

			ampageOn = true;
			resistanceOn = false;
			voltageOn = false;

		}

	}

	//Sets Resistance to true, rest to false
	public void SetResistanceBool (){

		if (resistanceOn != true) {

			ampageOn = false;
			resistanceOn = true;
			voltageOn = false;

		}

	}

	public void SetActiveButtonsObject (GameObject a){

		activeButtonsObject = a;

	}

	public void SetInactive1ButtonsObject (GameObject a){

		inactive1ButtonsObject = a;

	}

	public void SetInactive2ButtonsObject (GameObject a){

		inactive2ButtonsObject = a;

	}

	public bool GetVoltageBool () {

		return voltageOn;

	}

	public bool GetAmpageBool () {

		return ampageOn;

	}

	public bool GetResistanceBool () {

		return resistanceOn;

	}

	/*public void FindVoltageButtons () {

		voltageLocationButtons = GameObject.FindGameObjectsWithTag ("Voltage Location Button");

	}

	public void FindAmpageButtons () {

		ampageLocationButtons = GameObject.FindGameObjectsWithTag ("Ampage Location Button");

	}

	public void FindResistanceButtons () {

		resistanceLocationButtons = GameObject.FindGameObjectsWithTag ("Resistance Location Button");

	}*/
		

	public void LocationButtonToggle () {

		activeButtonsObject.SetActive (true);
		inactive1ButtonsObject.SetActive (false);
		inactive2ButtonsObject.SetActive (false);

		GameObject voltageButton = GameObject.Find ("Voltage Select");
		GameObject ampageButton = GameObject.Find ("Ampage Select");
		GameObject resistanceButton = GameObject.Find ("Resistance Select");

		if (voltageOn == true) {

			voltageButton.GetComponent<Button>().interactable = false;
			ampageButton.GetComponent<Button>().interactable = true;
			resistanceButton.GetComponent<Button>().interactable = true;

		}

		if (ampageOn == true) {

			voltageButton.GetComponent<Button>().interactable = true;
			ampageButton.GetComponent<Button>().interactable = false;
			resistanceButton.GetComponent<Button>().interactable = true;

		}

		if (resistanceOn == true) {

			voltageButton.GetComponent<Button>().interactable = true;
			ampageButton.GetComponent<Button>().interactable = true;
			resistanceButton.GetComponent<Button>().interactable = false;

		}

	}

	public void ResetMeasurements () {

		measurementAssignment A = GameObject.FindGameObjectWithTag ("ScriptHolder").GetComponent<measurementAssignment> ();

		A.SetHighMeasurementID (0);
		A.SetLowMeasurementID (0);
		A.SetAmpage (0);
		A.SetResistance (0);
		A.SetHighMeasurementValue (0);
		A.SetLowMeasurementValue (0);
		A.HighMeasurementButtonToggle ();
		A.HighMeasurementButtonToggle ();
		A.LowMeasurementButtonToggle ();
		A.LowMeasurementButtonToggle ();
		A.ResetReadings ();
	}
}
