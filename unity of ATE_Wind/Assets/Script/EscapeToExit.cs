﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapeToExit : MonoBehaviour {


    public GameObject exitPanel;
    public KeyCode escapeKey = KeyCode.Escape;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(escapeKey))
        {
            exitPanel.SetActive(!exitPanel.activeSelf);
        }
	}
}
