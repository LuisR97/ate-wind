﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateObjectsAtRuntime : MonoBehaviour {

    public Transform[] parents;
    public GameObject prefab;
    public string newObjectName;

    public void InstantiatePrefabs() {
        foreach(Transform parent in parents) {
            GameObject newObject = GameObject.Instantiate(prefab, new Vector3(Screen.height / 2, Screen.width / 2, 0), Quaternion.identity, parent);
            if(newObjectName==null) {
                newObjectName = prefab.name;
            }

            newObject.name = newObjectName;
        }
    }

}
