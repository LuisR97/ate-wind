﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelKeyboardControl : MonoBehaviour {

	public GameObject Panel;


	// Use this for initialization
	void Start () {		
		
	}
	
	// Update is called once per frame
	void Update () {
		StopMoveByKeyboard ();
	}


	public void StopMoveByKeyboard(){

		if (Panel.activeInHierarchy == true) {
			GameObject.Find ("Main Camera").GetComponent<CameraFly> ().enabled = false;
			GameObject.Find ("Main Camera").GetComponent<TeleportTo> ().enabled = false;
			GameObject.Find ("Scriptholder").GetComponent<Movetoward> ().enabled = false;
			GameObject.Find ("Flashlight").GetComponent<Flashlight> ().enabled = false;
		} 

		else{
			//GameObject.Find ("Main Camera").GetComponent<CameraFly> ().enabled = true;
			GameObject.Find ("Scriptholder").GetComponent<Movetoward> ().enabled = true;
			GameObject.Find ("Main Camera").GetComponent<TeleportTo> ().enabled = true;
			GameObject.Find ("Flashlight").GetComponent<Flashlight> ().enabled = false;
		}
			

	}


}
