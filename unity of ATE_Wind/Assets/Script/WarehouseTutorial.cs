﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarehouseTutorial : MonoBehaviour {

	public int NOrder;
	public bool TutorialMode;

	public bool B_step3;
	public bool B_step4;
	public bool B_step5;
	public bool B_step6;
	public bool B_step7;
	public bool B_step8;
	public bool B_step9;
	public bool B_step10;
	public bool B_step11;
	public bool B_step12;
	public bool B_step13;
	public bool B_step14;
	public bool B_step15;

	public GameObject TutorialPanel3;
	public GameObject TutorialPanel4;
	public GameObject TutorialPanel5;
	public GameObject TutorialPanel6;
	public GameObject TutorialPanel7;
	public GameObject TutorialPanel8;
	public GameObject TutorialPanel9;
	public GameObject TutorialPanel10;
	public GameObject TutorialPanel11;
	public GameObject TutorialPanel12;
	public GameObject TutorialPanel13;
	public GameObject TutorialPanel14;
	public GameObject TutorialPanel15;

	public GameObject Multimeter;
	public GameObject Helmet;
	public GameObject Gloves;
	public GameObject EarPlugs;
	public GameObject Harness;
	public GameObject Glasses;
	public GameObject LanyardHook;
	public GameObject Lanyard;



	public Text ControlText;


	// Use this for initialization
	void Start () {
		TutorialPanel3.SetActive (true);
		TutorialMode = true;
		B_step3 = false;
		B_step4 = false;
		B_step8 = false;
	}
	
	// Update is called once per frame
	void Update () {
		Tutorial();
	}

	public void Tutorial()
	{
		B_step3 = true;

		if (B_step3) {
			NOrder = 3;
			TutorialPanel3.SetActive (true);
		}


		if (B_step4) {
			NOrder = 4;
			TutorialPanel4.SetActive (true);
			TutorialPanel3.SetActive (false);

			ControlText.GetComponent<Text> ().color = Color.yellow; 
		}

		if (B_step5) {
			NOrder = 5;
			TutorialPanel5.SetActive (true);
			TutorialPanel4.SetActive (false);

			ControlText.GetComponent<Text> ().color = Color.black; 

			if (Input.GetMouseButtonUp (1)) {
				B_step6 = true;			
			}
		}

		if (B_step6) {
			NOrder = 6;
			TutorialPanel6.SetActive (true);
			TutorialPanel5.SetActive (false);

			if (Input.GetKeyUp (KeyCode.W)) {
				B_step7 = true;
			}
		}

		if (B_step7) {
			NOrder = 7;
			TutorialPanel7.SetActive (true);
			TutorialPanel6.SetActive (false);
	
		}

		if (B_step8) {
			NOrder = 8;
			TutorialPanel8.SetActive (true);
			TutorialPanel7.SetActive (false);
		}

		if (B_step9) {
			NOrder = 9;
			TutorialPanel9.SetActive (true);
			TutorialPanel8.SetActive (false);

		//	if (Input.GetKeyUp (KeyCode.W)) {
		//		B_step10 = true;
		//	}

		}

		if (B_step10) {
			NOrder = 10;
			TutorialPanel10.SetActive (true);
			TutorialPanel9.SetActive (false);

		//	if (Input.GetMouseButtonUp (0)) {
		//		B_step11 = true;			
		//	}
		}

		if (B_step11) {
			NOrder = 11;
			TutorialPanel11.SetActive (true);
			TutorialPanel10.SetActive (false);
		}

		if (B_step12) {
			NOrder = 12;
			TutorialPanel12.SetActive (true);
			TutorialPanel11.SetActive (false);
		}

		if (B_step13) {
			NOrder = 13;
			TutorialPanel13.SetActive (true);
			TutorialPanel12.SetActive (false);
		}

		if (B_step14) {
			NOrder = 14;
			TutorialPanel14.SetActive (true);
			TutorialPanel13.SetActive (false);


		}

		if (B_step15) {
			NOrder = 15;
			TutorialPanel15.SetActive (true);
			TutorialPanel14.SetActive (false);
		}



		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 8)) {	

				if (hit.collider.name == "Screen") {		
					B_step8 = true;
				}

				if (hit.collider.name == "Hub Safety Sheet1"||hit.collider.name == "Hub Safety Sheet2") {		
					B_step9 = true;
				}

				if (hit.collider.name == "shelf_2") {		
					B_step10 = true;
				}
					
				if (Multimeter.activeSelf == false) {		
					B_step11 = true;
				}

				if (Glasses.activeSelf == false && Multimeter.activeSelf == false
					&& Helmet.activeSelf == false && Gloves.activeSelf == false
					&& EarPlugs.activeSelf == false && Harness.activeSelf == false
					&& LanyardHook.activeSelf == false && Lanyard.activeSelf ==false) {		
					
					B_step15 = true;
				}
			}

		}



	}

	public void step4()
	{
		B_step4 = true;
	}

	public void step5()
	{
		B_step5 = true;
	}

	public void step12()
	{
		B_step12 = true;
	}

	public void step13()
	{
		B_step13 = true;
	}

	public void step14()
	{
		B_step14 = true;
	}
}
