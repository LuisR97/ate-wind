﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//this script handles the large red button on the center box which acts as a "kill switch"

public class CenterBoxRedButton : MonoBehaviour {


    //the hover text object and the string to show when hovering
    public GameObject HoverText;
    public string HoverString="Left Click to Disable Power";

    //a boolean to detect if the mouse is hovering over the button's collider
    private bool mouseHover = false;

    //an array of menus which can overlay the button and prevent hovering (if this is not set, the button can be clicked while a fullscreen menu is open)
    public GameObject[] menus;

    //the text for manual mode settings in the center box
    public GameObject manualModeOff, manualModeOn;

    //if mouse over and no menus are active and in the way, set hover to true and show hover text
    public void OnMouseOver() {
        foreach(GameObject menu in menus) {
            if(menu.activeSelf) {
                return;
            }
        }


        Debug.Log("hover");
        mouseHover = true;
        HoverText.SetActive(true);
        HoverText.GetComponent<Text>().text = HoverString;
    }

    public void OnMouseStay() {
        foreach (GameObject menu in menus) {
            if (menu.activeSelf) {
                return;
            }
        }

        mouseHover = true;
        HoverText.SetActive(true);
        HoverText.GetComponent<Text>().text = HoverString;
    }


    //cancel hovering when mouse exits collider
    public void OnMouseExit() {
        mouseHover = false;
        HoverText.SetActive(false);
    }
    
    //if hovering and player left clicks, turn on manual mode
    public void Update() {
        if(mouseHover && Input.GetMouseButtonDown(0)) {
            manualModeOff.SetActive(false);
            manualModeOn.SetActive(true);
        }
    } 
}
