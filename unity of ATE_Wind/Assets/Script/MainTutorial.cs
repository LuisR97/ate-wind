﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainTutorial : MonoBehaviour {

	public int NOrder;
	public bool TutorialMode;

	public bool B_step0;
	public bool B_step1;
	public bool B_step2;


	public GameObject TutorialPanel0;
	public GameObject TutorialPanel1;
	public GameObject TutorialPanel2;
	public GameObject MenuButton;
	public GameObject MainMenuButton;
	public GameObject EducationModulePanel;

	public Text TBSText;
	public Text PCSText;


	// Use this for initialization
	void Start () {
		
		TutorialPanel0.SetActive (true);
		TutorialMode = true;
		B_step0 = false;
		B_step1 = false;
		B_step2 = false;

	}
	
	// Update is called once per frame
	void Update () {
		Tutorial();
	}


	public void Tutorial()
	{

		if(TutorialMode){

			B_step0 = true;

			if (B_step0) {
				NOrder = 0;
				TutorialPanel0.SetActive (true);
			}


			if (B_step1) {
				NOrder = 1;
				TutorialPanel1.SetActive (true);
				TutorialPanel0.SetActive (false);
				MenuButton.SetActive (true);
				MainMenuButton.SetActive (false);

				TBSText.GetComponent<Text> ().color = Color.yellow;       //Change button's color
			}

			if (B_step2) {
				TutorialPanel1.SetActive (false);
				TutorialPanel2.SetActive (true);

				PCSText.GetComponent<Text> ().color = Color.yellow;
			}
		}
	}

	public void StartTutorial()
	{
		TutorialMode = true;
	}

	public void CloseTutorial()
	{
		TutorialMode = false;
		TutorialPanel0.SetActive (false);
	}

	public void step1()
	{
		B_step1 = true;
	}

	public void step2()
	{
		B_step2 = true;
	}


}
