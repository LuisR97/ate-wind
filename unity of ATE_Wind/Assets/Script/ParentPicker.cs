﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParentPicker : MonoBehaviour {

    public GameObject pickParentPanel;

    public InputField currentField;
   
    public void SetNode(string nodeName) {
        currentField.text = nodeName;
    }
    
}
