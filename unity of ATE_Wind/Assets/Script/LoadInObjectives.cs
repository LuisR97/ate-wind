﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadInObjectives : MonoBehaviour {

    //SCADA text 
    public Text scadaText;

    //To calculate the distace between the click point and the camera
    public float distance;
    public GameObject cam;

    string textToSend;
    bool inRange;

    public bool triggerSendFlag = false;
    public bool sendData = false;

    ObjectivePanel scriptholder;

	// Use this for initialization
	void Start () {

        cam = GameObject.FindGameObjectWithTag("MainCamera");

	}
	
	// Update is called once per frame
	void Update () {

        if (sendData)
        {

            sendData = false;
            LoadInValues();
            scriptholder.FormTextArrayForObjectiveText();
            scriptholder.WriteObjectiveText();

        }

        if (triggerSendFlag)
        {

            triggerSendFlag = false;
            sendData = true;

        }

    }

    void FixedUpdate()
    {

        DistanceCheck();

        

    }

    void OnMouseOver ()
    {

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {

            MouseClickFunction();

        }

    }

    void LoadInValues()
    {

        textToSend = scadaText.text;
        scriptholder = GameObject.Find("ObjectivesController").GetComponent<ObjectivePanel>();
        scriptholder.scadaTextString = textToSend;
    }

    void DistanceCheck()
    {

        if (Vector3.Distance(this.gameObject.transform.position, cam.transform.position) < distance)
        {

            inRange = true;

        } else
        {

            inRange = false;

        }

    }

    void MouseClickFunction()
    {

        if (inRange)
        {

            triggerSendFlag = true;

        }


    }
}
