﻿using UnityEngine;
using System.Collections;

public class OnHover : MonoBehaviour {

    public GameObject HoverShow;

    void OnMouseOver()
    {
        HoverShow.SetActive(true);
        Debug.Log("Mouse on hover.");
    }
}
