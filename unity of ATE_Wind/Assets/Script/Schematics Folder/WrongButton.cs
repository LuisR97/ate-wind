﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WrongButton : MonoBehaviour {

	public GameObject wrongPanel;
	public Color wrongButtonColor;
	// Use this for initialization
	void Start () {
		//wrongPanel = GameObject.Find("Wrong Panel");
	}
	
	public void MakeButtonRed()
	{
		GetComponent<Image>().color = wrongButtonColor;
		wrongPanel.SetActive(true);
		StartCoroutine(ExampleCoroutine());
	}

	IEnumerator ExampleCoroutine()
	{
		yield return new WaitForSeconds(2.0f);
		wrongPanel.SetActive(false);
	}
}
