﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CorrectButton : MonoBehaviour {

	public GameObject rightPanel;
	public Color rightButtonColor;
	// Use this for initialization
	void Start () {
		//rightPanel = GameObject.Find("Right Panel");
	}

	public void MakeButtonGreen()
	{
		GetComponent<Image>().color = rightButtonColor;
		rightPanel.SetActive(true);
		StartCoroutine(ExampleCoroutine());
	}

	IEnumerator ExampleCoroutine()
	{
		yield return new WaitForSeconds(2.0f);
		rightPanel.SetActive(false);
	}
}
