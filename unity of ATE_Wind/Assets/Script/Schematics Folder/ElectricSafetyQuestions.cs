﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ElectricSafetyQuestions : MonoBehaviour 
{
	public Text mCScore;
	public int mCTalley;
	public List<MCQuestion> MCQuestions = new List<MCQuestion>();

//Method to calculate the score of the Multiple Choice section
	public void CheckMCScore()
	{
		mCTalley = 0;
		foreach(MCQuestion question in MCQuestions)
		{
			for(int i = 0; i < question.choiceList.Count; i++)
			{
				if( (question.choiceList[i].isOn) && (i == question.answer) )
				{
					mCTalley++;
					//Debug.Log("correct");
				}
				else
				{
					//Debug.Log("wrong");
				}
			}
		}
		mCScore.text = mCTalley.ToString();
		//Debug.Log("Score Calculated");
	}
}
