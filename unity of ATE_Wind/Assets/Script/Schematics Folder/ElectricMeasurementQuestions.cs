﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ElectricMeasurementQuestions : MonoBehaviour 
{
	public Text tFScore, FITBScore;
	public int tFTalley, FITBTalley;
	public List<TFQuestion> questions = new List<TFQuestion>();
	public List <FillInTheBlankQuestion> fillInTheBlankQuestions = new List<FillInTheBlankQuestion>();
	public List <string> choices = new List<string>();
	public List <Dropdown> dropdowns = new List<Dropdown>();

	void Start()
	{	
		FillDropDown();
	}

	public void FillDropDown()
	{
		foreach(Dropdown dropdown in dropdowns)
		{
			dropdown.ClearOptions();
			dropdown.AddOptions(choices);
		}
		Debug.Log("FillDropDown");
	}

	//Method to calculate the score of the True False section
	public void CheckScore()
	{
		Debug.Log("Check Score");
		tFTalley = 0;
		foreach(TFQuestion question in questions)
		{
			if( (question.trueToggle.isOn == true) && (question.answer == true) )
			{
				tFTalley++;
			}
			else if( (question.trueToggle.isOn == true) && (question.answer == false) )
			{
			}
			else if( (question.falseToggle.isOn == true) && (question.answer == false) )
			{
				tFTalley++;
			}
			else if( (question.falseToggle.isOn == true) && (question.answer == true) )
			{
			}
		} 
		tFScore.text = tFTalley.ToString();

	}

	public void CheckFITBScore()
	{
		FITBTalley = 0;
		foreach(FillInTheBlankQuestion question in fillInTheBlankQuestions)
		{
			if (question.Dropdown.value == question.answer)
			{
				FITBTalley++;
				Debug.Log("For Each True");
			}
			else
			{
				Debug.Log(question.Dropdown.itemText);
			}
			Debug.Log("For Each Loop");
		}
		FITBScore.text = FITBTalley.ToString();
		Debug.Log("CheckFITBScore");
	}
}

[System.Serializable]
public class FillInTheBlankQuestion
{
	public Dropdown Dropdown;
	public int answer; 

	public FillInTheBlankQuestion()
	{

	}
}