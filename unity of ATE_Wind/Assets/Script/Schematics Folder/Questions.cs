﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Questions : MonoBehaviour 
{
	public Text tFScore, mCScore;
	public int tFTalley, mCTalley;
	public List<TFQuestion> questions = new List<TFQuestion>();
	public List<MCQuestion> MCQuestions = new List<MCQuestion>();

//Method to calculate the score of the True False section
	public void CheckScore()
	{
		tFTalley = 0;
		foreach(TFQuestion question in questions)
		{
			/*
			if((question.trueToggle.isOn) == (question.answer))
			{
				tFTalley++;
			}
			else
			{} */

			if( (question.trueToggle.isOn == true) && (question.answer == true) )
			{
				tFTalley++;
			}
			else if( (question.trueToggle.isOn == true) && (question.answer == false) )
			{
			}
			else if( (question.falseToggle.isOn == true) && (question.answer == false) )
			{
				tFTalley++;
			}
			else if( (question.falseToggle.isOn == true) && (question.answer == true) )
			{
			}
		} 
		tFScore.text = tFTalley.ToString();
	}

//Method to calculate the score of the Multiple Choice section
	public void CheckMCScore()
	{
		mCTalley = 0;
		foreach(MCQuestion question in MCQuestions)
		{
			for(int i = 0; i < question.choiceList.Count; i++)
			{
				if( (question.choiceList[i].isOn) && (i == question.answer) )
				{
					mCTalley++;
				}
			}
		}
		mCScore.text = mCTalley.ToString();
	}
}


//Class for True False questions
[System.Serializable]
public class TFQuestion
{
	public Toggle trueToggle;
	public Toggle falseToggle;
	public bool answer;
	public TFQuestion()
	{
	}
}

//Class for Multiple Choice questions
[System.Serializable]
public class MCQuestion
{
	public Toggle choice1, choice2, choice3, choice4;
	public List<Toggle> choiceList = new List<Toggle>();
	public int answer = 0;
	public MCQuestion()
	{
		choiceList.Add(choice1);
		choiceList.Add(choice2);
		choiceList.Add(choice3);
		choiceList.Add(choice4);
	}
}