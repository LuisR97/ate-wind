﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PageNagivation : MonoBehaviour 
{
	public List<Page> Pages = new List<Page>();
	public int i = 0;
	public GameObject notice;
	public Text pageNumber;
	public GameObject rightPanel, wrongPanel;

	// Update is called once per frame
	void Update () 
	{
		 if (Input.GetButtonDown("SlideAdvanceForward"))
        {
            NextPage();
        }
		else if (Input.GetButtonDown("SlideAdvanceBackward"))
        {
            BackPage();
        }
	}

	public void NextPage()
	{
		if((i != (Pages.Count - 1)) && Pages[i].allowed == true)
		{
			Pages[i].slide.SetActive(false);
			Pages[i + 1].slide.SetActive(true);
			i++;
			pageNumber.text = i.ToString();
			rightPanel.SetActive(false);
			wrongPanel.SetActive(false);
		}
		else if((i != (Pages.Count - 1)) && (Pages[i].allowed == false))
		{
			notice.SetActive(true);
		}
	}

	public void BackPage()
	{		
		if(i != 0)
		{
			Pages[i].slide.SetActive(false);
			Pages[i - 1].slide.SetActive(true);
			i--;
			rightPanel.SetActive(false);
			wrongPanel.SetActive(false);
			pageNumber.text = i.ToString();
		}
	}

	public void MakeAllowable()
	{
		Pages[i].allowed = true;
	}

	public void LoadMainMenu()
	{
		SceneManager.LoadScene("NewMainMenu", LoadSceneMode.Single);
	}
}

[System.Serializable]
public class Page
{
	public GameObject slide;
	public bool allowed = true;

	public Page()
	{

	}
}