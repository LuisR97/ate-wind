﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ModuleNavigation : MonoBehaviour 
{
	public List<Page> Pages = new List<Page>();
	private int i = 0;
	public GameObject notice;
	public Text pageNumber;
	public GameObject ScoreButton;
	public Questions questionsScript;
	public int max = 0;
	public int min = 0;


	// Update is called once per frame
	void Update () 
	{
		 if (Input.GetButtonDown("SlideAdvanceForward"))
        {
            NextPage();
        }
		else if (Input.GetButtonDown("SlideAdvanceBackward"))
        {
            BackPage();
        }
		
		if(i == max)
		{
			ScoreButton.SetActive(true);
		}
		if(i == min)
		{
			ScoreButton.SetActive(false);
		}
		if(i == max +1)
		{
			questionsScript.CheckMCScore();
			questionsScript.CheckScore();
		}

	}

	public void NextPage()
	{
		if((i != (Pages.Count - 1)) && Pages[i].allowed == true)
		{
			Pages[i].slide.SetActive(false);
			Pages[i + 1].slide.SetActive(true);
			i++;
			pageNumber.text = i.ToString();

			if(i == max)
			{
				ScoreButton.SetActive(true);
			}
		}
		else if((i != (Pages.Count - 1)) && (Pages[i].allowed == false))
		{
			notice.SetActive(true);
		}
	}

	public void BackPage()
	{		
		if(i != 0)
		{
			Pages[i].slide.SetActive(false);
			Pages[i - 1].slide.SetActive(true);
			i--;
			pageNumber.text = i.ToString();

			if(i == min)
			{
				ScoreButton.SetActive(false);
			}
		}
	}

	public void MakeAllowable()
	{
		Pages[i].allowed = true;
	}

	public void LoadMainMenu()
	{
		SceneManager.LoadScene("NewMainMenu", LoadSceneMode.Single);
	}
}

