﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Script used to clear the opposite toggle when the toggle that this script is attached to is pressed
public class ToggleSwitch : MonoBehaviour {

	public Toggle oppToggle;
	private Toggle thisToggle;
	// Use this for initialization
	void Start () 
	{
		thisToggle = GetComponent<Toggle>();
		thisToggle.onValueChanged.AddListener((bool yes) => 
		{
			if (thisToggle.isOn == true)
			{
				oppToggle.isOn = false;
			}
		});
	}




}
