﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ElectricalMeasurement : MonoBehaviour {

	public List<Page> Pages = new List<Page>();
	public int i = 0;
	public GameObject notice;
	public Text pageNumber;
	public GameObject ScoreButton;
	public ElectricMeasurementQuestions questionsScript;


	// Update is called once per frame
	void Update () 
	{
		 if (Input.GetButtonDown("SlideAdvanceForward"))
        {
            NextPage();
        }
		else if (Input.GetButtonDown("SlideAdvanceBackward"))
        {
            BackPage();
        }
		
		if(i == 57)
		{
			ScoreButton.SetActive(true);
		}
		if(i == 56)
		{
			ScoreButton.SetActive(false);
		} 
		if(i==58)
		{
			questionsScript.CheckScore();
			questionsScript.CheckFITBScore();
		}

		
	}

	public void NextPage()
	{
		if((i != (Pages.Count - 1)) && Pages[i].allowed == true)
		{
			Pages[i].slide.SetActive(false);
			Pages[i + 1].slide.SetActive(true);
			i++;
			pageNumber.text = i.ToString();

		}
		else if((i != (Pages.Count - 1)) && (Pages[i].allowed == false))
		{
			notice.SetActive(true);
		}

		Debug.Log("NextPAge");
	}

	public void BackPage()
	{		
		if(i != 0)
		{
			Pages[i].slide.SetActive(false);
			Pages[i - 1].slide.SetActive(true);
			i--;
			pageNumber.text = i.ToString();

		}
	}

	public void MakeAllowable()
	{
		Pages[i].allowed = true;
	}

	public void LoadMainMenu()
	{
		SceneManager.LoadScene("NewMainMenu", LoadSceneMode.Single);
	}
}


