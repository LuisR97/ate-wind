﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SafetyNavigation : MonoBehaviour 
{
	public List<Page> Pages = new List<Page>();
	public int i = 1;
	public GameObject notice;
	public Text pageNumber, outOf;

	void Start()
	{
		pageNumber.text = i.ToString();
		outOf.text = "/" + Pages.Count.ToString();
	}
	// Update is called once per frame
	void Update () 
	{
		 if (Input.GetButtonDown("SlideAdvanceForward"))
        {
            NextPage();
        }
		else if (Input.GetButtonDown("SlideAdvanceBackward"))
        {
            BackPage();
        }
	}

	public void NextPage()
	{
		if((i != (Pages.Count - 1)) && Pages[i].allowed == true)
		{
			Pages[i].slide.SetActive(false);
			Pages[i + 1].slide.SetActive(true);
			i++;
			pageNumber.text = (i + 1).ToString();
		}
		else if((i != (Pages.Count - 1)) && (Pages[i].allowed == false))
		{
			notice.SetActive(true);
		}
	}

	public void BackPage()
	{		
		if(i != 0)
		{
			Pages[i].slide.SetActive(false);
			Pages[i - 1].slide.SetActive(true);
			i--;
			pageNumber.text = (i + 1).ToString();
		}
	}

	public void MakeAllowable()
	{
		Pages[i].allowed = true;
	}

	public void LoadMainMenu()
	{
		SceneManager.LoadScene("NewMainMenu", LoadSceneMode.Single);
	}
}