﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BasicElectricScript : MonoBehaviour 
{
	public List<Page> Pages = new List<Page>();
	public int i = 0;
	public GameObject notice;
	public Text pageNumber;
	public GameObject ScoreButton;
	public Questions questionsScript;


	// Update is called once per frame
	void Update () 
	{
		 if (Input.GetButtonDown("SlideAdvanceForward"))
        {
            NextPage();
        }
		else if (Input.GetButtonDown("SlideAdvanceBackward"))
        {
            BackPage();
        }
		if(i == 17)
		{
			ScoreButton.SetActive(true);
		}
		if(i == 16)
		{
			ScoreButton.SetActive(false);
		}
		if(i==18)
		{
			questionsScript.CheckScore();
			questionsScript.CheckMCScore();
		}

	}

	public void NextPage()
	{
		if((i != (Pages.Count - 1)) && Pages[i].allowed == true)
		{
			Pages[i].slide.SetActive(false);
			Pages[i + 1].slide.SetActive(true);
			i++;
			pageNumber.text = i.ToString();

			if(i == 17)
			{
				ScoreButton.SetActive(true);
			}
		}
		else if((i != (Pages.Count - 1)) && (Pages[i].allowed == false))
		{
			notice.SetActive(true);
		}
	}

	public void BackPage()
	{		
		if(i != 0)
		{
			Pages[i].slide.SetActive(false);
			Pages[i - 1].slide.SetActive(true);
			i--;
			pageNumber.text = i.ToString();

			if(i == 16)
			{
				ScoreButton.SetActive(false);
			}
		}
	}

	public void MakeAllowable()
	{
		Pages[i].allowed = true;
	}

	public void LoadMainMenu()
	{
		SceneManager.LoadScene("NewMainMenu", LoadSceneMode.Single);
	}
}


