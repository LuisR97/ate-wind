﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Script used to toggle off all the other toggles when the toggle this script is attached to is pressed
public class ChoiceSwitch : MonoBehaviour {

	private Toggle thisToggle;
	public Toggle toggle1, toggle2, toggle3;

	// Use this for initialization
	void Start () 
	{
		thisToggle = GetComponent<Toggle>();
		thisToggle.onValueChanged.AddListener((bool yes) => 
		{
			if (thisToggle.isOn == true)
			{
				toggle1.isOn = false;
				toggle2.isOn = false;
				toggle3.isOn = false;
			}
		});
	}
}
