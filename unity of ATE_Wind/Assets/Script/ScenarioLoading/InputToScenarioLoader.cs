﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputToScenarioLoader : MonoBehaviour {

    //Tests as the input to the scenarioName in the ScenarioDescriptionLoader on the 

    GameObject scriptholder;
    ScenarioDescriptionLoader scenarioLoader;

    //Name to be populated in the ScenarioDescriptionLoader
    public string scenarioName;

	// Use this for initialization
	void Start () {

        scriptholder = GameObject.Find("ScenarioDescriptionLoader");
        scenarioLoader = scriptholder.GetComponent<ScenarioDescriptionLoader>();

        //scenarioLoader.AssignScenarioName(scenarioName);

	}

    public void AssignScenarioToDescriptionLoader()
    {

        scenarioLoader.AssignScenarioName(scenarioName);

    }

}
