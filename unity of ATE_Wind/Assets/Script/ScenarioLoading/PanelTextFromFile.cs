﻿//This script reads a text file and sets the text of multiple panels in the scene at run-time
//Written by Jack Moreland, May 20, 2020
//Tested on Unity 2019.2.4f1
//Usage:
//Create an empty game object and name it scriptHolder
//Add this script to a scriptHolder in the scene
//
//Create a UI Canvas
//Add a UI Panel to the canvas
//Add a UI Text-TextMeshPro to the panel
//Duplicate and adjust to create as many panels as you like
//
//Drag your canvas onto scriptHolder "My Canvas"
//Set number of panels in scriptHolder "My Panel" Size
//Drag your panels onto scriptHolder "Element 0"...
//
//Create Assets/Resources/myPanelText.txt using the following 2 lines for each panel:
//[Panel Name]
//[Panel Text]

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
//**using TMPro;

public class PanelTextFromFile : MonoBehaviour
{

    public GameObject myCanvas;
    public GameObject[] myPanel;
    //**private TextMeshProUGUI myText;

    // Start is called before the first frame update
    void Start()
    {
        //set the text for all panels at run-time
        setPanelText();
    }

    public void setPanelText()
    {
        //set path for the panel .txt file
        string path = "Assets/Resources/myPanelText.txt";

        StreamReader reader = new StreamReader(path);

        //read in the text file. It is expecting 2 lines per panel:
        //1st line is panel name as it appears in the Unity Hierarchy
        //2nd line the text that should be displayed to the panel
        //**while (reader.Peek() >= 0)
        //{
        //    //get the panel name
        //    myText = myCanvas.transform.Find(reader.ReadLine()).Find("Text (TMP)").GetComponent<TextMeshProUGUI>();

        //    //set the panel text
        //    myText.text = reader.ReadLine();
        //**}
        
        reader.Close();
    }

}
