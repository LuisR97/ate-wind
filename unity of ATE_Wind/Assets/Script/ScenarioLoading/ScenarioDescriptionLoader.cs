﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Created by Kyle Toth 5/20/2020
//

public class ScenarioDescriptionLoader : MonoBehaviour {

    //This script is going to work similar to a combination the player preferences file script and custom scenario script
    //The function will be to allow a folder named ScenarioDescriptions to have .txt files uploaded into it for scenarios that will be loaded at several points to allow different scenarios to have different text loaded in

    //Gameobjects that will be written to (can be expanded as new things are identified)
    //public GameObject scadaText;

    //public string scadaInputText;

    //Path to ScenarioDescriptions folder (will be generated if it cannot be found)
    public string folderPath;

    //File Currently Called (Will generate a placeholder if one does not exist) 
    public string fileCalled;

    //Final File Name Generated w/ path
    public string fullFilePath;

    //Scenario Name and boolean to track change
    public string scenarioName;
    public bool scenarioChange;

    public GameObject[] listOfScenarioLoaders;

	// Use this for initialization
	void Start () {

        FolderInitializationRoutine();
        DestroyRedundantScenarioLoaderAtMainMenu();

	}
	
	// Update is called once per frame
	void Update () {
		
        if (scenarioChange == true)
        {

            CallforFile(scenarioName);
            scenarioChange = false;

        }

	}

    private void FolderInitializationRoutine()
    {

        folderPath = Application.dataPath + "/ScenarioTextFiles/";

        //Check if Folder at path exists, otherwise create it
        if (!Directory.Exists(folderPath))
        {

            Debug.Log("Directory at location " + folderPath + " does not exist, creating...");
            Directory.CreateDirectory(folderPath);


        }
        else
        {

            Debug.Log("Directory at location " + folderPath + " exists");

        }
    }

    //Make function that checks for the file requested 
    private void CallforFile (string fileName, string fileExtension = ".txt")
    {

        string tempName = fileName;
        string tempExtension;
        string tempFullName;

        //Basic check for correct file extension format
        if (fileExtension[0] != '.' || (fileExtension == null))
        {

            tempExtension = ".txt";

        } else
        {

            tempExtension = fileExtension;

        }

        tempFullName = tempName + tempExtension;

        //Check for file existence
        if (!File.Exists(folderPath + tempFullName)) 
        {

            Debug.Log("File " + tempFullName + " does not exist at location " + folderPath + ", creating...");

            File.CreateText(folderPath + tempFullName);
            File.CreateText(folderPath + tempFullName).Dispose();
            //DefaultFileGeneration(tempName, tempExtension);

        } else
        {

            Debug.Log("File " + folderPath + tempFullName + " exists");

        }

        fullFilePath = folderPath + tempFullName;

    }

    //If file did not exist at check, generate the file, and then populate it using this function
    private void DefaultFileGeneration (string filename, string fileExtension = ".txt")
    {

        //This is a placeholder for now

    }

    //This will scan the .txt file for a keyword associated with different 
    public void SetPanelText (string Keyword, Text panelText) 
    {

        //Kyles version of the text to panel script that Jack supplied that uses a slightly different system

        //Keeping track of current line being read in from file
        string currentLine = null;

        //Bool set up to check for the keyword
        bool startReadingIn = false;

        //Bool to flag end of content
        bool stopReadingIn = false;

        //Temporary variables to allow us to manipulate the inputs without changing them directly
        string tempKeyword = Keyword;

        //Variable to hold collective panel text to be inserted
        string bodyText = null;

        //Variable to hold memory of data in case there is nothing to read
        string tempString = panelText.text;

        //Trim white-space to keep user error to a minimum
        tempKeyword = tempKeyword.Trim();

        StreamReader reader = new StreamReader(fullFilePath);

        Debug.Log("File is ready to read.");

        //Unlike Jack's script, this script is going to read in lines to match exactly how it looks in the text, after seeing the specified Keyword

        //Read in line from file
        while ((currentLine = reader.ReadLine()) != null)
        {

            if (stopReadingIn == true)
            {

                stopReadingIn = false;
                break;

            }
            
            //If it sees the Keyword on the sheet (ex. *SCADA Readout*), prepare to read in
            if (currentLine.Trim() == "*"+tempKeyword+"*")
            {

                Debug.Log("Keyword has been detected");
                startReadingIn = true;

            } else if (startReadingIn == true)
            {
                Debug.Log("Reading in line... " + currentLine);
                if (currentLine.Trim() == "//")
                {

                    startReadingIn = false;
                    stopReadingIn = true;
                    break;

                }

                Debug.Log("Adding line... " + currentLine);
                bodyText = bodyText + currentLine + '\n';

            }

        }

        //If it reads in nothing, leave loop and reset 
        Debug.Log("File is either ended or empty.");
        startReadingIn = false;
        stopReadingIn = false;

        reader.Close();

        if ((bodyText != null) && (bodyText != ""))
        {

            panelText.text = bodyText;

        }
        //else
        //{

        //    panelText.text = tempString;

        //}
        
        

    }


    //Assigns the scenario we are loading, and can be called either from another function, or by a button with a string input
    public void AssignScenarioName (string scenario)
    {

        scenarioName = scenario;
        scenarioChange = true; 

    }

    //Using this as an example

    //public void setPanelText()
    //{

    //    //Jack's file from his PanelTextFromFiles.cs

    //    //set path for the panel .txt file
    //    string path = "Assets/Resources/myPanelText.txt";

    //    StreamReader reader = new StreamReader(path);

    //    //read in the text file. It is expecting 2 lines per panel:
    //    //1st line is panel name as it appears in the Unity Hierarchy
    //    //2nd line the text that should be displayed to the panel
    //    while (reader.Peek() >= 0)
    //    {
    //        //get the panel name
    //        myText = myCanvas.transform.Find(reader.ReadLine()).Find("Text (TMP)").GetComponent<TextMeshProUGUI>();

    //        //set the panel text
    //        myText.text = reader.ReadLine();
    //    }

    //    reader.Close();
    //}

    /// <summary>
    /// This will allow the Scenario Loader to be destroyed when reloading the main scene
    /// </summary>
    public void DestroyRedundantScenarioLoaderAtMainMenu ()
    {

        //listOfScenarioLoaders = new GameObject[GameObject.FindGameObjectsWithTag("ScenarioDescriptionLoader").Length];

        listOfScenarioLoaders = GameObject.FindGameObjectsWithTag("ScenarioDescriptionLoader");


        foreach (GameObject scenarioLoader in listOfScenarioLoaders)
        //if ((SceneManager.GetActiveScene().name == "NewMainMenu") && (scenarioName != ""))
        if (scenarioLoader != gameObject)
        {

            Destroy(scenarioLoader);

        }
        

    }

}
