﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadPanelTextFromScenarioLoader : MonoBehaviour {

    GameObject scriptholder;
    ScenarioDescriptionLoader scenarioLoader;

    public string keywordToSearch;

    // Use this for initialization
    void Start () {

        scriptholder = GameObject.Find("ScenarioDescriptionLoader");

        if (scriptholder != null)
        {

            scenarioLoader = scriptholder.GetComponent<ScenarioDescriptionLoader>();

        }
        

        if (keywordToSearch != null) {

            LoadPanelText(keywordToSearch);
                
         }

    }
	
    public void LoadPanelText (string keyword)
    {

        scenarioLoader.SetPanelText(keyword, this.gameObject.GetComponent<Text>());

    }

}
