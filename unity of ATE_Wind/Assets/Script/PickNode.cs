﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//this script is attached to each node button in the parent picker window
//when clicking the button, it finds the parent picker script and sets the node in the script to the corresponding node on the button

public class PickNode : MonoBehaviour {

    public string ObjectName;
    public ParentPicker p;
    

    public void Start() {
        if(p==null) {
            p = GameObject.Find("ParentPicker").GetComponent<ParentPicker>();
        }

        GetComponent<Button>().onClick.AddListener(PickNodeFunction);
    }

    public void PickNodeFunction() {
        Debug.Log("Picked node: " + ObjectName + "-" + transform.name);

        p.SetNode(ObjectName + "-" + transform.name);

        if (gameObject.transform.parent.parent.gameObject.name.Contains("Pick Parent"))
        {

            transform.parent.parent.gameObject.SetActive(false);    //this closes the panel after selecting a node

        } else
        {

            gameObject.transform.parent.parent.parent.parent.parent.gameObject.SetActive(false);

        }
        
    }


}
