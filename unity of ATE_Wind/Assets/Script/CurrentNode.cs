﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentNode : MonoBehaviour {

    public GameObject currentlyEditingNode;

    public void UpdateValues() {
        currentlyEditingNode.GetComponent<EditNodeValues>().UpdateValues();
    }

    public void PickParent() {
        currentlyEditingNode.GetComponent<EditNodeValues>().PickParent();
    }
}
