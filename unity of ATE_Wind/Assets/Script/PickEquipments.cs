﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PickEquipments : MonoBehaviour {

    public GameObject ComputerScreen_Panel;

    public GameObject SafetySheet_Panel;
    public GameObject _SafetySheet;
    public GameObject Pickup_Panel;
	public GameObject Pickup_Panel_Image;
	public GameObject Pickup_Panel_InspectText;
    public EquipmentConnection EquipConection;
	public GameObject _Earplug;
	public GameObject _Gloves;
	public GameObject _Multimeter;
	public GameObject _N_R_Lanyard;
	public GameObject _Lanyard;
	public GameObject _Harness;
	public GameObject _Glasses;
	public GameObject _Helmet;
	public GameObject _Level4Equip;
	public GameObject _Level2Equip;
    public BoxCollider cellingNacelle; 

	public bool B_Earplug;
	public bool B_Gloves;
	public bool B_Multimeter;
	public bool B_N_R_Lanyard;
	public bool B_Lanyard;
	public bool B_Harness;
	public bool B_Glasses;
	public bool B_Helmet;
	public bool B_Level4Equip;
	public bool B_Level2Equip;
    public bool B_WarehouseScene;  // true if it is in the warehouse scene // set in the inspector to true bc default is false
    public bool B_TurbineOnlyScene;  // true if it is in the turbine scene - starts in the turbine scene // set in the inspector to true bc default is false
	public Sprite[] Equipment_Image;

    public int countTies = 0;
    //
    public EquipmentConnection equipConnect;

	//For inventory system
	public const int TOTAL =12;
	public int x = 0;    // x is the index of the Sprites in the Equipment Image array
	public int c = 0;   // c is the index of the Gameobject slots array. it is the index of the items in the Inventory array and is consecutive
	public GameObject[] slots = new GameObject[TOTAL];  //inventory array 
	public GameObject SlotPanel;
	public List<bool> slotList;   //list of boolean to track the elements in the inventory  

	public string[] namesEqui = new string[TOTAL];   // name of the current equipmets in the inventory 

	//
	// output
	public string nameOfEquipLevel4;
	public string nameOfEquipLevel2;
	public bool isLevel4InInventory;
	public bool inInventoryL4;   // to set level 4 in inventory 
	public bool readyToSwitchL4;

	public bool isLevel2InInventory;
	public bool inInventoryL2;  // to set level 2 in inventory 
	public bool readyToSwitchL2;
	public int indexL2 = -1;
	public int indexL4 = -1;
	public int pos;
    public int numberOfEquipmentsInInventory = 0;

    public string nameOfHarness;
    public bool isHarnessInInventory;
    public bool isSafetyRopeRightSelected;
    public bool isSafetyRopeLeftSelected;
    public bool isCarabinerRightSelected;
    public bool isCarabinerRight2Selected;
    public bool isCarabinerRight3Selected;
    public bool isCarabinerLeftSelected;
    public bool isCarabinerLeft2Selected;
    public bool isCarabinerLeft3Selected;
    public bool isHatchSelected;
    public bool isHatchBackSelected;

    public bool isHandrailRight_1Selected;
    public bool isHandrailRight_2Selected;
    public bool isHandrailRight_3Selected;
    public bool isHandrailLeft_1Selected;
    public bool isHandrailLeft_2Selected;
    public bool isHandrailLeft_3Selected;


    public bool isPanelReadyToOpen;

    // Use this for initialization
    void Start () {
        if(GameObject.FindGameObjectWithTag("InventoryTransfer")!=null) {
            GameObject.FindGameObjectWithTag("InventoryTransfer").GetComponent<InventorySceneTransfer>().OverrideCurrentInventory();
        }

    }
	
	// Update is called once per frame
	void Update () {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast (ray, out  hit, 1000)) {
			if (Input.GetMouseButtonDown (0)) 
			{
				if(Pickup_Panel_InspectText!=null) {
                    Pickup_Panel_InspectText.SetActive(false);
                }
				


				if (hit.transform.tag == "EarPlug") 
				{
					B_Earplug = true;
					Pickup_Panel.SetActive (true);
					_Earplug.SetActive (false);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[0];
				}


				if (hit.transform.tag == "Harness") 
				{
					B_Harness = true;
					Pickup_Panel.SetActive (true);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[1];
					_Harness.SetActive (false);
				}

				if (hit.transform.tag == "safety_glasses2") 
				{
					B_Glasses = true;
					Pickup_Panel.SetActive (true);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[2];
					_Glasses.SetActive (false);

				}
				if (hit.transform.tag == "Gloves") 
				{
					B_Gloves = true;
					Pickup_Panel.SetActive (true);
					_Gloves.SetActive (false);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[3];
				}

				if (hit.transform.tag == "Multimeter") 
				{
					B_Multimeter = true;
					Pickup_Panel.SetActive (true);
					_Multimeter.SetActive (false);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[4];
				}
				if (hit.transform.tag == "N_R_Lanyard") 
				{
					B_N_R_Lanyard = true;
					_N_R_Lanyard.SetActive (false);
					Pickup_Panel.SetActive (true);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[5];
				}

				if(hit.transform.tag== "Self lifeline Lanyard")
				{
					B_Lanyard=true;
					Pickup_Panel.SetActive(true);
					_Lanyard.SetActive(false);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[6];
				}

				if (hit.transform.tag == "Helmet") 
				{
					B_Helmet = true;
					Pickup_Panel.SetActive (true);
					_Helmet.SetActive (false);
					Pickup_Panel_Image.GetComponent<Image>().sprite=Equipment_Image[7];
				}

				if(hit.transform.tag == "Level4Equip")
                {
					B_Level4Equip = true;
                    Pickup_Panel.SetActive(true);
                    //_Level4Equip.SetActive(false);  //Since we want to replace this item we dont want it to be innactive at this stage
                    Pickup_Panel_Image.GetComponent<Image>().sprite = Equipment_Image[8];
                }
                else if(hit.transform.tag == "Level2Equip")
				{
                    B_Level2Equip = true;
                    Pickup_Panel.SetActive(true);
					//_Level2Equip.SetActive(false);	//Since we want to replace this item we dont want it to be innactive at this stage
					Pickup_Panel_Image.GetComponent<Image>().sprite = Equipment_Image[9];
                }

                /*if (hit.transform.tag == "SafetyRope")
                {
                    B_Helmet = true;
                    Pickup_Panel.SetActive(true);
                    _Helmet.SetActive(false);
                    Pickup_Panel_Image.GetComponent<Image>().sprite = Equipment_Image[7];
                }*/

                if (hit.transform.tag == "SafetyRopeRight")//need to delete after safety at necelle is completed
                {
                    isSafetyRopeRightSelected = true;
                    countTies = 3;
                }

                if (hit.transform.tag == "SafetyRopeLeft")//need to delete after safety at necelle is completed
                {
                    isSafetyRopeLeftSelected = true;
                }

                if (hit.transform.tag == "CarabinerRight") 
                {
                    isCarabinerRightSelected = true;
                    countTies = 3;
                    isPanelReadyToOpen = true;
                }

                if (hit.transform.tag == "CarabinerRight2") 
                {
                    isCarabinerRight2Selected = true;
                }

                if (hit.transform.tag == "CarabinerRight3") 
                {
                    isCarabinerRight3Selected = true;
                }

                if (hit.transform.tag == "CarabinerLeft")
                {
                    isCarabinerLeftSelected = true;
                    countTies = 3;
                    isPanelReadyToOpen = true;
                }

                if (hit.transform.tag == "CarabinerLeft2")
                {
                    isCarabinerLeft2Selected = true;
                }

                if (hit.transform.tag == "CarabinerLeft3")
                {
                    isCarabinerLeft3Selected = true;
                }

                if (hit.transform.tag == "Handrail_Right_1")
                {
                    isHandrailRight_1Selected = true;
                    countTies = 1;
                }

                if (hit.transform.tag == "Handrail_Right_2")
                {
                    isHandrailRight_2Selected = true;

                }

                if (hit.transform.tag == "Handrail_Right_3")
                {
                    isHandrailRight_3Selected = true;
                    
                }

                if (hit.transform.tag == "Handrail_Left_1")
                {
                    isHandrailLeft_1Selected = true;
                    countTies = 2;
                }

                if (hit.transform.tag == "Handrail_Left_2")
                {
                    isHandrailLeft_2Selected = true;
                }

                if (hit.transform.tag == "Handrail_Left_3")
                {
                    isHandrailLeft_3Selected = true;
                }

                if (hit.transform.tag == "Hatch")
                {
                    isHatchSelected = true;
                    Debug.Log("-------------------------entro en el hatch");
                }

                if (hit.transform.tag == "HatchBack")
                {
                    isHatchBackSelected = true;
                    Debug.Log("-------------------------entro en el hatchBack");
                }

                if (hit.transform.tag=="HubSafetySheet")
				{
					SafetySheet_Panel.SetActive(true);
				}
			
				if(hit.transform.tag=="ComputerScreen")
				{
					ComputerScreen_Panel.SetActive(true);
				}
			}
		}

		GetNameOfEquipments();
		SetIndexOfEquipmentL2L4();
		CheckAndSet_IfL2InInventory();
		CheckAndSet_IfL4InInventory();
        CheckHarnessInInventory();
        RemoveOneEquipment(slots);
		c = numberOfEquipmentsInInventory;
	}


	public void SetActive(GameObject g)
	{
		if (g.activeSelf) {
			g.SetActive (false);
		} else 
		{
			g.SetActive(true);
		}
	}

	public void Cancel()
	{
		Pickup_Panel.SetActive (false);
		if (B_Earplug) 
		{
			_Earplug.SetActive (true);
			B_Earplug = false;
		}

		if (B_Gloves) 
		{
			_Gloves.SetActive (true);
			B_Gloves = false;
		}
		if (B_Multimeter) 
		{
			_Multimeter.SetActive (true);
			B_Multimeter = false;
		}
		if (B_N_R_Lanyard) 
		{
			_N_R_Lanyard.SetActive (true);
			B_N_R_Lanyard = false;

		}

		if (B_Harness)
		{
			_Harness.SetActive (true);
			B_Harness = false;
		}
		if (B_Glasses) 
		{
			_Glasses.SetActive (true);
			B_Glasses = false;
		}
		if (B_Helmet) 
		{
			_Helmet.SetActive (true);
			B_Helmet = false;
		}

		if (B_Level4Equip) 
		{
            _Level4Equip.SetActive (true);
            B_Level4Equip = false;
		}

        if (B_Level2Equip) 
		{
            _Level2Equip.SetActive (true);
            B_Level2Equip = false;
		}



	}

	public void Pick_Yes()
	{
		Pickup_Panel.SetActive (false);
		if (B_Earplug) 
		{
			_Earplug.SetActive (false);
			B_Earplug = false;
			x=0;
		}

		if (B_Gloves) 
		{
			_Gloves.SetActive (false);
			B_Gloves = false;
			x=3;
		}
		if (B_Multimeter) 
		{
			_Multimeter.SetActive (false);
			B_Multimeter = false;
			x=4;
		}
		if (B_N_R_Lanyard) 
		{
			_N_R_Lanyard.SetActive (false);
			B_N_R_Lanyard = false;
			x=5;
		}

		if (B_Harness)
		{
			_Harness.SetActive(false);
			B_Harness = false;
			x=1;
		}
		if (B_Glasses) 
		{
			_Glasses.SetActive (false);
			B_Glasses = false;
			x=2;
		}
		if (B_Lanyard) 
		{
			_Lanyard.SetActive(false);
			B_Lanyard=false;
			x=6;
		}
		if (B_Helmet) 
		{
			_Helmet.SetActive (false);
			B_Helmet = false;
			x = 7;
		}

		if (B_Level4Equip)	//CHECK THIS 
		{
			_Level4Equip.SetActive(false);
			B_Level4Equip = false;
			inInventoryL4 = true;
			x = 8;
			readyToSwitchL2 = true;
		}

		if (B_Level2Equip)  //CHECK THIS
		{
			readyToSwitchL4 = true;
			_Level2Equip.SetActive(false);
			B_Level2Equip = false;
			inInventoryL2 = true;
			x = 9;
		}
	}

	public void Pick_Inspect()
	{
		Pickup_Panel_InspectText.SetActive (true);
	}



	//public void AddtoInventory()   //Original code
	//{
	//	if (slotList.Contains (false))
	//       {
	//		c = slotList.IndexOf(false);
	//	}
	//       else
	//       {
	//		c = slotList.Count;
	//		slotList.Add (true);		
	//	} 
	//	slots[c].GetComponent<Image>().sprite =Equipment_Image[x];

	//       GameObject.FindGameObjectWithTag("InventoryTransfer").GetComponent<InventorySceneTransfer>().UpdateThisInventory(slots);

	//   }


	/*public void AddtoInventory()	//SECOND ORIGINAL FULLIY WORKING 
    {


		EquipConection = GameObject.FindGameObjectWithTag("ScriptHolder").GetComponent<EquipmentConnection>();
        int currentSize = EquipConection.currentSize + 1;

        if(B_WarehouseScene == true)
        {
            if (slotList.Contains(false))
            {
                c = slotList.IndexOf(false);
            }
            else
            {
                c = slotList.Count;
                slotList.Add(true);
            }
        }
        else
        {
            c = currentSize;
            slotList.Add(true);
        }

		slots[c].GetComponent<Image>().sprite = Equipment_Image[x];

		GameObject.FindGameObjectWithTag("InventoryTransfer").GetComponent<InventorySceneTransfer>().UpdateThisInventory(slots);  //or
    }*/	
	public void AddtoInventory()
    {
		// c - is the pointer to a new empty slot in the invetory

		EquipConection = GameObject.FindGameObjectWithTag("ScriptHolder").GetComponent<EquipmentConnection>();
		int currentSize = EquipConection.currentSize + 1;

		if (B_WarehouseScene == true)
        {
            if (slotList.Contains(false))
            {
                c = slotList.IndexOf(false);
            }
            else
            {
				if(isLevel2InInventory == true && readyToSwitchL2 == true )
                {
					c = -1;
					readyToSwitchL2 = false;
					isLevel4InInventory = false;
					Debug.Log("entra 1 warehouse");
				}
				else if(isLevel4InInventory == true && readyToSwitchL4 == true)
                {
					c = -1;
					readyToSwitchL4 = false;
					isLevel2InInventory = false;
					Debug.Log("entra 2 warehouse");
				}
                else
                {
					c = slotList.Count;
					slotList.Add(true);
					Debug.Log("entra 3 warehouse");
				}
            }
        }
        else
        {
			if(B_TurbineOnlyScene == true)
            {
				if (isLevel2InInventory == true && readyToSwitchL2 == true)
				{
					c = -1;
					readyToSwitchL2 = false;
					isLevel4InInventory = false;
					Debug.Log("entra 1 warehouse");
				}
				else if (isLevel4InInventory == true && readyToSwitchL4 == true)
				{
					c = -1;
					readyToSwitchL4 = false;
					isLevel2InInventory = false;
					Debug.Log("entra 2 warehouse");
				}
				else
				{
					c = slotList.Count;
					//c = slotList.IndexOf(false); ;
					slotList.Add(true);
					Debug.Log("+++++++++++++++++only the turbine scene");
				}
			}
			else
            {
				if (isLevel2InInventory == true && readyToSwitchL2 == true)
				{
					c = -1;
					readyToSwitchL2 = false;
					isLevel4InInventory = false;
					Debug.Log("================================entra 4");
				}
				else if (isLevel4InInventory == true && readyToSwitchL4 == true)
				{
					c = -1;
					readyToSwitchL4 = false;
					isLevel2InInventory = false;
					Debug.Log("==============================entra 5");
				}
				else
				{
					c = currentSize;
					slotList.Add(true);
					Debug.Log("=======================entra 6");
				}
			}      
        }
		
		if(c != -1)
        {
			if(B_TurbineOnlyScene == true)
            {
				slots[c].GetComponent<Image>().sprite = Equipment_Image[x];
			}
            else
            {
				slots[c].GetComponent<Image>().sprite = Equipment_Image[x];
				GameObject.FindGameObjectWithTag("InventoryTransfer").GetComponent<InventorySceneTransfer>().UpdateThisInventory(slots);  //or
			}

		}
		
    }



	public void GetNameOfEquipments()
    {
		for (int i = 0; i < slots.Length; i++)
		{
			if (slots[i].GetComponent<Image>().sprite != null)
			{
				//Debug.Log("the elements in the inventory are : " + slots[i].GetComponent<Image>().sprite.name);
				namesEqui[i] = slots[i].GetComponent<Image>().sprite.name;
				numberOfEquipmentsInInventory= i;
			}
		}
	}

	public int GetIndexOfEquipment(string[] array, string targetEquipment)
    {
		// Search for the first occurrence of the duplicated value.
		string searchString = targetEquipment;
		int index = System.Array.IndexOf(array, searchString);
		//Debug.Log("The first occurrence of " + searchString + " is at index" + index);
		return index;
	}

	public void SetIndexOfEquipmentL2L4()
    {
		indexL2 = GetIndexOfEquipment(namesEqui, nameOfEquipLevel2);
		indexL4 = GetIndexOfEquipment(namesEqui, nameOfEquipLevel4);
	}

    //Find if an element is in the array of names of the inventory
    public void CheckAndSet_IfL2InInventory()
    {
        string value1 = System.Array.Find(namesEqui, element => element.Equals(nameOfEquipLevel2));
        //Debug.Log("the values is ------------- " + value1);
        if (value1 == nameOfEquipLevel2)
        {
            isLevel2InInventory = true;
        }
	}

	public void CheckAndSet_IfL4InInventory()
	{
		string value1 = System.Array.Find(namesEqui, element => element.Equals(nameOfEquipLevel4));
        //Debug.Log("the values is ------------- " + value1);
        if (value1 == nameOfEquipLevel4)
        {
            isLevel4InInventory = true;
        }
	}

    public void CheckHarnessInInventory()
    {
        string value1 = System.Array.Find(namesEqui, element => element.Equals(nameOfHarness));
        //Debug.Log("ENTRO???the values is ------------- " + value1);
        if (value1 == nameOfHarness)
        {
            isHarnessInInventory = true;
        }
    }

    public void RemoveOneEquipment(GameObject[] slotArray)
    {
		if (isLevel4InInventory == true && inInventoryL4 == true)
		{
			pos = indexL4;
			if (isLevel2InInventory == false && B_Level2Equip == true)
			{
				slotArray[pos].GetComponent<Image>().sprite = Equipment_Image[9];   // CHANGE TO L2
				_Level4Equip.SetActive(true);
				isLevel2InInventory = true;
				isLevel4InInventory = false;
				inInventoryL4 = false;

			}
		}

		if (isLevel2InInventory == true && inInventoryL2 == true)
		{
			pos = indexL2;
			if (isLevel4InInventory == false && B_Level4Equip == true)
			{
				slotArray[pos].GetComponent<Image>().sprite = Equipment_Image[8];   //ChangeColor to l4
				_Level2Equip.SetActive(true);
				isLevel4InInventory = true;
				isLevel2InInventory = false;
				inInventoryL2 = false;
			}
		}
	}
}
