﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class CursorControl : MonoBehaviour {

    //the first person controller script which needs to be disabled to move the cursor without camera movement
    public FirstPersonControllerFly fps;

    //the current locked status, serialized to show in the inspector
    [SerializeField] private bool isLocked;


    void Start() {
        //sets the locked variable to the current enabled status of the fps on start
        isLocked = fps.enabled;
    }

    void Update () {
		//if user presses cursor control button as defined in input manager
        if(Input.GetButtonDown("CursorControl")) {

            //toggle conditional
            if(isLocked) {
                //disables fps controller, unlocks and shows cursor, sets locked to false
                fps.enabled = false;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                isLocked = false;
            }else {
                //enables fps controller, locks and hides cursor, sets locked to true
                fps.enabled = true;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                isLocked = true;
            }
        }

	}
}
