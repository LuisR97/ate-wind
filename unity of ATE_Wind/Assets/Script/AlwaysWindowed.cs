﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this script was designed to make the game force windowed mode, even if fullscreen is selected in the client
//this was a requested feature
//note that this change will happen directly after the splash screen

public class AlwaysWindowed : MonoBehaviour {

    //this setting is changed in the inspector on the first scene (check the build settings to find the first scene)
	public enum screenMode {defaultMode, forceWindowed, forceFullscreen };

    //by default, it is set to force windowed
    public screenMode currenScrrenMode = screenMode.forceWindowed;

    //based on the settings, it will change the screen mode (if set to default, the script will do nothing, leaving the current state intact)
    void Start() {
        if(currenScrrenMode==screenMode.forceWindowed) {
            Screen.fullScreen = false;
        }
        if(currenScrrenMode==screenMode.forceFullscreen) {
            Screen.fullScreen = true;
        }
    }
}
