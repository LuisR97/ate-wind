﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadSceneButton : MonoBehaviour {

    //this script is simply used to link each button to a directory and load that directoy on click
    public string path;
    private bool wasPressedAlready = false;

    public void LoadScene() {
        if(wasPressedAlready) {
            return;
        }

        Debug.Log("Loading scene: " + path);
        GameObject.Find("CustomSceneHolder").GetComponent<CustomSceneDontDestroyOnLoad>().CustomScenePath = path;

        GameObject.Find("LevelLoader").GetComponent<LoadingBar>().loadingBar.SetActive(true);
        GameObject.Find("LevelLoader").GetComponent<LoadingBar>().LoadMainScene();

        wasPressedAlready = true;
    }

    public void Start() {
        GetComponent<Button>().onClick.AddListener(LoadScene);
    }
}
