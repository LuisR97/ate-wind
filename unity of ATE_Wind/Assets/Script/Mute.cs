﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mute : MonoBehaviour {

	// Use this for initialization
	public void ifMute()
	{
		AudioListener.pause = !AudioListener.pause;
	}
		
}
