﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ObjectivePanel : MonoBehaviour {

    public string objectiveTextString;
    public string scadaTextString;
    public string currentObjectiveTextString;
    //public List<Text> objectiveArray;

    //public Text objectiveText;
    //public Text scadaText;
    public Text currentObjectiveText;

    // Use this for initialization
    void Start () {

        objectiveTextString = "Your Objective:";

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //public void GetTextFromScada (Text input)
    //{

    //    scadaText = input;
    //    scadaTextString = input.text;

    //}

    public void FormTextArrayForObjectiveText ()
    {

        currentObjectiveTextString = objectiveTextString + "\n" + "\n" + scadaTextString;

    }

    public void WriteObjectiveText()
    {

        if (currentObjectiveTextString != null)
        {

            currentObjectiveText.text = currentObjectiveTextString;

        }
        
    }
}
