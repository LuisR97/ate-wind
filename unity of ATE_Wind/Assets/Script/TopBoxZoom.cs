﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopBoxZoom : MonoBehaviour {

    public RectTransform content;

    public enum ZoomMode { Smooth, Sharp };
    public enum ZoomDirection { ZoomIn, ZoomOut };
    public float zoomPerFrame = 0.025f;

    public ZoomMode mode = ZoomMode.Sharp;

    public float maxZoom = 5f;
    public float minZoom = 0.5f;

    public float zoomAmount = 0.5f;

    private bool CoroutineIsRunning = false;

    public void Start() {
        zoomAmount = content.localScale.x;
    }

    public void ZoomIn() {
        if(zoomAmount+0.5f>maxZoom || CoroutineIsRunning) {
            return;
        }

        if(mode==ZoomMode.Sharp) {
            zoomAmount += 0.5f;
            UpdateZoom();
        }else if(mode==ZoomMode.Smooth) {
            StartCoroutine(SmoothZoom(zoomAmount + 0.5f, ZoomDirection.ZoomIn));
        }
    }

    public void ZoomOut() {
        if(zoomAmount-0.5f<minZoom || CoroutineIsRunning) {
            return;
        }

        if (mode == ZoomMode.Sharp) {
            zoomAmount -= 0.5f;
            UpdateZoom();
        } else if (mode == ZoomMode.Smooth) {
            StartCoroutine(SmoothZoom(zoomAmount - 0.5f, ZoomDirection.ZoomOut));
        }
    }

    public void ZoomInMultiply() {
        if (zoomAmount * 2f > maxZoom || CoroutineIsRunning) {
            return;
        }

        if (mode == ZoomMode.Sharp) {
            zoomAmount *= 2f;
            UpdateZoom();
        } else if (mode == ZoomMode.Smooth) {
            StartCoroutine(SmoothZoom(zoomAmount *2f, ZoomDirection.ZoomIn));
        }
    }

    public void ZoomOutMultiply() {
        if (zoomAmount/2f< minZoom || CoroutineIsRunning) {
            return;
        }

        if (mode == ZoomMode.Sharp) {
            zoomAmount /= 2f;
            UpdateZoom();
        } else if (mode == ZoomMode.Smooth) {
            StartCoroutine(SmoothZoom(zoomAmount /2f, ZoomDirection.ZoomOut));
        }
    }

    public void UpdateZoom() {
        content.localScale = new Vector2(zoomAmount,zoomAmount);
    }


    

    public IEnumerator SmoothZoom(float targetZoom, ZoomDirection dir) {
        CoroutineIsRunning = true;
        if(dir==ZoomDirection.ZoomIn) {
            while (zoomAmount<targetZoom) {
               
                zoomAmount += zoomPerFrame;
                
                UpdateZoom();
                yield return null;
            }
        }else if(dir==ZoomDirection.ZoomOut){
            while (zoomAmount > targetZoom) {

                zoomAmount -= zoomPerFrame;

                UpdateZoom();
                yield return null;
            }
        }

        zoomAmount = targetZoom;
        UpdateZoom();
        CoroutineIsRunning = false;
        yield return null;
    }


}
