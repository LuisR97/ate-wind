using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Open_Side_Menu : MonoBehaviour {

	public GameObject Menu;
	public GameObject Floating_Text;
	public GameObject Main_Camera;
	public GameObject Main_Object;
	public Vector3 Normalized_Mouse_Location;
	public Vector3 Mouse_Location;
	public float Normalized_X;
	public float Normalized_Y;
	private GameObject[] allContextualMenus;

    public float interactionDistance = 1;

    public bool debugOn = false;

    //debug variables

    //public float screenWidth;
    //public float screenHeight;

    //public Vector3 screenSpaceMenuLocationOffset;
    //public Vector3 inputOffsetFromMousePointer;

    //public Vector3 finalLocation;
    //public float scaleConversionFactor;



    // Use this for initialization
    void Start () {

        //screenHeight = Screen.height;
        //screenWidth = Screen.width;

        //float tempWidth;
        //float tempHeight;
        //Vector3 menuZeroMatch = new Vector3(0, 0, 0);

        //tempWidth = screenWidth / 2;
        //tempHeight = screenHeight / 2;

        //scaleConversionFactor = Screen.width / (Floating_Text.transform.parent.gameObject.GetComponent<CanvasScaler>().referenceResolution.x);

        //menuZeroMatch.Set(-(tempWidth), -(tempHeight), 0);

        //menuZeroMatch = menuZeroMatch / scaleConversionFactor;

        //screenSpaceMenuLocationOffset = menuZeroMatch;


	}
	
	// Update is called once per frame
	void Update () {

		if ((Main_Camera.transform.position - Main_Object.transform.position).magnitude > interactionDistance) {
		
			Menu.SetActive (false);

		}

		allContextualMenus = GameObject.FindGameObjectsWithTag ("Menu");

        if (debugOn)
        {

            Debug.Log("The values of " + gameObject.name + " are the following:");
            Debug.Log("Distance: " + (Main_Camera.transform.position - Main_Object.transform.position).magnitude.ToString() + " HelpText: " + Floating_Text.active.ToString() + " Menu: " + Menu.active.ToString());

        }

    }

	void OnMouseOver()
	{

        if (debugOn)
        {

           Debug.Log("The Mouse is over the object in question.");

       }

        if ((Main_Camera.transform.position - Main_Object.transform.position).magnitude <= interactionDistance) {

            if (debugOn)
            {
               Debug.Log("The Distance is within range.");

            }

            Floating_Text.GetComponent<RectTransform>().localScale = new Vector3(0.2f,0.1f,0.2f);

            Mouse_Location = (Input.mousePosition);

			Normalized_X = (Mouse_Location [0] - ((Screen.width) / 2)) / 2.5f;

			Normalized_Y = (Mouse_Location [1] - ((Screen.height) / 2)) / 2.5f;

			Normalized_Mouse_Location = new Vector3 (Normalized_X, Normalized_Y, 0);

			if (Menu.activeSelf == false) {

                if (debugOn)
                {

                    Debug.Log("Flagged to turn on Help Text.");

                }
                Floating_Text.SetActive (true);

                Floating_Text.transform.localPosition = Normalized_Mouse_Location;
                //finalLocation = Mouse_Location + screenSpaceMenuLocationOffset + inputOffsetFromMousePointer;
                //Floating_Text.transform.localPosition = finalLocation;


            } else {

                if (debugOn)
                {

                    Debug.Log("Flagged to turn off Help Text.");

                }
                Floating_Text.SetActive (false);  
			    
			}

			if (Input.GetMouseButtonDown (1)) {

				foreach (GameObject a in allContextualMenus) {
					
					a.SetActive (false);

				}

				Menu.SetActive (true);

			}

		}

		else {

            if (debugOn)
            {

               Debug.Log("Flagged to reset.");

            }
            Floating_Text.SetActive (false);
			Menu.SetActive (false);

		}
	}

	void OnMouseExit()
    {

       if (debugOn)
       {

            Debug.Log("Mouse has left object in question");

       }
        Floating_Text.SetActive (false);

	}
}
