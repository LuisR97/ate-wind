﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour {


    public GameObject loadingBar;
    public LoadingBar loadingScript;

    public GameObject SkipButton;

    public bool DontRepeatTutorial = true;      //boolean to determine if the script should auto skip the tutorial if the player has already completed it

    public bool allowSkippingWithoutCompletion = false;

    public bool AutoStartTutorial = true;

    private bool hasCompletedTutorial = false;

    public GameObject[] tutorialPanels; //an array of the tutorial panels

    private int panelIndex = 0;

    void Start() {
        if(PlayerPrefs.HasKey("TutorialComplete")){
            hasCompletedTutorial = PlayerPrefs.GetInt("TutorialComplete") == 1;
        }else {
            PlayerPrefs.SetInt("TutorialComplete", -1);
        }

        if(AutoStartTutorial) {
            if(DontRepeatTutorial && hasCompletedTutorial) {
                
            }else {
                StartTutorial();
            }
        }

        if(hasCompletedTutorial || allowSkippingWithoutCompletion) {
            SkipButton.GetComponent<Button>().interactable = true;
        }else {
            SkipButton.GetComponent<Button>().interactable = false;
        }

    }

    public void StartTutorial() {
        tutorialPanels[0].SetActive(true);
        panelIndex = 0;
    }

    public void SkipTutorial() {
        foreach(GameObject panel in tutorialPanels) {
            panel.SetActive(false);
        }
        panelIndex = -1;
    }


    public void SkipTutorialButton() {
        if(hasCompletedTutorial || allowSkippingWithoutCompletion) {
            LoadTroubleshootingBoxScene();
        }
    }

    public void NextPanel() {
        if(panelIndex<tutorialPanels.Length-1) {
            panelIndex++;
            tutorialPanels[panelIndex - 1].SetActive(false);
            tutorialPanels[panelIndex].SetActive(true);
        }else {
            PlayerPrefs.SetInt("TutorialComplete", 1);
            LoadTroubleshootingBoxScene();
        }

        
    }

    public void PreviousPanel() {
        if(panelIndex>0) {
            panelIndex--;
            tutorialPanels[panelIndex + 1].SetActive(false);
            tutorialPanels[panelIndex].SetActive(true);
        }
        
    }

    public void CompleteTutorial() {
        SkipTutorial();
        PlayerPrefs.SetInt("TutorialComplete", 1);
    }

    public bool MeasurementCheck(NewMeasurement node1, NewMeasurement node2) {
        return false;
    }


    public void LoadTroubleshootingBoxScene() {
        loadingBar.SetActive(true);
        loadingScript.LoadTBoxScene();
    }

    public void Update() {
        if(Input.GetKeyDown(KeyCode.Keypad6)) {
            NextPanel();
        }
        if(Input.GetKeyDown(KeyCode.Keypad4)) {
            PreviousPanel();
        }
    }
}
