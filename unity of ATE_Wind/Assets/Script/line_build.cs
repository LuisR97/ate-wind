﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class line_build : MonoBehaviour
{
    [SerializeField] private Transform[] points;
    [SerializeField] private line_LineController lr;

    private void Start()
    {
        lr.SetUpLine(points);
    }
}