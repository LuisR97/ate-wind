﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PostProcessing;

public class GraphicsController : MonoBehaviour {

    //the dropdown object for preset selection
    public Dropdown presetDrowdown;

    //the list of posible presets, defined by class preset below
    public GraphicsPreset[] presets;

    //the various sliders and toggles for reference in the options panel
    public Slider distanceSlider;
    public Slider densitySlider;
    public Slider cameraRangeSlider;
    public Toggle ppeToggle;
    public Toggle treeToggle;

    //the distance and density with which to render the grass
    private int grassDrawDistnace;
    private float grassDensity;

    //a script on the terrain which needs to be accessed for controlling certain properties
    public TerrainDistanceController tdc;

    //shoudl post processing be used or not?
    private bool usePostProcessingEffects;

    //the necessary references for the background trees, the camera, and the post processing script on the camera
    public GameObject trees;
    public Camera cam;
    public PostProcessingBehaviour ppb;


    //applies the current settings on start
    void Start() {
        OverrideSettings();   
    }
    void Awake() {
        OverrideSettings();
    }

    //function used for getting settings from the previous scene
    void OverrideSettings() {

        //this is a dontdestroyonload object from the title for carrying graphics settings between settings

        GraphicsOverrideController gco = null;
        if(GameObject.FindGameObjectWithTag("GraphicsControllerOverride")!=null) {
            gco =GameObject.FindGameObjectWithTag("GraphicsControllerOverride").GetComponent<GraphicsOverrideController>();
            Debug.Log("found new graphics controller " + gco.gameObject.name);
        }else {
            return;
        }

        //Debug.Log(gco.name);

        /*distanceSlider.value=GraphicsOverrideController.grassDrawDistance;
        densitySlider.value = GraphicsOverrideController.grassDensity;
        ppeToggle.isOn = GraphicsOverrideController.usePostProcessingEffects;
        treeToggle.isOn = GraphicsOverrideController.renderTrees;
        cameraRangeSlider.value=GraphicsOverrideController.cameraRange;*/


        //sets all ui element values to the gco values and applies the settings
        distanceSlider.value = gco.getGrassDistance();
        densitySlider.value =gco.getGrasssDensity();
        ppeToggle.isOn = gco.getUsePPE();
        treeToggle.isOn = gco.getRenderTrees();
        cameraRangeSlider.value = gco.getCameraRange();

        gco.TestVaribaleValues();

        UpdateSettings();
    }


    //function used for applying settings from the menu in the scene
    public void UpdateSettings()
    {
        grassDrawDistnace = (int)distanceSlider.value;
        grassDensity = densitySlider.value;
        usePostProcessingEffects = ppeToggle.isOn;

        tdc.Distance = grassDrawDistnace;
        tdc.density = grassDensity;
        ppb.enabled = usePostProcessingEffects;
        trees.SetActive(treeToggle.isOn);
        cam.farClipPlane = cameraRangeSlider.value;

        //changes the settings in the dontdestroyonload object to the custom settings to carry over to next scene
        GraphicsOverrideController.ApplyCustomSettings(grassDrawDistnace, grassDensity, usePostProcessingEffects, treeToggle.isOn, cameraRangeSlider.value);

    }

    //function used for updating the preset
    public void UpdatePreset()
    {
        ApplyPreset(presetDrowdown.value);
    }

    //changes the current settings to the settings defined in the preset
    public void ApplyPreset(int index)
    {
        GraphicsPreset p = presets[index];
        distanceSlider.value = p.grassDistance;
        densitySlider.value = p.grassDensity;
        ppeToggle.isOn = p.usePPE;
        treeToggle.isOn = p.RenderTrees;
        cameraRangeSlider.value = p.CameraRange;
    }


}


//the class used for defining preset settings
[System.Serializable]
public class GraphicsPreset
{
    public string name;
    public int grassDistance;
    public float grassDensity;
    public bool usePPE;
    public bool RenderTrees;
    public int CameraRange;
}
