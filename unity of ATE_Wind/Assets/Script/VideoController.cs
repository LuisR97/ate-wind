﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoController : MonoBehaviour {

    //设置VideoPlayer、RawImage和当前播放视频索引参数    
    private VideoPlayer videoPlayer;
    private RawImage rawImage;
    private int currentClipIndex;

    //设置相关文本和按钮参数以及视频列表    
    public Text text_PlayOrPause;
    public Button button_PlayOrPause;
    public VideoClip[] videoClips;

    // Use this for initialization 
    void Start () {
        //获取VideoPlayer和RawImage组件，以及初始化当前视频索引       
        videoPlayer = this.GetComponent<VideoPlayer>();
        rawImage = this.GetComponent<RawImage>();
        currentClipIndex = 0;
        //设置相关按钮监听事件        
        button_PlayOrPause.onClick.AddListener(OnPlayOrPauseVideo);
        Debug.Log(videoPlayer);
    }

    // Update is called once per frame 
    void Update () {
        //没有视频则返回，不播放       
        if (videoPlayer.texture == null) {
            
            return;
        }
        //渲染视频到UGUI上        
        rawImage.texture = videoPlayer.texture;
    }    /// <summary>   
    /// 播放和暂停当前视频    
    /// /// </summary>    
    private void OnPlayOrPauseVideo() {
        //判断视频播放情况，播放则暂停，暂停就播放，并更新相关文本        
        if (videoPlayer.isPlaying == true) {
            videoPlayer.Pause();
            text_PlayOrPause.text = "PLAY";
        }
        else {
            videoPlayer.Play();
            text_PlayOrPause.text = "PAUSE";
        }
    }
}