﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadServerButton : MonoBehaviour {

    //this script is simply used to link each button to a directory and load that directoy on click
    private bool wasPressedAlready = false;
    public bool isOnServer = false;
    public InputField urlInput;

    public void LoadSceneFromServer() {
        if(wasPressedAlready) {
            return;
        }

        Debug.Log("Loading scene: " + urlInput.text);
        GameObject.Find("CustomSceneHolder").GetComponent<CustomSceneDontDestroyOnLoad>().CustomScenePath = urlInput.text;
        GameObject.Find("CustomSceneHolder").GetComponent<CustomSceneDontDestroyOnLoad>().isOnServer = true;

        GameObject.Find("LevelLoader").GetComponent<LoadingBar>().loadingBar.SetActive(true);
        GameObject.Find("LevelLoader").GetComponent<LoadingBar>().LoadMainScene();

        wasPressedAlready = true;
    }

    public void Start() {
        GetComponent<Button>().onClick.AddListener(LoadSceneFromServer);
    }
}
