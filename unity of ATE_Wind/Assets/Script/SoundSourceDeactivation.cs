﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSourceDeactivation : MonoBehaviour {

    //starting by establishing the sound source game object that needs to be disabled.
    //This script is assuming that the source to be disabled is going to be under one gameobject in the hierarchy, and that's the target
    //For a bunch of different object is different places, this script would need to be modified to do what the loop does, but for all objects in a user defined array
    public GameObject[] soundSourcesToDisable;
    public Sprite[] onOffSprites;

    private void Start()
    {
        
        if(this.gameObject.GetComponent<Image>() != null)
        {

            this.gameObject.GetComponent<Image>().sprite = onOffSprites[0];

        }

    }

    public void ToggleSoundSources ()
    {
        
        foreach (GameObject soundSourceToDisable in soundSourcesToDisable)
        {


            for (int i = 0; i < soundSourceToDisable.transform.childCount; i++)
            {

                if (soundSourceToDisable.transform.GetChild(i).gameObject.GetComponent<AudioSource>() != null)
                {

                    soundSourceToDisable.transform.GetChild(i).gameObject.GetComponent<AudioSource>().mute = !soundSourceToDisable.transform.GetChild(i).gameObject.GetComponent<AudioSource>().mute;

                }


                //Debug.Log("The items interacting " + soundSourceToDisable.name.ToString() + " " + soundSourceToDisable.transform.GetChild(i).gameObject.name + " " + soundSourceToDisable.transform.GetChild(i).gameObject.GetComponent<AudioSource>().mute.ToString());

            }

            if (this.gameObject.GetComponent<Image>().sprite == onOffSprites[0])
            {

                this.gameObject.GetComponent<Image>().sprite = onOffSprites[1];

            } else
            {

                this.gameObject.GetComponent<Image>().sprite = onOffSprites[0];

            }

        }



    }

}
