﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Reflection;
using System.Text;
using System;

/// <summary>
/// Read and Writes the User Preferences File in the project folder
/// </summary>
public class ReadWriteOutputFile : MonoBehaviour {

    [Tooltip("Data lines used for testing")]
    public List<string> testOutputData;

    [Tooltip("Data being read in")]
    public List<string> testInputData;

    [Tooltip("Names pulled from file for Subtopics")]
    public List<string> formattedNames;

    [Tooltip("Values pulled from files for Subtopic Values")]
    public List<string> formattedNumbers;

    [SerializeField]
    public Dictionary<string, List<string>> groupsOfSubjects = new Dictionary<string, List<string>>();

    [SerializeField]
    public Dictionary<string, List<string>> subjectValues = new Dictionary<string, List<string>>();

    [Tooltip("Main topic names (these can be used to find individual tables of Subtopics and Subtopic Values")]
    public List<string> groupNames;

    [Tooltip("The location of the project (automatically generated at start-up)")]
    public string destinationDirectoryName;

    [Tooltip("The location of the preferences file (automatically generated at start up")]
    public string destinationFileName;

    [Tooltip("Whether or not the directory for the file exists")]
    private bool playerPrefsExists = false;

    [Tooltip("Whether or not the playerPrefs file exists")]
    private bool playerPrefsFileExists = false;

    private string currentGroupName = null;
    private string currentValueGroupName = null;

    // Use this for initialization
    void Start() {

        destinationDirectoryName = Application.dataPath + "/PlayerPrefs";
        destinationFileName = destinationDirectoryName + "/PlayerPreferences.txt";

        InitializePlayerPreferences();
        //WriteDataToFile();
        ReadDataFromFile();
        FormatDataInput();
        //ClearTopicFromPlayerPrefs("Topic2");

    }

    // Update is called once per frame
    void Update() {

    }

    //---------
    /// <summary>
    /// This Function will look for the correct file directory and file name and
    /// add them if necessary
    /// </summary>
    void InitializePlayerPreferences()
    {

        playerPrefsExists = Directory.Exists(destinationDirectoryName);

        if (playerPrefsExists == false)
        {

            Directory.CreateDirectory(destinationDirectoryName);

            playerPrefsExists = Directory.Exists(destinationDirectoryName);

        } else
        {

            playerPrefsFileExists = File.Exists(destinationFileName);

            if (playerPrefsFileExists == false)
            {

                File.CreateText(destinationFileName).Dispose();

                playerPrefsFileExists = File.Exists(destinationFileName);

            }

        }

    }


    //----------
    /// <summary>
    /// Outputs data stored in a list of strings to PlayerPreferences file located
    /// at Application.dataPath/PlayerPrefs/PlayerPreferences.txt
    /// </summary>
    public void WriteDataToFile()
    {

        
        //CLEAR PREVIOUS DOCUMENT
        File.Delete(destinationFileName);

        if (!File.Exists(destinationFileName))
        {

            print("file erased successfully");

        }

        //RECREATE NEW DOCUMENT
        File.CreateText(destinationFileName).Dispose();

        if (File.Exists(destinationFileName))
        {

            print("file remade successfully");

        }

        StreamWriter outputWriter = new StreamWriter(destinationFileName, true);

        foreach (string topic in groupNames)
        {


            outputWriter.WriteLine("-" + topic);

            string lineToAdd = "";

            for (int i = 0; i < groupsOfSubjects[topic].Count; i++)
            {

                lineToAdd = lineToAdd + groupsOfSubjects[topic][i] + ": " + subjectValues[topic+"Values"][i];
                outputWriter.WriteLine(lineToAdd);
                lineToAdd = "";

            }

            outputWriter.WriteLine("" + Environment.NewLine);

        }

        outputWriter.Close();

        //    File.AppendAllText(destinationFileName, "");
        //    File.AppendAllText(destinationFileName, "-" + topic);

        //    string lineToAdd = "";

        //    for (int i = 0; i < subtopics.Count; i++)
        //    {

        //        lineToAdd = lineToAdd + subtopics[i] + ": " + subtopicValues[i];
        //        File.AppendAllText(destinationFileName, lineToAdd);

        //    }

        //} else
        //{

        //    print("Error in Input Format");

        //}



    }

    //-----------
    /// <summary>
    /// Imports data stored in a text file and outputs it into a list of strings
    /// that can
    /// </summary>
    void ReadDataFromFile()
    {

        ClearAllLists();
        string currentReadingLine;
        StreamReader fileReader = new StreamReader(destinationFileName);

        while ((currentReadingLine = fileReader.ReadLine()) != null)
        {

            //Debug.Log(currentReadingLine);
            testInputData.Add(currentReadingLine);

        }

        fileReader.Close();


        //Debug.Log("reading in data");
        //FormatDataInput();

    }

    //----------
    /// <summary>
    /// Formats data stored from reading the input using parsing
    /// Also creates sublists for each topic and values created by each
    /// </summary>
    void FormatDataInput()
    {

        string tempTitle;
        string tempTitle2;

        //ClearAllLists();

        foreach (string listItem in testInputData)
        {

            if (!(string.IsNullOrEmpty(listItem)))
            {

                if (listItem[0] == '-')
                {

                    tempTitle = "";
                    tempTitle2 = "";

                    for (int j = 1; j < listItem.Length; j++)
                    {

                        tempTitle = tempTitle + listItem[j];

                    }

                    tempTitle2 = tempTitle + "Values";

                    groupsOfSubjects.Add(tempTitle, new List<string>());
                    subjectValues.Add(tempTitle2, new List<string>());

                    currentGroupName = tempTitle;
                    currentValueGroupName = tempTitle2;

                    groupNames.Add(tempTitle);

                    Debug.Log(tempTitle);
                    Debug.Log(tempTitle2);

                }
                else
                {

                    string tempStringName = "";
                    string tempStringValue = "";
                    int i = 0;
                    while (listItem[i] != ':')
                    {


                        tempStringName = tempStringName + listItem[i];
                        i++;

                    }

                    tempStringValue = "";

                    if (listItem[listItem.Length - 2] != ' ')
                    {

                        tempStringValue = listItem[listItem.Length - 2].ToString() + listItem[listItem.Length - 1].ToString();

                    } else
                    {

                        tempStringValue = listItem[listItem.Length - 1].ToString();

                    }


                    formattedNames.Add(tempStringName);
                    formattedNumbers.Add(tempStringValue);

                    groupsOfSubjects[currentGroupName].Add(tempStringName);
                    subjectValues[currentValueGroupName].Add(tempStringValue);

                }

            }

        }

    }

    //------------
    /// <summary>
    /// Allows users to find list of Subtopics by their primary topic name
    /// </summary>
    /// <param name="topic">Name of the main topic from the player preferences</param>
    /// <returns>Subtopics from the name given in a List<string> format</returns>
    public List<string> GetSubtopicsFromName(string topic)
    {

        if (groupsOfSubjects.ContainsKey(topic))
        {

            return groupsOfSubjects[topic];

        } else
        {

            print("Keyword Not Found");
            return null;

        }



    }

    //--------------
    /// <summary>
    /// Allows users to find list of Subtopic values by primary topic name
    /// </summary>
    /// <param name="topic">Name of the main topic from the player preferences</param>
    /// <returns>Subtopic values from the name given in a List<string> format</returns>
    public List<string> GetSubtopicValuesFromName(string topic)
    {
        if (groupsOfSubjects.ContainsKey(topic))
        {

            return subjectValues[topic+"Values"];

        }
        else
        {

            print("Keyword Not Found");
            return null;

        }

    }

    public void AddToUserPreferences(string topic, List<string> subtopics, List<string> subtopicValues)
    {

        groupNames.Add(topic);
        groupsOfSubjects.Add(topic, new List<string>());
        subjectValues.Add(topic+"Values", new List<string>());

        for (int count = 0; count<subtopics.Count; count++)
        {

            groupsOfSubjects[topic].Add(subtopics[count]);
            subjectValues[topic+"Values"].Add(subtopicValues[count]);

        }

    } 

    void ClearAllLists()
    {

        groupNames.Clear();
        subjectValues.Clear();
        groupsOfSubjects.Clear();
        formattedNames.Clear();
        formattedNumbers.Clear();
        testInputData.Clear();

    }

    public void ClearTopicFromPlayerPrefs(string topicName)
    {

        if (groupsOfSubjects.ContainsKey(topicName))
        {

            groupsOfSubjects[topicName].Clear();
            subjectValues[topicName + "Values"].Clear();
            groupsOfSubjects.Remove(topicName);
            subjectValues.Remove(topicName);
            groupNames.Remove(topicName);

        } else
        {

            print("Topic doesn't exist");

        }

    }

    public void UpdateTopicValues (string topicName, List<string> subtopicNames, List<string> values)
    {

        if (groupsOfSubjects.ContainsKey(topicName))
        {

            groupsOfSubjects[topicName].Clear();
            subjectValues[topicName].Clear();

            for (int i=0; i<subtopicNames.Count; i++)
            {

                groupsOfSubjects[topicName].Add(subtopicNames[i]);
                subjectValues[topicName].Add(values[i]);

            }

        } else
        {

            print("Topic doesn't exist");

        }
        



    } 


}
