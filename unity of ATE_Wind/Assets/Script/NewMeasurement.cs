﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//this script was created from a desire to make a system where nodes inherit the disconnected status from parent nodes and all values (voltage, isAC, etc.) are stored in one script rather than multiple button onClick functions
public class NewMeasurement : MonoBehaviour {
    
    public RectTransform highProbe, lowProbe;//don't need to be set manually, the rect transforms for the draggable probes

    public enum testingMode {button, material };    //button mode is for implementation, material mode is for testing purposes only
    public testingMode mode =testingMode.button;

    public float voltageValue; 

    public bool isAC=false;

    public int phaseNumber;

    public bool isNeutral = false;

    public bool isSinglePhase = false;

    public bool isDisconnected = false;     //value showing if this object itself is disconnected
    public bool disconnectInherit;     //value showing if this object will inherit disconnect from its parent
    private bool finalDisconnect;       //value showing the final disconnect status

    private Material mat;   //if using material mode, the material which is changed in color (must be unique to the object, cannot be applied to multiple objects)

    public NewMeasurement branchParent;     //the parent node which this node inherits from
    public string branchParentName;

    public measurementAssignment multimeterCode;    //the scriptholder script for displaying measurement data

    private bool highProbeOverlapping = false, lowProbeOverlapping = false;

    public void Start() {

        if (highProbe == null)
        {
            highProbe = GameObject.Find("HighProbe").GetComponent<RectTransform>();
        }

        if (lowProbe == null)
        {
            lowProbe = GameObject.Find("BlackProbePNG").GetComponent<RectTransform>();
        }


        if (multimeterCode==null) {  //finds scriptholder measurement assignment script if not manually associated
            multimeterCode = GameObject.FindGameObjectWithTag("ScriptHolder").GetComponent<measurementAssignment>();
        }

        SetDisconnectInherit(); //checks to see if the node inherits the disconnect status (see function below)

        if(mode==testingMode.button) {  //adds the necessary onclick funcions automatically when applied to a button

            //GetComponent<Button>().onClick.AddListener(LocationButtonOnClick);

            if(GetComponent<Button>()!=null) {
                GetComponent<Button>().onClick.AddListener(DCMeasure);
            }
        }
    }


    //this functions checks whether or not this node inherits disconnect
    //if it has a parent, it runs the GetDisconnectInherit function on that node to check
    private void SetDisconnectInherit() {
        if(branchParent==null) {
            disconnectInherit = false;
        }else {
            disconnectInherit = branchParent.GetDisconnectInherit();
        }
    }


    public void DCMeasure() {
        PassValuesToMultimeter();
        //GetComponentInChildren<ProbePopup>().OnClickUpdate();
    }


    //checks if this node should return a disconnected status to its child nodes
    public bool GetDisconnectInherit() {

        if (isDisconnected == true)
        {

            return true;

        }
        else
                if (branchParent==null) {
            return false;  
        } else {
        
                return (branchParent.GetDisconnectInherit());

        }
    }

    //function which sends all relevant values to the multimeter script
    private void PassValuesToMultimeter() {

        //Debug.Log("Passing values to multimeter");

        if(finalDisconnect) {
            multimeterCode.LocationIsDisconnected();
        }

        if(isAC) {
            multimeterCode.LocationIsAC();
        }

        if(isNeutral) {
            multimeterCode.IsNeutral();
        }

        if(isSinglePhase)
        {

            multimeterCode.IsSinglePhase();

        }

        multimeterCode.LocationPhase(phaseNumber);
        multimeterCode.DCMeasurementAssignment(voltageValue);
        
    }


    //used for material mode testing, changes material color based on disconnect status to show inheritance graphically
    public void Update() {
        if(mode==testingMode.material) {
            if(mat==null && GetComponent<MeshRenderer>()!=null) {
                mat = GetComponent<MeshRenderer>().material;
            }

            if(mat!=null) {
                if(isDisconnected && !disconnectInherit) {
                    mat.color = Color.red;
                }else if(isDisconnected && disconnectInherit) {
                    mat.color = Color.yellow;
                }else if(disconnectInherit && !isDisconnected) {
                    mat.color = Color.blue;
                }else if(!disconnectInherit && !isDisconnected) {
                    mat.color = Color.green;
                }
            }
        }


        //if (rectOverlaps(highProbe, GetComponent<RectTransform>()))
        //{
        //    Debug.Log("high probe is overlapping" + gameObject.name);
        //}

        //if (rectOverlaps(lowProbe, GetComponent<RectTransform>()))
        //{
        //    Debug.Log("low probe is overlapping");
        //}
    }


    //script which is called when clicking the button to get final disconnect status and send values to multimeter before processing
    public void LocationButtonOnClick() {
        //Debug.Log("Location button on click");
        finalDisconnect = (disconnectInherit || isDisconnected) ? true : false;
        PassValuesToMultimeter();
    }


    //script which will change the current branch parent
    public void ChangeBranchParent(NewMeasurement newParent) {
        branchParent = newParent;
    }


    bool rectOverlaps(RectTransform rectTransform1, RectTransform rectTransform2)
    {
        Rect rect1 = new Rect(rectTransform1.localPosition.x, rectTransform1.localPosition.y, rectTransform1.rect.width, rectTransform1.rect.height);
        Rect rect2 = new Rect(rectTransform2.localPosition.x, rectTransform2.localPosition.y, rectTransform2.rect.width, rectTransform2.rect.height);

        return rect1.Overlaps(rect2);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "HighProbeTip")
        {
            //Debug.Log("High probe overlap");
            LocationButtonOnClick();
            multimeterCode.SetHighIsHovering(true);
            highProbeOverlapping = true;
        }
        else if (other.gameObject.name == "LowProbeTip")
        {
            //Debug.Log("Low probe overlap");
            LocationButtonOnClick();
            multimeterCode.SetLowIsHovering(true);
            lowProbeOverlapping = true;
        }

        LocationButtonOnClick();

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.name == "HighProbeTip")
        {
            //Debug.Log("High probe overlap");
            if (multimeterCode.GetHighIsHovering() == true)
            {
            LocationButtonOnClick();
            }
            transform.GetChild(2).gameObject.SetActive(true);
            transform.GetChild(0).GetComponent<ProbePopup>().OnClickUpdate();
            highProbeOverlapping = true;
        }
        else if (other.gameObject.name == "LowProbeTip")
        {
            //Debug.Log("Low probe overlap");
            if (multimeterCode.GetLowIsHovering() == true)
            {
                LocationButtonOnClick();
            }
            transform.GetChild(2).gameObject.SetActive(true);
            transform.GetChild(0).GetComponent<ProbePopup>().OnClickUpdate();
            lowProbeOverlapping = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name == "HighProbeTip")
        {
            // Debug.Log("High probe overlap");
            //LocationButtonOnClick();
            transform.GetChild(2).gameObject.SetActive(false);
            multimeterCode.SetHighIsHovering(false);
            highProbeOverlapping = true;
        }
        else if (other.gameObject.name == "LowProbeTip")
        {
            // Debug.Log("Low probe overlap");
            //LocationButtonOnClick();
            transform.GetChild(2).gameObject.SetActive(false);
            multimeterCode.SetLowIsHovering(false);
            lowProbeOverlapping = true;
        }
    }

    public bool isHighProbeOverlapping() {
        return highProbeOverlapping;
    }

    public bool isLowProbeOverlapping() {
        return lowProbeOverlapping;
    }

}
