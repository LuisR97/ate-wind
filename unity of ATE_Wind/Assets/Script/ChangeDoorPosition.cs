﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeDoorPosition : MonoBehaviour {


	public bool Doorisopen = false;




	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (0)) 
		{
			//print ("mouse left hit");
			//			print (Camera.current);
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit, 8)) {
				if (hit.collider.name == "RightDoorTB" || hit.collider.name == "LeftDoorTB") { 
					if (Doorisopen == false) {
						GameObject.Find ("RightDoorTB").transform.rotation = Quaternion.Euler (-90, 90, 45);
						GameObject.Find ("LeftDoorTB").transform.rotation = Quaternion.Euler (-90, 0, 45);
						Doorisopen = true;

					}
					else 
					{
						GameObject.Find ("RightDoorTB").transform.rotation = Quaternion.Euler (-90, 0, 0);
						GameObject.Find ("LeftDoorTB").transform.rotation = Quaternion.Euler (270, -180, 0);

					//	GameObject.Find ("RightDoor").transform.rotation = Quaternion.Euler (90, 0, 90);
					//	GameObject.Find ("LeftDoor").transform.rotation = Quaternion.Euler (270, 0, -90);
						Doorisopen = false;

					}
				}
			}
		}
	}

	void LateUpdate ()
    {
	
		//OpenToClosed = false;
		//ClosedToOpen = false;

	}

}
