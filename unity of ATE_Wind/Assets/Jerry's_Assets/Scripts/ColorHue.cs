﻿using UnityEngine;
using System.Collections;

namespace ColorUtil {

public class ColorHue {
   
   public static int color_wheel_max = 320;
   public static int color_wheel_min = 0;

   private enum c_state { ZERO, ONE, DELTA }    

   public static float hue_to_norm_value(Color c)
   {
      c_state r, g, b;
      float deg = -1.0f, val = -1.0f;        

      if (c.r == 0) 
         r = c_state.ZERO;
      else if (c.r == 1) 
         r = c_state.ONE;
      else /* 0 < c < 1 */
         r = c_state.DELTA;

      if (c.g == 0) 
         g = c_state.ZERO;
      else if (c.g == 1) 
         g = c_state.ONE;
      else /* 0 < c < 1 */
         g = c_state.DELTA;

      if (c.b == 0) 
         b = c_state.ZERO;
      else if (c.b == 1) 
         b = c_state.ONE;
      else /* 0 < c < 1 */
         b = c_state.DELTA;

	  /* Corner cases... */
	  /* 0 degrees */
	  if (r == c_state.ONE && g == c_state.ZERO && b == c_state.ZERO)
		  g = c_state.DELTA;
	  /* 60 degrees */
	  if (r == c_state.ONE && g == c_state.ONE && b == c_state.ZERO)
		  r = c_state.DELTA;
	  /* 120 degrees */
	  if (r == c_state.ZERO && g == c_state.ONE && b == c_state.ZERO)
	 	  b = c_state.DELTA;
	  /* 180 degrees */
	  if (r == c_state.ZERO && g == c_state.ONE && b == c_state.ONE)
		  g = c_state.DELTA;
	  /* 240 degrees */
      if (r == c_state.ZERO && g == c_state.ZERO && b == c_state.ONE)
		  r = c_state.DELTA;
	  /* 300 degrees */
	  if (r == c_state.ONE && g == c_state.ZERO && b == c_state.ONE)
	      b = c_state.DELTA;

      if (r == c_state.ONE && g == c_state.DELTA && b == c_state.ZERO) 
         deg = 60.0f * c.g;        
      if (r == c_state.DELTA && g == c_state.ONE && b == c_state.ZERO) 
         deg = 60.0f + 60.0f * (1.0f - c.r);
      if (r == c_state.ZERO && g == c_state.ONE && b == c_state.DELTA) 
         deg = 120.0f + 60.0f * c.b;
      if (r == c_state.ZERO && g == c_state.DELTA && b == c_state.ONE) 
         deg = 180.0f + 60.0f * (1.0f - c.g);
      if (r == c_state.DELTA && g == c_state.ZERO && b == c_state.ONE) 
         deg = 240.0f + 60.0f * c.r;
      if (r == c_state.ONE && g == c_state.ZERO && b == c_state.DELTA) 
         deg = 300.0f + 60.0f * (1.0f - c.b);

      if (deg >= color_wheel_min && deg <= color_wheel_max)
         val = (deg - color_wheel_min)/(color_wheel_max - color_wheel_min);

      return val;
   }

   public static Color value_to_hue(float val,float low, float high)
   {
      float r, g, b, a = 1.0f;

      /* Hue coloration based on 0-1 normalized scale. */
      float norm_val = (val - low)/(high - low);       

      /* Restrict "color wheel" to 0-MAX_CWHEEL degrees */          
      float pos = norm_val * color_wheel_max;       

      /* Coloration: see Figure 24 at http://en.wikipedia.org/wiki/HSL_and_HSV */

      if (norm_val > 1 || norm_val < 0)
         return new Color(0,0,0,a);

      if (pos <= 60) {
         r = 1.0f;
         g = pos/60.0f;
         b = 0.0f;
         return new Color(r,g,b,a);
      }
      if (pos <= 120) {
         r = 1.0f - (pos - 60.0f)/60.0f;
         g = 1.0f;
         b = 0.0f;
         return new Color(r,g,b,a);
      }
      if (pos <= 180) {
         r = 0.0f;
         g = 1.0f;
         b = (pos - 120.0f)/60.0f;
         return new Color(r,g,b,a);
      }
      if (pos <= 240) {
         r = 0.0f;
         g = 1.0f - (pos - 180.0f)/60.0f;
         b = 1.0f;
         return new Color(r,g,b,a);
      }
      if (pos <= 300) {
         r = (pos - 240.0f)/60.0f;
         g = 0.0f;
         b = 1.0f;
         return new Color(r,g,b,a);
      }
      if (pos <= 360) {
         r = 1.0f;
         g = 0.0f;
         b = 1.0f - (pos - 300.0f)/60.0f;
         return new Color(r,g,b,a);
      }

      return new Color(0,0,0,a);
   }

   public static void set_alpha(ref Texture2D t,float alpha)
   {     
      if (t == null)
         return;

      Color[] all = t.GetPixels(0,0,t.width,t.height);
      int n = 0;
      while (n < all.Length) {
         if (all[n].r == 0 && all[n].g == 0 && all[n].b == 0)
             all[n].a = 0;
         else
             all[n].a = alpha;
         n++;
      }
      t.SetPixels(all);
      t.Apply();
   }

   public static void test_map(ref Texture2D t)
   {
      if (t == null)
         return;

      float pixels = t.width * t.height;
      Color[] c = new Color[(int)pixels];

      int m = 0;
      int n = 0;
      while (n < t.height) {
         m = 0;
         while (m < t.width) {
             Color hue = value_to_hue(2 * n * m - pixels/4,0,pixels);
             c[n * t.width + m] = hue; 
             m++;
         }
         n++;
      }
      t.SetPixels(c);
      t.Apply();
   }
}
}

