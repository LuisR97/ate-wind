﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TaskItem;

namespace QuestionItem {

public class QuestionAnswer {
   public string    answer;
   public int       choice = -1;
   //public float     value;   
}

public class QuestionPair {
   public Question         q;
   public QuestionAnswer   a;
   public bool             attempted;

   public QuestionPair(Question question)
   {
      this.q = question;
      this.a = new QuestionAnswer();

      attempted = false;
   }
}


public class QuestionManager {

   public List<QuestionPair> items;
   private int show_item = 0;
   private Rect window_rect;
   private bool show = false;

   private bool[] selection;  

   public QuestionManager(List<Question> question_list)
   {
      items = new List<QuestionPair>();

      foreach (Question qn in question_list) {
         items.Add(new QuestionPair(qn));
      }

      window_rect = new Rect(0,0,500,500);     
   }

   public void show_gui()
   {
      show = true;
   }
      

   public bool questions_gui()
   {
      if (show) {
         window_rect = GUI.Window(
            0,window_rect,question_window,"<color=blue><b>Work Order</b></color>");
      } 
	  return show;
   }	

   private void question_window(int id)
   {      
      if (GUI.Button(new Rect(230,20,40,25),"Done")) {        
         show = false;               
      }
      if (GUI.Button(new Rect(10,20,40,25),"<")) {
          show_item--; 
          if (show_item < 0)
             show_item = 0;
          selection = null;         
      }
      if (GUI.Button(new Rect(450,20,40,25),">")) {
          show_item++; 
          if (show_item >= items.Count)
             show_item = items.Count - 1;
         selection = null;        
      }

      //GUI.skin.label.alignment = TextAnchor.UpperLeft;
      GUI.skin.label.alignment = TextAnchor.LowerLeft;
      GUI.skin.box.alignment = TextAnchor.LowerLeft;
      GUI.skin.textArea.alignment = TextAnchor.UpperLeft;
      switch (items[show_item].q.type) {
         case QuestionType.LongAnswer:
            text_question();
            break;
         case QuestionType.SchematicLocation:
            location_question();
            break;
         case QuestionType.Value:
            value_question();
            break;
         case QuestionType.MultipleChoice:
            choice_question();
            break;        
         default:
            break;
      }

      GUI.DragWindow();
   }

   private void choice_question()
   {
      int n = 0;
      int y = 250;
      int last_sel = items[show_item].a.choice;
      int new_sel  = items[show_item].a.choice;

      if (selection == null) {
         selection = new bool[items[show_item].q.meta_answer.Count];
      }

      GUI.Label(new Rect(10,50,480,190),items[show_item].q.challenge);

      while (n < selection.Length) {
         if (selection[n] && n != last_sel) {
            new_sel = n;   
            items[show_item].attempted = true;
         }
         n++;
      }

      if (items[show_item].attempted) {
         if (new_sel != last_sel) {
            items[show_item].a.choice = new_sel;
            if (last_sel > -1)
               selection[last_sel] = false;            
         }
         selection[new_sel] = true;
      }
      
      n = 0;
      foreach (string s in items[show_item].q.meta_answer) {
         selection[n] = GUI.Toggle(new Rect(40,y,280,30),selection[n],"\t" + s);
         n++;
         y += 30;
      }      
   }

   private void text_question()
   {
      if (items[show_item].a.answer == null)
         items[show_item].a.answer = "";

      if (items[show_item].a.answer != "")
         items[show_item].attempted = true;

      GUI.Label(new Rect(10,50,480,190),items[show_item].q.challenge);
      items[show_item].a.answer = GUI.TextArea(new Rect(10,250,480,240),items[show_item].a.answer);
   }

   private void location_question()
   {
      GUI.Label(new Rect(10,50,480,190),items[show_item].q.challenge);

   }

   private void value_question()
   {      
      if (items[show_item].a.answer == null)
         items[show_item].a.answer = "";

      if (items[show_item].a.answer != "")
         items[show_item].attempted = true;

      GUI.Label(new Rect(10,50,480,190),items[show_item].q.challenge);
      items[show_item].a.answer = GUI.TextField(new Rect(10,250,480,20),items[show_item].a.answer);
   }   
}
}