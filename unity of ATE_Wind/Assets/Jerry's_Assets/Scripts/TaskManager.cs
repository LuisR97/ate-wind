﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using TaskItem;
using ColorUtil;
//using Movement; 
using QuestionItem;

public class Probe {

    public GameObject model;    
    public Vector3 pre;
    public float val = -1;
    public string button_name;
    public bool active;

    public Probe(GameObject g, string b_name)
    {
       this.model = g;
       this.pre = new Vector3(
            g.transform.position.x,
            g.transform.position.y,
            g.transform.position.z);
        this.button_name = b_name;
        this.active = true;
    }    

    public void reset()
    {
       model.transform.localPosition = pre;       
    }

    public void show(bool yes)
    {
      model.SetActive(yes && active);       
    }
}

public class IButton {
   public string id;
   public string name;

   public IButton(string i, string n)
   {
      this.id = i;
      this.name = n;
   }
}

public class TaskManager : MonoBehaviour {    

    public GameObject game_cam;
    public GameObject view_cam;

    public List<Task> tasks;
    public int current_task = 0;    
	public string data_dir = "user_data/";

    public GameObject surface;
    public GameObject surface2;

    public GameObject storage;
    public GameObject probe1;
    public GameObject probe2;
        
    public Material red;
    public Material gray;

    public bool isAdmin;
    public float overlay_alpha;
    public bool dump_colortest;
    
    private List<Probe> probes;     
    private List<IButton> available_probes = new List<IButton>();
    private List<string> my_tools;

    private Texture2D show;
    private Texture2D overlay;   

    private int active_gui = -1;
    private int active_overlay = 0;
    private int screen_x = 0;
    private int screen_y = 0;    
    
    private enum NavState { Undef, Enter, At, Probing, Leave, Left, NotAt, Move }  
    private NavState nav = NavState.Move;

    private Rect mask = new Rect(0,0,0,0);
    private string value_out = "";
    private string units_out = "";
    private string name_out  = "";
   
    public Rect display;

    public static QuestionManager qm;
   
	
	void Start () 
    {     
        display = new Rect(0,0,250,200);
        tasks = TaskParser.parse_file(data_dir + "problem_schema.txt");       
        //mover = user.GetComponent<SimpleMove>();
        my_tools = tasks[0].tool_bag;

        qm = new QuestionManager(tasks[0].questions);
        
	    //TaskDumper.dump_tasks(tasks,"test.txt");

        surface.SetActive(false);
        surface2.SetActive(false);

        probes = new List<Probe>();
        Probe p = new Probe(probe1,"Fire1");       
        probes.Add(p);
        p = new Probe(probe2,"Fire2");        
        probes.Add(p);        
	}
	
	
   void Update () 
   {     
      switch (nav) {
         case NavState.Enter:

            //if (marker != null && red != null)
            //   marker.renderer.material = red; 
            load_main_image();
            update_gui_list();
			game_cam.SetActive(false);
            view_cam.SetActive(true);
			Cursor.visible = true;
			Screen.lockCursor = false;
            nav = NavState.At;
            break;
         case NavState.At:
            screen_x = Screen.width;
            screen_y = Screen.height;
            break;
         case NavState.Probing:
            screen_x = Screen.width;
            screen_y = Screen.height;            
            if (check_probes())
               update_meter();
            break;
         case NavState.Leave:
			view_cam.SetActive(false);
            game_cam.SetActive(true);
            nav = NavState.Left;            
            break;
         case NavState.Left:
            check_locations();
            //mover.do_navigation();
            break;
         case NavState.NotAt:
            //if (marker != null && gray != null)
            //   marker.renderer.material = gray;
            nav = NavState.Move;
            break;
         case NavState.Move:
            check_locations();
            //mover.do_navigation();
            break;
         case NavState.Undef:
            break;
         default:
            break;
      }      
   }

   private void check_locations()
   {
      int n = 0;          

      foreach (Resource r in tasks[current_task].resources) {
         if ((game_cam.transform.position - r.at).magnitude <= r.radius) {             

            if (nav == NavState.Move) {
               nav = NavState.Enter;             
               active_gui = n; 
               return;
            } 
 
            if (nav == NavState.Left) {
               if (active_gui != n) {
                  nav = NavState.Enter;
                  active_gui = n;                  
               }
               return;
            }
         }
         n++;
      }
      if (nav == NavState.Left)
         nav = NavState.NotAt;      
   }   

   private void activate_instrument(string which_id)
   {
       List<ImageOverlay> io = tasks[current_task].resources[active_gui].overlays;
       int which = -1;      
       
       if (io == null) {
          active_overlay = -1;
          return;
       }

       foreach (ImageOverlay i in io) {
          if (i.use.id == which_id)
             which = io.IndexOf(i);
       }         

       if (which > -1) {

         get_texture(ref overlay, data_dir + io[which].file);         

         if (overlay != null && isAdmin) {
            ColorHue.set_alpha(ref overlay,overlay_alpha); 
            surface2.transform.localScale = new Vector3(
               (float)overlay.width/(float)overlay.height,1.0f,1.0f);
            surface2.SetActive(true);
            surface2.GetComponent<Renderer>().material.mainTexture = overlay;
         }                                    
      }
      else 
         overlay = null;  
    
      if (overlay != null)
         active_overlay = which;
      else
         active_overlay = -1;

      setup_probes(); 
   }

   private void load_main_image()
   {
      string image = tasks[current_task].resources[active_gui].image;

      if (get_texture(ref show,data_dir + image)) {
         surface.transform.localScale = new Vector3(
            (float)show.width/(float)show.height,1.0f,1.0f);                
         surface.SetActive(true);
         surface.GetComponent<Renderer>().material.mainTexture = show;       

         if (dump_colortest) {            
            overlay = new Texture2D(show.width,show.height);
            ColorHue.test_map(ref overlay);            
            dump_texture(overlay,data_dir + "overlay_" + image + ".png");
            dump_colortest = false;
         }  
      }
      else
         show = null;      
   }

   private void update_gui_list()
   {
      List<ImageOverlay> io = tasks[current_task].resources[active_gui].overlays;      
      available_probes.Clear();

      if (io == null || my_tools == null)
         return;

      foreach (ImageOverlay i in io) {
         if (my_tools.Contains(i.use.toolbag_name)) {
            available_probes.Add(new IButton(i.use.id,i.use.name));
         }
      }
   }

   private bool get_texture(ref Texture2D t, string file_name)
   {
      bool ret = false;      

      try {
         byte[] raw_img = File.ReadAllBytes(file_name);         
         if (raw_img.Length > 0) {
            t = new Texture2D(0,0);
            if (t.LoadImage(raw_img))
               ret = true;
            else {
               ret = false;
               t = null;
            }
         }
      }
      catch {
         ret = false;
         t = null;
      }
      
      return ret;
   }

   private bool check_probes()
   {
      if (active_overlay < 0)
         return false;

      if (mask.Contains(Input.mousePosition)) {         
         Cursor.visible = true;
         return false;
      }    

      List<Probe> p = probes;
      int n = 0;
      bool active = false;
      float ms_x = (Input.mousePosition.x - screen_x/2)/200.0f;
      float ms_y = (Input.mousePosition.y - screen_y/2)/200.0f;       

      while (n < p.Count) {

         if (p[n].active && Input.GetButton(p[n].button_name)) {
          
            active = true;             
            p[n].model.transform.localPosition = new Vector3(ms_x,ms_y,p[n].pre.z);               
                      
            Ray ray = Camera.main.ScreenPointToRay(
               Camera.main.WorldToScreenPoint(p[n].model.transform.position));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 20)) {

               //Debug.DrawLine(ray.origin, rhit.point);
               Renderer r = hit.collider.GetComponent<Renderer>();
               MeshCollider mc = hit.collider as MeshCollider;
               if (r != null && r.sharedMaterial != null && 
                  r.sharedMaterial.mainTexture != null && 
                  mc != null) {

                  Texture2D tex = (Texture2D)r.material.mainTexture;
                  Vector2 pixelUV = hit.textureCoord;
                  pixelUV.x *= tex.width;
                  pixelUV.y *= tex.height;

                  p[n].val = ColorHue.hue_to_norm_value(
                     overlay.GetPixel((int)pixelUV.x,(int)pixelUV.y));
               }
            }
            else
               p[n].val = -1;
         }
         n++;
      }

      if (active)
         Cursor.visible = false;
      else
         Cursor.visible = true;

      return active;
   }

   private void update_meter()
   {
      Task t = tasks[current_task]; 
      List<Probe> p = probes;
      Instrument i = t.resources[active_gui].overlays[active_overlay].use;

      if (i == null)
         return;

      if (i.probe_high == null)
         return;      
      
      if (i.range_type == RangeType.Constant) {          
          value_out = String.Format("{0:N1}",i.range_high);
      }

      else if (i.range_type == RangeType.Discrete) {

         if (i.probe_low != null) {
            if (p[0].val >= 0 && p[1].val >= 0) {          
               if (p[1].val - p[0].val > 0)                  
                  value_out = String.Format("{0:N0}",i.range_high);
               else                   
                  value_out = String.Format("{0:N0}",i.range_low);
            }
            else
               value_out = "--";
         }
         else {
            if (p[1].val >= 0) { 
               if (p[1].val > 0)                 
                  value_out = String.Format("{0:N0}",i.range_high);
               else                  
                  value_out = String.Format("{0:N0}",i.range_low); 
            }
            else
               value_out = "--";
         }
      }

      else if (i.range_type == RangeType.Continuous) {
		/* Non-polarized readings, such as AC voltage) */
		if (i.probe_low != null && i.range_low >= 0) {
            if (p[0].val >= 0 && p[1].val >= 0) {          
               float high = p[1].val * (i.range_high - i.range_low) - i.range_low;
               float low  = p[0].val * (i.range_high - i.range_low) - i.range_low;               
               value_out = String.Format("{0:N1}",Math.Abs(high - low));
            }
            else
               value_out = "--";
        }
		/* Polarized readings, such as DC voltage */
		else if (i.probe_low != null) {
			if (p[0].val >= 0 && p[1].val >= 0) {          
				float high = p[1].val * (i.range_high - i.range_low) - i.range_low;
				float low  = p[0].val * (i.range_high - i.range_low) - i.range_low;               
				value_out = String.Format("{0:N1}",high - low);
			}
			else
				value_out = "--";
		}
        else {
			if (p[1].val >= 0) {          
				float high = p[1].val * (i.range_high - i.range_low) - i.range_low;               
				value_out = String.Format("{0:N1}",high);
			}
			else
				value_out = "--";
        }
     }     
  }   

   
    private void dump_texture(Texture2D t, string file_name)
    {
        byte[] b = t.EncodeToPNG();
        File.WriteAllBytes(file_name,b);
    } 
   
   private void setup_probes()
   {  
      if (active_overlay < 0)
         return;      

      Instrument i = tasks[current_task].resources[active_gui].overlays[active_overlay].use;      
      Transform t = storage.transform.Find(i.probe_low);
      if (t != storage.transform) {
         t.parent = probes[0].model.transform;
         probes[0].active = true;
         probes[0].reset();
         probes[0].show(true);
      }
      else 
         Debug.Log("Probe 1 '" + i.probe_low + "' not found.");       

           
      t = storage.transform.Find(i.probe_high);
      if (t != storage.transform) {
         t.parent = probes[1].model.transform;
         probes[1].active = true;
         probes[1].reset();
         probes[1].show(true);
      }
      else 
         Debug.Log("Probe 2 '" + i.probe_high + "' not found.");     
     
      if (probes[1].active) {
         nav = NavState.Probing; 
         value_out = "--";
         units_out = i.units;
         name_out  = i.name;
      }
   }

   private void hide_probes()
   { 
      if (active_overlay < 0)
         return;  
    
      nav = NavState.At;

      foreach (Probe p in probes) {
         p.reset();
         p.show(false);
         p.active = false;
      }      
      Cursor.visible = true;
      value_out = "--";
      units_out = "";
      name_out  = "";

      if (probes[0].model.transform.childCount > 0)
          probes[0].model.transform.GetChild(0).parent = storage.transform;
      if (probes[1].model.transform.childCount > 0)
          probes[1].model.transform.GetChild(0).parent = storage.transform;     
   }

    void OnGUI()
    {
         int b_width  = 200;
         int b_height = 30;
         int x_space = b_width + 10;
         int y_space = b_height + 10;
         int x = screen_x - x_space;
         int y = screen_y - y_space;
         int begin_x = x;
         int begin_y = y;

         GUI.backgroundColor = Color.black;
         GUI.color = Color.white;
         GUI.contentColor = Color.white;
         GUI.skin.label.alignment = TextAnchor.MiddleCenter;
		 GUI.skin.button.alignment = TextAnchor.MiddleCenter;

         /*if (nav == NavState.Left || nav == NavState.NotAt || nav == NavState.Move) {
            mask.x     = begin_x;
            mask.width = b_width;
            mask.y     = 10;

            if (GUI.Button(new Rect(x,y,b_width,b_height),"Work Order")) {  
                qm.show_gui();   
            }   
            y -= y_space;  
            if (GUI.Button(new Rect(x,y,b_width,b_height),"Schematics")) {  
                        
            }   
            y -= y_space; 
            
            mask.height = begin_y - y;
            qm.questions_gui();
         } */

         if (nav == NavState.At) { 
           
            mask.x     = begin_x;
            mask.width = b_width;
            mask.y     = 10;

            if (GUI.Button(new Rect(x,y,b_width,b_height),"Leave Area")) {  
               nav = NavState.Leave;              
            }   
            y -= 2 * y_space;  

            foreach (IButton i in available_probes) {
               if (GUI.Button(new Rect(x ,y,b_width,b_height),i.name)) {
                  activate_instrument(i.id);                  
               }
               y -= y_space;
            } 
            mask.height = begin_y - y;         
        }  
     
        if (nav == NavState.Probing) {   
            
            display = GUI.Window(
               0,display,probe_window,"<color=blue><b>" + name_out + "</b></color>");
            mask = display;
            mask.y = screen_y - mask.y - mask.height;            
        }          
    }  
 
    private void probe_window(int id) 
    {       
       GUI.skin.label.richText = true;
       GUI.skin.button.richText = true;
	   GUI.skin.box.alignment = TextAnchor.UpperCenter;
	   GUI.skin.button.alignment = TextAnchor.UpperCenter;
       
       GUI.Box(
          new Rect(5,20,240,140),
          "<size=60><color=red><b>" + value_out + "</b></color></size>");      
       GUI.Label(
          new Rect(5,120,240,30),
          "<size=20><color=white><b>" + units_out + "</b></color></size>");

       GUI.backgroundColor = Color.red;       
       if (GUI.Button(new Rect(105,170,40,25),
          "<size=18><color=white><b>O</b></color></size>")) {
           hide_probes();               
       }

       GUI.DragWindow();
    }

}
