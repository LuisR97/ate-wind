﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TaskItem {

    public enum QuestionType {
        Undef,
        LongAnswer,
        SchematicLocation,
        Value,
        MultipleChoice,
        //TrueFalse
    }

    public enum EffectType {
        Undef,
        Sparks,
        Smoke,
        Glow,
        Arc,
        Leak
    }

    public enum RangeType {
        Undef,
        Constant,
        Discrete,
        Continuous,
    }

    [System.Serializable]
    public class Instrument {
        public string               id;
        public string               name;
        public float                range_low;
        public float                range_high;
        public string               units;
        public string               probe_low;
        public string               probe_high;
        public RangeType            range_type;
        public string               toolbag_name;
    }

    [System.Serializable]
    public class ImageOverlay {
        public string               file;
        public Instrument           use;
    }

    [System.Serializable]
    public class Schematic {
        public string               file;
    }

    [System.Serializable]
    public class Resource {
        public Vector3              at;
        public float                radius;
        public string               image;
        public List<ImageOverlay>   overlays = new List<ImageOverlay>();
        public EffectType           effect;
    }

    [System.Serializable]
    public class Question {
        public QuestionType         type;
        public string               challenge;
        public List<string>         meta_answer = new List<string>();
        public int                  correct_choice;
        //public float                correct_value;        
    }

    [System.Serializable]
    public class Task {
        public string               name;
        public string               description;
        public int                  problem_class;
        public List<string>         tool_bag = new List<string>();
        public List<Instrument>     instruments = new List<Instrument>();
        public List<Schematic>      schematics = new List<Schematic>();
        public List<Resource>       resources = new List<Resource>();
        public List<Question>       questions = new List<Question>();
    }
}

