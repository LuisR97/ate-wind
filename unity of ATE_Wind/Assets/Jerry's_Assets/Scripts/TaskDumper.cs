﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

namespace TaskItem {

    public class TaskDumper {

        static private string str;
                        
        static public void dump_tasks(List<Task> tl, string filename)
        {
            str = "";

            foreach (Task t in tl)
                write_task(t);
            
            File.WriteAllText(filename,str);
        }

        static private void write_task(Task ta)
        {            
            str += "task {\r\n";
            str += "\tname { " + ta.name + " }\r\n";
            str += "\tdescription { " + ta.description + " }\r\n";
            str += "\tclass { " + ta.problem_class + " }\r\n\r\n";

            write_tools(ta.tool_bag);
            write_instruments(ta.instruments);
            write_schematics(ta.schematics);
            write_resources(ta.resources);
            write_questions(ta.questions);

            str += "}\r\n\r\n";
        }

        static private void write_tools(List<string> tl)
        {
            int n = 1;
            if (tl.Count > 0) {
                str += "\ttoolbag {\r\n";
                str += "\t\t" + tl[0];
                while (n < tl.Count)
                    str += ",\r\n\t\t" + tl[n++];                
                str += "\r\n\t}\r\n\r\n";
            }
        }

        static private void write_instruments(List<Instrument> it)
        {
            if (it.Count > 0) {
                str += "\tinstruments {\r\n";
                foreach (Instrument i in it) {
                    str += "\r\n\t\t" + i.id  + " {\r\n";
                    str += "\t\t\tname { " + i.name + " }\r\n";

                    switch (i.range_type) {
                        case RangeType.Undef:
                            break;
                        case RangeType.Constant:
                            str += "\t\t\trange { " + i.range_low + " }\r\n";
                            break;
                        case RangeType.Discrete:
                            str += "\t\t\trange { " + i.range_low;
                            str += " or " + i.range_high + " }\r\n";
                            break;
                        case RangeType.Continuous:
                            str += "\t\t\trange { " + i.range_low;
                            str += " to " + i.range_high + " }\r\n";
                            break;
                        default:
                            break;
                    }
                    
                    str += "\t\t\tunits { " + i.units + " }\r\n";
                    str += "\t\t\tprobe_high { " + i.probe_high + " }\r\n";
                    if (i.probe_low != null && i.probe_low != "")
                        str += "\t\t\tprobe_low { " + i.probe_low + " }\r\n";
                    str += "\t\t\ttoolbag_name { " + i.toolbag_name + " }\r\n";
                    str += "\t\t}\r\n";
                }
                str += "\t}\r\n\r\n";
            }
        }

        static private void write_schematics(List<Schematic> sc)
        {                          
            foreach (Schematic s in sc) {
                str += "\tschematic { " + s.file + " }\r\n\r\n";               
            }            
        }

        static private void write_resources(List<Resource> re)
        {
            foreach (Resource r in re) {
                str += "\tresource {\r\n";
                str += "\t\tat { " + r.at.x + "," + r.at.y + "," + r.at.z + ",";
                str += r.radius + " }\r\n";
                if (r.image != null && r.image != "")
                    str += "\t\timage { " + r.image + " }\r\n";
                foreach (ImageOverlay i in r.overlays) 
                    str += "\t\t" + i.use.id  + " { " + i.file + " }\r\n";
                if (r.effect != EffectType.Undef)
                    str += "\t\teffect { " + r.effect + " }\r\n";
                
                str += "\t}\r\n\r\n";
            }       
        }
        

        static private void write_questions(List<Question> qn)
        {
            int n;

            foreach (Question q in qn) {
                str += "\tquestion {\r\n";
                str += "\t\tchallenge { " + q.challenge + " }\r\n";
                switch (q.type) {
                    case QuestionType.Undef:
                        break;
                    case QuestionType.LongAnswer:
                        str += "\t\tanswer_contains { " + q.meta_answer[0];
                        n = 1;
                        while (n < q.meta_answer.Count)
                            str += "," + q.meta_answer[n++];                            
                        str += " }\r\n";
                        break;
                    case QuestionType.SchematicLocation:
                        str += "\t\tanswer_schematic { " + q.meta_answer[0];
                        n = 1;
                        while (n < q.meta_answer.Count)
                            str += "," + q.meta_answer[n++];                            
                        str += " }\r\n";
                        break;
                    case QuestionType.Value:
                        //str += "\t\tanswer_value { " + q.correct_value + " }\r\n";
                        str += "\t\tanswer_value { " + q.meta_answer[0] + " }\r\n";
                        break;
                    case QuestionType.MultipleChoice:
                        str += "\t\tanswer_choice {\r\n";
                        n = 0;
                        while (n < q.meta_answer.Count) {
                            str += "\t\t\t{" + ((n == q.correct_choice) ? "+ " : " ");                             
                            str += q.meta_answer[n++];
                            str += " }\r\n";
                        }
                        str += "\t\t}\r\n";
                        break;                               
                    default:
                        break;
                }
                str += "\t}\r\n\r\n";
            }    
        }    
    }
}
