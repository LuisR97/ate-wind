﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

namespace TaskItem {

    public class TaskParser {

        private enum p_state { OK, ERR };
        static private int i = 0;
        static private string str;
       

        static public List<Task> parse_file(string file_name)
        {
            List<Task> ret = new List<Task>();
            Task t;

            if (!File.Exists(file_name)) {
                MonoBehaviour.print("Bad File Name");
            }
            else {                
                str = File.ReadAllText(file_name);               
                
                while ((t = parse_task()) != null) {
                    ret.Add(t);
                }
                return ret;
            }

            return null;
        }

        /* Finds next tag name */
        static private string next_tagname()
        {
            int at = str.IndexOf('{', i);
            int check = str.IndexOf('}', i);

            if (at > -1 && at < check)       
                return str.Substring(i, at - i).Trim();            
            else
                return null;
        }

        static private bool enter_block()
        {
            int at = str.IndexOf('{', i);
            int check = str.IndexOf('}',i);
            
            if (at > -1 && at < check) {
                i = at + 1;                
                return true;
            }
            else
                return false;
        }

        static private int end_of_block()
        {
            int at = str.IndexOf('}',i);          

            if (at > -1)                        
                return at - 1;            
            else
                return -1;        
        }

        static private bool leave_block()
        {
            int at = str.IndexOf('}', i);
            int check = str.IndexOf('{',i);

            if (at > -1 && at < check) {
                i = at + 1;
                return true;
            }
            else
                return false;
        }

        /* Generic string block */
        static private string parse_string()
        {
            if (enter_block()) {

                int at = i, to = end_of_block();
                
                if (to > 0 && leave_block())                     
                    return str.Substring(at, to - at + 1).Trim();                
            }

            return null;            
        }

        /* Generic int block */
        static private int parse_int()
        {
            string value = parse_string();

            if (value != null)
                return int.Parse(value);
            return 0;
        }

        /* Generic csv block */
        static private List<string> parse_csv()
        {
            char[] delim = { ',' };            
            string csv = parse_string();
            
            if (csv != null) {
                string[] ret = csv.Split(delim);
                List<string> ret_list = new List<string>();                

                foreach (string s in ret)
                    ret_list.Add(s.Trim());                

                return ret_list;
            }            

            return null;
        }
        
       
        static private Task parse_task()
        {
            /* Find 'task' tag.
             * For each internal tag : do subroutine.
             * Sub routines "eat" all data which applies.
             * If '}' encoutered, complete task object.
             * Continue until EOF */            
           
            if (next_tagname() == "task") {

                if (enter_block()) {

                    Task t = new Task();
                    bool run = true;

                    while (run) {

                        string tag = next_tagname();
                        
                        //MonoBehaviour.print(tag);

                        if (tag == "name") {
                            t.name = parse_string();
                           // MonoBehaviour.print(t.name);
                        }
                        else if (tag == "description") {
                            t.description = parse_string();
                            //MonoBehaviour.print(t.description);
                        }
                        else if (tag == "class") {
                            t.problem_class = parse_int();
                            //MonoBehaviour.print(t.problem_class);
                        }
                        else if (tag == "toolbag") {
                            t.tool_bag = parse_csv();
                            //foreach (string s in t.tool_bag)
                            //    MonoBehaviour.print(s);
                        }
                        else if (tag == "schematic")
                            t.schematics.Add(parse_schematic());
                        else if (tag == "instruments")
                            t.instruments = parse_instruments();
                        else if (tag == "resource")
                            t.resources.Add(parse_resource(ref t.instruments));
                        else if (tag == "question")
                            t.questions.Add(parse_question());
                        else
                            run = false;
                    }

                    leave_block();
                    return t;
                }
    
               

                //MonoBehaviour.print(sub);
            }
            return null;
        }      
       
                

        static private List<Instrument> parse_instruments()
        {
            if (enter_block()) {

                List<Instrument> ret_list = new List<Instrument>();
                Instrument ret;
                
                while ((ret = parse_instrument()) != null)
                    ret_list.Add(ret);

                leave_block();
                return ret_list;
                
            }
            return null;
        }

        static private Instrument parse_instrument()
        {
            string id = next_tagname();

            if (enter_block()) {
                
                Instrument inst = new Instrument();
                inst.id = id;
                string p_name;

                while ((p_name = next_tagname()) != null) {

                    if (p_name == "name")
                        inst.name = parse_string();

                    else if (p_name == "range") {

                        char[] delim = { '@' };
                        string r = parse_string();

                        if (r.Contains("or")) {
                            r = r.Replace("or","@");
                            inst.range_type = RangeType.Discrete;
                        }
                        else if (r.Contains("to")) {
                            r = r.Replace("to","@");
                            inst.range_type = RangeType.Continuous;
                        }

                        string[] lh = r.Split(delim);
                        //MonoBehaviour.print(lh[0] + " -> " + lh[1]);

                        if (lh.Length == 1) {
                            inst.range_type = RangeType.Constant;
                            inst.range_low = inst.range_high = float.Parse(lh[0]);
                        }
                        if (lh.Length == 2) {
                            inst.range_low = float.Parse(lh[0]);
                            inst.range_high = float.Parse(lh[1]);
                        }                        
                    }

                    else if (p_name == "units")
                        inst.units = parse_string();

                    else if (p_name == "probe_high")
                        inst.probe_high = parse_string();

                    else if (p_name == "probe_low")
                        inst.probe_low = parse_string();

                    else if (p_name == "toolbag_name") 
                        inst.toolbag_name = parse_string(); 

                    else {
                        while (enter_block());
                        while (leave_block());
                    }
                }

                leave_block();
                return inst;
            }
            return null;
        }


        static private Schematic parse_schematic()
        {
            string file = parse_string();

            if (file != null) {
                Schematic s = new Schematic();
                s.file = file;
                return s;
            }

            return null;
        }


        static private Resource parse_resource(ref List<Instrument> instruments)
        {
            if (enter_block()) {                
                Resource res = new Resource();                
                string p_name;

                while ((p_name = next_tagname()) != null) {

                    if (p_name == "at") {
                        List<string> coord = parse_csv();
                        if (coord.Count == 4) {
                            res.at = new Vector3(
                                float.Parse(coord[0]),
                                float.Parse(coord[1]),
                                float.Parse(coord[2]));
                            res.radius = float.Parse(coord[3]);
                        }
                    }
                    else if (p_name == "image") {
                        res.image = parse_string();                        
                    }
                    else if (p_name == "effect") {

                        string eff = parse_string();
                        EffectType et;

                        try {
                            et = (EffectType)Enum.Parse(typeof(EffectType),eff);
                        }
                        catch {
                            et = EffectType.Undef;
                        }
                       
                        res.effect = et;
                        //MonoBehaviour.print(et);
                    }
                    

                    else {
                        Instrument found = null;

                        foreach (Instrument ins in instruments){
                            //MonoBehaviour.print(ins.id);
                            if (p_name == ins.id)
                                found = ins;
                        }
                     
                        if (found != null) {
                            ImageOverlay io = new ImageOverlay();
                            io.use = found;
                            io.file = parse_string();
                            res.overlays.Add(io);
                            //MonoBehaviour.print(io.use.id);
                        }
                        else {
                            enter_block();
                            leave_block();
                        }                       
                    }
                }

                leave_block();
                return res;
            }
            return null;            
        }



        static private Question parse_question()
        {
            if (enter_block()) {

                Question q = new Question();
                q.type = QuestionType.Undef;
                string tag;

                while ((tag = next_tagname()) != null) {
                    if (tag == "challenge")
                        q.challenge = parse_string();
                    else if (tag == "answer_contains")
                        parse_answer_contains(ref q);
                    else if (tag == "answer_schematic")
                        parse_answer_schematic(ref q);
                    else if (tag == "answer_value")
                        parse_answer_value(ref q);
                    else if (tag == "answer_choice")
                        parse_answer_choice(ref q);                    
                    else {
                        while (enter_block());
                        while (leave_block());
                    }
                }
                leave_block();
                //foreach (string s in q.meta_answer)
                //    MonoBehaviour.print(s);
                return q;
            }

            return null;
        }

        static private void parse_answer_contains(ref Question q)
        {
            q.type = QuestionType.LongAnswer;
            string s = parse_string();
            if (s != null) {
                string[]  words = s.Split(',');
                foreach (string w in words)
                    q.meta_answer.Add(w.Trim());
            }
        }

        static private void parse_answer_schematic(ref Question q)
        {
            q.type = QuestionType.SchematicLocation;
            string s = parse_string();
            if (s != null) {
                string[]  vals = s.Split(',');
                foreach (string v in vals)
                    q.meta_answer.Add(v.Trim());
            }
        }

        static private void parse_answer_value(ref Question q)
        {
            q.type = QuestionType.Value;
            string ans = parse_string();
            if (ans != null)
                q.meta_answer.Add(ans);
                //q.correct_value = float.Parse(ans);            
        }

        static private void parse_answer_choice(ref Question q)
        {
            q.type = QuestionType.MultipleChoice;

            if (enter_block()) {
                string choice;
                while ((choice = parse_string()) != null) {
                    if (choice[0] == '+') {
                        q.correct_choice = q.meta_answer.Count;
                        choice = choice.Substring(1).Trim();
                    }
                    q.meta_answer.Add(choice);
                }
                leave_block();            
            }
        }        
    }
}
