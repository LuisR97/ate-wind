﻿using UnityEngine;
using System.Collections;
//using Awesomium.Core;
//using Awesomium.Core.Data;
using Awesomium.Unity;



public class BrowserControl : MonoBehaviour {
	public GameObject game_cam;
	public GameObject view_cam;
    public GameObject outplane;
	public GameObject main;
	public string data_dir = "user_data/";
    //private WebUIComponent wuic;
    private string src1 = "1.svg";
    private string src2 = "2.svg";
    private int counter = 0;
	private bool wait = false;


	private bool show = false;

	// Use this for initialization
	void Start () {
        //wuic = outplane.GetComponent<WebUIComponent>();	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (main.GetComponent<PlayerStatusTrack>().schematicOn) {
			main.GetComponent<PlayerStatusTrack>().schematicOn = false;
			show_browser ();
		}

		/* if (show) {

			if (wait && !Input.anyKey)
				wait = false;
		
			if (wait)
				return;

			if (Input.GetKey (KeyCode.KeypadPlus)) {
			    outplane.GetComponent<WebUIComponent>().ZoomIn ();
				wait = true;
			}
			else if (Input.GetKey (KeyCode.KeypadMinus)) {
				outplane.GetComponent<WebUIComponent>().ZoomOut();
				wait = true;
			}
		} */

	}

	public void show_browser()
	{
		game_cam.SetActive(false);
		view_cam.SetActive(true);
		show = true;
	}


    void OnGUI()
    {
		if (show) {
	        if (GUI.Button(new Rect(10, 10, 150, 100), "Change Source"))
	        {      
	            string src = "file:///" + data_dir + "index.html?";

	            if (counter%2 != 0) 
	               src += src1;           
	            else
	               src += src2;
	            
	            outplane.GetComponent<WebUIComponent>().Source = new System.Uri(src);
	            counter++;


	            //Debug.Log(outplane.GetComponent<WebUIComponent>().Source);
	        } 
			if (GUI.Button(new Rect(10, 110, 150, 100), "Done"))
			{      
				show = false;
				game_cam.SetActive(true);
				view_cam.SetActive(false);				
				
				//Debug.Log(outplane.GetComponent<WebUIComponent>().Source);
			} 
		}
    }
}
