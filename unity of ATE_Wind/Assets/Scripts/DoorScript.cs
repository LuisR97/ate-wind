﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {

	public GameObject door;
	private int HasBeenTriggered = 0;	

	
	public void OnTriggerEnter (Collider collision)
	{
		if (collision.gameObject.tag == "Player") { 
			if(HasBeenTriggered == 0) {
				door.GetComponent<Animation>().Play ();
				HasBeenTriggered = 1;
			}
		}
	}
}
