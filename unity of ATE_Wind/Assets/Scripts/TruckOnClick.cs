﻿using UnityEngine;
using System.Collections;

public class TruckOnClick : MonoBehaviour {
	public bool truckB;
	public bool safeB;
	float yieldTime;
	public float safetyMeetingTime;
	public float safeStateTime;
	
	void Start(){
		safetyMeetingTime = gameObject.GetComponent<PlayerStatusTrack>().safetyMeetingTime;
		safeStateTime = gameObject.GetComponent<PlayerStatusTrack>().turbineSafeStateTime;
	}
	
	void Update(){
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
		if(Input.GetMouseButtonDown(0) && !gameObject.GetComponent<GUIManager>().showOther){
			if(Physics.Raycast(ray,out hit,80)){
				Debug.Log(hit.collider.name);
				if(hit.collider.name == "Turbine Teleporter"){
					truckB = true;
					yieldTime = 0;
				}
				if(hit.collider.name =="Safe mode"){
					safeB = true;
					yieldTime = 0;
				}
			}
		}

		if(safeB){
			gameObject.GetComponent<GUIManager>().GUIMessageTimed("Turbine is in safe state now");
			gameObject.GetComponent<TimerLadder>().AddElapsed (safeStateTime * 60);
			safeB = false;
			gameObject.GetComponent<PlayerStatusTrack>().turbineSafeState = true;
		}
	}

	void OnGUI(){
		if (truckB){
			yieldTime += Time.deltaTime;
			gameObject.GetComponent<GUIManager>().showOther = true;
			GUI.Box(new Rect(Screen.width/2-200,Screen.height/2-50,400,100)," o	Identify hazards likely to encounter \n o Muster place \n o	Emergency action plan overview",gameObject.GetComponent<Inventory>().InventoryStyle);
			//GUI.Label(new Rect(Screen.width/2+40,20,80,30), "+ " + safetyMeetingTime.ToString() + " Min.");
			//GUI.Label(new Rect(Screen.width/2+30,0,80,30), "<color=#ffffffff> + " + safetyMeetingTime.ToString() + " Min.</color>");

			if(GUI.Button(new Rect(Screen.width/2-50,Screen.height/2+60,100,40),"done", gameObject.GetComponent<Inventory>().InventoryStyle)){
				truckB = false;
				gameObject.GetComponent<GUIManager>().showOther = false;
				//gameObject.GetComponent<TimerLadder>().timer += Mathf.RoundToInt(safetyMeetingTime * 60);
				gameObject.GetComponent<PlayerStatusTrack>().safetyMeeting += 1;
				gameObject.GetComponent<TimerLadder>().AddElapsed (safetyMeetingTime * 60);
			}
		}
		//if(safeB){
		//	gameObject.GetComponent<GUIManager>().GUIMessageTimed("Turbine is in safe state now");
		//	gameObject.GetComponent<TimerLadder>().AddElapsed (safeStateTime * 60);
		//	safeB = false;
			//yieldTime += Time.deltaTime;
			//GUI.Box(new Rect(Screen.width/2-200,Screen.height/2-50,400,100),"Turbine is in safe state now",gameObject.GetComponent<Inventory>().InventoryStyle);
			//GUI.skin.label.alignment = TextAnchor.UpperLeft;
			//GUI.skin.label.richText = true;
			//GUI.Label(new Rect(Screen.width/2+30,0,80,30), "<color=#ffffffff> + " + safeStateTime.ToString() + " Min.</color>");
		//	gameObject.GetComponent<PlayerStatusTrack>().turbineSafeState = true;
			//if (yieldTime > 5) {
			//	safeB = false;
				//gameObject.GetComponent<TimerLadder>().timer += Mathf.RoundToInt(safeStateTime* 60);
			//	gameObject.GetComponent<TimerLadder>().AddElapsed (safeStateTime * 60);
			//}
		//}
	}
}

