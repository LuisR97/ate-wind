﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {
	
	public Transform destination;
	Collider player;
	float yieldTime;
	bool timeB;
	public float transTime;
	private bool show_dialog = false;
	private GUIManager gm;
	private GUIStyle style;
	public Texture2D icon;
	
	// Use this for initialization
	void Start () {
		transTime = GameObject.Find("GUIManager").GetComponent<PlayerStatusTrack>().truckTransTime;
		gm = GameObject.Find("GUIManager").GetComponent<GUIManager>();
		style = GameObject.Find("GUIManager").GetComponent<Inventory>().InventoryStyle;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other){
    	player = other;    	
		show_dialog = true;
	}
	
	void Tele(){
		player.transform.position = destination.position;
    	player.transform.rotation = destination.rotation;
	}
	
	void OnGUI(){

		if (show_dialog) {
			gm.showOther = true;

			GUI.Button (new Rect(Screen.width/2-75, Screen.height/2-75, 150, 150),icon,style);
			GUI.Button (new Rect(Screen.width/2-75, Screen.height/2-75-60, 150, 50), "Transportation",style);
			GUI.Button (new Rect(Screen.width/2-100, Screen.height/2-75+160, 200, 80), "Leave this area?",style);
		
			if (GUI.Button (new Rect(Screen.width/2-100, Screen.height/2-75+160+90, 100, 40), "Yes",style)){
				doTransport();
				gm.showOther = false;
				show_dialog = false;
			}
			if (GUI.Button (new Rect(Screen.width/2, Screen.height/2-75+160+90, 100, 40), "No",style)){
				show_dialog = false;
				gm.showOther = false;
			}
		}

		if(timeB){

			yieldTime += Time.deltaTime;
			//GUI.skin.label.alignment = TextAnchor.UpperLeft;
			//GUI.Label(new Rect(Screen.width/2+30,0,80,30), "+ " + transTime.ToString() + " Min.");
			if(yieldTime > 5){
				timeB = false;
				//player.GetComponentInChildren<TimerLadder>().timer += Mathf.RoundToInt(60*transTime);
				player.GetComponentInChildren<TimerLadder>().AddElapsed (60*transTime);
			}
		}
	}

	public void doTransport()
	{
		GameObject.Find("SceneFade").GetComponent<SceneFadeInOut>().EndScene();
		timeB = true;
		yieldTime = 0;
		Invoke("Tele",1);
	}
	
}
