﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(PropertiesFromCSV))]
public class ShowInExplorer : Editor {

    public override void OnInspectorGUI() {
        PropertiesFromCSV pfc = (PropertiesFromCSV)target;

        DrawDefaultInspector();

        if(GUILayout.Button("Open in Explorer")) {
            pfc.ShowInExplorer();
        }

        if(GUILayout.Button("GetFromServer")) {
            pfc.ReadCSVFromServer("localhost/customScenes/customScene634.csv");
        }
    }

}
