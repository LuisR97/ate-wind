﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(InstantiateObjectsAtRuntime))]
public class InstantiateInScene : Editor {
    public override void OnInspectorGUI() {
        InstantiateObjectsAtRuntime inst = (InstantiateObjectsAtRuntime)target;

        DrawDefaultInspector();

        if(GUILayout.Button("Instantiate")) {
            inst.InstantiatePrefabs();
        }
    }

}
