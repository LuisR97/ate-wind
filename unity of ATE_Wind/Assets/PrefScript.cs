﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrefScript : MonoBehaviour {

    public Button CurrentTopicButton;
    public string currentTopic;

    public void GetSafetyTopic(Button topic)
    {
        currentTopic = topic.GetComponentInChildren<Text>().text.ToString().Trim();
        Debug.Log(currentTopic);
    }

    public string GetCurrentTopic()
    {
        return currentTopic;
    }
}
