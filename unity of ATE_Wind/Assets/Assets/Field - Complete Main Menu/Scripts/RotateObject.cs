﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Michsky.UI.FieldCompleteMainMenu
{
    public class RotateObject : MonoBehaviour
    {
        public float xAngle, yAngle, zAngle;
        private bool _rotationEnable;
        private void Update()
        {
            if (_rotationEnable)
                transform.Rotate(xAngle, yAngle * Time.deltaTime, zAngle, Space.World);
        }

        public void RotateModel(bool rotate)
        {
            _rotationEnable = rotate;
        }

    }
}